'use strict';
var apiUrl = "http://192.168.1.19/tennis/users/";
var uploadUrl = "http://192.168.1.19/tennis/uploads/";
var db = '';
var calendar = '';
Stripe.setPublishableKey('pk_test_6pRNASCoBOKtIshFeQd4XMUh');
// Declare app level module which depends on views, and components
angular.module('myApp', [
    'ngRoute',
    'myApp.login',
    'myApp.dashboard',
    'myApp.bookings',
    'myApp.coaches',
    'myApp.clubsetup',
    'myApp.version',
    'ui.calendar',
    'kendo.directives',
    'datePicker',
    'angucomplete-alt'
]).
    config(function ($locationProvider, $routeProvider) {
        $locationProvider.hashPrefix('!');

        $routeProvider.otherwise({ redirectTo: '/login' });

    })

    .run(function ($rootScope, $templateCache) {
        $rootScope.$on('$routeChangeStart', function (event, next, current) {
            if (typeof (current) !== 'undefined') {
                $templateCache.remove(current.templateUrl);
            }
        });
    })

    .directive('compileMe', function ($compile) {
        return {
            template: '',
            scope: {
                htmlToBind: '=',
                variables: '='
            },
            link: function (scope, element, attrs) {
                var content = angular.element(scope.htmlToBind);
                var compiled = $compile(content);
                element.append(content);
                compiled(scope);
            }
        };
    })



    .directive("addExternalHtml", function ($templateRequest, $compile) {
        return {
            scope: {
                htmlToBind: '=',
                variables: '='
            },
            link: function (scope, element, attrs) {
                scope.name = attrs.foo;
                scope.show = false;
                $templateRequest("foo.html").then(function (html) {
                    element.append($compile(html)(scope));
                });
            }
        };
    })

    .filter('formatTime', function ($filter) {
        return function (time, format) {
            var parts = time.split(':');
            var date = new Date(0, 0, 0, parts[0], parts[1], parts[2]);
            return $filter('date')(date, format || 'h:mm a');
        }
    })
    
    .filter('formatDropDownTime', function ($filter) {
        return function (time, format) {
            var date = new Date(0, 0, 0, time, 0, 0);
            return $filter('date')(date, format || 'h a');
        }
    })

    .filter('formatDate', function ($filter) {
        return function (date, format) {
            var parts = date.split(' ');
            var date = new Date(parts[0]);
            return $filter('date')(date, format || 'dd MM yyyy');
        }
    })


    .controller('myAppCtrl', function ($scope, $location, $rootScope, $http, $timeout) {
        //db = window.openDatabase("Ludo", "1.0", "Ludo", 2 * 1024 * 1024);

        $scope.remoteUrl = '';
        $scope.addFormPlayers = [];
        $scope.search = {};
        $scope.showAlert = function (type, message) {
            $("#errMsg").text(message);
            if (type == 1) {
                $("#errTitle").text("ERROR!!");
            } else {
                $("#errTitle").text("SUCCESS!!");
            }
            $(".overlay").removeClass("hide");
            $("#boxAlert").removeClass("hide");
            //alert(message);
        }

        $scope.calculateAge = function (birthday) { // birthday is a date
            birthday = new Date(birthday);
            var ageDifMs = Date.now() - birthday.getTime();
            var ageDate = new Date(ageDifMs); // miliseconds from epoch
            return Math.abs(ageDate.getUTCFullYear() - 1970);
        }

        $scope.closeErrPopup = function () {
            $(".overlay").addClass("hide");
            $(".box-main").addClass("hide");
        }

        $scope.showConfirm = function (msg, id, func) {
            $(".overlay").removeClass("hide");
            $("#boxConfirm").removeClass("hide");
            $("#errConfirmTitle").text("CONFIRM!!");
            $("#errConfirmMsg").text(msg);
            $("#boxConfirm").attr("func-name", func.toString());
            $("#boxConfirm").attr("func-id", id);
            /*if (confirm(msg)) {
                func(id);
            }*/
        }

        $scope.getConfirm = function () {
            var funcName = $("#boxConfirm").attr("func-name");
            var funcId = $("#boxConfirm").attr("func-id");
            $scope.$broadcast(funcName, { id: funcId })
            $(".overlay").addClass("hide");
            $(".box-main").addClass("hide");

        }

        $scope.logOut = function () {
            $rootScope.userName = '';
            window.location.href = '#!/login';
        }

        $scope.openAddForm = function () {
            $scope.remoteUrl = apiUrl + 'getSearchPlayers/' + $scope.teamNumber + '/';
            $("#rightSideBarAddForm").show();
            //$scope.rightSideBarAddForm = true;
            //$("#rightSideBarAddForm").animate({ width: 'toggle' }, 350);
            $(".app-content-body").addClass("sideBarOn");
            $(".button-bottom").addClass("sideBarOn");
            $scope.openAddCoachBox();
            $("#chooseForm option[value='2']").attr("selected", false);
            $("#chooseForm option[value='1']").attr("selected", true);
        }

        $scope.openAddCoachBox = function () {
            $("#collapseAddOne").collapse('toggle');
            if ($("#coachAddInfoSign").hasClass("reguler-plus")) {
                $("#coachAddInfoSign").removeClass("reguler-plus").addClass("reguler-minus");
            } else {
                $("#coachAddInfoSign").removeClass("reguler-minus").addClass("reguler-plus");
            }
        }

        $scope.openAddPlayerBox = function () {
            $("#collapseAddTwo").collapse('toggle');
            if ($("#playerAddInfoSign").hasClass("reguler-plus")) {
                $("#playerAddInfoSign").removeClass("reguler-plus").addClass("reguler-minus");
            } else {
                $("#playerAddInfoSign").removeClass("reguler-minus").addClass("reguler-plus");
            }
        }

        $scope.selectedAddMember = function (item) {
            //console.log(item);

            if ($("#totalAddPlayer").val() != '') {
                if (parseInt($("#totalAddPlayer").val()) > $scope.addFormPlayers.length) {
                    $http({
                        method: 'post',
                        url: apiUrl + 'getPlayerInfo',
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded'
                        },
                        data: $.param({
                            playerId: item.originalObject.id,
                            playersIds: $scope.addFormPlayers
                        }),
                    }).then(function success(res) {
                        if (res.data.status) {
                            if ($.inArray(item.originalObject.id, $scope.addFormPlayers) == -1) {
                                $scope.addFormPlayers.push(item.originalObject.id);
                                $scope.teams = res.data.data;
                                console.log($scope.addFormPlayers);
                                console.log($scope.teams);
                                console.log($scope.addFormPlayers.join(","))
                            } else {
                                $scope.showAlert(1, "This player already added");
                            }
                            $("#AddMembers_value").val('');
                        } else {
                            $scope.showAlert(1, "Can not add player");
                        }


                    }, function error(response) {
                        // do nothing
                    });
                } else {
                    $scope.showAlert(1, "Only " + $("#totalAddPlayer").val() + " players can be add");
                }
            } else {
                $scope.showAlert(1, "Please select number of players");
            }

        }

        $scope.closeRightSideBarAddForm = function () {
            $("#rightSideBarAddForm").hide();
            //$scope.rightSideBarAddForm = false;
            $(".app-content-body").removeClass("sideBarOn");
            $(".button-bottom").removeClass("sideBarOn");
        }

        $scope.removeAddPlayer = function (id) {
            $scope.showConfirm("Are you sure1?", id, "confirmRemoveAddPlayer");
        }

        $scope.$on('confirmRemoveAddPlayer', function (ev, args) {
            var index = $scope.addFormPlayers.indexOf(args.id);
            $scope.addFormPlayers.splice(index, 1);
            $("#teamAdd" + args.id).remove();
        });

        $scope.addEventBooking = function () {
            $http({
                method: 'post',
                url: apiUrl + 'bookClub',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: $.param({
                    book_date: $("#bookAddDate").val(),
                    court_id: $("#eventAddCourt").val(),
                    start_hours: $("#eventAddStartHours").val(),
                    start_minutes: $("#eventAddStartMinutes").val(),
                    end_hours: $("#eventAddEndHours").val(),
                    end_minutes: $("#eventAddEndMinutes").val(),
                    first_name: $("#playerAddFirstName").val(),
                    last_name: $("#playerAddLastName").val(),
                    phone: $("#playerAddContactNo").val(),
                    booked_contact: $("#playerAddContactNo").val(),
                    email: $("#playerAddEmail").val(),
                    booked_email: $("#playerAddEmail").val(),
                    coach_id: $("#eventAddCoach").val(),
                    sub_coach_id: $("#eventAddSubCoach").val(),
                    club_id: localStorage.getItem('club_id'),
                    user_id: localStorage.getItem('user_id'),
                    player_ids: $scope.addFormPlayers.join(",")
                }),
            }).then(function success(res) {
                if (res.data.status) {
                    //$scope.showAlert(2, "Booking details added!");
                    $scope.openDetailsConfirm(res.data.data);
                } else {
                    if(res.data.title==undefined){
                        $scope.showAlert(1, res.data.message, '');
                    }else{
                        $scope.showAlert(1, res.data.message, res.data.title);
                    }
                    
                }


            }, function error(response) {
                $scope.showAlert(1, "Can not add booking details!");
            });
        }

        $scope.openDetailsConfirm = function (bookingId) {
            $http({
                method: 'post',
                url: apiUrl + 'getEventDetails',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: $.param({
                    booking_id: bookingId
                }),
            }).then(function success(res) {
                res = res.data;
                $("#rightSideBarAddForm").hide();
                //$scope.rightSideBarAddForm = false;
                $("#rightSideBarBookingDetailsConfirm").show();
                //$("#rightSideBarBookingDetailsConfirm").show();
                $scope.bookerName = res.data.UserDetail.Player.first_name + " " + res.data.UserDetail.Player.last_name;
                $scope.playerContactNo = res.data.UserDetail.Player.phone;
                $scope.playerEmail = res.data.UserDetail.User.email;
                $scope.teamNumber = res.data.ClubBooking.team_number;
                $scope.remoteUrl = apiUrl + 'getSearchPlayers/' + $scope.teamNumber + '/';
                $scope.bookingId = bookingId;
                $scope.totalPlayer = res.data.Team.length;
                $scope.teams = res.data.Team;
                $scope.coachName = res.data.Coach.name;
                $scope.subCoachName = res.data.SubCoach.name;
                $scope.startTime = res.data.ClubBooking.book_start_time;
                $scope.endTime = res.data.ClubBooking.book_end_time;
                $scope.courtBookingFee = res.data.Court.rate;
                $scope.coachBookingFee = parseFloat(res.data.CoachAvailableHour.rate) * parseFloat(res.data.ClubBooking.total_coach_hours);
                $scope.subTotal = parseFloat($scope.courtBookingFee) + parseFloat($scope.coachBookingFee);
                $scope.taxFee = res.data.Club.tax_rate;
                $scope.taxAmount = ($scope.subTotal * parseFloat($scope.taxFee)) / 100;
                $scope.grandTotal = $scope.subTotal + $scope.taxAmount;
                $scope.clubMembershipId = res.data.Club.membership_id;
                $scope.userAppId = res.data.ClubBooking.user_id;
                var edate = new Date(res.data.ClubBooking.book_date + " " + res.data.ClubBooking.book_start_time);
                var eedate = new Date(res.data.ClubBooking.book_date + " " + res.data.ClubBooking.book_end_time);
                $scope.eventPlayTime = moment(edate).format('h:mma') + " - " + moment(eedate).format('h:mma');
                $scope.courtName = res.data.Court.name;
                $scope.start = res.data.ClubBooking.book_date;
                $scope.eventDate = moment(edate).format('Do MMMM YYYY');
                $scope.eventDay = moment(edate).format('dddd');
                $scope.bookingNumber = res.data.ClubBooking.prefix_booking_number + ("0000" + res.data.ClubBooking.booking_number).slice(-6);
                $scope.matchType = res.data.ClassType.name;
                $scope.picUrl = uploadUrl;
            }, function error(response) {
                $scope.showAlert(1, response);
            });

        }

        $scope.closeRightSideBarBookingDetailsConfirm = function () {
            $("#rightSideBarAddForm").show();
            //$scope.rightSideBarAddForm = true;
            $("#rightSideBarBookingDetailsConfirm").hide();
            $("#chooseForm option[value='2']").attr("selected", false);
            $("#chooseForm option[value='1']").attr("selected", true);
        }

        $scope.makePayment = function (type, bookingId, grandTotal) {

            if (type == 1 || type == 3) {
                $http({
                    method: 'post',
                    url: apiUrl + 'makePayment',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    data: $.param({
                        booking_id: bookingId,
                        payment_type: type
                    }),
                }).then(function success(res) {
                    res = res.data;
                    if (res.status) {
                        $scope.bookingConfirm(bookingId);
                    } else {
                        $scope.showAlert(1, res.message);
                    }
                }, function error(response) {
                    $scope.showAlert(1, response);
                });
            } else if (type == 2) {
                $("#rightSideBarBookingDetailsConfirm").hide();
                $("#rightSideBarPaymentForm").show();
                $scope.bookingId = bookingId;
                $scope.grandTotal = grandTotal;
            }
        }

        $scope.confirmCardPayment = function (bookingId) {
            var $form = $('#payment-form');
            console.log(Stripe)
            Stripe.card.createToken($form, function (error, response) {
                console.log(error);
                console.log(response);
                if (error != 200) {
                    $scope.showAlert(1, response.error.message);
                } else {
                    $http({
                        method: 'post',
                        url: apiUrl + 'makePayment',
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded'
                        },
                        data: $.param({
                            booking_id: bookingId,
                            payment_type: 2,
                            card_type: response.card.brand,
                            card_number: response.card.last4,
                            card_name: response.card.name
                        }),
                    }).then(function success(res) {
                        res = res.data;
                        if (res.status) {
                            $scope.paymentGateway(response, bookingId);
                        } else {
                            $scope.showAlert(1, res.message);
                        }
                    }, function error(response) {
                        $scope.showAlert(1, response);
                    });
                }
            });
            return false;

        }

        $scope.paymentGateway = function (payment, bookingId) {
            $http({
                method: 'post',
                url: apiUrl + 'chargeCard',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: $.param({
                    booking_id: bookingId,
                    payment: payment,

                }),
            }).then(function success(res) {
                res = res.data;
                if (res.status) {
                    $scope.bookingConfirm(bookingId);
                } else {
                    $scope.showAlert(1, res.message);
                }
            }, function error(response) {
                $scope.showAlert(1, response);
            });
        }

        $scope.bookingConfirm = function (bookingId) {
            $http({
                method: 'post',
                url: apiUrl + 'getEventDetails',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: $.param({
                    booking_id: bookingId
                }),
            }).then(function success(res) {
                res = res.data;
                $("#rightSideBarPrint").show();
                $("#rightSideBarBookingDetailsConfirm").hide();
                $("#rightSideBarPaymentForm").hide();
                $scope.bookerName = res.data.UserDetail.Player.first_name + " " + res.data.UserDetail.Player.last_name;
                $scope.playerContactNo = res.data.UserDetail.Player.phone;
                $scope.playerEmail = res.data.UserDetail.User.email;
                $scope.teamNumber = res.data.ClubBooking.team_number;
                $scope.remoteUrl = apiUrl + 'getSearchPlayers/' + $scope.teamNumber + '/';
                $scope.bookingId = bookingId;
                $scope.totalPlayer = res.data.Team.length;
                $scope.teams = res.data.Team;
                $scope.coachName = res.data.Coach.name;
                $scope.subCoachName = res.data.SubCoach.name;
                $scope.startTime = res.data.ClubBooking.book_start_time;
                $scope.endTime = res.data.ClubBooking.book_end_time;
                $scope.courtBookingFee = res.data.ClubBooking.court_booking_fee;
                $scope.coachBookingFee = res.data.ClubBooking.coach_booking_fee;
                $scope.subTotal = res.data.ClubBooking.sub_total;
                $scope.taxFee = res.data.ClubBooking.tax_fee;
                $scope.taxAmount = res.data.ClubBooking.tax_amount;
                $scope.grandTotal = res.data.ClubBooking.grand_total;
                $scope.paymentMode = res.data.ClubBooking.payment_mode;
                $scope.clubMembershipId = res.data.Club.membership_id;
                $scope.userAppId = res.data.ClubBooking.user_id;
                $scope.paymentDate = res.data.ClubBooking.payment_date;
                $scope.paymentMode = res.data.ClubBooking.payment_mode;
                $scope.cardType = res.data.Transaction.card_type;
                $scope.cardNumber = res.data.Transaction.card_number;
                $scope.cardName = res.data.Transaction.card_name;
                var edate = new Date(res.data.ClubBooking.book_date + " " + res.data.ClubBooking.book_start_time);
                var eedate = new Date(res.data.ClubBooking.book_date + " " + res.data.ClubBooking.book_end_time);
                $scope.eventPlayTime = moment(edate).format('h:mma') + " - " + moment(eedate).format('h:mma');
                $scope.courtName = res.data.Court.name;
                $scope.start = res.data.ClubBooking.book_date;
                $scope.eventDate = moment(edate).format('Do MMMM YYYY');
                $scope.eventDay = moment(edate).format('dddd');
                $scope.bookingNumber = res.data.ClubBooking.prefix_booking_number + ("0000" + res.data.ClubBooking.booking_number).slice(-6);
                $scope.matchType = res.data.ClassType.name;
                $scope.courtName = res.data.Court.name;
                $scope.picUrl = uploadUrl;
                $scope.clubName = res.data.Club.name;
                $scope.clubAddress = res.data.Club.address;
                $scope.clubContact = res.data.Club.contact;
                $scope.clubProfilePicture = res.data.Club.profile_picture;
            }, function error(response) {
                $scope.showAlert(1, response);
            });
        }

        $scope.closeRightSideBarPaymentForm = function () {
            $("#rightSideBarBookingDetailsConfirm").show();
            $("#rightSideBarPaymentForm").hide();
        }

        $scope.closeRightSideBarPrint = function () {
            $("#rightSideBarPrint").hide();
            $(".app-content-body").removeClass("sideBarOn");
            $(".button-bottom").removeClass("sideBarOn");
        }

        $scope.openSearchBox = function () {
            $("#rightSideBarSearch").show();
            //$("#rightSideBarSearch").animate({ width: 'toggle' }, 350);
            $(".app-content-body").addClass("sideBarOn");
            $(".button-bottom").addClass("sideBarOn");
        }

        $scope.closeRightSideBarSearch = function () {
            $("#rightSideBarSearch").hide();
            $(".app-content-body").removeClass("sideBarOn");
            $(".button-bottom").removeClass("sideBarOn");
        }

        $scope.searchBookings = function () {
            console.log($scope.search);
            $rootScope.searchData = $scope.search;
            $rootScope.searchData.bookDate = $("#searchDate").val();
            window.location.href = "#!/bookings";
        }



        $scope.getEventDetails = function (bookingId) {
            $http({
                method: "POST",
                url: apiUrl + "getEventDetails",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: $.param({ booking_id: bookingId }),
            }).then(function success(eventResponse) {
                var edate = new Date(eventResponse.data.data.ClubBooking.book_date);
                $scope.eventDate = moment(edate).format('Do MMMM YYYY');
                $scope.eventDay = moment(edate).format('dddd');
                var edate = new Date(eventResponse.data.data.ClubBooking.book_date + " " + eventResponse.data.data.ClubBooking.book_start_time);
                $scope.startTimeHour = moment(edate).format('h');
                $scope.startTimeAM = moment(edate).format('a');
                var eedate = new Date(eventResponse.data.data.ClubBooking.book_date + " " + eventResponse.data.data.ClubBooking.book_end_time);
                $scope.eventPlayTime = moment(edate).format('h:mma') + " - " + moment(eedate).format('h:mma');
                $scope.matchType = eventResponse.data.data.ClassType.name;
                $scope.courtName = eventResponse.data.data.Court.name;
                $scope.bookingNumber = eventResponse.data.data.ClubBooking.prefix_booking_number + ("0000" + eventResponse.data.data.ClubBooking.booking_number).slice(-6);
                $scope.teams = eventResponse.data.data.Team;
                $scope.picUrl = uploadUrl;
                $scope.bookingId = eventResponse.data.data.ClubBooking.id;
            }, function error(eventResponse) {
                $scope.showAlert(1, 'Can not get event details!');
            });
        }

        $scope.closeRightSideBarDetails = function () {
            $("#rightSideBar").show();
            $("#rightSideBarDetails").hide();
        }

        $scope.selectedMember = function (item) {
            if ($("#totalPlayer").val() != '') {
                $http({
                    method: 'post',
                    url: apiUrl + 'addTeamMember',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    data: $.param({
                        teamNumber: $scope.teamNumber,
                        playerId: item.originalObject.id
                    }),
                }).then(function success(res) {
                    if (res.data.status) {
                        if (parseInt($("#totalPlayer").val()) >= $scope.teams.length) {
                            $scope.teams = res.data.data;
                            $("#members_value").val('');
                            $("#AddMembers_value").val('');
                        } else {
                            $scope.showAlert(1, "Only " + $("#totalPlayer").val() + " players can be add");
                        }
                    } else {
                        $scope.showAlert(1, "Can not add player");
                    }
                }, function error(response) {
                    // do nothing
                });
            } else {
                $scope.showAlert(1, "Please select number of players");
            }
        }

        $scope.updateEventBooking = function (bookingId) {
            $http({
                method: 'post',
                url: apiUrl + 'updateClubBook',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: $.param({
                    book_date: $("#bookDate").val(),
                    court_id: $("#eventCourt").val(),
                    start_hours: $("#eventStartHours").val(),
                    start_minutes: $("#eventStartMinutes").val(),
                    end_hours: $("#eventEndHours").val(),
                    end_minutes: $("#eventEndMinutes").val(),
                    first_name: $scope.playerFirstName,
                    last_name: $scope.playerLastName,
                    phone: $scope.playerContactNo,
                    email: $scope.playerEmail,
                    coach_id: $("#eventCoach").val(),
                    sub_coach_id: $("#eventSubCoach").val(),
                    booking_id: bookingId
                }),
            }).then(function success(res) {
                if (res.data.status) {
                    $scope.showAlert(2, "Booking details updated!");
                    $scope.closeRightSideBarDetails();
                } else {
                    if(res.data.title==undefined){
                        $scope.showAlert(1, res.data.message, '');
                    }else{
                        $scope.showAlert(1, res.data.message, res.data.title);
                    }
                }


            }, function error(response) {
                $scope.showAlert(1, "Can not update booking details!");
            });
        }

        $scope.removePlayer = function (id) {
            $scope.showConfirm("Are you sure?", id, "confirmRemovePlayer");
        }

        $scope.$on('confirmRemovePlayer', function (ev, args) {
            var id = args.id;
            $http({
                method: 'post',
                url: apiUrl + 'removeTeamMember',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: $.param({
                    id: id,
                    teamNumber: $scope.teamNumber
                }),
            }).then(function success(res) {
                if (res.data.status) {
                    $("#team" + id).remove();
                } else {
                    $scope.showAlert(1, res.data.message);
                }
            }, function error(response) {
                $scope.showAlert(1, response);
            });
        });



        $scope.getNumber = function (num) {
            return new Array(num);
        }

        $scope.getHoursMinutes = function (time) {
            var stime = time.split(":");
            var sh = stime[0];
            var sm = 0;
            if (stime <= 15) {
                sm = 15;
            } else if (stime <= 30) {
                sm = 30;
            } else if (stime <= 45) {
                sm = 45;
            } else {
                sm = 0;
            }
            return sh + ":" + sm;
        }

        $scope.openPlayerBox = function () {
            $("#collapseTwo").collapse('toggle');
            if ($("#playerInfoSign").hasClass("reguler-plus")) {
                $("#playerInfoSign").removeClass("reguler-plus").addClass("reguler-minus");
            } else {
                $("#playerInfoSign").removeClass("reguler-minus").addClass("reguler-plus");
            }
        }

        $scope.openCoachBox = function () {
            $("#collapseOne").collapse('toggle');
            if ($("#coachInfoSign").hasClass("reguler-plus")) {
                $("#coachInfoSign").removeClass("reguler-plus").addClass("reguler-minus");
            } else {
                $("#coachInfoSign").removeClass("reguler-minus").addClass("reguler-plus");
            }
        }



        $scope.getRightSideBarDetails = function (bookingId) {
            $http({
                method: 'post',
                url: apiUrl + 'getEventDetails',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: $.param({
                    booking_id: bookingId
                }),
            }).then(function success(res) {
                res = res.data;
                $("#rightSideBar").hide();
                $("#rightSideBarBookingDetails").show();
                $scope.bookerName = res.data.UserDetail.Player.first_name + " " + res.data.UserDetail.Player.last_name;
                $scope.playerContactNo = res.data.UserDetail.Player.phone;
                $scope.playerEmail = res.data.UserDetail.User.email;
                $scope.teamNumber = res.data.ClubBooking.team_number;
                $scope.remoteUrl = apiUrl + 'getSearchPlayers/' + $scope.teamNumber + '/';
                $scope.bookingId = bookingId;
                $scope.totalPlayer = res.data.Team.length;
                $scope.teams = res.data.Team;
                $scope.coachName = res.data.Coach.name;
                $scope.subCoachName = res.data.SubCoach.name;
                $scope.startTime = res.data.ClubBooking.book_start_time;
                $scope.endTime = res.data.ClubBooking.book_end_time;
                $scope.courtBookingFee = res.data.ClubBooking.court_booking_fee;
                $scope.coachBookingFee = res.data.ClubBooking.coach_booking_fee;
                $scope.subTotal = res.data.ClubBooking.sub_total;
                $scope.taxFee = res.data.ClubBooking.tax_fee;
                $scope.taxAmount = res.data.ClubBooking.tax_amount;
                $scope.grandTotal = res.data.ClubBooking.grand_total;
                $scope.paymentMode = res.data.ClubBooking.payment_mode;
                $scope.clubMembershipId = res.data.Club.membership_id;
                $scope.userAppId = res.data.ClubBooking.user_id;
                $scope.paymentDate = res.data.ClubBooking.payment_date;
                $scope.cardType = res.data.Transaction.card_type;
                $scope.cardNumber = res.data.Transaction.card_number;
                $scope.cardName = res.data.Transaction.card_name;
                var edate = new Date(res.data.ClubBooking.book_date + " " + res.data.ClubBooking.book_start_time);
                var eedate = new Date(res.data.ClubBooking.book_date + " " + res.data.ClubBooking.book_end_time);
                $scope.eventPlayTime = moment(edate).format('h:mma') + " - " + moment(eedate).format('h:mma');
                $scope.courtName = res.data.Court.name;
                $scope.start = res.data.ClubBooking.book_date;
                $scope.eventDate = moment(edate).format('Do MMMM YYYY');
                $scope.eventDay = moment(edate).format('dddd');
                $scope.bookingNumber = res.data.ClubBooking.prefix_booking_number + ("0000" + res.data.ClubBooking.booking_number).slice(-6);
                $scope.matchType = res.data.ClassType.name;
                $scope.courtName = res.data.Court.name;
                $scope.picUrl = uploadUrl;
            }, function error(response) {
                $scope.showAlert(1, response);
            });

        }

        $scope.closeRightSideBarBookingDetails = function () {
            $("#rightSideBar").show();
            $("#rightSideBarBookingDetails").hide();
        }

        $scope.cancelBooking = function () {
            $("#rightSideBarBookingDetails").hide();
            $("#rightSideBarCancelBooking").show();
        }

        $scope.closeRightSideBarCancelBooking = function () {
            $("#rightSideBarCancelBooking").hide();
            $("#rightSideBarBookingDetails").show();
        }

        $scope.confirmCancelBooking = function () {
            if ($("#reasonText").val() == '') {
                $scope.showAlert(1, "Please enter reason");
            } else {
                $http({
                    method: 'post',
                    url: apiUrl + 'deleteClubBooking',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    data: $.param({
                        booking_id: $scope.bookingId,
                        user_id: localStorage.getItem('user_id'),
                        reason: $("#reasonText").val()
                    }),
                }).then(function success(res) {
                    if (res.status) {
                        $scope.showAlert(2, "Booking Canceled");
                        $scope.closeRightSideBarCancelBooking();
                        $scope.closeRightSideBarBookingDetails();
                        $scope.closeRightSideBar();
                    }
                }, function error(response) {
                    $scope.showAlert(1, response);
                });
            }
        }

        $scope.printReciept = function () {
            /*alert("here")
            var myWindow = $("#rightSideBarPrint").html();
            window.print();*/
            //Get the HTML of div
            var divElements = document.getElementById("rightSideBarPrint").innerHTML;
            //Get the HTML of whole page
            var oldPage = document.body.innerHTML;

            //Reset the page's HTML with div's HTML only
            document.body.innerHTML =
                "<html><head><title></title></head><body>" +
                divElements + "</body>";

            //Print Page
            window.print();

            //Restore orignal HTML
            document.body.innerHTML = oldPage;
        }



        $scope.getCourtFee = function ($court) {
            $http({
                method: 'post',
                url: apiUrl + 'getCourtDetails',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: $.param({
                    court_id: $court,
                }),
            }).then(function success(res) {
                if (res.data.status) {
                    console.log(res.data)
                    $scope.courtFee = parseFloat(res.data.data.Court.rate);
                    $scope.taxRate = parseFloat(res.data.data.Club.tax_rate);
                    if ($scope.totalCoachFee != undefined) {
                        $scope.subTotal = $scope.totalCoachFee + $scope.courtFee;
                    } else {
                        $scope.subTotal = $scope.courtFee;
                    }
                    if ($scope.subTotal != undefined) {
                        $scope.taxAmt = ($scope.subTotal * $scope.taxRate) / 100;
                    } else {
                        $scope.taxAmt = 0;
                    }
                    $scope.grandTotal = parseFloat($scope.subTotal) + parseFloat($scope.taxAmt);
                    $scope.perHeadFee = $scope.grandTotal / $scope.totalClassPlayer;
                } else {
                    $scope.showAlert(1, res.data.message);
                }
            }, function error(response) {
                $scope.showAlert(1, response);
            });
        }

        $scope.getCoachFee = function ($coach) {
            if ($("#classStartHour").val() == "") {
                $("#classCoach").val("");
                $scope.showAlert(1, "Please choose class start hour");
            } else if ($("#classStartMinute").val() == "") {
                $("#classCoach").val("");
                $scope.showAlert(1, "Please choose class start minute");
            } else if ($("#classEndHour").val() == "") {
                $("#classCoach").val("");
                $scope.showAlert(1, "Please choose class end hour");
            } else if ($("#classEndMinute").val() == "") {
                $("#classCoach").val("");
                $scope.showAlert(1, "Please choose class end minute");
            } else {
                $http({
                    method: 'post',
                    url: apiUrl + 'getCoachRate',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    data: $.param({
                        coach_id: $coach,
                        book_date: $("#classBookDate").val(),
                        start_hours: $("#classStartHour").val(),
                        start_minutes: $("#classStartMinute").val(),
                        end_hours: $("#classEndHour").val(),
                        end_minutes: $("#classEndMinute").val()
                    }),
                }).then(function success(res) {
                    if (res.data.status) {
                        console.log(res.data);
                        $scope.totalCoachFee = parseFloat(res.data.data);
                        if ($scope.courtFee != undefined) {
                            $scope.subTotal = $scope.totalCoachFee + $scope.courtFee;
                        } else {
                            $scope.subTotal = $scope.totalCoachFee;
                        }
                        if ($scope.taxRate != undefined) {
                            $scope.taxAmt = ($scope.subTotal * $scope.taxRate) / 100;
                        } else {
                            $scope.taxAmt = 0;
                        }
                        $scope.grandTotal = parseFloat($scope.subTotal) + parseFloat($scope.taxAmt);
                        $scope.perHeadFee = $scope.grandTotal / $scope.totalClassPlayer;
                        //$scope.courtFee = res.data.data.Court.rate;
                    } else {
                        $scope.showAlert(1, res.data.message);
                    }
                }, function error(response) {
                    $scope.showAlert(1, response);
                });
            }
        }

        $scope.addGroupClass = function () {
            var isClubMember = $("#isClubMember").is(":checked") ? 1 : 2;
            var ageGroup = $("#classPlayerAgeGroup").val().split("-");
            $http({
                method: 'post',
                url: apiUrl + 'addGroupClass',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: $.param({
                    is_club_member: isClubMember,
                    class_name: $("#className").val(),
                    class_description: $("#classDescription").val(),
                    book_date: $("#classBookDate").val(),
                    court_id: $("#classCourtNo").val(),
                    start_hours: $("#classStartHour").val(),
                    start_minutes: $("#classStartMinute").val(),
                    end_hours: $("#classEndHour").val(),
                    end_minutes: $("#classEndMinute").val(),
                    min_player_rating: $("#classPlayerRating").val(),
                    min_player_age: ageGroup[0],
                    max_player_age: ageGroup[1],
                    no_player: $("#classTotalPlayer").val(),
                    coach_id: $("#classCoach").val(),
                    sub_coach_id: $("#classSubCoach").val(),
                    court_booking_fee: $scope.courtFee,
                    coach_booking_fee: $scope.totalCoachFee,
                    sub_total: $scope.subTotal,
                    tax_amount: $scope.taxAmt,
                    tax_fee: $scope.taxRate,
                    grand_total: $scope.grandTotal,
                    club_id: localStorage.getItem('club_id'),
                    user_id: localStorage.getItem('user_id')
                }),
            }).then(function success(res) {
                if (res.data.status) {
                    $scope.groupClassbooked(res.data.data);
                } else {
                    $scope.showAlert(1, res.data.message);
                }
            }, function error(response) {
                $scope.showAlert(1, response);
            });
        }

        $scope.groupClassbooked = function (bookingId) {
            $http({
                method: 'post',
                url: apiUrl + 'getEventSessionDetails',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: $.param({
                    booking_id: bookingId
                }),
            }).then(function success(res) {
                res = res.data;
                $("#rightSideBarAddGroupClass").hide();
                $("#rightSideBarGroupClassBooked").show();
                $scope.bookingId = bookingId;
                $scope.totalPlayer = res.data.ClubBookingSession.no_player;
                $scope.teams = res.data.Team;
                $scope.coachName = res.data.Coach.name;
                $scope.subCoachName = res.data.SubCoach.name;
                $scope.startTime = res.data.ClubBookingSession.book_start_time;
                $scope.endTime = res.data.ClubBookingSession.book_end_time;
                $scope.courtBookingFee = res.data.ClubBookingSession.court_booking_fee;
                $scope.coachBookingFee = res.data.ClubBookingSession.coach_booking_fee;
                $scope.subTotal = res.data.ClubBookingSession.sub_total;
                $scope.taxFee = res.data.ClubBookingSession.tax_fee;
                $scope.taxAmount = res.data.ClubBookingSession.tax_amount;
                $scope.grandTotal = res.data.ClubBookingSession.grand_total;
                var edate = new Date(res.data.ClubBookingSession.book_date + " " + res.data.ClubBookingSession.book_start_time);
                var eedate = new Date(res.data.ClubBookingSession.book_date + " " + res.data.ClubBookingSession.book_end_time);
                $scope.eventPlayTime = moment(edate).format('h:mma') + " - " + moment(eedate).format('h:mma');
                $scope.courtName = res.data.Court.name;
                $scope.start = res.data.ClubBookingSession.book_date;
                $scope.eventDate = moment(edate).format('Do MMMM YYYY');
                $scope.eventDay = moment(edate).format('dddd');
                $scope.bookingNumber = res.data.ClubBookingSession.prefix_booking_number + ("0000" + res.data.ClubBookingSession.booking_number).slice(-6);
                $scope.matchType = res.data.ClassType.name;
                $scope.courtName = res.data.Court.name;
                $scope.clubName = res.data.Club.name;
                $scope.clubAddress = res.data.Club.address;
                $scope.clubContact = res.data.Club.contact;
                $scope.clubProfilePicture = res.data.Club.profile_picture;
            }, function error(response) {
                $scope.showAlert(1, response);
            });
        }

        $scope.closeRightSideBarAddGroupClass = function () {
            $("#rightSideBarAddGroupClass").hide();
            $(".app-content-body").removeClass("sideBarOn");
            $(".button-bottom").removeClass("sideBarOn");
        }

        $scope.closeRightSideBarGroupClassBooked = function () {
            $("#rightSideBarGroupClassBooked").hide();
            $(".app-content-body").removeClass("sideBarOn");
            $(".button-bottom").removeClass("sideBarOn");
        }

        $scope.setClassTotalPlayer = function (classPlayer) {
            $scope.totalClassPlayer = classPlayer;
            $scope.perHeadFee = $scope.grandTotal / $scope.totalClassPlayer;
        }
        
        

        $scope.$on('$viewContentLoaded', function () {
            $timeout(function () {
                $("#rightSideBarAddForm").hide();
                $("#rightSideBarBookingDetailsConfirm").hide();
                $("#rightSideBarPaymentForm").hide();
                $("#rightSideBarPrint").hide();
                $("#rightSideBarSearch").hide();
                $("#rightSideBar").hide();
                $("#rightSideBarDetails").hide();
                $("#rightSideBarBookingDetails").hide();
                $("#rightSideBarCancelBooking").hide();
                $("#rightSideBarAddGroupClass").hide();
                $("#rightSideBarGroupClassBooked").hide();
            }, 1000);

            $scope.openMenu = function () {
                $("#wrapper").addClass("active");
                $("#sidebar-wrapper").removeClass("active");
            };

            $scope.closeMenu = function () {
                $("#sidebar-wrapper").addClass("active");
                $("#wrapper").removeClass("active");
            };

            $scope.closeRightSideBar = function () {
                $("#rightSideBar").hide();
                //$("#rightSideBar").animate({ width: 'toggle' }, 350);
                $(".app-content-body").removeClass("sideBarOn");
                $(".button-bottom").removeClass("sideBarOn");
            };


        });
    });


var showForm = function (ev) {
    if (ev == 1) {
        $("#rightSideBarAddForm").show();
        $("#rightSideBarAddGroupClass").hide();
        $("#chooseForm option[value='2']").attr("selected", false);
        $("#chooseForm option[value='1']").attr("selected", true);
        //$("#chooseFormSchedule").val(1);
    } else if (ev == 2) {
        $("#rightSideBarAddForm").hide();
        $("#rightSideBarAddGroupClass").show();
        $("#chooseForm option[value='2']").attr("selected", true);
        $("#chooseForm option[value='1']").attr("selected", false);
        //$("#chooseFormGroupClass").val(2);
    }
    //$("#chooseForm").val(ev);
}

