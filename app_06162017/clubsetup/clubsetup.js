'use strict';

angular.module('myApp.clubsetup', ['ngRoute'])

        .config(['$routeProvider', function ($routeProvider) {
                $routeProvider.when('/clubsetup', {
                    templateUrl: 'clubsetup/clubsetup.html',
                    controller: 'ClubSetupCtrl'
                });
            }])



        .controller('ClubSetupCtrl', function ($scope, $http, $timeout, $compile, $rootScope, $templateCache) {
            console.log("club setup ctrl");
            $scope.stime = [];
            for (var i = 1; i <= 24; i++) {
                $scope.stime.push(i);
            }

            $scope.$on('$viewContentLoaded', function () {
                $timeout(function () {
                    $("#clubSetupMenu li a").each(function (i, v) {
                        var cid = $(this).text();
                        console.log(cid)
                        $("#" + cid).hide();
                    });
                    $("#General").show();
                }, 500);

                $http({
                    method: 'post',
                    url: apiUrl + 'getGeneralSettings',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                }).then(function success(res) {
                    if (res.data.status) {
                        $("#advance_booking").val(res.data.data.Setting.advance_booking);
                        $("#non_member_fee").val(res.data.data.Setting.non_member_fee);
                        var paymentMethods = res.data.data.Setting.payment_method.split(",");
                        for (var i = 0; i < paymentMethods.length; i++) {
                            if (paymentMethods[i] == 1) {
                                $("#payment_method_credit_card").prop("checked", true);
                            }
                            if (paymentMethods[i] == 2) {
                                $("#payment_method_cash").prop("checked", true);
                            }
                            if (paymentMethods[i] == 3) {
                                $("#payment_method_sponsored").prop("checked", true);
                            }
                        }
                    } else {
                        $scope.showAlert(1, res.data.message);
                    }
                }, function error(response) {
                    $scope.showAlert(1, response);
                });
            });

            $scope.saveGeneralSettings = function () {
                var paymentMethods = [];
                if ($("#payment_method_credit_card").is(":checked")) {
                    paymentMethods.push(1);
                }
                if ($("#payment_method_cash").is(":checked")) {
                    paymentMethods.push(2);
                }
                if ($("#payment_method_sponsored").is(":checked")) {
                    paymentMethods.push(3);
                }
                if (paymentMethods.length == 0) {
                    $scope.showAlert(1, "Please select atleast one payment method");
                } else {
                    $http({
                        method: 'post',
                        url: apiUrl + 'saveGeneralSettings',
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded'
                        },
                        data: $.param({
                            advance_booking: $("#advance_booking").val(),
                            non_member_fee: $("#non_member_fee").val(),
                            payment_method: paymentMethods
                        }),
                    }).then(function success(res) {
                        $scope.showAlert(2, "General settings saved");
                    }, function error(response) {
                        $scope.showAlert(1, response);
                    });
                }
            }

            $scope.saveFacilitiesTimings = function () {
                $http({
                    method: 'post',
                    url: apiUrl + 'saveFacilitiesSettings',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    data: $.param({
                        advance_booking: $("#advance_booking").val(),
                        non_member_fee: $("#non_member_fee").val(),
                        payment_method: paymentMethods
                    }),
                }).then(function success(res) {
                    $scope.showAlert(2, "General settings saved");
                }, function error(response) {
                    $scope.showAlert(1, response);
                });
            }

            $scope.openMenuItem = function (menu, ev) {
                $("#clubSetupMenu li").removeClass("active-li");
                $(ev.target).parent("li").addClass("active-li");
                $("#clubSetupMenu li a").each(function (i, v) {
                    var cid = $(this).text();
                    $("#" + cid).hide();
                });
                var eid = $(ev.target).text();
                $("#" + eid).show();
            }
        })
