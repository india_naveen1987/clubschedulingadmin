'use strict';

angular.module('myApp.login', ['ngRoute'])

    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/login', {
            templateUrl: 'login/login.html',
            controller: 'LoginCtrl'
        });
    }])

    .controller('LoginCtrl', function ($scope, $http, $location, $rootScope) {
        $(".background-div").css("height", $(window).height());
        $scope.username = '';
        $scope.password = '';
        $scope.submitForm = function () {
            if ($scope.username == '') {
                $("div.alert-danger").removeClass("hide");
                $scope.message = "Email address is required!";
            } else if ($scope.password == '') {
                $("div.alert-danger").removeClass("hide");
                $scope.message = "Password is required!";
            } else {
                $("div.alert-danger").addClass("hide");
                $http({
                    method: "POST",
                    url: apiUrl + "login",
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    data: $.param({ email: $scope.username, password: $scope.password }),
                }).then(function success(response) {
                    console.log(response)
                    if (response.data.status) {
                        if (response.data.data.User.role_id == 3) {
                            $rootScope.userName = response.data.data.Club.name;
                            localStorage.setItem('club_id', response.data.data.Club.id);
                            localStorage.setItem('user_id', response.data.data.User.id);
                            localStorage.setItem('court_id', response.data.data.Court[0].id);
                        } else if (response.data.data.User.role_id == 2) {
                            $rootScope.userName = response.data.data.Coach.name;
                            localStorage.setItem('coach_id', response.data.data.Coach.id);
                            localStorage.setItem('user_id', response.data.data.User.id);
                        }else if(response.data.data.User.role_id == 4){
                            $rootScope.userName = response.data.data.User.email;
                            localStorage.setItem('user_id', response.data.data.User.id);
                            localStorage.setItem('club_id', 0);
                            localStorage.setItem('court_id', 0);
                        }
                        $rootScope.role_id = response.data.data.User.role_id;
                        localStorage.setItem('role_id', response.data.data.User.role_id);
                        localStorage.setItem('user', JSON.stringify(response.data.data.User));
                        notificationAjax = setInterval(function(){
                            getNotifications();
                        },30000);
                        window.location.href = '#!/dashboard';
                    } else {
                        $("div.alert-danger").removeClass("hide");
                        $scope.message = response.data.message;
                    }
                }, function error(response) {
                    $("div.alert-danger").removeClass("hide");
                    $scope.message = response.statusText;
                });
            }
        }
    });