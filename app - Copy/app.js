'use strict';
var apiUrl = "http://localhost/tennis/users/";
var uploadUrl = "http://localhost/tennis/uploads/";
var db = '';
var calendar = '';
var notificationAjax = '';
Stripe.setPublishableKey('pk_test_g7NBeukxcCGLBRpBUXgiS66R'); //pk_test_6pRNASCoBOKtIshFeQd4XMUh
// Declare app level module which depends on views, and components
angular.module('myApp', [
    'ngRoute',
    'myApp.login',
    'myApp.dashboard',
    'myApp.bookings',
    'myApp.coaches',
    'myApp.clubsetup',
    'myApp.myprofile',
    //'myApp.playerperformance',
    'myApp.version',
    'ui.calendar',
    'kendo.directives',
    'datePicker',
    'angucomplete-alt',
    'myApp.groupclasses'
]).
        config(function ($locationProvider, $routeProvider) {
            $locationProvider.hashPrefix('!');

            $routeProvider.otherwise({redirectTo: '/login'});

        })

        .run(function ($rootScope, $templateCache) {
            $rootScope.$on('$routeChangeStart', function (event, next, current) {
                if (typeof (current) !== 'undefined') {
                    $templateCache.remove(current.templateUrl);
                }
            });
        })

        .directive('compileMe', function ($compile) {
            return {
                template: '',
                scope: {
                    htmlToBind: '=',
                    variables: '='
                },
                link: function (scope, element, attrs) {
                    var content = angular.element(scope.htmlToBind);
                    var compiled = $compile(content);
                    element.append(content);
                    compiled(scope);
                }
            };
        })



        .directive("addExternalHtml", function ($templateRequest, $compile) {
            return {
                scope: {
                    htmlToBind: '=',
                    variables: '='
                },
                link: function (scope, element, attrs) {
                    scope.name = attrs.foo;
                    scope.show = false;
                    $templateRequest("foo.html").then(function (html) {
                        element.append($compile(html)(scope));
                    });
                }
            };
        })

        .filter('formatTime', function ($filter) {
            return function (time, format) {
                var parts = time.split(':');
                var date = new Date(0, 0, 0, parts[0], parts[1], parts[2]);
                return $filter('date')(date, format || 'h:mm a');
            }
        })

        .filter('formatDropDownTime', function ($filter) {
            return function (time, format) {
                var date = new Date(0, 0, 0, time, 0, 0);
                return $filter('date')(date, format || 'h a');
            }
        })

        .filter('formatDate', function ($filter) {
            return function (date, format) {
                var parts = date.split(' ');
                var date = new Date(parts[0]);
                return $filter('date')(date, format || 'dd MM yyyy');
            }
        })

        .filter('showDayName', function ($filter) {
            return function (day) {
                if (day == 0) {
                    var dayName = 'Sunday';
                } else if (day == 1) {
                    var dayName = 'Monday';
                } else if (day == 2) {
                    var dayName = 'Tuesday';
                } else if (day == 3) {
                    var dayName = 'Wednesday';
                } else if (day == 4) {
                    var dayName = 'Thursday';
                } else if (day == 5) {
                    var dayName = 'Friday';
                } else if (day == 6) {
                    var dayName = 'Saturday';
                }
                return dayName;
            }
        })


        .controller('myAppCtrl', function ($scope, $location, $rootScope, $http, $timeout, uiCalendarConfig, $compile) {
            //db = window.openDatabase("Ludo", "1.0", "Ludo", 2 * 1024 * 1024);

            $scope.remoteUrl = '';
            $scope.addFormPlayers = [];
            $scope.search = {};

            $scope.totalRatingRange = [];
            $scope.cuurentMenu = '';
            $scope.minDate = moment(new Date());

            for (var i = 1; i <= 16; i++) {
                $scope.totalRatingRange.push(i);
            }

            $scope.showAlert = function (type, message, title = '') {
                $("#errMsg").text(message);
                if (title != '') {
                    $("#errTitle").text(title);
                } else if (type == 1) {
                    $("#errTitle").text("ERROR!!");
                } else {
                    $("#errTitle").text("Changes saved!!");
                }
                $(".overlay").removeClass("hide");
                $("#boxAlert").removeClass("hide");
                //alert(message);
            }




            $scope.calculateAge = function (birthday) { // birthday is a date
                birthday = new Date(birthday);
                var ageDifMs = Date.now() - birthday.getTime();
                var ageDate = new Date(ageDifMs); // miliseconds from epoch
                return Math.abs(ageDate.getUTCFullYear() - 1970);
            }

            $scope.closeErrPopup = function () {
                $(".overlay").addClass("hide");
                $(".box-main").addClass("hide");
            }

            $scope.showConfirm = function (msg, id, func, title = '') {
                $(".overlay").removeClass("hide");
                $("#boxConfirm").removeClass("hide");
                if (title != '') {
                    $("#errConfirmTitle").text(title);
                } else {
                    $("#errConfirmTitle").text("CONFIRM!!");
                }
                $("#errConfirmMsg").text(msg);
                $("#boxConfirm").attr("func-name", func.toString());
                $("#boxConfirm").attr("func-id", id);
                /*if (confirm(msg)) {
                 func(id);
                 }*/
            }

            $scope.getConfirm = function () {
                var funcName = $("#boxConfirm").attr("func-name");
                var funcId = $("#boxConfirm").attr("func-id");
                $scope.$broadcast(funcName, {id: funcId})
                $(".overlay").addClass("hide");
                $(".box-main").addClass("hide");

            }

            $scope.logOut = function () {
                $rootScope.userName = '';
                localStorage.removeItem('user_id');
                clearInterval(notificationAjax);
                window.location.href = '#!/login';
            }

            $scope.openAddForm = function () {
                $scope.remoteUrl = apiUrl + 'getSearchPlayers/' + $scope.teamNumber + '/';
                $scope.closeAllForms('rightSideBarAddForm');
                $scope.steams = [];
                $("#addEventBooking input").each(function () {
                    $(this).val('');
                });
                $("#addEventBooking select").each(function () {
                    $(this).val('');
                });

                //$scope.rightSideBarAddForm = true;
                //$("#rightSideBarAddForm").animate({ width: 'toggle' }, 350);
                ////$(".app-content-body").addClass("sideBarOn");
                ////$(".button-bottom").addClass("sideBarOn");
                $scope.openAddCoachBox();
                $("#chooseForm option[value='2']").attr("selected", false);
                $("#chooseForm option[value='1']").attr("selected", true);
            }

            $scope.openAddCoachBox = function () {
                $("#collapseAddOne").collapse('toggle');
                if ($("#coachAddInfoSign").hasClass("reguler-plus")) {
                    $("#coachAddInfoSign").removeClass("reguler-plus").addClass("reguler-minus");
                } else {
                    $("#coachAddInfoSign").removeClass("reguler-minus").addClass("reguler-plus");
                }
            }

            $scope.openAddPlayerBox = function () {
                $("#collapseAddTwo").collapse('toggle');
                if ($("#playerAddInfoSign").hasClass("reguler-plus")) {
                    $("#playerAddInfoSign").removeClass("reguler-plus").addClass("reguler-minus");
                } else {
                    $("#playerAddInfoSign").removeClass("reguler-minus").addClass("reguler-plus");
                }
            }



            $scope.selectedAddMember = function (item) {
                //console.log(item);

                /*if ($("#totalAddPlayer").val() != '') {
                 if (parseInt($("#totalAddPlayer").val()) > $scope.addFormPlayers.length) {*/
                $http({
                    method: 'post',
                    url: apiUrl + 'getPlayerInfo',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    data: $.param({
                        playerId: item.originalObject.id,
                        playersIds: $scope.addFormPlayers
                    }),
                }).then(function success(res) {
                    if (res.data.status) {
                        if ($.inArray(item.originalObject.id, $scope.addFormPlayers) == -1) {
                            $scope.addFormPlayers.push(item.originalObject.id);
                            $scope.teams = res.data.data;
                            console.log($scope.addFormPlayers);
                            console.log($scope.teams);
                            console.log($scope.addFormPlayers.join(","))
                        } else {
                            $scope.showAlert(1, "This player already added");
                        }
                        $("#AddMembers_value").val('');
                    } else {
                        $scope.showAlert(1, "Can not add player");
                    }


                }, function error(response) {
                    // do nothing
                });
                /*} else {
                 $scope.showAlert(1, "Only " + $("#totalAddPlayer").val() + " players can be add");
                 }
                 } else {
                 $scope.showAlert(1, "Please select number of players");
                 }*/

            }

            $scope.closeRightSideBarAddForm = function () {
                $("#rightSideBarAddForm").hide();
                //$scope.rightSideBarAddForm = false;
                //$(".app-content-body").removeClass("sideBarOn");
                //$(".button-bottom").removeClass("sideBarOn");
            }

            $scope.removeAddPlayer = function (id) {
                $scope.showConfirm("Are you sure1?", id, "confirmRemoveAddPlayer");
            }

            $scope.$on('confirmRemoveAddPlayer', function (ev, args) {
                var index = $scope.addFormPlayers.indexOf(args.id);
                $scope.addFormPlayers.splice(index, 1);
                $("#teamAdd" + args.id).remove();
                $("#steamAdd" + args.id).remove();
            });

            $scope.addEventBooking = function () {
                $http({
                    method: 'post',
                    url: apiUrl + 'bookClub',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    data: $.param({
                        book_date: $("#bookAddDate").val(),
                        court_id: $("#eventAddCourt").val(),
                        start_hours: $("#eventAddStartHours").val(),
                        start_minutes: $("#eventAddStartMinutes").val(),
                        end_hours: $("#eventAddEndHours").val(),
                        end_minutes: $("#eventAddEndMinutes").val(),
                        first_name: $("#playerAddFirstName").val(),
                        last_name: $("#playerAddLastName").val(),
                        phone: $("#playerAddContactNo").val(),
                        booked_contact: $("#playerAddContactNo").val(),
                        email: $("#playerAddEmail").val(),
                        booked_email: $("#playerAddEmail").val(),
                        coach_id: $("#eventAddCoach").val(),
                        sub_coach_id: $("#eventAddSubCoach").val(),
                        club_id: localStorage.getItem('club_id'),
                        user_id: localStorage.getItem('user_id'),
                        player_ids: $scope.addFormPlayers.join(",")
                    }),
                }).then(function success(res) {
                    if (res.data.status) {
                        //$scope.showAlert(2, "Booking details added!");
                        $scope.openDetailsConfirm(res.data.data);
                    } else {
                        if (res.data.title != undefined) {
                            $scope.showAlert(1, res.data.message, res.data.title);
                        } else {
                            $scope.showAlert(1, res.data.message);
                        }

                    }


                }, function error(response) {
                    $scope.showAlert(1, "Can not add booking details!");
                });
            }

            $scope.openDetailsConfirm = function (bookingId) {
                $http({
                    method: 'post',
                    url: apiUrl + 'getEventDetails',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    data: $.param({
                        booking_id: bookingId
                    }),
                }).then(function success(res) {
                    res = res.data;

                    $scope.closeAllForms('rightSideBarBookingDetailsConfirm');
                    $scope.bookerName = res.data.ClubBooking.booked_first_name + " " + res.data.ClubBooking.booked_last_name;
                    $scope.playerContactNo = res.data.ClubBooking.booked_contact;
                    $scope.playerEmail = res.data.ClubBooking.booked_email;
                    $scope.teamNumber = res.data.ClubBooking.team_number;
                    $scope.remoteUrl = apiUrl + 'getSearchPlayers/' + $scope.teamNumber + '/';
                    $scope.bookingId = bookingId;
                    $scope.totalPlayer = res.data.Team.length;
                    $scope.teams = res.data.Team;
                    $scope.coachName = res.data.Coach.name;
                    $scope.subCoachName = res.data.SubCoach.name;
                    $scope.startTime = res.data.ClubBooking.book_start_time;
                    $scope.endTime = res.data.ClubBooking.book_end_time;
                    $scope.courtBookingFee = res.data.Court.rate;
                    $scope.coachBookingFee = parseFloat(res.data.CoachAvailableHour.rate) * parseFloat(res.data.ClubBooking.total_coach_hours);
                    $scope.subTotal = parseFloat($scope.courtBookingFee) + parseFloat($scope.coachBookingFee);
                    $scope.taxFee = res.data.Club.tax_rate;
                    $scope.taxAmount = ($scope.subTotal * parseFloat($scope.taxFee)) / 100;
                    $scope.grandTotal = $scope.subTotal + $scope.taxAmount;
                    $scope.clubMembershipId = res.data.Club.membership_id;
                    $scope.userAppId = res.data.ClubBooking.user_id;
                    var edate = new Date(res.data.ClubBooking.book_date + " " + res.data.ClubBooking.book_start_time);
                    var eedate = new Date(res.data.ClubBooking.book_date + " " + res.data.ClubBooking.book_end_time);
                    $scope.eventPlayTime = moment(edate).format('h:mma') + " - " + moment(eedate).format('h:mma');
                    $scope.courtName = res.data.Court.name;
                    $scope.start = res.data.ClubBooking.book_date;
                    $scope.eventDate = moment(edate).format('Do MMMM YYYY');
                    $scope.eventDay = moment(edate).format('dddd');
                    $scope.bookingNumber = res.data.ClubBooking.prefix_booking_number + ("0000" + res.data.ClubBooking.booking_number).slice(-6);
                    $scope.matchType = res.data.ClassType.name;
                    $scope.picUrl = uploadUrl;
                }, function error(response) {
                    $scope.showAlert(1, response);
                });

            }

            $scope.closeRightSideBarBookingDetailsConfirm = function () {
                $scope.closeAllForms('rightSideBarAddForm');
                $("#chooseForm option[value='2']").attr("selected", false);
                $("#chooseForm option[value='1']").attr("selected", true);
            }

            $scope.makePayment = function (type, bookingId, grandTotal) {

                if (type == 1 || type == 3) {
                    $http({
                        method: 'post',
                        url: apiUrl + 'makePayment',
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded'
                        },
                        data: $.param({
                            booking_id: bookingId,
                            payment_type: type
                        }),
                    }).then(function success(res) {
                        res = res.data;
                        if (res.status) {
                            $scope.bookingConfirm(bookingId);
                        } else {
                            $scope.showAlert(1, res.message);
                        }
                    }, function error(response) {
                        $scope.showAlert(1, response);
                    });
                } else if (type == 2) {
                    $scope.closeAllForms('rightSideBarPaymentForm');
                    $scope.bookingId = bookingId;
                    $scope.grandTotal = grandTotal;
                }
            }

            $scope.confirmCardPayment = function (bookingId) {
                var $form = $('#payment-form');
                console.log(Stripe)
                Stripe.card.createToken($form, function (error, response) {
                    console.log(error);
                    console.log(response);
                    if (error != 200) {
                        $scope.showAlert(1, response.error.message);
                    } else {
                        $http({
                            method: 'post',
                            url: apiUrl + 'makePayment',
                            headers: {
                                'Content-Type': 'application/x-www-form-urlencoded'
                            },
                            data: $.param({
                                booking_id: bookingId,
                                payment_type: 2,
                                card_type: response.card.brand,
                                card_number: response.card.last4,
                                card_name: response.card.name
                            }),
                        }).then(function success(res) {
                            res = res.data;
                            if (res.status) {
                                $scope.paymentGateway(response, bookingId);
                            } else {
                                $scope.showAlert(1, res.message);
                            }
                        }, function error(response) {
                            $scope.showAlert(1, response);
                        });
                    }
                });
                return false;

            }

            $scope.paymentGateway = function (payment, bookingId) {
                $http({
                    method: 'post',
                    url: apiUrl + 'chargeCard',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    data: $.param({
                        booking_id: bookingId,
                        payment: payment,

                    }),
                }).then(function success(res) {
                    res = res.data;
                    if (res.status) {
                        $scope.bookingConfirm(bookingId);
                    } else {
                        $scope.showAlert(1, res.message);
                    }
                }, function error(response) {
                    $scope.showAlert(1, response);
                });
            }

            $scope.bookingConfirm = function (bookingId) {
                $http({
                    method: 'post',
                    url: apiUrl + 'getEventDetails',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    data: $.param({
                        booking_id: bookingId
                    }),
                }).then(function success(res) {
                    res = res.data;
                    $scope.closeAllForms('rightSideBarPrint');
                    $scope.bookerName = res.data.ClubBooking.booked_first_name + " " + res.data.ClubBooking.booked_last_name;
                    $scope.playerContactNo = res.data.ClubBooking.booked_contact;
                    $scope.playerEmail = res.data.ClubBooking.booked_email;
                    $scope.teamNumber = res.data.ClubBooking.team_number;
                    $scope.remoteUrl = apiUrl + 'getSearchPlayers/' + $scope.teamNumber + '/';
                    $scope.bookingId = bookingId;
                    $scope.totalPlayer = res.data.Team.length;
                    $scope.teams = res.data.Team;
                    $scope.coachName = res.data.Coach.name;
                    $scope.subCoachName = res.data.SubCoach.name;
                    $scope.startTime = res.data.ClubBooking.book_start_time;
                    $scope.endTime = res.data.ClubBooking.book_end_time;
                    $scope.courtBookingFee = res.data.ClubBooking.court_booking_fee;
                    $scope.coachBookingFee = res.data.ClubBooking.coach_booking_fee;
                    $scope.subTotal = res.data.ClubBooking.sub_total;
                    $scope.taxFee = res.data.ClubBooking.tax_fee;
                    $scope.taxAmount = res.data.ClubBooking.tax_amount;
                    $scope.grandTotal = res.data.ClubBooking.grand_total;
                    $scope.paymentMode = res.data.ClubBooking.payment_mode;
                    $scope.clubMembershipId = res.data.Club.membership_id;
                    $scope.userAppId = res.data.ClubBooking.user_id;
                    $scope.paymentDate = res.data.ClubBooking.payment_date;
                    $scope.paymentMode = res.data.ClubBooking.payment_mode;
                    $scope.cardType = res.data.Transaction.card_type;
                    $scope.cardNumber = res.data.Transaction.card_number;
                    $scope.cardName = res.data.Transaction.card_name;
                    var edate = new Date(res.data.ClubBooking.book_date + " " + res.data.ClubBooking.book_start_time);
                    var eedate = new Date(res.data.ClubBooking.book_date + " " + res.data.ClubBooking.book_end_time);
                    $scope.eventPlayTime = moment(edate).format('h:mma') + " - " + moment(eedate).format('h:mma');
                    $scope.courtName = res.data.Court.name;
                    $scope.start = res.data.ClubBooking.book_date;
                    $scope.eventDate = moment(edate).format('Do MMMM YYYY');
                    $scope.eventDay = moment(edate).format('dddd');
                    $scope.bookingNumber = res.data.ClubBooking.prefix_booking_number + ("0000" + res.data.ClubBooking.booking_number).slice(-6);
                    $scope.matchType = res.data.ClassType.name;
                    $scope.courtName = res.data.Court.name;
                    $scope.picUrl = uploadUrl;
                    $scope.clubName = res.data.Club.name;
                    $scope.clubAddress = res.data.Club.address;
                    $scope.clubContact = res.data.Club.contact;
                    $scope.clubProfilePicture = res.data.Club.profile_picture;
                    $scope.$broadcast("setCalendar", {id: localStorage.getItem('club_id'), court_id: localStorage.getItem('court_id'), type: 2});
                }, function error(response) {
                    $scope.showAlert(1, response);
                });
            }

            $scope.closeRightSideBarPaymentForm = function () {
                $scope.closeAllForms('rightSideBarBookingDetailsConfirm');
            }

            $scope.closeRightSideBarPrint = function () {
                $("#rightSideBarPrint").hide();
                //$(".app-content-body").removeClass("sideBarOn");
                //$(".button-bottom").removeClass("sideBarOn");
            }

            $scope.openSearchBox = function () {
                $scope.closeAllForms('rightSideBarSearch');
                $("#searchBookings input").each(function () {
                    $(this).val('');
                });
                //$("#rightSideBarSearch").animate({ width: 'toggle' }, 350);
                //$(".app-content-body").addClass("sideBarOn");
                //$(".button-bottom").addClass("sideBarOn");
            }

            $scope.closeRightSideBarSearch = function () {
                $("#rightSideBarSearch").hide();
                //$(".app-content-body").removeClass("sideBarOn");
                //$(".button-bottom").removeClass("sideBarOn");
            }

            $scope.searchBookings = function () {
                console.log($scope.search);
                $rootScope.searchData = $scope.search;
                $rootScope.searchData.bookDate = $("#searchDate").val();
                window.location.href = "#!/bookings";
            }



            $scope.getEventDetails = function (bookingId) {
                $http({
                    method: "POST",
                    url: apiUrl + "getEventDetails",
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    data: $.param({booking_id: bookingId}),
                }).then(function success(eventResponse) {
                    var edate = new Date(eventResponse.data.data.ClubBooking.book_date);
                    $scope.eventDate = moment(edate).format('Do MMMM YYYY');
                    $scope.eventDay = moment(edate).format('dddd');
                    var edate = new Date(eventResponse.data.data.ClubBooking.book_date + " " + eventResponse.data.data.ClubBooking.book_start_time);
                    $scope.startTimeHour = moment(edate).format('h');
                    $scope.startTimeAM = moment(edate).format('a');
                    var eedate = new Date(eventResponse.data.data.ClubBooking.book_date + " " + eventResponse.data.data.ClubBooking.book_end_time);
                    $scope.eventPlayTime = moment(edate).format('h:mma') + " - " + moment(eedate).format('h:mma');
                    $scope.matchType = eventResponse.data.data.ClassType.name;
                    $scope.courtName = eventResponse.data.data.Court.name;
                    $scope.bookingNumber = eventResponse.data.data.ClubBooking.prefix_booking_number + ("0000" + eventResponse.data.data.ClubBooking.booking_number).slice(-6);
                    $scope.teams = eventResponse.data.data.Team;
                    $scope.picUrl = uploadUrl;
                    $scope.bookingId = eventResponse.data.data.ClubBooking.id;
                }, function error(eventResponse) {
                    $scope.showAlert(1, 'Can not get event details!');
                });
            }

            $scope.closeRightSideBarDetails = function () {
                $scope.closeAllForms('rightSideBar');
            }

            $scope.selectedMember = function (item) {
                $http({
                    method: 'post',
                    url: apiUrl + 'addTeamMember',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    data: $.param({
                        teamNumber: $scope.teamNumber,
                        playerId: item.originalObject.id
                    }),
                }).then(function success(res) {
                    if (res.data.status) {
                        if (parseInt($("#totalPlayer").val()) > $scope.teams.length) {
                            $scope.teams = res.data.data;
                            $("#members_value").val('');
                            $("#AddMembers_value").val('');
                        } else {
                            $scope.showAlert(1, "Only " + $("#totalPlayer").val() + " players can be add");
                        }
                    } else {
                        $scope.showAlert(1, "Can not add player");
                    }
                }, function error(response) {
                    // do nothing
                });

            }

            $scope.updateEventBooking = function (bookingId) {
                $http({
                    method: 'post',
                    url: apiUrl + 'updateClubBook',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    data: $.param({
                        book_date: $("#bookDate").val(),
                        court_id: $("#eventCourt").val(),
                        start_hours: $("#eventStartHours").val(),
                        start_minutes: $("#eventStartMinutes").val(),
                        end_hours: $("#eventEndHours").val(),
                        end_minutes: $("#eventEndMinutes").val(),
                        first_name: $scope.playerFirstName,
                        last_name: $scope.playerLastName,
                        phone: $scope.playerContactNo,
                        email: $scope.playerEmail,
                        coach_id: $("#eventCoach").val(),
                        sub_coach_id: $("#eventSubCoach").val(),
                        booking_id: bookingId
                    }),
                }).then(function success(res) {
                    if (res.data.status) {
                        $scope.showAlert(2, "Booking details updated!");
                        $scope.closeRightSideBarDetails();
                    } else {
                        $scope.showAlert(1, res.data.message);
                    }


                }, function error(response) {
                    $scope.showAlert(1, "Can not update booking details!");
                });
            }

            $scope.removePlayer = function (id) {
                var index = $scope.teams.indexOf(id);
                $scope.showConfirm("Are you sure?", id, "confirmRemovePlayer");
                $scope.teams.splice(index, 1);
            }

            $scope.$on('confirmRemovePlayer', function (ev, args) {
                var id = args.id;
                $http({
                    method: 'post',
                    url: apiUrl + 'removeTeamMember',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    data: $.param({
                        id: id,
                        teamNumber: $scope.teamNumber
                    }),
                }).then(function success(res) {
                    if (res.data.status) {
                        $("#team" + id).remove();
                        $("#steam" + id).remove();
                    } else {
                        $scope.showAlert(1, res.data.message);
                    }
                }, function error(response) {
                    $scope.showAlert(1, response);
                });
            });



            $scope.getNumber = function (num) {
                return new Array(num);
            }

            $scope.getHoursMinutes = function (time) {
                var stime = time.split(":");
                var sh = stime[0];
                var sm = 0;
                if (stime <= 15) {
                    sm = 15;
                } else if (stime <= 30) {
                    sm = 30;
                } else if (stime <= 45) {
                    sm = 45;
                } else {
                    sm = 0;
                }
                return sh + ":" + sm;
            }

            $scope.openPlayerBox = function () {
                $("#collapseTwo").collapse('toggle');
                if ($("#playerInfoSign").hasClass("reguler-plus")) {
                    $("#playerInfoSign").removeClass("reguler-plus").addClass("reguler-minus");
                } else {
                    $("#playerInfoSign").removeClass("reguler-minus").addClass("reguler-plus");
                }
            }

            $scope.openCoachBox = function () {
                $("#collapseOne").collapse('toggle');
                if ($("#coachInfoSign").hasClass("reguler-plus")) {
                    $("#coachInfoSign").removeClass("reguler-plus").addClass("reguler-minus");
                } else {
                    $("#coachInfoSign").removeClass("reguler-minus").addClass("reguler-plus");
                }
            }



            $scope.getRightSideBarDetails = function (bookingId) {
                $http({
                    method: 'post',
                    url: apiUrl + 'getEventDetails',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    data: $.param({
                        booking_id: bookingId
                    }),
                }).then(function success(res) {
                    res = res.data;
                    $scope.closeAllForms('rightSideBarBookingDetails');
                    $scope.bookerName = res.data.ClubBooking.booked_first_name + " " + res.data.ClubBooking.booked_last_name;
                    $scope.playerContactNo = res.data.UserDetail.Player.phone;
                    $scope.playerEmail = res.data.UserDetail.User.email;
                    $scope.teamNumber = res.data.ClubBooking.team_number;
                    $scope.remoteUrl = apiUrl + 'getSearchPlayers/' + $scope.teamNumber + '/';
                    $scope.bookingId = bookingId;
                    $scope.totalPlayer = res.data.Team.length;
                    $scope.teams = res.data.Team;
                    $scope.coachName = res.data.Coach.name;
                    $scope.subCoachName = res.data.SubCoach.name;
                    $scope.startTime = res.data.ClubBooking.book_start_time;
                    $scope.endTime = res.data.ClubBooking.book_end_time;
                    $scope.courtBookingFee = res.data.ClubBooking.court_booking_fee;
                    $scope.coachBookingFee = res.data.ClubBooking.coach_booking_fee;
                    $scope.subTotal = res.data.ClubBooking.sub_total;
                    $scope.taxFee = res.data.ClubBooking.tax_fee;
                    $scope.taxAmount = res.data.ClubBooking.tax_amount;
                    $scope.grandTotal = res.data.ClubBooking.grand_total;
                    $scope.paymentMode = res.data.ClubBooking.payment_mode;
                    $scope.clubMembershipId = res.data.Club.membership_id;
                    $scope.userAppId = res.data.ClubBooking.user_id;
                    $scope.paymentDate = res.data.ClubBooking.payment_date;
                    $scope.cardType = res.data.Transaction.card_type;
                    $scope.cardNumber = res.data.Transaction.card_number;
                    $scope.cardName = res.data.Transaction.card_name;
                    var edate = new Date(res.data.ClubBooking.book_date + " " + res.data.ClubBooking.book_start_time);
                    var eedate = new Date(res.data.ClubBooking.book_date + " " + res.data.ClubBooking.book_end_time);
                    $scope.eventPlayTime = moment(edate).format('h:mma') + " - " + moment(eedate).format('h:mma');
                    $scope.courtName = res.data.Court.name;
                    $scope.start = res.data.ClubBooking.book_date;
                    $scope.eventDate = moment(edate).format('Do MMMM YYYY');
                    $scope.eventDay = moment(edate).format('dddd');
                    $scope.bookingNumber = res.data.ClubBooking.prefix_booking_number + ("0000" + res.data.ClubBooking.booking_number).slice(-6);
                    $scope.matchType = res.data.ClassType.name;
                    $scope.courtName = res.data.Court.name;
                    $scope.picUrl = uploadUrl;
                }, function error(response) {
                    $scope.showAlert(1, response);
                });

            }

            $scope.closeRightSideBarBookingDetails = function () {
                $scope.closeAllForms('rightSideBar');
            }

            $scope.cancelBooking = function () {
                $scope.closeAllForms('rightSideBarCancelBooking');
                $("#reasonText").val('');
            }

            $scope.closeRightSideBarCancelBooking = function () {
                $scope.closeAllForms('rightSideBarBookingDetails');
            }

            $scope.confirmCancelBooking = function () {
                if ($("#reasonText").val() == '') {
                    $scope.showAlert(1, "Please enter reason");
                } else {
                    $http({
                        method: 'post',
                        url: apiUrl + 'deleteClubBooking',
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded'
                        },
                        data: $.param({
                            booking_id: $scope.bookingId,
                            user_id: localStorage.getItem('user_id'),
                            reason: $("#reasonText").val()
                        }),
                    }).then(function success(res) {
                        if (res.status) {
                            $("#upcomingBookings").removeClass("active");
                            $scope.$broadcast("setCalendar", {id: localStorage.getItem('club_id'), court_id: localStorage.getItem('court_id'), type: 2});
                            $scope.$broadcast("showUpcomingBookings");
                            $scope.showAlert(2, "This booking has been cancelled successfully. All players involved will be notified. ", "Booking Cancelled");
                            $scope.closeRightSideBarCancelBooking();
                            $scope.closeRightSideBarBookingDetails();
                            $scope.closeRightSideBar();
                        }
                    }, function error(response) {
                        $scope.showAlert(1, response);
                    });
                }
            }

            $scope.printReciept = function () {
                /*alert("here")
                 var myWindow = $("#rightSideBarPrint").html();
                 window.print();*/
                //Get the HTML of div
                /*    var divElements = document.getElementById("rightSideBarPrint").innerHTML;
                 //Get the HTML of whole page
                 var oldPage = document.body.innerHTML;
                 
                 //Reset the page's HTML with div's HTML only
                 document.body.innerHTML =
                 "<html><head><title></title></head><body>" +
                 divElements + "</body>";
                 
                 //Print Page
                 window.print();
                 
                 //Restore orignal HTML
                 document.body.innerHTML = oldPage; */
                window.print();
            }



            $scope.getCourtFee = function ($court) {
                $http({
                    method: 'post',
                    url: apiUrl + 'getCourtDetails',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    data: $.param({
                        court_id: $court,
                    }),
                }).then(function success(res) {
                    if (res.data.status) {
                        console.log(res.data)
                        $scope.courtFee = parseFloat(res.data.data.Court.rate);
                        $scope.taxRate = parseFloat(res.data.data.Club.tax_rate);
                        if ($scope.totalCoachFee != undefined) {
                            $scope.subTotal = $scope.totalCoachFee + $scope.courtFee;
                        } else {
                            $scope.subTotal = $scope.courtFee;
                        }
                        if ($scope.subTotal != undefined) {
                            $scope.taxAmt = ($scope.subTotal * $scope.taxRate) / 100;
                        } else {
                            $scope.taxAmt = 0;
                        }
                        $scope.grandTotal = parseFloat($scope.subTotal) + parseFloat($scope.taxAmt);
                        $scope.perHeadFee = $scope.grandTotal / $scope.totalClassPlayer;
                    } else {
                        $scope.showAlert(1, res.data.message);
                    }
                }, function error(response) {
                    $scope.showAlert(1, response);
                });
            }

            $scope.getCoachFee = function ($coach) {
                if ($("#classStartHour").val() == "") {
                    $("#classCoach").val("");
                    $scope.showAlert(1, "Please choose class start hour");
                } else if ($("#classStartMinute").val() == "") {
                    $("#classCoach").val("");
                    $scope.showAlert(1, "Please choose class start minute");
                } else if ($("#classEndHour").val() == "") {
                    $("#classCoach").val("");
                    $scope.showAlert(1, "Please choose class end hour");
                } else if ($("#classEndMinute").val() == "") {
                    $("#classCoach").val("");
                    $scope.showAlert(1, "Please choose class end minute");
                } else {
                    $http({
                        method: 'post',
                        url: apiUrl + 'getCoachRate',
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded'
                        },
                        data: $.param({
                            coach_id: $coach,
                            book_date: $("#classBookDate").val(),
                            start_hours: $("#classStartHour").val(),
                            start_minutes: $("#classStartMinute").val(),
                            end_hours: $("#classEndHour").val(),
                            end_minutes: $("#classEndMinute").val()
                        }),
                    }).then(function success(res) {
                        if (res.data.status) {
                            console.log(res.data);
                            $scope.totalCoachFee = parseFloat(res.data.data);
                            if ($scope.courtFee != undefined) {
                                $scope.subTotal = $scope.totalCoachFee + $scope.courtFee;
                            } else {
                                $scope.subTotal = $scope.totalCoachFee;
                            }
                            if ($scope.taxRate != undefined) {
                                $scope.taxAmt = ($scope.subTotal * $scope.taxRate) / 100;
                            } else {
                                $scope.taxAmt = 0;
                            }
                            $scope.grandTotal = parseFloat($scope.subTotal) + parseFloat($scope.taxAmt);
                            $scope.perHeadFee = $scope.grandTotal / $scope.totalClassPlayer;
                            //$scope.courtFee = res.data.data.Court.rate;
                        } else {
                            $scope.showAlert(1, res.data.message);
                        }
                    }, function error(response) {
                        $scope.showAlert(1, response);
                    });
                }
            }

            $scope.addGroupClass = function () {
                var isClubMember = $("#isClubMember").is(":checked") ? 1 : 2;
                var ageGroup = $("#classPlayerAgeGroup").val().split("-");
                $http({
                    method: 'post',
                    url: apiUrl + 'addGroupClass',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    data: $.param({
                        is_club_member: isClubMember,
                        class_name: $("#className").val(),
                        class_description: $("#classDescription").val(),
                        book_date: $("#classBookDate").val(),
                        court_id: $("#classCourtNo").val(),
                        start_hours: $("#classStartHour").val(),
                        start_minutes: $("#classStartMinute").val(),
                        end_hours: $("#classEndHour").val(),
                        end_minutes: $("#classEndMinute").val(),
                        min_player_rating: $("#classPlayerRating").val(),
                        min_player_age: ageGroup[0],
                        max_player_age: ageGroup[1],
                        no_player: $("#classTotalPlayer").val(),
                        coach_id: $("#classCoach").val(),
                        sub_coach_id: $("#classSubCoach").val(),
                        court_booking_fee: $scope.courtFee,
                        coach_booking_fee: $scope.totalCoachFee,
                        sub_total: $scope.subTotal,
                        tax_amount: $scope.taxAmt,
                        tax_fee: $scope.taxRate,
                        grand_total: $scope.grandTotal,
                        club_id: localStorage.getItem('club_id'),
                        user_id: localStorage.getItem('user_id')
                    }),
                }).then(function success(res) {
                    if (res.data.status) {
                        //$scope.showAlert(2, 'Your class has been created successfully. To view upcoming group classes, please go to Group Classes', 'Class Created');
                        $scope.groupClassbooked(res.data.data);
                    } else {
                        $scope.showAlert(1, res.data.message);
                    }
                }, function error(response) {
                    $scope.showAlert(1, response);
                });
            }

            $scope.groupClassbooked = function (bookingId) {
                $http({
                    method: 'post',
                    url: apiUrl + 'getEventSessionDetails',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    data: $.param({
                        booking_id: bookingId
                    }),
                }).then(function success(res) {
                    res = res.data;
                    $scope.closeAllForms('rightSideBarGroupClassBooked');
                    $scope.bookingId = bookingId;
                    $scope.totalPlayer = res.data.ClubBookingSession.no_player;
                    $scope.teams = res.data.Team;
                    $scope.coachName = res.data.Coach.name;
                    $scope.subCoachName = res.data.SubCoach.name;
                    $scope.startTime = res.data.ClubBookingSession.book_start_time;
                    $scope.endTime = res.data.ClubBookingSession.book_end_time;
                    $scope.courtBookingFee = res.data.ClubBookingSession.court_booking_fee;
                    $scope.coachBookingFee = res.data.ClubBookingSession.coach_booking_fee;
                    $scope.subTotal = res.data.ClubBookingSession.sub_total;
                    $scope.taxFee = res.data.ClubBookingSession.tax_fee;
                    $scope.taxAmount = res.data.ClubBookingSession.tax_amount;
                    $scope.grandTotal = res.data.ClubBookingSession.grand_total;
                    var edate = new Date(res.data.ClubBookingSession.book_date + " " + res.data.ClubBookingSession.book_start_time);
                    var eedate = new Date(res.data.ClubBookingSession.book_date + " " + res.data.ClubBookingSession.book_end_time);
                    $scope.eventPlayTime = moment(edate).format('h:mma') + " - " + moment(eedate).format('h:mma');
                    $scope.courtName = res.data.Court.name;
                    $scope.start = res.data.ClubBookingSession.book_date;
                    $scope.eventDate = moment(edate).format('Do MMMM YYYY');
                    $scope.eventDay = moment(edate).format('dddd');
                    $scope.bookingNumber = res.data.ClubBookingSession.prefix_booking_number + ("0000" + res.data.ClubBookingSession.booking_number).slice(-6);
                    $scope.matchType = res.data.ClassType.name;
                    $scope.courtName = res.data.Court.name;
                    $scope.clubName = res.data.Club.name;
                    $scope.clubAddress = res.data.Club.address;
                    $scope.clubContact = res.data.Club.contact;
                    $scope.clubProfilePicture = res.data.Club.profile_picture;
                }, function error(response) {
                    $scope.showAlert(1, response);
                });
            }

            $scope.closeRightSideBarAddGroupClass = function () {
                $("#rightSideBarAddGroupClass").hide();
                //$(".app-content-body").removeClass("sideBarOn");
                //$(".button-bottom").removeClass("sideBarOn");
            }

            $scope.closeRightSideBarGroupClassBooked = function () {
                $("#rightSideBarGroupClassBooked").hide();
                //$(".app-content-body").removeClass("sideBarOn");
                //$(".button-bottom").removeClass("sideBarOn");
            }

            $scope.setClassTotalPlayer = function (classPlayer) {
                $scope.totalClassPlayer = classPlayer;
                $scope.perHeadFee = $scope.grandTotal / $scope.totalClassPlayer;
            }

            $scope.closeAllForms = function (id) {
                $("#rightSideBarAddForm").hide();
                $("#rightSideBarBookingDetailsConfirm").hide();
                $("#rightSideBarPaymentForm").hide();
                $("#rightSideBarPrint").hide();
                $("#rightSideBarSearch").hide();
                $("#rightSideBar").hide();
                $("#rightSideBarDetails").hide();
                $("#rightSideBarBookingDetails").hide();
                $("#rightSideBarCancelBooking").hide();
                $("#rightSideBarAddGroupClass").hide();
                $("#rightSideBarGroupClassBooked").hide();
                $("#" + id).show();
            }



            $scope.openNotifications = function () {
                $http({
                    method: 'post',
                    url: apiUrl + 'getUnreadNotifications',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    data: $.param({
                        user_id: localStorage.getItem('user_id')
                    }),
                }).then(function success(res) {
                    if (res.data.status) {
                        $scope.uploadUrl = uploadUrl;
                        $scope.notifications = res.data.data;
                    } else {
                        $scope.notifications = [];
                    }
                    $timeout(function () {
                        var template = $("#notificationsMainDiv").html();
                        var $html = $('<div />', {html: template});
                        $html.find('.box-main-inner').removeClass("hide");
                        $html.find('#1a').attr("id", "1aa");
                        $html.find('#2a').attr("id", "2aa");
                        $html.find('#3a').attr("id", "3aa");
                        $html.find('#4a').attr("id", "4aa");
                        $html.find('#5a').attr("id", "5aa");
                        template = $html.html();
                        $("#commonPopup").html(template);
                        $(".overlay").removeClass("hide");
                        $("#commonPopup").removeClass("hide");
                        $compile($("#1aa"))($scope);
                        $compile($("#2aa"))($scope);
                    }, 100);

                }, function error(response) {
                    $scope.showAlert(1, response);
                });

            }

            $scope.showNotificationDetail = function (id, type, nid) {
                $http({
                    method: 'post',
                    url: apiUrl + 'setReadNotification',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    data: $.param({
                        id: nid,
                        user_id: localStorage.getItem('user_id')
                    }),
                }).then(function success(res) {
                    if (res.data.status) {
                        if (type == 1) {
                            var currentPage = $location.path();
                            if (currentPage == '/bookings') {
                                $scope.getRightSideBarDetails(id);
                                closeNotifications();
                            } else {
                                window.location.href = '#!/bookings';
                                $timeout(function () {
                                    var currentPage = $location.path();
                                    if (currentPage == '/bookings') {
                                        $scope.getRightSideBarDetails(id);
                                        closeNotifications();
                                    }
                                }, 3000);
                            }
                        }
                    } else {
                        $scope.showAlert(1, res.data.message);
                    }
                }, function error(response) {
                    $scope.showAlert(1, response);
                });
            }

            $scope.openDetails = function (bid) {
                $http({
                    method: 'post',
                    url: apiUrl + 'getEventDetails',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    data: $.param({
                        booking_id: bid
                    }),
                }).then(function success(res) {
                    $("#updateEventBooking input").each(function () {
                        $(this).val('');
                    });
                    $("#updateEventBooking select").each(function () {
                        $(this).val('');
                    });
                    $("#updateEventBooking textarea").each(function () {
                        $(this).val('');
                    });
                    res = res.data;
                    $scope.closeAllForms('rightSideBarDetails');
                    $scope.openCoachBox();
                    //$("#rightSideBarDetails").animate({ width: 'toggle' }, 350);
                    //$(".app-content-body").addClass("sideBarOn");
                    //$(".button-bottom").addClass("sideBarOn");
                    $scope.playerFirstName = res.data.UserDetail.Player.first_name;
                    $scope.playerLastName = res.data.UserDetail.Player.last_name;
                    $scope.playerContactNo = res.data.UserDetail.Player.phone;
                    $scope.playerEmail = res.data.UserDetail.User.email;
                    $scope.teamNumber = res.data.ClubBooking.team_number;
                    $scope.remoteUrl = apiUrl + 'getSearchPlayers/' + $scope.teamNumber + '/';
                    $scope.bookingId = bid;
                    $("#totalPlayer").val(res.data.Team.length);
                    $scope.teams = res.data.Team;
                    $scope.steams = res.data.Team;
                    $("#eventCoach").val(res.data.ClubBooking.coach_id);
                    $("#eventSubCoach").val(res.data.ClubBooking.sub_coach_id);
                    var startTime = res.data.ClubBooking.book_start_time;
                    startTime = $scope.getHoursMinutes(startTime);
                    startTime = startTime.split(":");
                    $("#eventStartHours").val(startTime[0]);
                    if (startTime[1] == 0) {
                        startTime[1] = "00";
                    }
                    $("#eventStartMinutes").val(startTime[1]);
                    var endTime = res.data.ClubBooking.book_end_time;
                    endTime = $scope.getHoursMinutes(endTime);
                    endTime = endTime.split(":");
                    $("#eventEndHours").val(endTime[0]);
                    if (endTime[1] == 0) {
                        endTime[1] = "00";
                    }
                    $("#eventEndMinutes").val(endTime[1]);
                    $("#eventCourt").val(res.data.ClubBooking.court_id);
                    //$scope.start = res.data.ClubBooking.book_date;
                    $("#bookDate").datepicker("update", new Date(res.data.ClubBooking.book_date));
                    //$scope.totalPlayer = res.data.Team.length;
                }, function error(response) {
                    // do nothing
                });

            }

            $scope.getRightSideBarDetails = function (bookingId) {
                $http({
                    method: 'post',
                    url: apiUrl + 'getEventDetails',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    data: $.param({
                        booking_id: bookingId
                    }),
                }).then(function success(res) {
                    res = res.data;
                    $scope.closeAllForms('rightSideBarBookingDetails');
                    //$("#rightSideBarBookingDetails").animate({ width: 'toggle' }, 350);
                    ////$(".app-content-body").addClass("sideBarOn");
                    ////$(".button-bottom").addClass("sideBarOn");
                    $scope.bookerName = res.data.UserDetail.Player.first_name + " " + res.data.UserDetail.Player.last_name;
                    $scope.playerContactNo = res.data.UserDetail.Player.phone;
                    $scope.playerEmail = res.data.UserDetail.User.email;
                    $scope.teamNumber = res.data.ClubBooking.team_number;
                    $scope.remoteUrl = apiUrl + 'getSearchPlayers/' + $scope.teamNumber + '/';
                    $scope.bookingId = bookingId;
                    $scope.totalPlayer = res.data.Team.length;
                    $scope.teams = res.data.Team;
                    $scope.steams = res.data.Team;
                    $scope.coachName = res.data.Coach.name;
                    $scope.subCoachName = res.data.SubCoach.name;
                    $scope.startTime = res.data.ClubBooking.book_start_time;
                    $scope.endTime = res.data.ClubBooking.book_end_time;
                    $scope.courtBookingFee = res.data.ClubBooking.court_booking_fee;
                    $scope.coachBookingFee = res.data.ClubBooking.coach_booking_fee;
                    $scope.subTotal = res.data.ClubBooking.sub_total;
                    $scope.taxFee = res.data.ClubBooking.tax_fee;
                    $scope.taxAmount = res.data.ClubBooking.tax_amount;
                    $scope.grandTotal = res.data.ClubBooking.grand_total;
                    $scope.paymentMode = res.data.ClubBooking.payment_mode;
                    $scope.clubMembershipId = res.data.Club.membership_id;
                    $scope.userAppId = res.data.ClubBooking.user_id;
                    $scope.paymentDate = res.data.ClubBooking.payment_date;
                    $scope.cardType = res.data.Transaction.card_type;
                    $scope.cardNumber = res.data.Transaction.card_number;
                    $scope.cardName = res.data.Transaction.card_name;
                    var edate = new Date(res.data.ClubBooking.book_date + " " + res.data.ClubBooking.book_start_time);
                    var eedate = new Date(res.data.ClubBooking.book_date + " " + res.data.ClubBooking.book_end_time);
                    $scope.eventPlayTime = moment(edate).format('h:mma') + " - " + moment(eedate).format('h:mma');
                    $scope.courtName = res.data.Court.name;
                    $scope.start = res.data.ClubBooking.book_date;
                    $scope.eventDate = moment(edate).format('Do MMMM YYYY');
                    $scope.eventDay = moment(edate).format('dddd');
                    $scope.bookingNumber = res.data.ClubBooking.prefix_booking_number + ("0000" + res.data.ClubBooking.booking_number).slice(-6);
                    $scope.matchType = res.data.ClassType.name;
                    $scope.courtName = res.data.Court.name;
                    $scope.picUrl = uploadUrl;
                }, function error(response) {
                    // do nothing
                });

            }

            $scope.closeRightSideBarSearchAppUser = function () {
                $("#rightSideBarSearchAppUser").hide();
            }

            $scope.closeRightSideBarSearchAppEditUser = function () {
                $("#rightSideBarSearchAppEditUser").hide();
            }

            $scope.openSearchAppForm = function () {
                if ($("#totalAddPlayer").val() != '') {
                    $("#rightSideBarSearchAppUser").show();
                    $scope.teams = [];
                } else {
                    $scope.showAlert(1, "Please select number of players");
                }

            }

            $scope.openSearchAppEditForm = function () {
                if ($("#totalPlayer").val() != '') {
                    $("#rightSideBarSearchAppEditUser").show();
                } else {
                    $scope.showAlert(1, "Please select number of players");
                }

            }

            $scope.addSelectedMembers = function () {
                if (parseInt($("#totalAddPlayer").val()) >= $scope.addFormPlayers.length) {
                    $http({
                        method: 'post',
                        url: apiUrl + 'getPlayerInfo',
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded'
                        },
                        data: $.param({
                            playersIds: $scope.addFormPlayers
                        }),
                    }).then(function success(res) {
                        if (res.data.status) {
                            $scope.steams = res.data.data;
                            $scope.closeRightSideBarSearchAppUser();
                        } else {
                            $scope.showAlert(1, "Can not add player");
                        }
                    }, function error(response) {
                        // do nothing
                    });
                } else {
                    $scope.showAlert(1, "Can not add more than " + $("#totalAddPlayer").val() + " players");
                }

            }

            $scope.addSelectedEditMembers = function () {
                var teamIds = [];
                $.each($scope.teams, function (i, v) {
                    teamIds.push(v.Player.id);
                });
                $http({
                    method: 'post',
                    url: apiUrl + 'getPlayerInfo',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    data: $.param({
                        playersIds: teamIds
                    }),
                }).then(function success(res) {
                    if (res.data.status) {
                        $scope.steams = res.data.data;
                        $scope.closeRightSideBarSearchAppEditUser();
                    } else {
                        $scope.showAlert(1, "Can not add player");
                    }
                }, function error(response) {
                    // do nothing
                });


            }


            $scope.setAvailableTimings = function (courtId, type) {
                if (type == 1) {
                    if ($("#bookAddDate").val() == '') {
                        $scope.showAlert(1, "Please select book date first", "ERROR!");
                        return false;
                    }else{
                        var bookDate =$("#bookAddDate").val();
                    }
                } else if (type == 2) {
                    if ($("#bookDate").val() == '') {
                        $scope.showAlert(1, "Please select book date first", "ERROR!");
                        return false;
                    }else{
                        var bookDate =$("#bookDate").val();
                    }
                }
                $http({
                    method: 'post',
                    url: apiUrl + 'checkAvailableTimings',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    data: $.param({
                        book_date: bookDate,
                        club_id: localStorage.getItem('club_id')
                    }),
                }).then(function success(res) {
                    if (res.data.status) {
                        $rootScope.allAvailableCoaches = res.data.data.Coaches;
                        var startTime = res.data.data.CoachStartTime[0];
                        var startHours = startTime.split(":");
                        var endTime = res.data.data.CoachEndTime[0];
                        var endHours = endTime.split(":");
                        $rootScope.startHours = [];
                        $rootScope.endHours = [];
                        for (var i = parseInt(startHours[0]); i <= parseInt(endHours[0]); i++) {
                            if (i < parseInt(endHours[0])) {
                                $rootScope.startHours.push(i);
                            }
                            if (i > parseInt(startHours[0])) {
                                $rootScope.endHours.push(i);
                            }
                        }
                        console.log($rootScope.startHours)
                    } else {
                        $scope.showAlert(1, res.data.message, 'ERROR!');
                    }
                }, function error(response) {
                    $scope.showAlert(1, "Some error occurred", "ERROR!");
                });

            }


            $scope.$on('$viewContentLoaded', function () {
                $timeout(function () {
                    var pageId = $location.path().replace("/", "");
                    $(".sidebar-nav-inner li").removeClass("active");
                    $("#" + pageId + "Menu").addClass("active");
                    $(".datepicker").datepicker({
                        autoclose: true
                    });
                    $('#bookAddDate').datepicker({
                        startDate: new Date(),
                        autoclose: true
                    });
                }, 1000);

                $scope.openMenu = function () {
                    $("#wrapper").addClass("active");
                    $("#sidebar-wrapper").removeClass("active");
                };

                $scope.closeMenu = function () {
                    $("#sidebar-wrapper").addClass("active");
                    $("#wrapper").removeClass("active");
                };

                $scope.closeRightSideBar = function () {
                    $("#rightSideBar").hide();
                    //$("#rightSideBar").animate({ width: 'toggle' }, 350);
                    //$(".app-content-body").removeClass("sideBarOn");
                    //$(".button-bottom").removeClass("sideBarOn");
                };



            });
        });


var showForm = function (ev) {
    if (ev == 1) {
        $("#addEventBooking input").each(function () {
            $(this).val('');
        });
        $("#addEventBooking select").each(function () {
            $(this).val('');
        });
        $("#rightSideBarAddForm").show();
        $("#rightSideBarAddGroupClass").hide();
        $("#chooseForm option[value='2']").attr("selected", false);
        $("#chooseForm option[value='1']").attr("selected", true);
        //$("#chooseFormSchedule").val(1);
    } else if (ev == 2) {
        $("#addGroupClass input").each(function () {
            $(this).val('');
        });
        $("#addGroupClass select").each(function () {
            $(this).val('');
        });
        $("#addGroupClass textarea").each(function () {
            $(this).val('');
        });
        $("#rightSideBarAddForm").hide();
        $("#rightSideBarAddGroupClass").show();
        $("#chooseForm option[value='2']").attr("selected", true);
        $("#chooseForm option[value='1']").attr("selected", false);
        //$("#chooseFormGroupClass").val(2);
    }
    //$("#chooseForm").val(ev);
}

var checkMainCoach = function () {
    if ($("#eventAddSubCoach").val() != '') {
        if ($("#eventAddCoach").val() == '') {
            $("#eventAddSubCoach").val('');
            showAlert(1, "Please choose coach 1 first");
        } else if ($("#eventAddCoach").val() == $("#eventAddSubCoach").val()) {
            $("#eventAddSubCoach").val('');
            showAlert(1, "Coach 1 and coach 2 can not be same");
        }
    }
}

var showAlert = function (type, message) {
    $("#errMsg").text(message);
    if (type == 1) {
        $("#errTitle").text("ERROR!!");
    } else {
        $("#errTitle").text("SUCCESS!!");
    }
    $(".overlay").removeClass("hide");
    $("#boxAlert").removeClass("hide");
    //alert(message);
}


var showNotificationTab = function (id) {
    $("#" + id).show();
    $("#" + id).addClass("active");
    if (id == '1aa') {
        $("#2aa, #3aa, #4aa, #5aa").hide();
        $("#2aa, #3aa, #4aa, #5aa").removeClass("active");
    } else if (id == '2aa') {
        $("#1aa, #3aa, #4aa, #5aa").hide();
        $("#1aa, #3aa, #4aa, #5aa").removeClass("active");
    } else if (id == '3aa') {
        $("#1aa, #2aa, #4aa, #5aa").hide();
        $("#1aa, #2aa, #4aa, #5aa").removeClass("active");
    } else if (id == '4aa') {
        $("#1aa, #2aa, #3aa, #5aa").hide();
        $("#1aa, #2aa, #3aa, #5aa").removeClass("active");
    } else if (id == '5aa') {
        $("#1aa, #2aa, #3aa, #4aa").hide();
        $("#1aa, #2aa, #3aa, #4aa").removeClass("active");
    }
}

var closeNotifications = function () {
    $("#commonPopup").html('');
    $(".overlay").addClass("hide");
    $("#commonPopup").addClass("hide");
}

var getNotifications = function () {
    $.ajax({
        method: 'post',
        url: apiUrl + 'getUnreadNotifications',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
        data: $.param({
            user_id: localStorage.getItem('user_id')
        }),
    }).then(function success(res) {
        if (res.status) {
            $(".noti-count").text(res.data.length);
        } else {
            $(".noti-count").text(0);
        }
    }, function error(response) {
        //showAlert(1, response);
    });
}