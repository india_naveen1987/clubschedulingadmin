'use strict';

angular.module('myApp.groupclasses', ['ngRoute'])

        .config(['$routeProvider', function ($routeProvider) {
                $routeProvider.when('/groupclasses', {
                    templateUrl: 'groupclasses/groupclasses.html',
                    controller: 'GroupclassesCtrl'
                });
            }])

        .controller('GroupclassesCtrl', function ($scope, $http, $timeout, $compile, $rootScope, $templateCache) {
            //console.log('Group Classes Loading...');
            if ($rootScope.searchData != undefined) {
                var searchData = $rootScope.searchData;
            } else {
                var searchData = '';
            }
            $scope.$on('$viewContentLoaded', function () {
                $scope.contentLoading(1);
            });

            $scope.addGroupClass = function () {
                var currentForm = $('#addGroupClassForm');
                var isClubMember = currentForm.find("#isClubMember").is(":checked") ? 1 : 2;
                var ageGroup = currentForm.find("#classPlayerAgeGroup").val().split("-");
                $http({
                    method: 'post',
                    url: apiUrl + 'addGroupClassByCurd',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    data: $.param({
                        is_club_member: isClubMember,
                        class_name: currentForm.find("#className").val(),
                        class_description: currentForm.find("#classDescription").val(),
                        book_date: currentForm.find("#classBookDate").val(),
                        court_id: currentForm.find("#classCourtNo").val(),
                        start_hours: currentForm.find("#classStartHour").val(),
                        start_minutes: currentForm.find("#classStartMinute").val(),
                        end_hours: currentForm.find("#classEndHour").val(),
                        end_minutes: currentForm.find("#classEndMinute").val(),
                        min_player_rating: currentForm.find("#classPlayerRating").val(),
                        min_player_age: ageGroup[0],
                        max_player_age: ageGroup[1],
                        no_player: currentForm.find("#classTotalPlayer").val(),
                        coach_id: currentForm.find("#classCoach").val(),
                        sub_coach_id: currentForm.find("#classSubCoach").val(),
                        court_booking_fee: $scope.courtFeeAdd,
                        coach_booking_fee: $scope.totalCoachFeeAdd,
                        sub_total: $scope.subTotalAdd,
                        tax_amount: $scope.taxAmtAdd,
                        tax_fee: $scope.taxRateAdd,
                        grand_total: $scope.grandTotalAdd,
                        club_id: localStorage.getItem('club_id'),
                        user_id: localStorage.getItem('user_id')
                    }),
                }).then(function success(res) {
                    if (res.data.status) {
                        $scope.groupClassbooked(res.data.data);
                        $scope.closeRightSideBarAddGroupClass();
                        $scope.contentLoading(1);
                        $scope.showAlert(2, 'Class created successfully');
                    } else {
                        $scope.showAlert(1, res.data.message);
                    }
                }, function error(response) {
                    $scope.showAlert(1, response);
                });
            }

            $scope.contentLoading = function (page_no) {
                $http({
                    method: 'post',
                    url: apiUrl + 'allGroupclasses',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    data: $.param({
                        page_no: page_no
                    }),
                }).then(function success(res) {
                    //console.log(res.data.data);
                    //console.log($scope);
                    if (res.data.status) {
                        var groupClasses = [];
                        $.each(res.data.data, function (i, val) {
                            var edate = new Date(val.ClubBookingSession.book_date + " " + val.ClubBookingSession.book_start_time);
                            val.ClubBookingSession.book_start_time = edate;
                            var edate = new Date(val.ClubBookingSession.book_date + " " + val.ClubBookingSession.book_end_time);
                            val.ClubBookingSession.book_end_time = edate;
                            val.ClubBookingSession.class_description = val.ClubBookingSession.class_description.substring(0, 100);
                            val.JoinGroupClass = val.JoinGroupClass.length;
                            groupClasses.push(val);
                        });
                        $rootScope.allCoaches = res.data.coaches;
                        $rootScope.courts = res.data.courts;
                        $scope.groupClasses = groupClasses;
                        $scope.totalPages = 1;
                        $scope.currentPage = 1;
                        if (res.data.totalPages) {
                            $scope.totalPages = res.data.totalPages;
                            $scope.currentPage = res.data.currentPage;
                        }
                    } else {
                        $scope.showAlert(1, res.data.message);
                    }
                }, function error(response) {
                    $scope.showAlert(1, JSON.stringify(response));
                });
            }

            $scope.getRightSideBarDetails = function (groupClassId) {
                $scope.closeRightSideBarAddGroupClass();
                $http({
                    method: 'post',
                    url: apiUrl + 'allGroupclasses',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    data: $.param({
                        group_class_id: groupClassId
                    }),
                }).then(function success(res) {
                    if (res.data.status) {
                        $("#rightSideBarGroupClassDetails").show();
                        $(".app-content-body").addClass("sideBarOn");
                        $(".button-bottom").addClass("sideBarOn");
                        var val = res.data.data;
                        var edate = new Date(val.ClubBookingSession.book_date + " " + val.ClubBookingSession.book_start_time);
                        val.ClubBookingSession.book_start_time = edate;
                        var edate = new Date(val.ClubBookingSession.book_date + " " + val.ClubBookingSession.book_end_time);
                        val.ClubBookingSession.book_end_time = edate;
                        //console.log(val);
                        $rootScope.groupClassDeatilData = val;
                        $rootScope.picUrl = uploadUrl;
                        var totalActive = 0;
                        var totalActiveCharge = 0;
                        var totalCancel = 0;
                        var totalCancelCharge = 0;
                        $.each(val.JoinGroupClass, function (i, classData) {
                            if (classData.status == 1) {
                                totalActive = parseInt(totalActive) + 1;
                                totalActiveCharge = parseFloat(totalActiveCharge) + parseFloat(classData.amount);
                            } else {
                                totalCancel = parseInt(totalCancel) + 1;
                                totalCancelCharge = parseFloat(totalCancelCharge) + parseFloat(classData.total_cancellation_charge);
                            }
                        });
                        $rootScope.totalActive = totalActive;
                        $rootScope.totalActiveCharge = totalActiveCharge;
                        $rootScope.totalCancel = totalCancel;
                        $rootScope.totalCancelCharge = totalCancelCharge;
                        $rootScope.cancelClassCount = val.CancelClass.length;
                    } else {
                        $scope.showAlert(1, res.data.message);
                    }
                }, function error(response) {
                    $scope.showAlert(1, JSON.stringify(response));
                });

            }

            $scope.showFormCreateGroupClassAdd = function () {
                $scope.closeRightSideBarAddGroupClass();
                $("#rightSideBarAddGroupClass").show();
                $("#rightSideBarAddGroupClass").find('select').prop('selectedIndex', 0);
                $("#rightSideBarAddGroupClass").find('input[type="text"], textarea').val('');
                $("#rightSideBarAddGroupClass").find('input[type="checkbox"]').prop('checked', false);
                $scope.courtFeeAdd = 0;
                $scope.totalCoachFeeAdd = 0;
                $scope.coachNameAdd = 0;
                $scope.subTotalAdd = 0;
                $scope.taxRateAdd = 0;
                $scope.taxAmtAdd = 0;
                $scope.grandTotalAdd = 0;
                $scope.totalClassPlayerAdd = 0;
                $scope.perHeadFeeAdd = 0;
            }

            $scope.getRightSideBarDetailsEdit = function (groupClassId) {
                $scope.closeRightSideBarAddGroupClass();
                $http({
                    method: 'post',
                    url: apiUrl + 'allGroupclasses',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    data: $.param({
                        group_class_id: groupClassId
                    }),
                }).then(function success(res) {
                    if (res.data.status) {
                        var editClass = $("#rightSideBarEditGroupClass");
                        editClass.show();
                        $(".app-content-body").addClass("sideBarOn");
                        $(".button-bottom").addClass("sideBarOn");
                        var val = res.data.data;
                        var book_start_time = val.ClubBookingSession.book_start_time.split(':');
                        var book_end_time = val.ClubBookingSession.book_end_time.split(':');
                        var edate = new Date(val.ClubBookingSession.book_date + " " + val.ClubBookingSession.book_start_time);
                        val.ClubBookingSession.book_start_time = edate;
                        var edate = new Date(val.ClubBookingSession.book_date + " " + val.ClubBookingSession.book_end_time);
                        val.ClubBookingSession.book_end_time = edate;
                        editClass.find('#isClubMember').prop('checked', val.ClubBookingSession.is_club_member);
                        editClass.find('#className').val(val.ClubBookingSession.class_name);
                        editClass.find('#classDescription').val(val.ClubBookingSession.class_description);
                        editClass.find('#classBookDateEdit').val(val.ClubBookingSession.book_date);
                        editClass.find('#classStartHourEdit').val(book_start_time[0]);
                        editClass.find('#classStartMinuteEdit').val(book_start_time[1]);
                        editClass.find('#classEndHourEdit').val(book_end_time[0]);
                        editClass.find('#classEndMinuteEdit').val(book_end_time[1]);
                        editClass.find('#classPlayerRating').val(val.ClubBookingSession.min_player_rating);
                        editClass.find('#classPlayerAgeGroup').val(val.ClubBookingSession.min_player_age + '-' + val.ClubBookingSession.max_player_age);
                        editClass.find('#classTotalPlayer').val(val.ClubBookingSession.no_player);
                        editClass.find('#groupClassId').val(val.ClubBookingSession.id);
                        $rootScope.groupClassDeatilData = val;
                        $rootScope.allCoaches = res.data.coaches;
                        $rootScope.courts = res.data.courts;
                        $rootScope.picUrl = uploadUrl;
                        $rootScope.perHeadFee = (val.ClubBookingSession.grand_total / val.ClubBookingSession.no_player).toFixed(4);
                        $rootScope.totalClassPlayer = val.ClubBookingSession.no_player;
                        $rootScope.grandTotal = val.ClubBookingSession.grand_total;
                        $rootScope.taxRate = val.ClubBookingSession.tax_fee;
                        $rootScope.taxAmt = val.ClubBookingSession.tax_amount;
                        $rootScope.subTotal = val.ClubBookingSession.sub_total;
                        $rootScope.coachName = val.Coach.name;
                        $rootScope.totalCoachFee = val.ClubBookingSession.coach_booking_fee;
                        $rootScope.courtFee = val.ClubBookingSession.court_booking_fee;
                        //console.log(val);
                        setTimeout(function () {
                            editClass.find('#classCoach').val(val.ClubBookingSession.coach_id);
                            editClass.find('#classSubCoach').val(val.ClubBookingSession.sub_coach_id);
                            editClass.find('#classCourtNo').val(val.ClubBookingSession.court_id);
                        }, 1000);
                    } else {
                        $scope.showAlert(1, res.data.message);
                    }
                }, function error(response) {
                    $scope.showAlert(1, JSON.stringify(response));
                });

            }

            $scope.searchGroupClasses = function () {
                ////console.log($scope.search)
                searchData = $scope.search;
                searchData.classEndDate = $('#classEndDate').val();
                searchData.classStartDate = $('#classStartDate').val();
                ////console.log($('#classEndDate').val());
                $http({
                    method: 'post',
                    url: apiUrl + 'allGroupclassesSearch',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    data: $.param({
                        searchData: searchData
                    }),
                }).then(function success(res) {
                    //console.log(res.data.data);
                    //console.log($scope);
                    if (res.data.status) {
                        var groupClasses = [];
                        $.each(res.data.data, function (i, val) {
                            var edate = new Date(val.ClubBookingSession.book_date + " " + val.ClubBookingSession.book_start_time);
                            val.ClubBookingSession.book_start_time = edate;
                            var edate = new Date(val.ClubBookingSession.book_date + " " + val.ClubBookingSession.book_end_time);
                            val.ClubBookingSession.book_end_time = edate;
                            val.ClubBookingSession.class_description = val.ClubBookingSession.class_description.substring(0, 100);
                            groupClasses.push(val);
                        });
                        $scope.groupClasses = groupClasses;
                        $scope.totalPages = 1;
                        $scope.currentPage = 1;
                        $scope.closeRightSideBarAddGroupClass();
                        $scope.$apply();
                    } else {
                        $scope.showAlert(1, res.data.message);
                    }
                }, function error(response) {
                    $scope.showAlert(1, JSON.stringify(response));
                });
            }

            $scope.editGroupClass = function () {
                var currentForm = $('#editGroupClassFrom');
                var isClubMember = currentForm.find("#isClubMember").is(":checked") ? 1 : 2;
                var ageGroup = currentForm.find("#classPlayerAgeGroup").val().split("-");
                $http({
                    method: 'post',
                    url: apiUrl + 'addGroupClassByCurd',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    data: $.param({
                        is_club_member: isClubMember,
                        id: currentForm.find("#groupClassId").val(),
                        class_name: currentForm.find("#className").val(),
                        class_description: currentForm.find("#classDescription").val(),
                        book_date: currentForm.find("#classBookDateEdit").val(),
                        court_id: currentForm.find("#classCourtNo").val(),
                        start_hours: currentForm.find("#classStartHourEdit").val(),
                        start_minutes: currentForm.find("#classStartMinuteEdit").val(),
                        end_hours: currentForm.find("#classEndHourEdit").val(),
                        end_minutes: currentForm.find("#classEndMinuteEdit").val(),
                        min_player_rating: currentForm.find("#classPlayerRating").val(),
                        min_player_age: ageGroup[0],
                        max_player_age: ageGroup[1],
                        no_player: currentForm.find("#classTotalPlayer").val(),
                        coach_id: currentForm.find("#classCoach").val(),
                        sub_coach_id: currentForm.find("#classSubCoach").val(),
                        court_booking_fee: $rootScope.courtFee,
                        coach_booking_fee: $rootScope.totalCoachFee,
                        sub_total: $rootScope.subTotal,
                        tax_amount: $rootScope.taxAmt,
                        tax_fee: $rootScope.taxRate,
                        grand_total: $rootScope.grandTotal,
                        club_id: localStorage.getItem('club_id'),
                        user_id: localStorage.getItem('user_id')
                    }),
                }).then(function success(res) {
                    if (res.data.status) {
                        $scope.closeRightSideBarAddGroupClass();
                        $scope.contentLoading(1);
                        $scope.showAlert(2, 'Class updated successfully');
                    } else {
                        $scope.showAlert(1, res.data.message);
                    }
                }, function error(response) {
                    $scope.showAlert(1, response);
                });
            }

            $scope.getCourtEditFee = function ($court) {
                $http({
                    method: 'post',
                    url: apiUrl + 'getCourtDetails',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    data: $.param({
                        court_id: $court,
                    }),
                }).then(function success(res) {
                    if (res.data.status) {
                        console.log(res.data)
                        $rootScope.courtFee = parseFloat(res.data.data.Court.rate);
                        $rootScope.taxRate = parseFloat(res.data.data.Club.tax_rate);
                        if ($rootScope.totalCoachFee != undefined) {
                            $rootScope.subTotal = $rootScope.totalCoachFee + $rootScope.courtFee;
                        } else {
                            $rootScope.subTotal = $rootScope.courtFee;
                        }
                        if ($rootScope.subTotal != undefined && $rootScope.taxRate > 0) {
                            $rootScope.taxAmt = ($rootScope.subTotal * $rootScope.taxRate) / 100;
                        } else {

                            $rootScope.taxAmt = 0;
                        }
                        $rootScope.grandTotal = parseFloat($rootScope.subTotal) + parseFloat($rootScope.taxAmt);
                        $rootScope.perHeadFee = $rootScope.grandTotal / $rootScope.totalClassPlayer;
                    } else {
                        $scope.showAlert(1, res.data.message);
                    }
                }, function error(response) {
                    $scope.showAlert(1, response);
                });
            }

            $scope.getCourtAddFee = function ($court) {
                $http({
                    method: 'post',
                    url: apiUrl + 'getCourtDetails',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    data: $.param({
                        court_id: $court,
                    }),
                }).then(function success(res) {
                    if (res.data.status) {
                        console.log(res.data)
                        $scope.courtFeeAdd = parseFloat(res.data.data.Court.rate);
                        $scope.taxRateAdd = parseFloat(res.data.data.Club.tax_rate);
                        if ($scope.totalCoachFeeAdd != undefined) {
                            $scope.subTotalAdd = $scope.totalCoachFeeAdd + $scope.courtFeeAdd;
                        } else {
                            $scope.subTotalAdd = $scope.courtFeeAdd;
                        }
                        if ($scope.subTotalAdd != undefined && $scope.taxRateAdd > 0) {
                            $scope.taxAmtAdd = ($scope.subTotalAdd * $scope.taxRateAdd) / 100;
                        } else {
                            $scope.taxAmtAdd = 0;
                        }
                        $scope.grandTotalAdd = parseFloat($scope.subTotalAdd) + parseFloat($scope.taxAmtAdd);
                        $scope.perHeadFeeAdd = $scope.grandTotalAdd / $scope.totalClassPlayerAdd;
                    } else {
                        $scope.showAlert(1, res.data.message);
                    }
                }, function error(response) {
                    $scope.showAlert(1, response);
                });
            }

            $scope.getCoachAddFee = function ($coach) {
                if ($("#classStartHour").val() == "") {
                    $("#classCoach").val("");
                    $scope.showAlert(1, "Please choose class start hour");
                } else if ($("#classStartMinute").val() == "") {
                    $("#classCoach").val("");
                    $scope.showAlert(1, "Please choose class start minute");
                } else if ($("#classEndHour").val() == "") {
                    $("#classCoach").val("");
                    $scope.showAlert(1, "Please choose class end hour");
                } else if ($("#classEndMinute").val() == "") {
                    $("#classCoach").val("");
                    $scope.showAlert(1, "Please choose class end minute");
                } else {
                    $http({
                        method: 'post',
                        url: apiUrl + 'getCoachRate',
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded'
                        },
                        data: $.param({
                            coach_id: $coach,
                            book_date: $("#classBookDate").val(),
                            start_hours: $("#classStartHour").val(),
                            start_minutes: $("#classStartMinute").val(),
                            end_hours: $("#classEndHour").val(),
                            end_minutes: $("#classEndMinute").val()
                        }),
                    }).then(function success(res) {
                        if (res.data.status) {
                            //console.log(res.data);
                            $scope.totalCoachFeeAdd = parseFloat(res.data.data);
                            if ($scope.courtFeeAdd != undefined) {
                                $scope.subTotalAdd = $scope.totalCoachFeeAdd + $scope.courtFeeAdd;
                            } else {
                                $scope.subTotalAdd = $scope.totalCoachFeeAdd;
                            }
                            if ($scope.taxRateAdd != undefined && $scope.taxRateAdd > 0) {
                                $scope.taxAmtAdd = ($scope.subTotalAdd * $scope.taxRateAdd) / 100;
                            } else {
                                $scope.taxAmtAdd = 0;
                            }
                            $scope.grandTotalAdd = parseFloat($scope.subTotalAdd) + parseFloat($scope.taxAmtAdd);
                            $scope.perHeadFeeAdd = $scope.grandTotalAdd / $scope.totalClassPlayerAdd;
                            //$scope.courtFee = res.data.data.Court.rate;
                        } else {
                            $scope.showAlert(1, res.data.message);
                            $("#classCoach").prop('selectedIndex', 0);
                        }
                    }, function error(response) {
                        $scope.showAlert(1, response);
                        $("#classCoach").prop('selectedIndex', 0);
                    });
                }
            }

            $scope.setClassTotalPlayerAdd = function (classPlayer) {
                $scope.totalClassPlayerAdd = classPlayer;
                $scope.perHeadFeeAdd = $scope.grandTotalAdd / $scope.totalClassPlayerAdd;
            }

            $scope.getCoachEditFee = function ($coach) {
                if ($("#classStartHourEdit").val() == "") {
                    $("#classCoachEdit").val("");
                    $scope.showAlert(1, "Please choose class start hour");
                } else if ($("#classStartMinuteEdit").val() == "") {
                    $("#classCoachEdit").val("");
                    $scope.showAlert(1, "Please choose class start minute");
                } else if ($("#classEndHourEdit").val() == "") {
                    $("#classCoachEdit").val("");
                    $scope.showAlert(1, "Please choose class end hour");
                } else if ($("#classEndMinuteEdit").val() == "") {
                    $("#classCoachEdit").val("");
                    $scope.showAlert(1, "Please choose class end minute");
                } else {
                    $http({
                        method: 'post',
                        url: apiUrl + 'getCoachRate',
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded'
                        },
                        data: $.param({
                            coach_id: $coach,
                            book_date: $("#classBookDateEdit").val(),
                            start_hours: $("#classStartHourEdit").val(),
                            start_minutes: $("#classStartMinuteEdit").val(),
                            end_hours: $("#classEndHourEdit").val(),
                            end_minutes: $("#classEndMinuteEdit").val()
                        }),
                    }).then(function success(res) {
                        if (res.data.status) {
                            //console.log(res.data);
                            $rootScope.totalCoachFee = parseFloat(res.data.data);
                            if ($rootScope.courtFee != undefined) {
                                $rootScope.subTotal = $rootScope.totalCoachFee + $rootScope.courtFee;
                            } else {
                                $rootScope.subTotal = $rootScope.totalCoachFee;
                            }
                            if ($rootScope.taxRate != undefined && $rootScope.taxRate > 0) {
                                $rootScope.taxAmt = ($rootScope.subTotal * $rootScope.taxRate) / 100;
                            } else {
                                $rootScope.taxAmt = 0;
                            }
                            $rootScope.grandTotal = parseFloat($rootScope.subTotal) + parseFloat($rootScope.taxAmt);
                            $rootScope.perHeadFee = $rootScope.grandTotal / $rootScope.totalClassPlayer;
                            //$scope.courtFee = res.data.data.Court.rate;
                        } else {
                            $scope.showAlert(1, res.data.message);
                            $("#classCoach").prop('selectedIndex', 0);
                        }
                    }, function error(response) {
                        $scope.showAlert(1, response);
                        $("#classCoach").prop('selectedIndex', 0);
                    });
                }
            }

            $scope.setClassTotalEditPlayer = function (classPlayer) {
                $rootScope.totalClassPlayer = classPlayer;
                $rootScope.perHeadFee = $rootScope.grandTotal / $rootScope.totalClassPlayer;
            }

            $scope.closeRightSideBarAddGroupClass = function () {
                $("#rightSideBarAddGroupClass").hide();
                $("#rightSideBarEditGroupClass").hide();
                $("#rightSideBarGroupClassDetails").hide();
                $("#rightSideBarSearchGroupClass").hide();
                $("#rightSideBarAddPlayerToClassForm").hide();
                $('#rightSideBarPaymentForm').hide();
                $(".app-content-body").removeClass("sideBarOn");
                $(".button-bottom").removeClass("sideBarOn");
            }

            $scope.openRightSideBarAddPlayerToClass = function (classId) {
                $scope.closeRightSideBarAddGroupClass();
                $("#rightSideBarAddPlayerToClassForm").show();
                $('.ajaxaddedData').remove();
                $(".app-content-body").addClass("sideBarOn");
                $(".button-bottom").addClass("sideBarOn");
                $http({
                    method: 'post',
                    url: apiUrl + 'getClassJoinData',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    data: $.param({
                        group_class_id: classId
                    }),
                }).then(function success(res) {
                    //console.log(res.data);
                    if (res.data.status) {
                        var val = res.data.data;
                        var edate = new Date(val.ClubBookingSession.book_date + " " + val.ClubBookingSession.book_start_time);
                        val.ClubBookingSession.book_start_time = edate;
                        var edate = new Date(val.ClubBookingSession.book_date + " " + val.ClubBookingSession.book_end_time);
                        val.ClubBookingSession.book_end_time = edate;
                        //console.log(val);
                        $rootScope.groupClassDeatilData = val;
                        $rootScope.picUrl = uploadUrl;
                        $rootScope.players = res.data.players;
                        $rootScope.ajaxplayers = [];
                        $rootScope.remoteUrl = apiUrl + 'getSearchPlayersForClass/' + val.ClubBookingSession.id + '/';
                        $rootScope.clubremoteUrl = apiUrl + 'getSearchPlayersForClass/' + val.ClubBookingSession.id + '/';
                        $rootScope.noofplayers = 0;
                        $rootScope.totalmountChargesub = 0;
                        $rootScope.classIdForJoin = val.ClubBookingSession.id;
                        var promotionPrice = 0;
                        $rootScope.totalmountCharge = $rootScope.totalmountChargesub - promotionPrice;
                    } else {
                        $scope.showAlert(1, res.data.message);
                    }
                }, function error(response) {
                    $scope.showAlert(1, JSON.stringify(response));
                });
            }

            $scope.addPlayerToJoinClass = function () {
                var playerId = $('#clubAppUserIdForJoin').val();
                if (playerId > 0 && playerId != '') {
                    $http({
                        method: 'post',
                        url: apiUrl + 'getPlayerInfo',
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded'
                        },
                        data: $.param({
                            playerId: playerId,
                            playersIds: $scope.addFormPlayers
                        }),
                    }).then(function success(res) {
                        if (res.data.status) {
                            var val = res.data.data;
                            if (val.length > 0) {
                                $rootScope.ajaxplayers.push(val[0]);
                                $('#clubAppUserFirstName, #clubAppUserSurname, #clubAppUserContectNo, #clubAppUserEmail, #clubAppUserIdForJoin').val('');
                                var noofplayers = parseInt($('#noofplayers').val()) + 1;
                                var perplayer_price = $('#perplayer_price').val();
                                $rootScope.noofplayers = noofplayers;
                                $rootScope.totalmountChargesub = noofplayers * perplayer_price;
                                var promotionPrice = 0;
                                $rootScope.totalmountCharge = $rootScope.totalmountChargesub - promotionPrice;
                            }
                        }
                    }, function error(response) {
                        // do nothing
                    });
                }
            }

            $scope.removePlayerFromClass = function (player) {
                var classId = $('#classIdForJoin').val();
                $http({
                    method: 'post',
                    url: apiUrl + 'cancelClassWithRefundUser',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    data: $.param({
                        group_class_id: classId,
                        player_id: player
                    }),
                }).then(function success(res) {
                    if (res.data.status) {
                        $('.alreadyAdded.playerBlok_' + player).remove();
                        $scope.contentLoading(1);
                        $scope.showAlert(2, res.data.message);
                    } else {
                        $scope.showAlert(1, res.data.message);
                    }
                }, function error(response) {
                    $scope.showAlert(1, JSON.stringify(response));
                });
                return;
            }

            $scope.removePlayerFromClassAjax = function (player) {
                $('.ajaxaddedData.playerBlok_' + player).remove();
                var noofplayers = parseInt($('#noofplayers').val()) - 1;
                var perplayer_price = $('#perplayer_price').val();
                $rootScope.noofplayers = noofplayers;
                $rootScope.totalmountChargesub = noofplayers * perplayer_price;
                var promotionPrice = 0;
                $rootScope.totalmountCharge = $rootScope.totalmountChargesub - promotionPrice;
                return;
            }

            $scope.makePaymentJoin = function (type) {
                var no_players = $('#no_players_data').val();
                var no_join_players = $('.playersInfoData').length;
                var forJoinClass = $('.selectedTOAddClass').length;
                var classId = $('#classIdForJoin').val();
                if (forJoinClass > 0) {
                    var playersIds = [];
                    $('.selectedTOAddClass').each(function () {
                        playersIds.push($(this).val());
                    });
                    $scope.closeRightSideBarAddGroupClass();
                    $('#rightSideBarPaymentForm').show();
                    $scope.bookingId = classId;
                    $scope.grandTotal = $rootScope.totalmountCharge;
                } else {
                    $scope.showAlert(1, 'Plaese add minimum 1 player');
                }
            }

            $scope.paymentGateway = function (payment, bookingId) {
                $http({
                    method: 'post',
                    url: apiUrl + 'chargeCardAddplayer',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    data: $.param({
                        booking_id: bookingId,
                        payment: payment,
                        amount: $rootScope.totalmountCharge
                    }),
                }).then(function success(res) {
                    res = res.data;
                    if (res.status) {
                        //console.log(res);
                        $scope.bookingConfirm(res.transction_id.Transaction.id);
                    } else {
                        $scope.showAlert(1, res.message);
                    }
                }, function error(response) {
                    $scope.showAlert(1, response);
                });
            }

            $scope.confirmCardPayment = function (bookingId) {
                var $form = $('#payment-form');
                //console.log(Stripe)
                Stripe.card.createToken($form, function (error, response) {
                    //console.log(error);
                    //console.log(response);
                    if (error != 200) {
                        $scope.showAlert(1, response.error.message);
                    } else {
                        $http({
                            method: 'post',
                            url: apiUrl + 'makePaymentAddplayer',
                            headers: {
                                'Content-Type': 'application/x-www-form-urlencoded'
                            },
                            data: $.param({
                                booking_id: bookingId,
                                payment_type: 2,
                                card_type: response.card.brand,
                                booking_type: 2,
                                card_number: response.card.last4,
                                card_name: response.card.name
                            }),
                        }).then(function success(res) {
                            res = res.data;
                            if (res.status) {
                                $scope.paymentGateway(response, bookingId);
                            } else {
                                $scope.showAlert(1, res.message);
                            }
                        }, function error(response) {
                            $scope.showAlert(1, response);
                        });
                    }
                });
                return false;

            }

            $scope.bookingConfirm = function (bookingId) {
                var no_players = $('#no_players_data').val();
                var no_join_players = $('.playersInfoData').length;
                var forJoinClass = $('.selectedTOAddClass').length;
                var classId = $('#classIdForJoin').val();
                var playersIds = [];
                $('.selectedTOAddClass').each(function () {
                    playersIds.push($(this).val());
                });
                $http({
                    method: 'post',
                    url: apiUrl + 'setClassJoinPlayers',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    data: $.param({
                        group_class_id: classId,
                        payment_type: 2,
                        playersIds: playersIds,
                        transction_id: bookingId,
                        amount: $rootScope.groupClassDeatilData.ClubBookingSession.grand_total / $rootScope.groupClassDeatilData.ClubBookingSession.no_player
                    }),
                }).then(function success(res) {
                    if (res.data.status) {
                        $scope.closeRightSideBarAddGroupClass();
                        $scope.showAlert(2, res.data.message);
                        $scope.contentLoading(1);
                    } else {
                        $scope.showAlert(1, res.data.message);
                    }
                }, function error(response) {
                    $scope.showAlert(1, JSON.stringify(response));
                });
            }

            $scope.selectedAddPlayerClass = function (item) {
                var no_players = $('#no_players_data').val();
                var no_join_players = $('.playersInfoData').length;
                if (no_join_players < no_players) {
                    $http({
                        method: 'post',
                        url: apiUrl + 'getPlayerInfo',
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded'
                        },
                        data: $.param({
                            playerId: item.originalObject.id,
                            playersIds: $scope.addFormPlayers
                        }),
                    }).then(function success(res) {
                        if (res.data.status) {
                            var val = res.data.data;
                            if (val.length > 0) {
                                $('#clubAppUserFirstName').val(val[0].Player.first_name);
                                $('#clubAppUserSurname').val(val[0].Player.last_name);
                                $('#clubAppUserContectNo').val(val[0].Player.phone);
                                $('#clubAppUserEmail').val(val[0].User.email);
                                $('#clubAppUserIdForJoin').val(val[0].Player.id);
                            }
                        } else {
                            $scope.showAlert(1, "Can not add player");
                        }
                    }, function error(response) {
                        // do nothing
                    });
                } else {
                    $scope.showAlert(1, "Class full");
                }

            }

            $scope.cashPaymentToJaoinClass = function (payment_type) {
                var no_players = $('#no_players_data').val();
                var no_join_players = $('.playersInfoData').length;
                var forJoinClass = $('.selectedTOAddClass').length;
                var classId = $('#classIdForJoin').val();
                if (forJoinClass > 0) {
                    var playersIds = [];
                    $('.selectedTOAddClass').each(function () {
                        playersIds.push($(this).val());
                    });
                    $http({
                        method: 'post',
                        url: apiUrl + 'setClassJoinPlayers',
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded'
                        },
                        data: $.param({
                            group_class_id: classId,
                            payment_type: payment_type,
                            playersIds: playersIds,
                            amount: $rootScope.groupClassDeatilData.ClubBookingSession.grand_total / $rootScope.groupClassDeatilData.ClubBookingSession.no_player
                        }),
                    }).then(function success(res) {
                        if (res.data.status) {
                            $scope.closeRightSideBarAddGroupClass();
                            $scope.showAlert(2, res.data.message);
                            $scope.contentLoading(1);
                        } else {
                            $scope.showAlert(1, res.data.message);
                        }
                    }, function error(response) {
                        $scope.showAlert(1, JSON.stringify(response));
                    });
                } else {
                    $scope.showAlert(1, 'Plaese add minimum 1 player');
                }
            }

            $scope.cancelClass = function (classId) {
                $http({
                    method: 'post',
                    url: apiUrl + 'cancelClassWithRefund',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    data: $.param({
                        group_class_id: classId,
                    }),
                }).then(function success(res) {
                    if (res.data.status) {
                        $scope.closeRightSideBarAddGroupClass();
                        $scope.showAlert(2, res.data.message);
                        $scope.contentLoading(1);
                    } else {
                        $scope.showAlert(1, res.data.message);
                    }
                }, function error(response) {
                    $scope.showAlert(1, JSON.stringify(response));
                });
            }

            $scope.openLeftSearchBar = function () {
                $("#rightSideBarSearchGroupClass").show();
                $("#rightSideBarSearchGroupClass").find('select').prop('selectedIndex', 0);
                $("#rightSideBarSearchGroupClass").find('input[type="text"]').val('');
                $("#rightSideBarSearchGroupClass").find('input[type="checkbox"]').prop('checked', false);
                /*var date = new Date();
                 var classStartDate = date.setMonth(date.getMonth()-6);
                 var month = date.getMonth()+1;
                 $('#classStartDate').val(date.getFullYear()+'-'+(month<10?'0'+month:month)+'-01');
                 var classEndDate = date.setMonth(date.getMonth() +12);
                 var month = date.getMonth()+1;
                 $('#classEndDate').val(date.getFullYear()+'-'+(month<10?'0'+month:month)+'-01');*/
            }

            $scope.range = function (min, max, step) {
                step = step || 1;
                var input = [];
                for (var i = min; i <= max; i += step) {
                    input.push(i);
                }
                return input;
            }
        })
	