'use strict';

angular.module('myApp.bookings', ['ngRoute'])

    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/bookings', {
            templateUrl: 'bookings/bookings.html',
            controller: 'BookingsCtrl'
        });
    }])

    .directive('csSelect', function () {
        return {
            require: '^stTable',
            template: '<input type="checkbox"/>',
            scope: {
                row: '=csSelect'
            },
            link: function (scope, element, attr, ctrl) {

                element.bind('change', function (evt) {
                    scope.$apply(function () {
                        ctrl.select(scope.row, 'multiple');
                    });
                });

                scope.$watch('row.isSelected', function (newValue, oldValue) {
                    if (newValue === true) {
                        element.parent().addClass('st-selected');
                    } else {
                        element.parent().removeClass('st-selected');
                    }
                });
            }
        };
    })

    /*.factory('Resource', ['$q', '$filter', '$timeout', '$http', function ($q, $filter, $timeout, $http) {

        //this would be the service to call your server, a standard bridge between your model an $http

        // the database (normally on your server)
        var randomsItems = [];
        $http({
            method: "POST",
            url: apiUrl + "upcomingClubBookings",
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            data: $.param({ club_id: localStorage.getItem('club_id') }),
        }).then(function success(response) {
            console.log(response)
            if (response.data.status) {
                $.each(response.data.data, function (index, val) {
                    var bn = ("0000" + val.ClubBooking.booking_number).slice(-6);
                    var req = { book_date: val.ClubBooking.book_date, start_time: val.ClubBooking.book_start_time, end_time: val.ClubBooking.book_end_time, court: val.Court.name, prefix_booking_number: val.ClubBooking.prefix_booking_number, booking_number: bn };
                    randomsItems.push(req);
                });
            }
        }, function error(response) {

        });
        /*function createRandomItem(id) {
         var heroes = ['Batman', 'Superman', 'Robin', 'Thor', 'Hulk', 'Niki Larson', 'Stark', 'Bob Leponge'];
         return {
         book_date: id,
         start_time: heroes[Math.floor(Math.random() * 7)],
         court: Math.floor(Math.random() * 1000),
         booking_number: Math.floor(Math.random() * 10000)
         };
         
         }
         
         for (var i = 0; i < 1000; i++) {
         randomsItems.push(createRandomItem(i));
         }*/


    //fake call to the server, normally this service would serialize table state to send it to the server (with query parameters for example) and parse the response
    //in our case, it actually performs the logic which would happened in the server
    /*function getPage(start, number, params) {
        alert("here1")
        var deferred = $q.defer();

        var filtered = params.search.predicateObject ? $filter('filter')(randomsItems, params.search.predicateObject) : randomsItems;

        if (params.sort.predicate) {
            filtered = $filter('orderBy')(filtered, params.sort.predicate, params.sort.reverse);
        }

        var result = filtered.slice(start, start + number);

        $timeout(function () {
            //note, the server passes the information about the data set size
            deferred.resolve({
                data: result,
                numberOfPages: Math.ceil(filtered.length / number)
            });
        }, 1500);


        return deferred.promise;
    }

    return {
        getPage: getPage
    };

}])*/

    .controller('BookingsCtrl', function ($scope, $http, $timeout, $compile, $rootScope, $filter) {
        console.log("bookings ctrl");
        console.log($rootScope.searchData);
        if ($rootScope.searchData != undefined) {
            var searchData = $rootScope.searchData;
        } else {
            var searchData = '';
        }
        var table;
        var k = 0;
        $scope.picUrl = uploadUrl;
        $scope.search = {};


        $scope.$on('$viewContentLoaded', function () {
            $http({
                method: 'post',
                url: apiUrl + 'clubProfile',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: $.param({
                    id: localStorage.getItem('club_id'),
                }),
            }).then(function success(res) {
                res = res.data;
                console.log(res);
                $scope.courts = res.data.Court;
                $scope.allCoaches = res.data.Coach;
                $scope.clubMinTime = res.data.ClubOperatingHour[0].start_time;
                $scope.clubMaxTime = res.data.ClubOperatingHour[0].end_time;
                console.log($scope.clubMinTime);
            }, function error(response) {
                // do nothing
            });
            if (searchData != '') {
                $scope.contentLoading(1);
            } else {
                $scope.showUpcomingBookings();
            }
        });


        $scope.contentLoading = function (type) {
            k++;
            if (k > 1) {
                table.destroy();
            }
            table = $('#example').DataTable({
                "autoWidth": false,
                
                "ajax": {
                    "destroy": true,
                    "url": apiUrl + 'upcomingClubBookings',
                    "type": 'post',
                    "headers": {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    "data": {
                        "club_id": localStorage.getItem("club_id"),
                        "type": type,
                        "searchData": searchData
                    },
                    "dataSrc": function (json) {
                        console.log(json);
                        var fData = [];
                        if (json.status) {
                            $.each(json.data, function (index, val) {
                                console.log(val);
                                var checkdelete = "<input type='checkbox' id='check" + val.ClubBooking.id + "' class='checkselect'  />&nbsp;&nbsp;";
                                var book_date = val.ClubBooking.book_date;
                                var book_time = $filter('formatTime')(val.ClubBooking.book_start_time) + " - " + $filter('formatTime')(val.ClubBooking.book_end_time);
                                var court = val.Court.name;
                                var courtSort = val.Court.name;
                                var booking_number = val.ClubBooking.prefix_booking_number + " - " + val.ClubBooking.booking_number;
                                var details = " <a href='javascript:' ng-click='openDetails(" + val.ClubBooking.id + ")'><img style='margin-right:30px' class='ancher-img' src='images/edit.png'/></a><a href='javascript:' ng-click='getRightSideBarDetails(" + val.ClubBooking.id + ")'><img src='images/ahed-arrow.png'/></a>";
                                var res = {
                                    checkdelete: checkdelete,
                                    book_date: book_date,
                                    book_time: book_time,
                                    court: court,
                                    courtSort: courtSort,
                                    booking_number: booking_number,
                                    details: details
                                }
                                fData.push(res);
                            });
                        }


                        return fData;
                        /*for (var i = 0, ien = json.length; i < ien; i++) {
                            json[i][0] = '<a href="/message/' + json[i][0] + '>View message</a>';
                        }

                        {
                            "_": "Court "+Court.name,
                            "sort": "Court.name"
                            };
                        return json;*/
                    }
                }, "fnDrawCallback": function () {
                    var html = '<a href="javascript:" class="form-control" id="deleteBookings" ng-click="deleteBookings()"><img src="images/delete.png"/></a>';
                    $("#example_info").html(html);
                    var html = '<img src="images/left-arrow.png"/>';
                    $("#example_previous").html(html);
                    var html = '<img src="images/right-arrow.png"/>';
                    $("#example_next").html(html);
                    $("#example_filter label input").attr("placeholder"," Type here to search");
                    $compile($("#example"))($scope);
                    $compile($("#example_info"))($scope);
                    $compile($("#example_previous"))($scope);
                    $compile($("#example_next"))($scope);
                },
                "columns": [
                    { "data": "checkdelete"},
                    { "data": "book_date" },
                    { "data": "book_time" },
                    {
                        "data": {
                            _: "court",
                            sort: "courtSort"
                        }
                    },
                    { "data": "booking_number" },
                    { "data": "details" }
                ],
                "pageLength": 5
                

            });

        }

        

        $scope.searchBookings = function () {
            $scope.search.bookDate = $("#searchDate").val();
            console.log($scope.search)
            searchData = $scope.search;
            $scope.contentLoading(1);
        }

        $scope.closeRightSideBarBookingDetails = function () {
            $("#rightSideBarBookingDetails").hide();
            //$(".app-content-body").removeClass("sideBarOn");
            //$(".button-bottom").removeClass("sideBarOn");
        }

        $scope.deleteBookings = function () {
            var n = $('input:checkbox[id^="check"]:checked').length;
            if (n == 0) {
                $scope.showAlert(1, "Please select bookings", "ERROR");
            } else {
                $scope.showConfirm("Would you like to delete the following booking(s)?", 0, "removeBookings", "Are you sure?");
            }

        }

        $scope.$on('removeBookings', function (ev, args) {
            var ids = [];
            $('input:checkbox[id^="check"]:checked').each(function () {
                ids.push($(this).attr("id").replace("check", ""));

            });
            if (ids.length > 0) {
                $http({
                    method: 'post',
                    url: apiUrl + 'deleteClubBooking',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    data: $.param({
                        ids: ids,
                        user_id: localStorage.getItem('user_id')
                    }),
                }).then(function success(res) {
                    $scope.showAlert(2, "The following booking(s) have been deleted successfully.", "Successful");
                    $("#upcomingBookings").removeClass("active");
                    $scope.showUpcomingBookings();
                    //window.location.reload();
                }, function error(response) {
                    $scope.showAlert(1, response);
                });
            }
        })

        $scope.$on('showUpcomingBookings', function () {
            if (!$("#upcomingBookings").hasClass("active") && searchData == '') {
                $("#upcomingBookings").addClass("active");
                $("#archievedBookings").removeClass("active");
                $scope.contentLoading(1);
            } else if (searchData != '') {
                searchData = '';
                $scope.contentLoading(1);
            }
        });

        $scope.showUpcomingBookings = function () {
            if (!$("#upcomingBookings").hasClass("active") && searchData == '') {
                $("#upcomingBookings").addClass("active");
                $("#archievedBookings").removeClass("active");
                $scope.contentLoading(1);
            } else if (searchData != '') {
                searchData = '';
                $scope.contentLoading(1);
            }
        }

        $scope.showArcheivedBookings = function () {
            if (!$("#archievedBookings").hasClass("active") && searchData == '') {
                $("#upcomingBookings").removeClass("active");
                $("#archievedBookings").addClass("active");
                $scope.contentLoading(2);
            } else if (searchData != '') {
                searchData = '';
                $scope.contentLoading(2);
            }
        }

        $scope.clearSearch = function () {
            searchData = '';
            $("#upcomingBookings").removeClass("active");
            $scope.showUpcomingBookings();
        }

        $scope.selectDeselectAll = function () {
            if ($("#checkAll").is(":checked")) {
                $(".checkselect").prop("checked", true);
            } else {
                $(".checkselect").prop("checked", false);
            }
        }

        



        $scope.closeRightSideBarDetails = function () {
            $("#rightSideBarDetails").hide();
            //$(".app-content-body").removeClass("sideBarOn");
            //$(".button-bottom").removeClass("sideBarOn");
        }



    })