'use strict';

angular.module('myApp.promotions', ['ngRoute'])

        .config(['$routeProvider', function ($routeProvider) {
                $routeProvider.when('/promotions', {
                    templateUrl: 'promotions/promotions.html',
                    controller: 'PromotionsCtrl'
                });
            }])

        .controller('PromotionsCtrl', function ($scope, $http, $timeout, $compile, $rootScope, $templateCache) {
            //console.log('Group Classes Loading...');
            $scope.remoteUrl = '';
            $rootScope.addFormPlayers = [];
            $scope.picUrl = uploadUrl;
            if ($rootScope.searchData != undefined) {
                var searchData = $rootScope.searchData;
            } else {
                var searchData = '';
            }
            $scope.$on('$viewContentLoaded', function () {
                $scope.contentLoading(1);
            });



            $scope.contentLoading = function (page_no) {
                $http({
                    method: 'post',
                    url: apiUrl + 'getAllPromotions',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    data: $.param({
                        page_no: page_no,
                        club_id: localStorage.getItem('club_id')
                    }),
                }).then(function success(res) {
                    if (res.data.status) {
                        $scope.promotions = res.data.data;
                        $scope.totalPages = 1;
                        $scope.currentPage = 1;
                        if (res.data.totalPages) {
                            $scope.totalPages = res.data.totalPages;
                            $scope.currentPage = res.data.currentPage;
                        }
                        
                    } else {
                        $scope.showAlert(1, res.data.message);
                    }
                }, function error(response) {
                    $scope.showAlert(1, "Can not get promotions due to system error");
                });
            }

            $scope.selectedPlayerInfo = function (item) {
                if (item != undefined) {
                    $("#PromotionMembers_value").val('');
                    console.log(item);
                    $http({
                        method: 'post',
                        url: apiUrl + 'getPlayerInfo',
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded'
                        },
                        data: $.param({
                            playerId: item.originalObject.id,
                            playersIds: $rootScope.addFormPlayers
                        }),
                    }).then(function success(res) {
                        if (res.data.status) {
                            if ($.inArray(item.originalObject.id, $rootScope.addFormPlayers) == -1) {
                                $rootScope.addFormPlayers.push(item.originalObject.id);
                                $rootScope.pteams = res.data.data;
                            } else {
                                $scope.showAlert(1, "This player already added");
                            }
                        } else {
                            $scope.showAlert(1, "Can not add player");
                        }
                    }, function error(response) {
                        // do nothing
                    });
                }



            }

            $scope.removePromotionPlayer = function (id) {
                $scope.showConfirm("Are you sure1?", id, "confirmRemovePromotionPlayer");
            }

            $scope.$on('confirmRemovePromotionPlayer', function (ev, args) {
                var index = $rootScope.addFormPlayers.indexOf(args.id);
                $rootScope.addFormPlayers.splice(index, 1);
                $("#steamPromo" + args.id).remove();
            });

            $scope.openPromoSearchBar = function () {
                $("#promoSearchForm").show();
                $("#searchPromo input").each(function () {
                    $(this).val('');
                });
                $("#searchPromo select").each(function () {
                    $(this).val('');
                });
            }

            $scope.createPromo = function () {
                $rootScope.remoteUrl = apiUrl + 'getSearchPlayers/0/' + localStorage.getItem('club_id') + '/3/';
                $rootScope.addFormPlayers = [];
                $("#promoAddForm").show();
                $("#promoTitle").text("Create New Promo");
                $("#submitTitle").text("CREATE PROMO");
                $("#promoAddForm input").each(function () {
                    $(this).val('');
                });
                $("#promoAddForm select").each(function () {
                    $(this).val(1);
                });
                $("#promoAddForm textarea").each(function () {
                    $(this).val('');
                });
                var tomorrow = new Date();
                //tomorrow.setDate(tomorrow.getDate() + 1);
                $("#addPromo input[name='start_date']").datepicker("remove");
                $("#addPromo input[name='start_date']").datepicker({
                    autoclose: true,
                    startDate: tomorrow
                });
            }

            $scope.closePromoAddForm = function () {
                $("#promoAddForm").hide();
            }

            $scope.closePromoSearchForm = function () {
                $("#promoSearchForm").hide();
            }

            $scope.closePromoDetails = function () {
                $("#promoDetails").hide();
            }

            $scope.addPromo = function () {
                $http({
                    method: 'post',
                    url: apiUrl + 'addPromo',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    data: $("#addPromo").serialize() + "&" + $.param({
                        club_id: localStorage.getItem('club_id'),
                        playersIds: $rootScope.addFormPlayers
                    }),
                }).then(function success(res) {
                    if (res.data.status) {
                        $rootScope.addFormPlayers = [];
                        $rootScope.pteams = [];
                        $scope.contentLoading(1);
                        $scope.closePromoAddForm();
                        if ($("#addPromo input[name='id']").val() > 0) {
                            $scope.showAlert(2, "Promotion has been updated successfully.", "Promotion updated");
                        } else {
                            $scope.showAlert(2, "Promotion has been created successfully.", "Promotion created");
                        }

                    } else {
                        $scope.showAlert(1, res.data.message, "Error");
                    }
                }, function error(response) {
                    $scope.showAlert(1, "A system error occurred, please try again later.", "System error");
                });

            }

            $scope.editPromo = function (promoId) {
                $http({
                    method: 'post',
                    url: apiUrl + 'getPromo',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    data: $.param({
                        id: promoId
                    }),
                }).then(function success(res) {
                    if (res.data.status) {
                        $scope.createPromo();
                        $timeout(function () {
                            $("#addPromo input[name='start_date']").datepicker("remove");
                            $("#addPromo input[name='start_date']").datepicker({
                                autoclose: true
                            });
                            $("#promoTitle").text("Edit Promo");
                            $("#submitTitle").text("SAVE CHANGES");
                            $("#addPromo input[name='name']").val(res.data.data.Promotion.name);
                            $("#addPromo textarea[name='description']").val(res.data.data.Promotion.description);
                            $("#addPromo select[name='promo_type']").val(res.data.data.Promotion.promo_type);
                            $("#addPromo input[name='promo_value']").val(res.data.data.Promotion.promo_value);
                            $("#addPromo input[name='usage_per_user']").val(res.data.data.Promotion.usage_per_user);
                            $("#addPromo select[name='session_type']").val(res.data.data.Promotion.session_type);
                            $("#addPromo select[name='applicable_for']").val(res.data.data.Promotion.applicable_for);
                            showPlayerDiv($("#addPromo select[name='applicable_for']"));
                            $("#addPromo textarea[name='terms']").val(res.data.data.Promotion.terms);
                            $("#addPromo input[name='id']").val(res.data.data.Promotion.id);
                            $("#addPromo input[name='start_date']").datepicker("update", new Date(res.data.data.Promotion.start_date));
                            $("#addPromo input[name='end_date']").datepicker("update", new Date(res.data.data.Promotion.end_date));
                            $rootScope.pteams = res.data.data.Player;
                            $.each(res.data.data.Player, function (i, v) {
                                $rootScope.addFormPlayers.push(v.Player.id);
                            });
                            console.log($scope.pteams)
                        }, 500);

                    } else {
                        $scope.showAlert(1, res.data.message, "Error");
                    }
                }, function error(response) {
                    $scope.showAlert(1, "A system error occurred, please try again later.", "System error");
                });
            }

            $scope.viewPromo = function (promoId) {
                $http({
                    method: 'post',
                    url: apiUrl + 'getPromo',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    data: $.param({
                        id: promoId
                    }),
                }).then(function success(res) {
                    if (res.data.status) {
                        $scope.closePromoAddForm();
                        $("#promoDetails").show();
                        $rootScope.promotion = res.data.data;
                    } else {
                        $scope.showAlert(1, res.data.message, "Error");
                    }
                }, function error(response) {
                    $scope.showAlert(1, "A system error occurred, please try again later.", "System error");
                });
            }

            $scope.searchPromo = function () {
                $http({
                    method: 'post',
                    url: apiUrl + 'getAllPromotions',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    data: $("#searchPromo").serialize() + "&" + $.param({
                        club_id: localStorage.getItem('club_id')
                    }),
                }).then(function success(res) {
                    if (res.data.status) {
                        $scope.closePromoSearchForm();
                        $scope.promotions = res.data.data;
                        $scope.totalPages = 1;
                        $scope.currentPage = 1;
                        if (res.data.totalPages) {
                            $scope.totalPages = res.data.totalPages;
                            $scope.currentPage = res.data.currentPage;
                        }
                    } else {
                        $scope.showAlert(1, res.data.message);
                    }
                }, function error(response) {
                    $scope.showAlert(1, "A system error occurred, please try again later.", "System error");
                });

            }
        })

function showPlayerDiv(ev) {
    if ($(ev).val() == 1) {
        $("#playerDetailsDiv").show();
    } else {
        $("#playerDetailsDiv").hide();
    }
}