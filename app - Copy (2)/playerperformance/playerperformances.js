'use strict';
angular.module('myApp.playerperformance', ['ngRoute'])

        .config(['$routeProvider', function ($routeProvider) {
                $routeProvider.when('/playerperformance', {
                    templateUrl: 'playerperformance/playerperformance.html',
                    controller: 'PlayerPerformanceCtrl'
                });
            }])



        .controller('PlayerPerformanceCtrl', function ($scope, $http, $timeout, $compile, $rootScope, $templateCache, $filter) {

            $scope.uploadUrl = uploadUrl;
            var j = 0;
            var table;
            var clubBookingSessionId = 0;
            var slider = '';
            var k = 0;
            var table1;

            $scope.$on('$viewContentLoaded', function () {
                $scope.contentLoading();
            });

            $scope.contentLoading = function () {
                j++;
                if (j > 1) {
                    table.destroy();
                }
                table = $('#classListTable').DataTable({
                    "autoWidth": false,

                    "ajax": {
                        "destroy": true,
                        "url": apiUrl + 'allClasses',
                        "type": 'post',
                        "headers": {
                            'Content-Type': 'application/x-www-form-urlencoded'
                        },
                        "data": {
                            "coach_id": localStorage.getItem("coach_id")
                        },
                        "dataSrc": function (json) {
                            console.log(json);
                            var fData = [];
                            if (json.status) {
                                $.each(json.data, function (index, val) {
                                    console.log(val);
                                    var name = (val.ClubBookingSession.class_name != '') ? "&nbsp;" + val.ClubBookingSession.class_name : "&nbsp;" + val.ClubBookingSession.title;
                                    var date = val.ClubBookingSession.book_date;
                                    var feedbacks = val.Feedback;
                                    var details = " <a href='javascript:' ng-click='openClassDetails(" + val.ClubBookingSession.id + ")'><img src='images/edit.png'/></a>";
                                    var res = {
                                        name: name,
                                        date: date,
                                        feedbacks: feedbacks,
                                        details: details
                                    }
                                    fData.push(res);
                                });
                            }


                            return fData;

                        }
                    }, "fnDrawCallback": function () {
                        var html = '<img src="images/left-arrow.png"/>';
                        $("#classListTable_previous").html(html);
                        var html = '<img src="images/right-arrow.png"/>';
                        $("#classListTable_next").html(html);
                        $("#classListTable_info").remove();
                        $("#classListTable_filter").remove();
                        $compile($("#classListTable"))($scope);
                    },
                    "pageLength": 5,
                    "columns": [
                        {"data": "name"},
                        {"data": "date"},
                        {"data": "feedbacks"},
                        {"data": "details"}
                    ]


                });
            }

            $scope.contentLoadingPlayerProfile = function (type, player_id) {
                k++;
                if (k > 1) {
                    table1.destroy();
                }
                table1 = $('#playerProfileTable').DataTable({
                    "autoWidth": false,

                    "ajax": {
                        "destroy": true,
                        "url": apiUrl + 'allClasses',
                        "type": 'post',
                        "headers": {
                            'Content-Type': 'application/x-www-form-urlencoded'
                        },
                        "data": {
                            "coach_id": localStorage.getItem("coach_id"),
                            type: type,
                            player_id: player_id
                        },
                        "dataSrc": function (json) {
                            console.log(json);
                            var fData = [];
                            if (json.status) {
                                $.each(json.data, function (index, val) {
                                    console.log(val);
                                    var book_date = val.ClubBookingSession.book_date;
                                    var book_time = $filter('formatTime')(val.ClubBookingSession.book_start_time) + " - " + $filter('formatTime')(val.ClubBookingSession.book_end_time);
                                    var class_type = val.ClassType.name;
                                    var details = " <a href='javascript:' ng-click='openBookingDetails(" + val.ClubBookingSession.id + ")'><img src='images/ahed-arrow.png'/></a>";
                                    var res = {
                                        book_date: book_date,
                                        book_time: book_time,
                                        class_type: class_type,
                                        details: details
                                    }
                                    fData.push(res);
                                });
                            }


                            return fData;

                        }
                    }, "fnDrawCallback": function () {
                        var html = '<img src="images/left-arrow.png"/>';
                        $("#playerProfileTable_previous").html(html);
                        var html = '<img src="images/right-arrow.png"/>';
                        $("#playerProfileTable_next").html(html);
                        $("#playerProfileTable_info").remove();
                        $("#playerProfileTable_filter").remove();
                        $compile($("#playerProfileTable"))($scope);
                    },
                    "pageLength": 5,
                    "columns": [
                        {"data": "book_date"},
                        {"data": "book_time"},
                        {"data": "class_type"},
                        {"data": "details"}
                    ]


                });
            }

            $scope.openBookingDetails = function (bookingId) {
                $http({
                    method: 'post',
                    url: apiUrl + 'getEventSessionDetails',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    data: $.param({
                        booking_id: bookingId
                    }),
                }).then(function success(res) {
                    res = res.data;
                    $("#coachBookingModalPopup").modal('show');
                    $rootScope.bookerName = res.data.UserDetail.Player.first_name + " " + res.data.UserDetail.Player.last_name;
                    $rootScope.playerContactNo = res.data.UserDetail.Player.phone;
                    $rootScope.playerEmail = res.data.UserDetail.User.email;
                    $rootScope.teamNumber = res.data.ClubBookingSession.team_number;
                    $rootScope.remoteUrl = apiUrl + 'getSearchPlayers/' + $scope.teamNumber + '/';
                    $rootScope.bookingId = bookingId;
                    $rootScope.totalPlayer = res.data.Team.length;
                    $rootScope.teams = res.data.Team;
                    $rootScope.coachName = res.data.Coach.name;
                    $rootScope.subCoachName = res.data.SubCoach.name;
                    $rootScope.startTime = res.data.ClubBookingSession.book_start_time;
                    $rootScope.endTime = res.data.ClubBookingSession.book_end_time;
                    $rootScope.courtBookingFee = res.data.ClubBookingSession.court_booking_fee;
                    $rootScope.coachBookingFee = res.data.ClubBookingSession.coach_booking_fee;
                    $rootScope.subTotal = res.data.ClubBookingSession.sub_total;
                    $rootScope.taxFee = res.data.ClubBookingSession.tax_fee;
                    $rootScope.taxAmount = res.data.ClubBookingSession.tax_amount;
                    $rootScope.grandTotal = res.data.ClubBookingSession.grand_total;
                    $rootScope.paymentMode = res.data.ClubBookingSession.payment_mode;
                    $rootScope.clubMembershipId = res.data.Club.membership_id;
                    $rootScope.userAppId = res.data.ClubBookingSession.user_id;
                    $rootScope.paymentDate = res.data.ClubBookingSession.payment_date;
                    $rootScope.cardType = res.data.Transaction.card_type;
                    $rootScope.cardNumber = res.data.Transaction.card_number;
                    $rootScope.cardName = res.data.Transaction.card_name;
                    var edate = new Date(res.data.ClubBookingSession.book_date + " " + res.data.ClubBookingSession.book_start_time);
                    var eedate = new Date(res.data.ClubBookingSession.book_date + " " + res.data.ClubBookingSession.book_end_time);
                    $rootScope.eventPlayTime = moment(edate).format('h:mma') + " - " + moment(eedate).format('h:mma');
                    $rootScope.courtName = res.data.Court.name;
                    $rootScope.start = res.data.ClubBookingSession.book_date;
                    $rootScope.eventDate = moment(edate).format('Do MMMM YYYY');
                    $rootScope.eventDay = moment(edate).format('dddd');
                    $rootScope.bookingNumber = res.data.ClubBookingSession.prefix_booking_number + ("0000" + res.data.ClubBookingSession.booking_number).slice(-6);
                    $rootScope.matchType = res.data.ClassType.name;
                    $rootScope.courtName = res.data.Court.name;
                    $rootScope.picUrl = uploadUrl;
                }, function error(response) {
                    $scope.showAlert(1, JSON.stringify(response));
                });
            }

            $scope.openClassDetails = function (session_id) {
                $http({
                    method: 'post',
                    url: apiUrl + 'allGroupclasses',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    data: $.param({
                        group_class_id: session_id
                    }),
                }).then(function success(res) {
                    if (res.data.status) {
                        $rootScope.session_id = session_id;
                        $("#rightSideBarGroupClassDetails").show();
                        $rootScope.groupClassDeatilData = res.data.data;
                        $rootScope.totalFeedback = 0;
                        $rootScope.totalJoinGroupClass = 0;
                        if (res.data.data.JoinGroupClass.length > 0) {
                            $.each(res.data.data.JoinGroupClass, function (i, v) {
                                $rootScope.totalFeedback += v.PlayerFeedback.length;
                                $rootScope.totalJoinGroupClass++;
                            });
                        }

                    } else {
                        $scope.showAlert(1, res.data.message);
                    }
                }, function error(response) {
                    $scope.showAlert(1, response);
                });
            }

            $scope.closeRightSideBarAddGroupClass = function () {
                $("#rightSideBarGroupClassDetails").hide();
            }

            $scope.closeRightSideBarFeedback = function () {
                $("#rightSideBarFeedback").hide();
            }

            $scope.showFeedbackPage = function (session_id, player_id) {
                clubBookingSessionId = session_id;
                $http({
                    method: 'post',
                    url: apiUrl + 'getPlayerInfo',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    data: $.param({
                        playerId: player_id
                    }),
                }).then(function success(res) {
                    if (res.data.status) {
                        $("#rightSideBarFeedback").show();
                        $rootScope.playerDetails = res.data.data;
                    } else {
                        $scope.showAlert(1, res.data.message);
                    }
                }, function error(response) {
                    $scope.showAlert(1, response);
                });
            }

            var rating = 1;
            $scope.minus = function () {
                if (rating > 1) {
                    rating--;
                    $(".urtRating").text(rating);
                    $("#utr_level").val(rating);
                }
            }

            $scope.plus = function () {
                if (rating < 10) {
                    rating++;
                    $(".urtRating").text(rating);
                    $("#utr_level").val(rating);
                }
            }

            $scope.saveFeedback = function (player_id, session_id) {
                $http({
                    method: 'post',
                    url: apiUrl + 'savePlayerFeedback',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    data: $("#playerFeedback").serialize() + "&" + $.param({
                        club_booking_id: session_id,
                        player_id: player_id,
                        user_id: localStorage.getItem("coach_id")
                    }),
                }).then(function success(res) {
                    if (res.data.status) {
                        $scope.closeRightSideBarFeedback();
                        $scope.closeRightSideBarAddGroupClass();
                        $scope.showAlert(2, "Feedback saved");
                    } else {
                        $scope.showAlert(1, res.data.message);
                    }
                }, function error(response) {
                    $scope.showAlert(1, response);
                });
            }

            $scope.showPlayerProfile = function (player_id) {
                $http({
                    method: 'post',
                    url: apiUrl + 'getPlayerInfo',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    data: $.param({
                        playerId: player_id
                    }),
                }).then(function success(res) {
                    if (res.data.status) {
                        $("#playerProfileOverlay").removeClass("hide");
                        $("#playerProfileMainDiv").removeClass("hide");
                        $rootScope.playerInfo = res.data.data[0];
                        $rootScope.picUrl = uploadUrl;
                        $rootScope.reviewLimit = 2;
                        var totalRating = 0;
                        for (var i = 0; i < (res.data.data[0].Review.length); i++) {
                            totalRating += parseFloat(res.data.data[0].Review[i].rate);
                        }
                        $rootScope.avgRating = totalRating / res.data.data[0].Review.length;
                        $rootScope.totalPlayer = res.data.data[0].Review.length;
                        $timeout(function () {
                            $(".bx-wrapper").remove();
                            if (slider == '') {
                                if ($rootScope.playerInfo.Badge.length > 0) {
                                    slider = $('.slider1').bxSlider({
                                        slideWidth: 200,
                                        minSlides: 2,
                                        maxSlides: 3,
                                        slideMargin: 10
                                    });
                                }

                            } else {
                                slider.destroySlider();
                                if ($rootScope.playerInfo.Badge.length > 0) {
                                    slider = $('.slider1').bxSlider({
                                        slideWidth: 200,
                                        minSlides: 2,
                                        maxSlides: 3,
                                        slideMargin: 10
                                    });
                                }
                            }
                        }, 1000);
                        $rootScope.player_id = player_id;
                        $scope.showUpcomingPlay();
                    } else {
                        $scope.showAlert(1, res.data.message);
                    }
                }, function error(response) {
                    $scope.showAlert(1, response);
                });
            }

            $scope.showMoreReviews = function () {
                $rootScope.reviewLimit += 2;
                $timeout(function () {
                    if ($rootScope.reviewLimit >= $rootScope.playerInfo.Review.length) {
                        $timeout(function () {
                            $("#loadMoreReviews").hide();
                        }, 100);
                    }
                }, 100);
            }

            $scope.closePlayerProfile = function () {
                $("#playerProfileOverlay").addClass("hide");
                $("#playerProfileMainDiv").addClass("hide");
            }

            $scope.showPlayerProfileTab = function (id) {
                $("#" + id).show();
                $("#" + id).addClass("active");
                if (id == '1a') {
                    $("#2a, #3a, #4a").hide();
                    $("#2a, #3a, #4a").removeClass("active");
                } else if (id == '2a') {
                    $("#1a, #3a, #4a").hide();
                    $("#1a, #3a, #4a").removeClass("active");
                } else if (id == '3a') {
                    $("#1a, #2a, #4a").hide();
                    $("#1a, #2a, #4a").removeClass("active");
                } else if (id == '4a') {
                    $("#1a, #2a, #3a").hide();
                    $("#1a, #2a, #3a").removeClass("active");
                }
            }

            $scope.showUpcomingPlay = function () {
                $("#upcomingPlay").addClass("active");
                $("#pastPlay").removeClass("active");
                $scope.contentLoadingPlayerProfile(1, $rootScope.player_id);
            }

            $scope.showPastPlay = function () {
                $("#upcomingPlay").removeClass("active");
                $("#pastPlay").addClass("active");
                $scope.contentLoadingPlayerProfile(2, $rootScope.player_id);
            }

        })
