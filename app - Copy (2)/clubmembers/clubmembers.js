'use strict';

angular.module('myApp.clubmembers', ['ngRoute'])

    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/clubmembers', {
            templateUrl: 'clubmembers/clubmembers.html',
            controller: 'ClubmembersCtrl'
        });
    }])

    .controller('ClubmembersCtrl', function ($scope, $http, $timeout, $compile, $rootScope, $filter) {
            console.log('ClubMembers Loading...');
            if ($rootScope.searchData != undefined) {
                var searchData = $rootScope.searchData;
            } else {
                var searchData = '';
            }
			var table;
			var k = 0;
			$scope.picUrl = uploadUrl;
			$scope.search = {};
			$scope.emailFormat = /^[a-z]+[a-z0-9._]+@[a-z]+\.[a-z.]{2,5}$/;
            $scope.$on('$viewContentLoaded', function () {
				$http({
                    method: 'post',
                    url: apiUrl + 'getAllCountries',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                }).then(function success(res) {
                    if (res.data.status) {
                        $scope.countries = res.data.data;
                    } else {
                        $scope.countries = [];
                        $scope.showAlert(1, res.data.message);
                    }
                }, function error(response) {
                    $scope.showAlert(1, response);
                });				
                $scope.contentLoading(1);
            });
			
			$scope.contentLoading = function (type) {
				var search_by = $('#search_by').val();
				var search_by_text = $('#search_by_text').val();
            k++;
            if (k > 1) {
                table.destroy();
            }
            table = $('#example').DataTable({
                "autoWidth": false,
                "searching": false,
                "ajax": {
                    "destroy": true,
                    "url": apiUrl + 'clubmembers',
                    "type": 'post',
                    "headers": {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    "data": {
                        "search_by":search_by,
						"search_by_text":search_by_text,
						"club_id": localStorage.getItem("club_id"),
						"type": type
                    },
                    "dataSrc": function (json) {
                        console.log(json);
                        var fData = [];
                        if (json.status) {
                            $.each(json.data, function (index, val) {
                                console.log(val);
                                var name = val.name;
									var alias = val.alias;
									var email = val.email;
									var contact = val.contact;
									var rating = val.rating;
									var last_played = val.last_played;
									var app_id = val.app_id;
									var clud_id = val.clud_id;	
									var details = "<a href='javascript:' ng-click='editClubMemberUser(" + val.id + ")'><img src='images/edit.png' style='width: 15%;'></a> &nbsp;&nbsp;&nbsp; <a href='javascript:' ng-click='showMemberProfile(" + val.user_id + ")'><img src='images/ahed-arrow.png'/></a>";
									var res = {
										name: name,
										alias:alias,
										email: email,
										contact: contact,
										rating: rating,
										last_played: last_played,
										app_id: app_id,
										clud_id:clud_id,
										details:details
									}
                                fData.push(res);
                            });
                        }


                        return fData;
                        
                    }
                }, "fnDrawCallback": function () {
                    var html = '<img src="images/left-arrow.png"/>';
                    $("#example_previous").html(html);
                    var html = '<img src="images/right-arrow.png"/>';
                    $("#example_next").html(html);
                    $("#example_filter label input").attr("placeholder"," Type here to search");
                    $compile($("#example"))($scope);
                    $compile($("#example_info"))($scope);
                    $compile($("#example_previous"))($scope);
                    $compile($("#example_next"))($scope);
                },
                "columns": [
                    { "data": "name" },
					{ "data": "alias" },
					{ "data": "email" },
					{ "data": "contact" },
					{ "data": "rating" },
					{ "data": "last_played" },
					{ "data": "app_id" },
					{ "data": "clud_id" },
					{ "data": "details" }
                ],
				"columnDefs": [{
					   "targets": [8],
					   "orderable": false
					 }],
                "pageLength": 5
            });

        }
					
			$scope.selectedBookerInfo = function (item) {
                $http({
                    method: 'post',
                    url: apiUrl + 'getPlayerInfo',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    data: $.param({
                        playerId: item.originalObject.id
                    }),
                }).then(function success(res) {
                    if (res.data.status) {
                        console.log(res.data.data[0])	
						$("#playerBio").val(res.data.data[0].Player.bio);
						$("#playerAddGender").val(res.data.data[0].Player.gender);
						$("#playerAddDob").datepicker({
									defaultDate:res.data.data[0].Player.dob,
									autoclose: true,
                        			endDate: new Date()
						});
						$("#playerAddDob").val(formatDate(res.data.data[0].Player.dob)).attr('readonly','readonly');
						$("#playerAddLocationCity").val(res.data.data[0].Player.city).attr('readonly','readonly');
						$("#playerAddLocationCountry").val(res.data.data[0].Player.country_id).attr('readonly','readonly');
                        $("#playerAddFirstName").val(res.data.data[0].Player.first_name).attr('readonly','readonly');
                        $("#playerAddLastName").val(res.data.data[0].Player.last_name).attr('readonly','readonly');
						$("#playerAddDisplayName").val(res.data.data[0].Player.display_name).attr('readonly','readonly');
                        $("#playerAddContactNo").val(res.data.data[0].Player.phone).attr('readonly','readonly');
                        $("#playerAddEmail").val(res.data.data[0].User.email).attr('readonly','readonly');
                        
                    } else {
                        $scope.showAlert(1, "Can not found information");
                    }
                }, function error(response) {
                    // do nothing
                });



            }
			
			$scope.closeRightSideBarEditForm = function () {
                $("#rightSideBarEditForm").hide();
            }
			
			$scope.addClubMemberUser = function () {
				var playerBio = $('#playerBio').val();
				var playerAddLocationCity = $('#playerAddLocationCity').val();
				var playerAddLocationCountry = $('#playerAddLocationCountry').val();
				var playerAddFirstName = $('#playerAddFirstName').val();
				var playerAddLastName = $('#playerAddLastName').val();
				var playerAddDisplayName = $('#playerAddDisplayName').val();
				var playerAddContactNo = $('#playerAddContactNo').val();
				var playerAddEmail = $('#playerAddEmail').val();
				var playerAddGender = $('#playerAddGender').val();
				var playerAddDob = $('#playerAddDob').val();
				var errorValdt = 0;
				if($.trim(playerAddFirstName) == ''){
					$scope.showAlert(1, "Enter first name.");
					errorValdt = 1;
				} else if($.trim(playerAddLastName) == ''){
					$scope.showAlert(1, "Enter last name.");
					errorValdt = 1;
				} else if($.trim(playerAddEmail) == '' || !isEmail(playerAddEmail)){
					$scope.showAlert(1, "Enter valid email.");
					errorValdt = 1;
				} else if($.trim(playerAddContactNo) == ''){
					$scope.showAlert(1, "Enter contact no.");
					errorValdt = 1;
				} else if($.trim(playerAddLocationCity) == ''){
					$scope.showAlert(1, "Enter city.");
					errorValdt = 1;
				} else if($.trim(playerAddLocationCountry) == ''){
					$scope.showAlert(1, "Select Country.");
					errorValdt = 1;
				} else if($.trim(playerAddGender) == ''){
					$scope.showAlert(1, "Select Gender.");
					errorValdt = 1;
				} else if($.trim(playerAddDob) == ''){
					$scope.showAlert(1, "Select DOB.");
					errorValdt = 1;
				} else if(playerBio.length > 500){
					$scope.showAlert(1, "Bio info not more 500 cars.");
					errorValdt = 1;
				}
				if(errorValdt == 0){
                $http({
                    method: 'post',
                    url: apiUrl + 'addPlayerToClub',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    data: $.param({
                        playerBio : playerBio,
						playerAddLocationCity : playerAddLocationCity,
						playerAddLocationCountry : playerAddLocationCountry,
						playerAddFirstName : playerAddFirstName,
						playerAddLastName : playerAddLastName,
						playerAddDisplayName : playerAddDisplayName,
						playerAddContactNo : playerAddContactNo,
						playerAddEmail : playerAddEmail,
						playerAddGender : playerAddGender,
						playerAddDob : playerAddDob,
						club_id: localStorage.getItem('club_id'),
                    }),
                }).then(function success(res) {
                    if (res.data.status) {
                        $scope.showAlert(2, "Club member added successfully");
                    } else {
                        $scope.showAlert(1, res.data.message);
                    }
                }, function error(response) {
                    $scope.showAlert(1, JSON.stringify(response));
                });
				}
            }
			
			$scope.editClubMemberUserForm = function () {
				var userIDMember = $('#userIDMember').val();
				var playerBio = $('#playerBio').val();
				var playerAddLocationCity = $('#playerEditLocationCity').val();
				var playerAddLocationCountry = $('#playerEditLocationCountry').val();
				var playerAddFirstName = $('#playerEditFirstName').val();
				var playerAddLastName = $('#playerEditLastName').val();
				var playerAddDisplayName = $('#playerEditDisplayName').val();
				var playerAddContactNo = $('#playerEditContactNo').val();
				var playerAddGender = $('#playerEditGender').val();
				var playerAddDob = $('#playerEditDob').val();
				var errorValdt = 0;
				if($.trim(playerAddFirstName) == ''){
					$scope.showAlert(1, "Enter first name.");
					errorValdt = 1;
				} else if($.trim(playerAddLastName) == ''){
					$scope.showAlert(1, "Enter last name.");
					errorValdt = 1;
				} else if($.trim(playerAddContactNo) == ''){
					$scope.showAlert(1, "Enter contact no.");
					errorValdt = 1;
				} else if($.trim(playerAddLocationCity) == ''){
					$scope.showAlert(1, "Enter city.");
					errorValdt = 1;
				} else if($.trim(playerAddLocationCountry) == ''){
					$scope.showAlert(1, "Select Country.");
					errorValdt = 1;
				} else if($.trim(playerAddGender) == ''){
					$scope.showAlert(1, "Select Gender.");
					errorValdt = 1;
				} else if($.trim(playerAddDob) == ''){
					$scope.showAlert(1, "Select DOB.");
					errorValdt = 1;
				} else if(playerBio.length > 500){
					$scope.showAlert(1, "Bio info not more 500 cars.");
					errorValdt = 1;
				}
				if(errorValdt == 0){
                $http({
                    method: 'post',
                    url: apiUrl + 'updatePlayerToClub',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    data: $.param({
						userIDMember : userIDMember,
                        playerBio : playerBio,
						playerAddLocationCity : playerAddLocationCity,
						playerAddLocationCountry : playerAddLocationCountry,
						playerAddFirstName : playerAddFirstName,
						playerAddLastName : playerAddLastName,
						playerAddDisplayName : playerAddDisplayName,
						playerAddContactNo : playerAddContactNo,
						playerAddGender : playerAddGender,
						playerAddDob : playerAddDob,
						club_id: localStorage.getItem('club_id'),
                    }),
                }).then(function success(res) {
                    if (res.data.status) {
                        $scope.showAlert(2, "Club member update successfully");
                    } else {
                        $scope.showAlert(1, res.data.message);
                    }
					$('#rightSideBarEditForm').hide();
                }, function error(response) {
                    $scope.showAlert(1, JSON.stringify(response));
                });
				}
            }
			
			$scope.seachMenbersClub = function(type){
				var search_by = $('#search_by').val();
				var search_by_text = $('#search_by_text').val();
            k++;
            if (k > 1) {
                table.destroy();
            }
            table = $('#example').DataTable({
                "autoWidth": false,
                "searching": false,
                "ajax": {
                    "destroy": true,
                    "url": apiUrl + 'clubmembers',
                    "type": 'post',
                    "headers": {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    "data": {
                        "search_by":search_by,
						"search_by_text":search_by_text,
						"club_id": localStorage.getItem("club_id"),
						"type": type
                    },
                    "dataSrc": function (json) {
                        console.log(json);
                        var fData = [];
                        if (json.status) {
                            $.each(json.data, function (index, val) {
                                console.log(val);
                                var name = val.name;
									var alias = val.alias;
									var email = val.email;
									var contact = val.contact;
									var rating = val.rating;
									var last_played = val.last_played;
									var app_id = val.app_id;
									var clud_id = val.clud_id;	
									var details = "<a href='javascript:' ng-click='editClubMemberUser(" + val.id + ")'><img src='images/edit.png' style='width: 15%;'></a> &nbsp;&nbsp;&nbsp; <a href='javascript:' ng-click='showMemberProfile(" + val.user_id + ")'><img src='images/ahed-arrow.png'/></a>";
									var res = {
										name: name,
										alias:alias,
										email: email,
										contact: contact,
										rating: rating,
										last_played: last_played,
										app_id: app_id,
										clud_id:clud_id,
										details:details
									}
                                fData.push(res);
                            });
                        }


                        return fData;
                        
                    }
                }, "fnDrawCallback": function () {
                    var html = '<img src="images/left-arrow.png"/>';
                    $("#example_previous").html(html);
                    var html = '<img src="images/right-arrow.png"/>';
                    $("#example_next").html(html);
                    $("#example_filter label input").attr("placeholder"," Type here to search");
                    $compile($("#example"))($scope);
                    $compile($("#example_info"))($scope);
                    $compile($("#example_previous"))($scope);
                    $compile($("#example_next"))($scope);
                },
                "columns": [
                    { "data": "name" },
					{ "data": "alias" },
					{ "data": "email" },
					{ "data": "contact" },
					{ "data": "rating" },
					{ "data": "last_played" },
					{ "data": "app_id" },
					{ "data": "clud_id" },
					{ "data": "details" }
                ],
				"columnDefs": [{
					   "targets": [8],
					   "orderable": false
					 }],
                "pageLength": 5
            });

        }
			
			$scope.editClubMemberUser = function(memberId){
				$('.right-bar.right-side-bar').hide();
				$scope.userIDMember = memberId;
				
				$http({
					method: 'post',
					url: apiUrl + 'getclubmemberprofile',
					headers: {
						'Content-Type': 'application/x-www-form-urlencoded'
					},
					data: $.param({
						memberId: memberId
					}),
				}).then(function success(res) {					
					if (res.data.status) {
						$scope.playerEditFirstName = res.data.data.Player.first_name;
						$scope.playerEditLastName = res.data.data.Player.last_name;
						$scope.playerEditDisplayName = res.data.data.Player.display_name;
						$scope.playerEditContactNo = res.data.data.Player.phone;
						$scope.playerEditEmail = res.data.data.User.email;
						if(res.data.data.Player.dob == ''){
							$scope.end = res.data.data.Player.dob;
						}
						else{
							var un_dob = new Date(res.data.data.Player.dob);
							var d = un_dob.getDate();
							var m =  un_dob.getMonth();
							m += 1;  // JavaScript months are 0-11
							var y = un_dob.getFullYear();
							$scope.end = m + "/" + d + "/" + y;
						}
						$('#playerEditLocationCountry').val(res.data.data.Player.country_id);
						$('#playerEditGender').val(res.data.data.Player.gender);
						$scope.playerEditLocationCity = res.data.data.Player.city;
						$scope.playerBio = res.data.data.Player.bio;
						
						$('#rightSideBarEditForm').show();
					} 
					else {
						$scope.showAlert(1, res.data.message);
					}
				}, function error(response) {
					$scope.showAlert(1, JSON.stringify(response));
				});
			}
			
			$scope.showMemberProfile = function (id) {
				$scope.memberId = id;
				$http({
					method: 'post',
					url: apiUrl + 'memberprofile',
					headers: {
						'Content-Type': 'application/x-www-form-urlencoded'
					},
					data: $.param({
						memberId: id
					}),
				}).then(function success(res) {					
					if (res.data.status) {
						$scope.userprofileImg = uploadUrl+res.data.data.Player.profile_picture;
						$scope.user_first_name = res.data.data.Player.first_name;
						$scope.user_dob = res.data.data.Player.dob;
						$scope.user_email = res.data.data.User.email;
						$scope.user_contact = res.data.data.Player.phone;
						$scope.user_address = res.data.data.Player.address_1+', '+res.data.data.Player.address_2;
						$scope.user_postcode = res.data.data.Player.postal_code;
						$scope.user_city = res.data.data.Player.city;
						$scope.user_country = res.data.data.Player.country;
						
						$('#myModal').modal();
					} 
					else {
						$scope.showAlert(1, res.data.message);
					}
				}, function error(response) {
					$scope.showAlert(1, JSON.stringify(response));
				});
	
	
			}
        })
	
	var isEmail = function (email) {
	  var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
	  return regex.test(email);
	}
	var formatDate = function (date) {
		var d = new Date(date),
			month = '' + (d.getMonth() + 1),
			day = '' + d.getDate(),
			year = d.getFullYear();
	
		if (month.length < 2) month = '0' + month;
		if (day.length < 2) day = '0' + day;
	
		return [day, month, year].join('/');
	}