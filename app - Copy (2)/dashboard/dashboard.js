'use strict';

angular.module('myApp.dashboard', ['ngRoute'])

        .config(['$routeProvider', function ($routeProvider) {
                $routeProvider.when('/dashboard', {
                    templateUrl: 'dashboard/dashboard.html',
                    controller: 'DashboardCtrl'
                });
            }])

        .controller('DashboardCtrl', function ($scope, $http, $filter, uiCalendarConfig, $timeout, $rootScope, $compile) {
            $(".background-div").css("height", $(window).height());


            $scope.eventSources = [];
            $scope.allCoaches = [];

            $scope.teamNumber = 0;

            $scope.clubMinTime = '';
            $scope.clubMaxTime = '';
            $scope.picUrl = uploadUrl;
            if (localStorage.getItem('role_id') == 3) {
                $scope.club_id = localStorage.getItem('club_id');
            } else if (localStorage.getItem('role_id') == 2) {
                $scope.coach_id = localStorage.getItem('coach_id');
            }


            $scope.payment = {};
            var viewName = '';
            var viewDate = '';
            var kendoCurrent = '';
            var kendoView = '';




            $scope.$on('$viewContentLoaded', function () {
                if (localStorage.getItem('role_id') == 3) {
                    $http({
                        method: 'post',
                        url: apiUrl + 'clubProfile',
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded'
                        },
                        data: $.param({
                            id: localStorage.getItem('club_id'),
                        }),
                    }).then(function success(res) {
                        res = res.data;
                        console.log(res);
                        $scope.courts = res.data.Court;
                        $scope.allCoaches = res.data.Coach;
                        $scope.clubMinTime = res.data.ClubOperatingHour[0].start_time;
                        $scope.clubMaxTime = res.data.ClubOperatingHour[0].end_time;
                        console.log($scope.clubMinTime);
                        $scope.$broadcast("setCalendar", {id: localStorage.getItem('club_id'), court_id: '', type: 1, class_type_id: ''});
                    }, function error(response) {
                        // do nothing
                    });
                } else if (localStorage.getItem('role_id') == 2) {
                    $http({
                        method: 'post',
                        url: apiUrl + 'getAllCourtsByCoachId',
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded'
                        },
                        data: $.param({
                            id: localStorage.getItem('coach_id'),
                        }),
                    }).then(function success(res) {
                        res = res.data;
                        console.log(res);
                        $scope.courts = res.data;
                        $scope.clubMinTime = "01:00:00";
                        $scope.clubMaxTime = "23:59:59";
                        $scope.$broadcast("setCalendar", {id: localStorage.getItem('coach_id'), court_id: '', type: 1, class_type_id: ''});
                    }, function error(response) {
                        // do nothing
                    });
                }

                $scope.role_id = localStorage.getItem('role_id');
                var $form = $('#payment-form');

            });

            $scope.setCalendarFlow = function (club_id, court_id, type) {
                var class_type_id = '';
                if (localStorage.getItem('role_id') == 3) {
                    var coach_id = $("#coachId").val();
                    $scope.$broadcast("setCalendar", {id: club_id, court_id: court_id, type: type, coach_id: coach_id, class_type_id: class_type_id});
                } else if (localStorage.getItem('role_id') == 2) {
                    $scope.$broadcast("setCalendar", {id: club_id, court_id: court_id, type: type, class_type_id: class_type_id});
                }
            }

            $scope.setCalendarFlowByCoach = function (club_id, coach_id, type) {
                var court_id = $("#courtId").val();
                var class_type_id = '';
                $scope.$broadcast("setCalendar", {id: club_id, court_id: court_id, type: type, coach_id: coach_id, class_type_id: class_type_id});
            }

            $scope.setCalendarFlowByClassType = function (id, ev, club_id, type) {
                if (localStorage.getItem('role_id') == 3) {
                    var coach_id = $("#coachId").val();
                    var court_id = $("#courtId").val();
                    var class_type_id = id;
                    if ($(ev.target).hasClass("calendarfiltertype")) {
                        $(ev.target).removeClass("calendarfiltertype").addClass("calendarfiltertypedisabled");
                        $scope.$broadcast("setCalendar", {id: club_id, court_id: court_id, type: type, coach_id: coach_id, class_type_id: class_type_id});
                    } else {
                        $(ev.target).removeClass("calendarfiltertypedisabled").addClass("calendarfiltertype");
                        $scope.$broadcast("setCalendar", {id: club_id, court_id: court_id, type: type, coach_id: coach_id, class_type_id: ''});
                    }
                } else if (localStorage.getItem('role_id') == 2) {
                    var court_id = $("#courtId").val();
                    var class_type_id = id;
                    if ($(ev.target).hasClass("calendarfiltertype")) {
                        $(ev.target).removeClass("calendarfiltertype").addClass("calendarfiltertypedisabled");
                        $scope.$broadcast("setCalendar", {id: club_id, court_id: court_id, type: type, class_type_id: class_type_id});
                    } else {
                        $(ev.target).removeClass("calendarfiltertypedisabled").addClass("calendarfiltertype");
                        $scope.$broadcast("setCalendar", {id: club_id, court_id: court_id, type: type, class_type_id: ''});
                    }
                }

            }


            $scope.showCurrentDayView = function () {
                var value = new Date();

                calendar.navigate(moment(value), "month");
                $timeout(function () {
                    uiCalendarConfig.calendars['myCalendar'].fullCalendar('changeView', 'agendaDay');
                    uiCalendarConfig.calendars['myCalendar'].fullCalendar('gotoDate', moment(value));
                }, 500);
            }

            $scope.$on('setCalendar', function (ev, args) {
                var id = args.id;
                var court_id = args.court_id;
                var coach_id = args.coach_id;
                var class_type_id = args.class_type_id;
                var type = args.type;
                //alert(id+":"+court_id)
                if (type == 2) {
                    var view = uiCalendarConfig.calendars['myCalendar'].fullCalendar('getView');
                    var date = uiCalendarConfig.calendars['myCalendar'].fullCalendar('getDate');
                    //var date = $("#calendar").fullCalendar('getDate');
                    //var month_int = date.getMonth();
                    console.log(moment(date).format('MM'));
                    console.log(view.name);
                    viewName = view.name;
                    viewDate = moment(date);
                    kendoCurrent = calendar.current();
                    kendoView = calendar.view();
                    console.log(kendoCurrent);
                    console.log(kendoView);
                    calendar.destroy();
                    $("#kendoCalendar").empty();
                }
                if (localStorage.getItem("role_id") == 3) {
                    var params = $.param({
                        id: id,
                        court_id: court_id,
                        coach_id: coach_id,
                        class_type_id: class_type_id,
                        role_id: 3
                    })
                } else if (localStorage.getItem("role_id") == 2) {
                    var params = $.param({
                        id: id,
                        court_id: court_id,
                        coach_id: coach_id,
                        class_type_id: class_type_id,
                        role_id: 2
                    })
                }
                $http({
                    method: 'post',
                    url: apiUrl + 'clubProfile',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    data: params,
                }).then(function success(res) {
                    console.log(res)
                    res = res.data;
                    var scheduleList = [];
                    if (res.status) {
                        var resString = JSON.stringify(res.data);
                        resString = resString.replace(/book_start_time/g, 'start');
                        resString = resString.replace(/book_end_time/g, 'end');
                        var schedules = JSON.parse(resString);

                        $.each(schedules, function (index, obj) {
                            var schedule = {};
                            var date = obj.ClubBooking.book_date;
                            schedule.start = date + 'T' + obj.ClubBooking.start;
                            schedule.end = date + 'T' + obj.ClubBooking.end;
                            schedule.type = obj.ClassType.name;
                            schedule.coachId = obj.Coach.id;
                            schedule.coachName = obj.Coach.name;
                            schedule.title = schedule.type + '\n' + schedule.coachName;
                            schedule.id = obj.ClubBooking.id;
                            schedule.data = obj;
                            if (obj.ClassType.id < 6) {
                                schedule.textColor = '#FFFFFF';
                                //schedule.rendering = 'background';
                                schedule.backgroundColor = '#7DB9CB';
                            } else if (obj.ClassType.id == 6) {
                                schedule.textColor = '#FFFFFF';
                                schedule.backgroundColor = '#FF994B';
                            }
                            scheduleList.push(schedule);
                        });
                        console.log(scheduleList)
                        $scope.uiConfig = {
                            calendar: {
                                editable: true,
                                selectable: true,
                                selectHelper: true,
                                allDaySlot: false,
                                slotDuration: '00:15:00',
                                eventBorderColor: 'transparent',
                                eventBackgroundColor: 'transparent',
                                minTime: $scope.clubMinTime,
                                maxTime: $scope.clubMaxTime,
                                eventOverlap: function (stillEvent, movingEvent) {
                                    return false;
                                },
                                header: {
                                    left: 'title,prev,next',
                                    center: '',
                                    right: 'month agendaWeek agendaDay'
                                },
                                events: scheduleList,
                                aspectRatio: 2,
                                dayClick: function (date, jsEvent, view) {
                                    //alert('Clicked on: ' + date.format());
                                    //alert('Coordinates: ' + jsEvent.pageX + ',' + jsEvent.pageY);
                                    //alert('Current view: ' + view.name);
                                    var events = uiCalendarConfig.calendars['myCalendar'].fullCalendar('clientEvents');

                                    //console.log(events)
                                    var currentEvent = false;

                                    for (var i = 0; i < events.length; i++) {
                                        //console.log(date.format());
                                        //console.log(events[i].start.format("YYYY-MM-DD"));
                                        if (date.format() == events[i].start.format("YYYY-MM-DD")) {
                                            currentEvent = true;
                                            break;
                                        }
                                    }
                                    //$scope.closeAllForms('rightSideBarAddForm');
                                    showForm(1);
                                    //$("#rightSideBarAddForm").animate({ width: 'toggle' }, 350);
                                    $("#bookAddDate").val(date.format());
                                    //$(".app-content-body").addClass("sideBarOn");
                                    //$(".button-bottom").addClass("sideBarOn");
                                },
                                viewRender: function (view, element) {
                                    console.log(view);
                                    console.log(element);
                                    console.log(calendar);
                                    console.log($("#kendoCalendar"))
                                    if (view.name == 'month') {
                                        var currentDate = uiCalendarConfig.calendars['myCalendar'].fullCalendar('getDate');
                                        //alert(new Date(currentDate)+" = "+calendar.current())
                                        //alert(moment(calendar.current()).format("MM")+" = "+moment(currentDate).format("MM"))
                                        if (moment(calendar.current()).format("MM") != moment(currentDate).format("MM")) {

                                            calendar.navigate(currentDate, view.name);
                                        }

                                    }
                                    $("#cdayview").remove();
                                    var html = '<img id="cdayview" ng-click="showCurrentDayView()" class="inactive-image" src="images/calander1.png" style="width:20px;height:20px" />';
                                    $(".fc-right").append(html);
                                    $compile($(".fc-right"))($scope);
                                    if (view.name == 'agendaWeek') {
                                        /*var dayName = moment(currentDate).format("ddd").toLowerCase();
                                         $(".fc-" + dayName).css("text-decoration", "underline");
                                         $(".fc-" + dayName).css("font-weight", "bold");*/
                                    }
                                    
                                    if (view.name == 'agendaDay') {
                                        console.log(event);
                                    }
                                },
                                eventClick: function (calEvent, jsEvent, view) {

                                    $scope.closeAllForms('rightSideBar');

                                    $scope.getEventDetails(calEvent.id);
                                },
                                eventRender: function (event, element, view) {
                                    if (view.name == 'agendaWeek') {
                                        console.log(event);
                                        console.log(element);
                                        //$('.fc-col' + event.start.getDay()).not('.fc-widget-header').css('background-color', 'blue');
                                    }
                                    
                                    if (view.name == 'agendaDay') {
                                        console.log(event);
                                    }
                                }
                            }
                        };
                        // kendo ui calendar
                        var eventDates = [];
                        $.each(scheduleList, function (index, obj) {
                            var cdate = new Date(obj.start);
                            cdate.setTime(cdate.getTime() + (cdate.getTimezoneOffset() * 60 * 1000));
                            console.log(cdate)
                            var fdate = +new Date(cdate.getFullYear(), cdate.getMonth(), cdate.getDate());
                            console.log(fdate)
                            eventDates.push(fdate);
                        });
                        console.log(eventDates);
                        $("#kendoCalendar").kendoCalendar({
                            value: new Date(),
                            dates: eventDates,
                            weekNumber: false,
                            month: {
                                // template for dates in month view
                                content: '# if ($.inArray(+data.date, data.dates) != -1) { #' +
                                        '<div class="exhibition"' +
                                        '">#= data.value #</div>' +
                                        '# } else { #' +
                                        '#= data.value #' +
                                        '# } #'
                            },
                            footer: false,
                            change: function () {
                                var value = this.value();
                                console.log(moment(value));
                                uiCalendarConfig.calendars['myCalendar'].fullCalendar('changeView', 'agendaDay');
                                uiCalendarConfig.calendars['myCalendar'].fullCalendar('gotoDate', moment(value));
                                var cdate = new Date();
                                cdate = moment(cdate).format("YYYY-MM-DD");
                                value = moment(value).format("YYYY-MM-DD");
                                if (cdate != value) {
                                    $("#cdayview").removeClass("inactive-image");
                                    $("#cdayview").attr("ng-click", "showCurrentDayView()");
                                    $compile($(".fc-right"))($scope);
                                } else {
                                    $("#cdayview").addClass("inactive-image");
                                    $("#cdayview").removeAttr("onClick");
                                }
                            },
                            navigate: function () {
                                var view = this.view();
                                console.log(view.name); //name of the current view

                                var current = this.current();
                                console.log(current); //currently focused date
                                uiCalendarConfig.calendars['myCalendar'].fullCalendar('changeView', view.name);
                                uiCalendarConfig.calendars['myCalendar'].fullCalendar('gotoDate', moment(current));
                                $("#cdayview").removeClass("inactive-image");
                            }
                        });
                        calendar = $("#kendoCalendar").data("kendoCalendar");
                        if (viewName != '') {
                            $timeout(function () {
                                calendar.navigate(kendoCurrent, kendoView.name);
                                $timeout(function () {
                                    uiCalendarConfig.calendars['myCalendar'].fullCalendar('changeView', viewName);
                                    uiCalendarConfig.calendars['myCalendar'].fullCalendar('gotoDate', moment(viewDate).format("YYYY-MM-DD"));
                                }, 500);
                            }, 500);
                        }
                    }
                }, function error(response) {
                    // do nothing
                });
            });


            $scope.editRightSideBarDetails = function (bookingId) {
                $http({
                    method: 'post',
                    url: apiUrl + 'getEventDetails',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    data: $.param({
                        booking_id: bookingId
                    }),
                }).then(function success(res) {
                    res = res.data;
                    $scope.closeAllForms('rightSideBarDetails');
                    $scope.openCoachBox();
                    $scope.playerFirstName = res.data.ClubBooking.booked_first_name;
                    $scope.playerLastName = res.data.ClubBooking.booked_last_name;
                    $scope.playerContactNo = res.data.ClubBooking.booked_contact;
                    $scope.playerEmail = res.data.ClubBooking.booked_email;
                    $scope.teamNumber = res.data.ClubBooking.team_number;
                    $scope.remoteUrl = apiUrl + 'getSearchPlayers/' + $scope.teamNumber + '/';
                    $scope.bookingId = bookingId;
                    $("#totalPlayer").val(res.data.Team.length);
                    $scope.teams = res.data.Team;
                    $("#eventCoach").val(res.data.ClubBooking.coach_id);
                    $("#eventSubCoach").val(res.data.ClubBooking.sub_coach_id);
                    var startTime = res.data.ClubBooking.book_start_time;
                    startTime = $scope.getHoursMinutes(startTime);
                    startTime = startTime.split(":");
                    $("#eventStartHours").val(startTime[0]);
                    if (startTime[1] == 0) {
                        startTime[1] = "00";
                    }
                    $("#eventStartMinutes").val(startTime[1]);
                    var endTime = res.data.ClubBooking.book_end_time;
                    endTime = $scope.getHoursMinutes(endTime);
                    endTime = endTime.split(":");
                    $("#eventEndHours").val(endTime[0]);
                    if (endTime[1] == 0) {
                        endTime[1] = "00";
                    }
                    $("#eventEndMinutes").val(endTime[1]);
                    $("#eventCourt").val(res.data.ClubBooking.court_id);
                    //$scope.start = res.data.ClubBooking.book_date;
                    alert(res.data.ClubBooking.book_date)
                    $("#bookDate").datepicker("update", new Date(res.data.ClubBooking.book_date));
                    //$scope.totalPlayer = res.data.Team.length;
                }, function error(response) {
                    // do nothing
                });

            }

            $scope.openBlockDate = function () {
                $scope.closeAllForms('rightSideBarBlockDate');
                $("#addBlockDate input").each(function () {
                    $(this).val('');
                });
                $("#addBlockDate select").each(function () {
                    $(this).val('');
                });
                $("#addBlockDate textarea").each(function () {
                    $(this).val('');
                });
            }

            $scope.closeRightSideBarBlockDate = function () {
                $("#rightSideBarBlockDate").hide();
            }

            $scope.addBlockDate = function () {
                $http({
                    method: 'post',
                    url: apiUrl + 'saveBlockDate',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    data: $("#addBlockDate").serialize() + "&" + $.param({
                        id: localStorage.getItem('coach_id'),
                    }),
                }).then(function success(res) {
                    if (res.data.status) {
                        $scope.closeRightSideBarBlockDate();
                        $scope.showAlert(2, "Selected date and time is blocked", "Date Blocked");
                    } else {
                        $scope.showAlert(1, res.data.message);
                    }
                }, function error(response) {
                    $scope.showAlert(1, JSON.stringify(response));
                });
            }

        });

