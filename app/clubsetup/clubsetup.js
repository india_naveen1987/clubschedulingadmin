'use strict';
angular.module('myApp.clubsetup', ['ngRoute'])

        .config(['$routeProvider', function ($routeProvider) {
                $routeProvider.when('/clubsetup', {
                    templateUrl: 'clubsetup/clubsetup.html',
                    controller: 'ClubSetupCtrl'
                });
            }])



        .controller('ClubSetupCtrl', function ($scope, $http, $timeout, $compile, $rootScope, $templateCache, $filter) {
            console.log("club setup ctrl");
            $scope.stime = [];
            $scope.surfaces = [];
            $scope.coaches = [];
            var k = 0;
            for (var i = 1; i <= 24; i++) {
                $scope.stime.push(i);
            }
            var coachingFeesTable = '';
            var facilitiesTable = '';
            $scope.$on('$viewContentLoaded', function () {
                $timeout(function () {
                    $("#clubSetupMenu li a").each(function (i, v) {
                        var cid = $(this).text();
                        console.log(cid);
                        cid = cid.replace(" ", "_");
                        $("#" + cid).hide();
                    });
                    //$("#addCourt").hide();
                    //$("#addPeakTime").hide();
                    //$("#addSpecialFees").hide();
                    //$("#addCoachFees").hide();
                    $("#General").show();
                }, 500);
                $scope.getGeneralSettings();
                $http({
                    method: 'post',
                    url: apiUrl + 'getAllCourtSurfaces',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                }).then(function success(res) {
                    if (res.data.status) {
                        $scope.surfaces = res.data.data;
                    } else {
                        $scope.showAlert(1, res.data.message);
                    }
                }, function error(response) {
                    $scope.showAlert(1, response);
                });
                $http({
                    method: 'post',
                    url: apiUrl + 'getAllCountries',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                }).then(function success(res) {
                    if (res.data.status) {
                        $scope.countries = res.data.data;
                    } else {
                        $scope.countries = [];
                        $scope.showAlert(1, res.data.message);
                    }
                }, function error(response) {
                    $scope.showAlert(1, response);
                });
                $scope.getAllCoaches();
            });
            $scope.saveGeneralSettings = function () {
                var paymentMethods = [];
                if ($("#payment_method_credit_card").is(":checked")) {
                    paymentMethods.push(1);
                }
                if ($("#payment_method_cash").is(":checked")) {
                    paymentMethods.push(2);
                }
                if ($("#payment_method_sponsored").is(":checked")) {
                    paymentMethods.push(3);
                }
                if (paymentMethods.length == 0) {
                    $scope.showAlert(1, "Please select at least one payment method to proceed.", "Error");
                } else {
                    $http({
                        method: 'post',
                        url: apiUrl + 'saveGeneralSettings',
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded'
                        },
                        data: $.param({
                            advance_booking: $("#advance_booking").val(),
                            non_member_fee: $("#non_member_fee").val(),
                            payment_method: paymentMethods
                        }),
                    }).then(function success(res) {
                        if (res.data.status) {
                            $scope.showAlert(2, "Your changes have been saved successfully.", "Changes saved");
                        } else {
                            $scope.showAlert(1, "Changes cannot be saved due to a system error. Please try again.");
                        }
                    }, function error(response) {
                        $scope.showAlert(1, "Changes cannot be saved due to a system error. Please try again.");
                    });
                }
            }

            $scope.saveFacilitiesTimings = function () {
                if ($("#apply_all").is(":checked")) {
                    var apply_all = 1;
                } else {
                    var apply_all = 2;
                }
                
                if(apply_all==2){
                    $scope.showAlert(1, "Please check apply to all courts");
                    //$scope.$broadcast('saveConfirmFacilitiesTimings', {id: apply_all});
                }else{
                    $scope.saveDefaultTImingCounter = 0;
                    $scope.showConfirm("Are you sure to overwrite all the facilities timings?", apply_all, 'saveConfirmFacilitiesTimings', "Confirm");
                }
                
            }

            $scope.$on('saveConfirmFacilitiesTimings', function (ev, args) {
                if($scope.saveDefaultTImingCounter==0){
                    var apply_all = args.id;
                    $http({
                        method: 'post',
                        url: apiUrl + 'addUpdateCourt',
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded'
                        },
                        data: $.param({
                            is_all: apply_all,
                            start_hours: $("#opening_hours").val(),
                            start_minutes: $("#opening_minutes").val(),
                            end_hours: $("#ending_hours").val(),
                            end_minutes: $("#ending_minutes").val(),
                            club_id: localStorage.getItem("club_id")
                        }),
                    }).then(function success(res) {
                        if (res.data.status) {
                            $scope.allFacilities();
                            $scope.showAlert(2, "Your changes have been saved successfully.", "Changes saved");
                        } else {
                            $scope.showAlert(1, res.data.message);
                        }
                    }, function error(response) {
                        $scope.showAlert(1, "Changes cannot be saved due to a system error. Please try again.");
                    });
                }
                
            })

            $scope.updateCourtStatus = function (id) {
                var fStatus = 1;
                if ($("#statusFacility" + id).is(":checked")) {
                    fStatus = 1;
                } else {
                    fStatus = 2;
                }
                $http({
                    method: 'post',
                    url: apiUrl + 'addUpdateCourt',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    data: $.param({
                        court_id: id,
                        status: fStatus
                    }),
                }).then(function success(res) {
                    if (res.data.status) {
                        // no need
                    } else {
                        $scope.showAlert(1, "Changes cannot be saved due to a system error. Please try again.");
                    }
                }, function error(response) {
                    $scope.showAlert(1, "Changes cannot be saved due to a system error. Please try again.");
                });
            }

            $scope.deleteCourts = function () {
                var n = $('input:checkbox[id^="check"]:checked').length;
                if (n == 0) {
                    $scope.showAlert(1, "Please select at least one court to proceed.", "Error");
                } else {
                    $scope.showConfirm("Are you sure you want to continue?", 0, "removeCourt", "Delete Opening Hours?");
                }
            }

            $scope.selectDeselectAll = function () {
                if ($("#checkAllFacilities").is(":checked")) {
                    $(".checkselect").prop("checked", true);
                } else {
                    $(".checkselect").prop("checked", false);
                }
            }

            $scope.$on('removeCourt', function (ev, args) {
                var ids = [];
                $('input:checkbox[id^="checkFacility"]:checked').each(function () {
                    ids.push($(this).attr("id").replace("checkFacility", ""));
                });
                if (ids.length > 0) {
                    $http({
                        method: 'post',
                        url: apiUrl + 'deleteCourt',
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded'
                        },
                        data: $.param({
                            ids: ids
                        }),
                    }).then(function success(res) {
                        $scope.allFacilities();
                        $scope.showAlert(2, "Opening hours deleted successfully.", "Deleted");
                        //window.location.reload();
                    }, function error(response) {
                        $scope.showAlert(1, "Changes cannot be saved due to a system error. Please try again.");
                    });
                }
            })

            $scope.openMenuItem = function (menu, ev) {
                $("#clubSetupMenu li").removeClass("active-li");
                $(ev.target).parent("li").addClass("active-li");
                $("#clubSetupMenu li a").each(function (i, v) {
                    var cid = $(this).text();
                    cid = cid.replace(" ", "_");
                    $("#" + cid).hide();
                });
                var eid = $(ev.target).text();
                eid = eid.replace(" ", "_");
                $("#" + eid).show();
                if(eid == 'General'){
                    $scope.getGeneralSettings();
                }else if (eid == 'Facilities') {
                    $scope.allFacilities();
                } else if (eid == 'Peak_Time') {
                    $scope.getAllPeakTimings();
                } else if (eid == 'Club_Profile') {
                    $scope.getClubProfile();
                } else if (eid == 'Court_Fees') {
                    $scope.getCourtFees();
                } else if (eid == 'Special_Fees') {
                    $scope.getSpecialFees();
                } else if (eid == 'Refund_Policy') {
                    $scope.getRefundPolicy();
                } else if (eid == 'Coach_Fees') {
                    $scope.getCommonCoachFees();
                    $scope.getIndividualCoachingFees();
                }
            }

            $scope.getGeneralSettings = function(){
                $http({
                    method: 'post',
                    url: apiUrl + 'getGeneralSettings',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                }).then(function success(res) {
                    if (res.data.status) {
                        $("#advance_booking").val(res.data.data.Setting.advance_booking);
                        $("#non_member_fee").val(res.data.data.Setting.non_member_fee);
                        var paymentMethods = res.data.data.Setting.payment_method.split(",");
                        for (var i = 0; i < paymentMethods.length; i++) {
                            if (paymentMethods[i] == 1) {
                                $("#payment_method_credit_card").prop("checked", true);
                            }
                            if (paymentMethods[i] == 2) {
                                $("#payment_method_cash").prop("checked", true);
                            }
                            if (paymentMethods[i] == 3) {
                                $("#payment_method_sponsored").prop("checked", true);
                            }
                        }
                    } else {
                        //$scope.showAlert(1, res.data.message);
                    }
                }, function error(response) {
                    $scope.showAlert(1, response);
                });
            }

            $scope.getAllPeakTimings = function () {
                $http({
                    method: 'post',
                    url: apiUrl + 'getAllPeakTimesByClubId',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    data: $.param({
                        club_id: localStorage.getItem("club_id")
                    }),
                }).then(function success(res) {
                    if (res.data.status) {
                        $scope.peakTimings = res.data.data;
                        $scope.totalPeakTimes = res.data.totalPeakTimes;
                        $scope.activePeakTimes = res.data.activePeakTimes;
                    } else {
                        //$scope.showAlert(1, res.data.message);
                    }
                }, function error(response) {
                    $scope.showAlert(1, response);
                });
            }

            $scope.allFacilities = function () {
                if (facilitiesTable != '') {
                    facilitiesTable.destroy();
                }
                $timeout(function () {
                    facilitiesTable = $('#facilitiesTable').DataTable({
                        "autoWidth": false,
                        "ajax": {
                            "destroy": true,
                            "url": apiUrl + 'getAllCourtsByClubId',
                            "type": 'post',
                            "headers": {
                                'Content-Type': 'application/x-www-form-urlencoded'
                            },
                            "data": {
                                "club_id": localStorage.getItem("club_id")
                            },
                            "dataSrc": function (json) {
                                console.log(json);
                                var fData = [];
                                if (json.status) {
                                    $.each(json.data, function (index, val) {
                                        console.log(val);
                                        var checkdelete = "<input type='checkbox' id='checkFacility" + val.Court.id + "' class='checkselect'  />";
                                        var status = '<div class="tg-list-item" style="float:left"><input class="tgl tgl-ios" id="statusFacility' + val.Court.id + '" type="checkbox" ng-change="updateCourtStatus(' + val.Court.id + ')" ng-model="facilityStatus' + val.Court.id + '" /><label class="tgl-btn" for="statusFacility' + val.Court.id + '"></label></div>';
                                        $scope['facilityStatus' + val.Court.id] = val.Court.status == 1 ? true : false;
                                        var status_sort = val.Court.status;
                                        var name = val.Court.name;
                                        var id = val.Court.id;
                                        var surface = val.CourtSurface.name;
                                        var operating_hours = $filter('formatTime')(val.Court.start_time) + " - " + $filter('formatTime')(val.Court.end_time);
                                        var operating_hours_sort = val.Court.start_time;
                                        var details = " <a href='javascript:' ng-click='editCourt(" + val.Court.id + ")'><img class='ancher-img' src='images/edit.png'/></a>";
                                        var res = {
                                            checkdelete: checkdelete,
                                            status: status,
                                            status_sort: status_sort,
                                            name: name,
                                            id: id,
                                            surface: surface,
                                            operating_hours: operating_hours,
                                            operating_hours_sort: operating_hours_sort,
                                            details: details
                                        }
                                        fData.push(res);
                                    });
                                }


                                return fData;
                            }
                        }, "fnDrawCallback": function () {
                            var html = '<a href="javascript:" id="deleteCourts" ng-click="deleteCourts()"><img src="images/delete-bg-white.png"/></a>';
                            $("#facilitiesTable_info").html(html);
                            var html = '<img src="images/left-arrow.png"/>';
                            $("#facilitiesTable_previous").html(html);
                            var html = '<img src="images/right-arrow.png"/>';
                            $("#facilitiesTable_next").html(html);
                            $("#facilitiesTable_filter").remove();
                            $compile($("#facilitiesTable"))($scope);
                            $compile($("#facilitiesTable_info"))($scope);
                            $compile($("#facilitiesTable_previous"))($scope);
                            $compile($("#facilitiesTable_next"))($scope);
                        },
                        "columns": [
                            {"data": "checkdelete"},
                            {"data": {
                                    _: "status",
                                    sort: "status_sort"
                                }
                            },
                            {"data": "name"},
                            {"data": "id"},
                            {"data": "surface"},
                            {"data": {
                                    _: "operating_hours",
                                    sort: "operating_hours_sort"
                                }
                            },
                            {"data": "details"}
                        ],
                        "pageLength": 5,
                        "order": [[1, "asc"]]
                    });
                }, 500);
            }

            $scope.getIndividualCoachingFees = function () {
                if (coachingFeesTable != '') {
                    coachingFeesTable.destroy();
                }
                coachingFeesTable = $('#coachingFeesTable').DataTable({
                    "autoWidth": false,
                    "ajax": {
                        "destroy": true,
                        "url": apiUrl + 'getAllIndividualCoachingFeesByClubId',
                        "type": 'post',
                        "headers": {
                            'Content-Type': 'application/x-www-form-urlencoded'
                        },
                        "data": {
                            "club_id": localStorage.getItem("club_id")
                        },
                        "dataSrc": function (json) {
                            console.log(json);
                            var fData = [];
                            if (json.status) {
                                $.each(json.data, function (index, val) {
                                    console.log(val);
                                    var checkdelete = "<input type='checkbox' id='checkCoachingFees" + val.CoachFees.id + "' class='checkselect'  />";
                                    var coachname = val.Coach.name;
                                    var privateClassNormalFees = val.CoachFees.private_class_normal_fees;
                                    var privateClassPeakFees = val.CoachFees.private_class_peak_fees;
                                    var groupClassNormalFees = val.CoachFees.group_class_normal_fees;
                                    var groupClassPeakFees = val.CoachFees.group_class_peak_fees;
                                    var id = val.CoachFees.id;
                                    var details = " <a href='javascript:' ng-click='editCoachFees(" + val.CoachFees.id + ")'><img style='margin-right:30px' class='ancher-img' src='images/edit.png'/></a>";
                                    var res = {
                                        checkdelete: checkdelete,
                                        coachname: coachname,
                                        privateClassNormalFees: privateClassNormalFees,
                                        privateClassPeakFees: privateClassPeakFees,
                                        groupClassNormalFees: groupClassNormalFees,
                                        groupClassPeakFees: groupClassPeakFees,
                                        details: details
                                    }
                                    fData.push(res);
                                });
                            }


                            return fData;
                        }
                    }, "fnDrawCallback": function () {
                        var html = '<a href="javascript:" id="deleteCoachFees" ng-click="deleteCoachFees()"><img src="images/delete-bg-white.png"/></a>';
                        $("#coachingFeesTable_info").html(html);
                        var html = '<img src="images/left-arrow.png"/>';
                        $("#coachingFeesTable_previous").html(html);
                        var html = '<img src="images/right-arrow.png"/>';
                        $("#coachingFeesTable_next").html(html);
                        $("#coachingFeesTable_filter").remove();
                        $compile($("#coachingFeesTable"))($scope);
                        $compile($("#coachingFeesTable_info"))($scope);
                        $compile($("#coachingFeesTable_previous"))($scope);
                        $compile($("#coachingFeesTable_next"))($scope);
                    },
                    "columns": [
                        {"data": "checkdelete"},
                        {"data": "coachname"},
                        {"data": "privateClassNormalFees"},
                        {"data": "privateClassPeakFees"},
                        {"data": "groupClassNormalFees"},
                        {"data": "groupClassPeakFees"},
                        {"data": "details"}
                    ],
                    "pageLength": 5


                });
            }

            $scope.getCommonCoachFees = function () {
                $http({
                    method: 'post',
                    url: apiUrl + 'clubProfile',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    data: $.param({
                        id: localStorage.getItem("club_id")
                    }),
                }).then(function success(res) {
                    if (res.data.status) {
                        $("input[name='private_class_max_players']").val(res.data.data.Club.private_class_max_players);
                        $("input[name='private_class_normal_fees']").val(res.data.data.Club.private_class_normal_fees);
                        $("input[name='private_class_peak_fees']").val(res.data.data.Club.private_class_peak_fees);
                        $("input[name='private_class_additional_player_fees']").val(res.data.data.Club.private_class_additional_player_fees);
                        $("input[name='group_class_max_players']").val(res.data.data.Club.group_class_max_players);
                        $("input[name='group_class_normal_fees']").val(res.data.data.Club.group_class_normal_fees);
                        $("input[name='group_class_peak_fees']").val(res.data.data.Club.group_class_peak_fees);
                        $("input[name='group_class_additional_player_fees']").val(res.data.data.Club.group_class_additional_player_fees);
                    } else {
                        //$scope.showAlert(1, res.data.message);
                    }
                }, function error(response) {
                    $scope.showAlert(1, response);
                });
            }

            $scope.getRefundPolicy = function () {
                $http({
                    method: 'post',
                    url: apiUrl + 'getRefundPolicy',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    data: $.param({
                        club_id: localStorage.getItem("club_id")
                    }),
                }).then(function success(res) {
                    if (res.data.status) {
                        $.each(res.data.data, function (i, v) {
                            console.log(v);
                            $("#tier" + (v.RefundPolicy.tier_level)).prop("checked", true);
                            $scope.enableTier(v.RefundPolicy.tier_level);
                            $("#days" + (v.RefundPolicy.tier_level)).val(v.RefundPolicy.days);
                            $("#charge" + (v.RefundPolicy.tier_level)).val(v.RefundPolicy.charge);
                        });
                    } else {
                        //$scope.showAlert(1, res.data.message);
                    }
                }, function error(response) {
                    $scope.showAlert(1, response);
                });
            }

            $scope.getSpecialFees = function () {
                $http({
                    method: 'post',
                    url: apiUrl + 'getAllSpecialFeesByClubId',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    data: $.param({
                        club_id: localStorage.getItem("club_id")
                    }),
                }).then(function success(res) {
                    if (res.data.status) {
                        $scope.specialFees = res.data.data;
                    } else {
                        //$scope.showAlert(1, res.data.message);
                    }
                }, function error(response) {
                    $scope.showAlert(1, response);
                });
            }

            $scope.getClubProfile = function () {
                $http({
                    method: 'post',
                    url: apiUrl + 'clubProfile',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    data: $.param({
                        id: localStorage.getItem("club_id")
                    }),
                }).then(function success(res) {
                    if (res.data.status) {
                        $scope.uploadUrl = uploadUrl;
                        $rootScope.clubDetails = res.data.data;
                        $("#country_id option[value='" + res.data.data.Country.id + "']").attr("selected", true);
                        if (res.data.data.Club.lat != '' && res.data.data.Club.lng != '') {
                            var position = {
                                coords: {
                                    latitude: parseFloat(res.data.data.Club.lat),
                                    longitude: parseFloat(res.data.data.Club.lng)
                                }
                            }
                            $scope.showPosition(position);
                        } else {
                            $scope.initMap();
                        }
                    } else {
                        //$scope.showAlert(1, res.data.message);
                    }
                }, function error(response) {
                    $scope.showAlert(1, response);
                });
            }

            $scope.openAddCourtForm = function () {
                $("#addCourt").show();
                $("#courtFormTitle").text("Add Court");
                $("#courtFormSubmitTitle").text("ADD COURT +");
                $("#courtIdDiv").hide();
                $("#courtId").val("");
                $("#courtStatus").prop("checked", false);
                $("#courtName").val("");
                $("#courtSurface").val("");
                $("#courtOpeningHours").val(1);
                $("#courtOpeningMinutes").val(0);
                $("#courtClosingHours").val(1);
                $("#courtClosingMinutes").val(0);
            }

            $scope.closeAddCourtForm = function () {
                $("#addCourt").hide();
            }

            $scope.addCourt = function () {
                var courtStatus = $("#courtStatus").is(":checked") ? 1 : 2;
                $http({
                    method: 'post',
                    url: apiUrl + 'addUpdateCourt',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    data: $.param({
                        status: courtStatus,
                        name: $("#courtName").val(),
                        court_surface_id: $("#courtSurface").val(),
                        start_hours: $("#courtOpeningHours").val(),
                        start_minutes: $("#courtOpeningMinutes").val(),
                        end_hours: $("#courtClosingHours").val(),
                        end_minutes: $("#courtClosingMinutes").val(),
                        court_id: $("#courtId").val(),
                        club_id: localStorage.getItem("club_id")
                    }),
                }).then(function success(res) {
                    if (res.data.status) {
                        $scope.allFacilities();
                        $scope.closeAddCourtForm();
                        $scope.showAlert(2, "Your changes have been saved successfully.", "Changes saved");
                    } else {
                        $scope.showAlert(1, res.data.message);
                    }
                }, function error(response) {
                    $scope.showAlert(1, "Changes cannot be saved due to a system error. Please try again.");
                });
            }

            $scope.editCourt = function (courtId) {
                $http({
                    method: 'post',
                    url: apiUrl + 'getCourtDetails',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    data: $.param({
                        court_id: courtId
                    }),
                }).then(function success(res) {
                    if (res.data.status) {
                        $scope.openAddCourtForm();
                        $("#courtFormTitle").text("Edit Court");
                        $("#courtFormSubmitTitle").text("SAVE CHANGES");
                        $("#courtIdDiv").show();
                        $("#courtId").val(res.data.data.Court.id);
                        if (res.data.data.Court.status == 1) {
                            $("#courtStatus").prop("checked", true);
                        } else {
                            $("#courtStatus").prop("checked", false);
                        }
                        $("#courtName").val(res.data.data.Court.name);
                        $("#courtSurface").val(res.data.data.Court.court_surface_id);
                        var openingHours = res.data.data.Court.start_time.split(":");
                        $("#courtOpeningHours").val(parseInt(openingHours[0]));
                        var openingMinutes = parseInt(openingHours[1]);
                        if (openingMinutes == 0) {
                            openingMinutes = '00';
                        }
                        $("#courtOpeningMinutes").val(openingMinutes);
                        var closingHours = res.data.data.Court.end_time.split(":");
                        $("#courtClosingHours").val(parseInt(closingHours[0]));
                        var closingMinutes = parseInt(closingHours[1]);
                        if (closingMinutes == 0) {
                            closingMinutes = '00';
                        }
                        $("#courtClosingMinutes").val(closingMinutes);
                    } else {
                        $scope.showAlert(1, "Changes cannot be saved due to a system error. Please try again.");
                    }
                }, function error(response) {
                    $scope.showAlert(1, "Changes cannot be saved due to a system error. Please try again.");
                });
            }

            $scope.addPeakTime = function () {
                $("#addPeakTime").show();
                $("#peakTimeFormTitle").text("Add Peak Time");
                $("#peakTimeFormSubmitTitle").text("ADD PEAK TIME +");
                $("#daysCheckBoxes").show();
                $("#addAnotherTime").show();
                $("#extraTimings").html('');
                $("#peakTimeForm input").each(function () {
                    $(this).val('');
                });
                $("#peakTimeForm select").each(function () {
                    $(this).val(1);
                });
                $("#peakTimeForm select[name='times[0][start_minutes]']").each(function () {
                    $(this).val('00');
                });
                $("#peakTimeForm select[name='times[0][end_minutes]']").each(function () {
                    $(this).val('00');
                });
            }

            $scope.deletePeakTimes = function () {
                var ids = [];
                $('input:checkbox[id^="peakTime"]:checked').each(function () {
                    ids.push($(this).attr("id").replace("peakTime", ""));
                });
                if (ids.length > 0) {
                    $scope.showConfirm("Are you sure you would like to proceed?", 0, "confirmDeletePeakTimes", "Delete Peak Time(s)?");
                } else {
                    $scope.showAlert(1, "Please select at least one peak time to continue.");
                }

            }

            $scope.$on('confirmDeletePeakTimes', function (ev, args) {
                var ids = [];
                $('input:checkbox[id^="peakTime"]:checked').each(function () {
                    ids.push($(this).attr("id").replace("peakTime", ""));
                });
                if (ids.length > 0) {
                    $http({
                        method: 'post',
                        url: apiUrl + 'deletePeakTime',
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded'
                        },
                        data: $.param({
                            ids: ids
                        }),
                    }).then(function success(res) {
                        $scope.getAllPeakTimings();
                        $scope.showAlert(2, "Peak time(s) have been deleted successfully.", "Deleted");
                    }, function error(response) {
                        $scope.showAlert(1, "Changes cannot be saved due to a system error. Please try again.");
                    });
                } else {
                    $scope.showAlert(1, "Please select at least one peak time to continue.");
                }
            })


            $scope.closeAddPeakTimeForm = function () {
                $("#addPeakTime").hide();
            }

            $scope.savePeakTime = function () {
                if ($("input[name='peaktime_id']").val() == '') {
                    var url = apiUrl + 'addPeakTime';
                } else {
                    var url = apiUrl + 'updatePeakTime';
                }
                $http({
                    method: 'post',
                    url: url,
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    data: $("#peakTimeForm").serialize() + "&" + $.param({
                        club_id: localStorage.getItem("club_id")
                    }),
                }).then(function success(res) {
                    if (res.data.status) {
                        $scope.closeAddPeakTimeForm();
                        $scope.getAllPeakTimings();
                        if ($("input[name='peaktime_id']").val() == '') {
                            $scope.showAlert(2, "Peak time has been added successfully.", "Peak Time Added");
                        } else {
                            $scope.showAlert(2, "Peak time has been updated successfully.", "Peak Time Updated");
                        }

                    } else {
                        $scope.showAlert(1, res.data.message);
                    }
                }, function error(response) {
                    $scope.showAlert(1, "Changes cannot be saved due to a system error. Please try again.");
                });
            }

            $scope.addMorePeakTime = function () {
                k++;
                var html = $("#peakTimeHoursDiv div.first:first, #peakTimeHoursDiv div.second:first").clone();
                var $html = $('<div />', {html: html});
                $html.find('select[name="times[' + (k - 1) + '][start_hours]"]').attr("name", "times[" + k + "][start_hours]");
                $html.find('select[name="times[' + (k - 1) + '][start_minutes]"]').attr("name", "times[" + k + "][start_minutes]");
                $html.find('select[name="times[' + (k - 1) + '][end_hours]"]').attr("name", "times[" + k + "][end_hours]");
                $html.find('select[name="times[' + (k - 1) + '][end_minutes]"]').attr("name", "times[" + k + "][end_minutes]");
                html = $html.html();
                $("#extraTimings").append(html);
            }

            $scope.editPeakTimeForm = function (pid) {
                $http({
                    method: 'post',
                    url: apiUrl + 'getPeakTime',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    data: $("#peakTimeForm").serialize() + "&" + $.param({
                        id: pid
                    }),
                }).then(function success(res) {
                    if (res.data.status) {
                        $("#addPeakTime").show();
                        $("#peakTimeFormTitle").text("Edit Peak Time");
                        $("#daysCheckBoxes").hide();
                        $("#addAnotherTime").hide();
                        $("#peakTimeFormSubmitTitle").text("SAVE PEAK TIME");
                        $("input[name='peaktime_id']").val(res.data.data.PeakTime.id);
                        var startTime = res.data.data.PeakTime.start_time.split(":");
                        $("select[name='times[0][start_hours]']").val(startTime[0]);
                        $("select[name='times[0][start_minutes]']").val(startTime[1]);
                        var endTime = res.data.data.PeakTime.end_time.split(":");
                        $("select[name='times[0][end_hours]']").val(endTime[0]);
                        $("select[name='times[0][end_minutes]']").val(endTime[1]);
                        $("#extraTimings").html('');
                    } else {
                        $scope.showAlert(1, res.data.message);
                    }
                }, function error(response) {
                    $scope.showAlert(1, "Changes cannot be saved due to a system error. Please try again.");
                });
            }

            $scope.updateClubProfile = function () {
                var expression = /[-a-zA-Z0-9@:%_\+.~#?&//=]{2,256}\.[a-z]{2,4}\b(\/[-a-zA-Z0-9@:%_\+.~#?&//=]*)?/gi;
                var regex = new RegExp(expression);
                var t = $("input[name='website']").val();
                if (!t.match(regex)) {
                    var webstring = t.replace("https://","");
                    if(webstring!=''){
                        webstring = t.replace("http://","");
                    }
                    if(webstring!=''){
                        $scope.showAlert(1, "Website url is not valid.");
                        return false;
                    }
                }
                console.log($("#updateClubProfileForm").serialize());
                var form = $("#updateClubProfileForm")[0]; // You need to use standard javascript object here
                var formData = new FormData(form);
                formData.append("id", localStorage.getItem("club_id"));
                $.ajax({
                    type: 'post',
                    url: apiUrl + 'updateClubProfile',
                    data: formData,
                    contentType: false, // NEEDED, DON'T OMIT THIS (requires jQuery 1.6+)
                    processData: false,
                }).then(function success(res) {
                    if (res.status) {
                        $scope.showAlert(2, "Your changes have been saved successfully.", "Changes saved");
                        $scope.getClubProfile();
                    } else {
                        $scope.showAlert(1, res.data.message);
                    }
                }, function error(response) {
                    $scope.showAlert(1, "Changes cannot be saved due to a system error. Please try again.");
                });
            }

            $scope.showPosition = function (position) {
                $("#lat").val(position.coords.latitude);
                $("#lng").val(position.coords.longitude);
                var style = [
                    {
                        "featureType": "all",
                        "elementType": "labels.text.fill",
                        "stylers": [
                            {
                                "saturation": 36
                            },
                            {
                                "color": "#333333"
                            },
                            {
                                "lightness": 40
                            }
                        ]
                    },
                    {
                        "featureType": "all",
                        "elementType": "labels.text.stroke",
                        "stylers": [
                            {
                                "visibility": "on"
                            },
                            {
                                "color": "#ffffff"
                            },
                            {
                                "lightness": 16
                            }
                        ]
                    },
                    {
                        "featureType": "all",
                        "elementType": "labels.icon",
                        "stylers": [
                            {
                                "visibility": "off"
                            }
                        ]
                    },
                    {
                        "featureType": "administrative",
                        "elementType": "geometry.fill",
                        "stylers": [
                            {
                                "color": "#fefefe"
                            },
                            {
                                "lightness": 20
                            }
                        ]
                    },
                    {
                        "featureType": "administrative",
                        "elementType": "geometry.stroke",
                        "stylers": [
                            {
                                "color": "#fefefe"
                            },
                            {
                                "lightness": 17
                            },
                            {
                                "weight": 1.2
                            }
                        ]
                    },
                    {
                        "featureType": "landscape",
                        "elementType": "geometry",
                        "stylers": [
                            {
                                "color": "#ffffff"
                            },
                            {
                                "lightness": 20
                            }
                        ]
                    },
                    {
                        "featureType": "poi",
                        "elementType": "geometry",
                        "stylers": [
                            {
                                "color": "#f1f1f1"
                            },
                            {
                                "lightness": 21
                            }
                        ]
                    },
                    {
                        "featureType": "road.highway",
                        "elementType": "geometry.fill",
                        "stylers": [
                            {
                                "color": "#dedede"
                            },
                            {
                                "lightness": 17
                            }
                        ]
                    },
                    {
                        "featureType": "road.highway",
                        "elementType": "geometry.stroke",
                        "stylers": [
                            {
                                "color": "#dedede"
                            },
                            {
                                "lightness": 29
                            },
                            {
                                "weight": 0.2
                            }
                        ]
                    },
                    {
                        "featureType": "road.arterial",
                        "elementType": "geometry",
                        "stylers": [
                            {
                                "color": "#dedede"
                            },
                            {
                                "lightness": 18
                            }
                        ]
                    },
                    {
                        "featureType": "road.local",
                        "elementType": "geometry",
                        "stylers": [
                            {
                                "color": "#ffffff"
                            },
                            {
                                "lightness": 16
                            }
                        ]
                    },
                    {
                        "featureType": "transit",
                        "elementType": "geometry",
                        "stylers": [
                            {
                                "color": "#f2f2f2"
                            },
                            {
                                "lightness": 19
                            }
                        ]
                    },
                    {
                        "featureType": "water",
                        "elementType": "geometry",
                        "stylers": [
                            {
                                "color": "#a0d6d1"
                            },
                            {
                                "lightness": 17
                            }
                        ]
                    }
                ];
                var map = new google.maps.Map(document.getElementById('map'), {
                    center: {lat: position.coords.latitude, lng: position.coords.longitude},
                    zoom: 13,
                    zoomControl: false,
                    mapTypeControl: false,
                    scaleControl: false,
                    streetViewControl: false,
                    rotateControl: false,
                    fullscreenControl: false,
                    disableDefaultUI: true,
                    styles: style
                });
                var marker = new google.maps.Marker({
                    position: {lat: position.coords.latitude, lng: position.coords.longitude},
                    icon: uploadUrl + "marker.png",
                    draggable: true,
                    map: map
                });
                marker.addListener('dragend', function (event) {
                    $("#lat").val(event.latLng.lat());
                    $("#lng").val(event.latLng.lng());
                    //alert(this.getPosition().lat() + ":" + this.getPosition().lng());
                });
            }

            $scope.initMap = function () {
                if (navigator.geolocation) {
                    navigator.geolocation.getCurrentPosition($scope.showPosition);
                } else {
                    var position = {
                        coords: {
                            latitude: 3.10192,
                            longitude: 101.59062
                        }
                    }
                    $scope.showPosition(position);
                }

            }

            $scope.openImagePicker = function (id) {
                $("#" + id).trigger("click");
            }

            $scope.removeClubPicture = function (id) {
                $scope.showConfirm("Are you sure you want to continue?", id, "confirmClubPictureRemoval", "Delete Photo")
            }

            $scope.$on('confirmClubPictureRemoval', function (ev, args) {
                $http({
                    method: 'post',
                    url: apiUrl + 'removeClubPictures',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    data: $.param({
                        id: args.id
                    }),
                }).then(function success(res) {
                    if (res.data.status) {
                        $scope.getClubProfile();
                        $scope.showAlert(2, "Your changes have been saved successfully.", "Changes saved");
                    } else {
                        $scope.showAlert(1, res.data.message);
                    }
                }, function error(response) {
                    $scope.showAlert(1, "Changes cannot be saved due to a system error. Please try again.");
                });
            })

            $scope.getCourtFees = function () {
                $http({
                    method: 'post',
                    url: apiUrl + 'getCourtFees',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    data: $.param({
                        club_id: localStorage.getItem("club_id")
                    }),
                }).then(function success(res) {
                    if (res.data.status) {
                        $rootScope.courtFees = res.data.data;
                    } else {
                        //$scope.showAlert(1, res.data.message);
                    }
                }, function error(response) {
                    $scope.showAlert(1, response);
                });
            }

            $scope.saveCourtFees = function () {
                /*if (($("#peak_fee").val()!='') && $("#peak_fee").val() <= $("#normal_fee").val()) {
                 $scope.showAlert(1, "Peak fees can not be less than normal fees.");
                 return false;
                 }*/
                $http({
                    method: 'post',
                    url: apiUrl + 'addUpdateCourt',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    data: $("#Court_Fees").serialize() + "&" + $.param({
                        is_all: 1,
                        club_id: localStorage.getItem("club_id")
                    }),
                }).then(function success(res) {
                    if (res.data.status) {
                        if ($("#peak_fee").val() != '') {
                            $scope.showAlert(2, "Peak fees are now " + $("#peak_fee").val() + "% on top of normal fees, with the values rounded up to the nearest integer.");
                        } else {
                            $scope.showAlert(2, "Your changes have been saved successfully.", "Changes saved");
                        }
                        $scope.getCourtFees();
                    } else {
                        $scope.showAlert(1, res.data.message);
                    }
                }, function error(response) {
                    $scope.showAlert(1, "Changes cannot be saved due to a system error. Please try again.");
                });
            }

            $scope.addSpecialFees = function () {
                $("#addSpecialFees").show();
                $("#specialFeesFormTitle").text("Add Special Fees");
                $("#specialFeesFormSubmitTitle").text("Add SPECIAL FEES +");
                $("#specialFeesForm input").each(function () {
                    $(this).val('');
                });
                $("#specialFeesForm select").each(function () {
                    $(this).val('');
                });
                $("#specialFeesForm textarea").each(function () {
                    $(this).val('');
                });
                $("input[name='start_date']").datepicker("remove");
                $("input[name='end_date']").datepicker("remove");
                $("#specialFeesForm input[name='start_date']").datepicker({
                    autoclose: true,
                    startDate: new Date()
                });
                $("#specialFeesForm input[name='end_date']").datepicker({
                    autoclose: true,
                    startDate: new Date()
                });
            }

            $scope.deleteSpecialFees = function () {
                var ids = [];
                $('input:checkbox[id^="spFee"]:checked').each(function () {
                    ids.push($(this).attr("id").replace("spFee", ""));
                });
                if (ids.length > 0) {
                    $scope.showConfirm("Are you sure you would like to proceed?", 0, "confirmDeleteSpecialFees", "Delete Special Fees?");
                } else {
                    $scope.showAlert(1, "Please select at least one special fees to continue.", "Special Fees");
                }

            }

            $scope.$on('confirmDeleteSpecialFees', function (ev, args) {
                var ids = [];
                $('input:checkbox[id^="spFee"]:checked').each(function () {
                    ids.push($(this).attr("id").replace("spFee", ""));
                });
                if (ids.length > 0) {
                    $http({
                        method: 'post',
                        url: apiUrl + 'deleteSpecialFees',
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded'
                        },
                        data: $.param({
                            ids: ids
                        }),
                    }).then(function success(res) {
                        $scope.getSpecialFees();
                        $scope.showAlert(2, "Special fees have been deleted successfully.", "Deleted");
                    }, function error(response) {
                        $scope.showAlert(1, "Changes cannot be saved due to a system error. Please try again.");
                    });
                } else {
                    $scope.showAlert(1, "Please select at least one special fees to continue.", "Special Fees");
                }
            })


            $scope.closeAddSpecialFeesForm = function () {
                $("#addSpecialFees").hide();
            }

            $scope.saveSpecialFees = function () {
                $http({
                    method: 'post',
                    url: apiUrl + 'addUpdateSpecialFee',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    data: $("#specialFeesForm").serialize() + "&" + $.param({
                        club_id: localStorage.getItem("club_id")
                    }),
                }).then(function success(res) {
                    if (res.data.status) {
                        $scope.getSpecialFees();
                        $scope.closeAddSpecialFeesForm();
                        if ($("input[name='specialfees_id']").val() != '') {
                            $scope.showAlert(2, "Special fees have been updated successfully.", "Special Fees Updated");
                        } else {
                            $scope.showAlert(2, "Special fees have been added successfully.", "Special Fees Added");
                        }
                    } else {
                        $scope.showAlert(1, res.data.message, "Error");
                    }
                }, function error(response) {
                    $scope.showAlert(1, "Changes cannot be saved due to a system error. Please try again.");
                });
            }


            $scope.editSpecialFeesForm = function (pid) {
                $http({
                    method: 'post',
                    url: apiUrl + 'getSpecialFees',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    data: $.param({
                        id: pid
                    }),
                }).then(function success(res) {
                    if (res.data.status) {
                        $("#specialFeesForm input").each(function () {
                            $(this).val('');
                        });
                        $("#specialFeesForm select").each(function () {
                            $(this).val('');
                        });
                        $("#specialFeesForm textarea").each(function () {
                            $(this).val('');
                        });
                        $("#addSpecialFees").show();
                        $("#specialFeesFormTitle").text("Edit Special Fees");
                        $("#specialFeesFormSubmitTitle").text("SAVE SPECIAL FEES");
                        $("input[name='specialfees_id']").val(res.data.data.SpecialFee.id);
                        $("input[name='title']").val(res.data.data.SpecialFee.title);
                        $("input[name='start_date']").datepicker("remove");
                        $("input[name='end_date']").datepicker("remove");
                        $("input[name='start_date']").datepicker({
                            autoclose: true
                        });
                        $("input[name='end_date']").datepicker({
                            autoclose: true
                        });
                        $("input[name='start_date']").datepicker("update", new Date(res.data.data.SpecialFee.start_date));
                        $("input[name='end_date']").datepicker("update", new Date(res.data.data.SpecialFee.end_date));
                        $("input[name='rate']").val(res.data.data.SpecialFee.rate);
                    } else {
                        $scope.showAlert(1, res.data.message);
                    }
                }, function error(response) {
                    $scope.showAlert(1, "Changes cannot be saved due to a system error. Please try again.");
                });
            }

            $scope.enableTier = function (id) {
                if ($("#tier" + id).is(":checked")) {
                    $("#days" + id).attr("disabled", false);
                    $("#charge" + id).attr("disabled", false);
                    $("#daysTr" + id).removeClass("enableOpacity");
                    $("#chargeTr" + id).removeClass("enableOpacity");
                } else {
                    $("#days" + id).attr("disabled", true);
                    $("#charge" + id).attr("disabled", true);
                    $("#daysTr" + id).addClass("enableOpacity");
                    $("#chargeTr" + id).addClass("enableOpacity");
                }
            }

            $scope.saveRefundPolicy = function () {
                $http({
                    method: 'post',
                    url: apiUrl + 'saveRefundPolicy',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    data: $("#refundPolicyForm").serialize() + "&" + $.param({
                        club_id: localStorage.getItem("club_id")
                    }),
                }).then(function success(res) {
                    if (res.data.status) {
                        $scope.getRefundPolicy();
                        $scope.showAlert(2, "Your changes have been saved successfully.", "Changes saved");
                    } else {
                        $scope.showAlert(1, res.data.message);
                    }
                }, function error(response) {
                    $scope.showAlert(1, "Changes cannot be saved due to a system error. Please try again.");
                });
            }

            $scope.saveCommonCoachFees = function () {
                $http({
                    method: 'post',
                    url: apiUrl + 'updateClubProfile',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    data: $("#commonCoachFeesForm").serialize() + "&" + $.param({
                        id: localStorage.getItem("club_id")
                    }),
                }).then(function success(res) {
                    if (res.data.status) {
                        $scope.getCommonCoachFees();
                        $scope.showAlert(2, "Your changes have been saved successfully.", "Changes saved");
                    } else {
                        $scope.showAlert(1, res.data.message);
                    }
                }, function error(response) {
                    $scope.showAlert(1, "Changes cannot be saved due to a system error. Please try again.");
                });
            }

            $scope.deleteCoachFees = function () {
                var n = $('input:checkbox[id^="check"]:checked').length;
                if (n == 0) {
                    $scope.showAlert(1, "Please select at least one coach to continue");
                } else {
                    $scope.showConfirm("Are you sure you would like to proceed?", 0, "removeCoachingFees", "Delete Coaching Fees?");
                }
            }

            $scope.selectDeselectAllCoachingFees = function () {
                if ($("#checkAllCoachingFees").is(":checked")) {
                    $(".checkselect").prop("checked", true);
                } else {
                    $(".checkselect").prop("checked", false);
                }
            }

            $scope.$on('removeCoachingFees', function (ev, args) {
                var ids = [];
                $('input:checkbox[id^="checkCoachingFees"]:checked').each(function () {
                    ids.push($(this).attr("id").replace("checkCoachingFees", ""));
                });
                if (ids.length > 0) {
                    $http({
                        method: 'post',
                        url: apiUrl + 'deleteCoachingFees',
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded'
                        },
                        data: $.param({
                            ids: ids
                        }),
                    }).then(function success(res) {
                        $scope.getIndividualCoachingFees();
                        $scope.showAlert(2, "Coaching fees have been deleted successfully.", "Deleted");
                        //window.location.reload();
                    }, function error(response) {
                        $scope.showAlert(1, "Changes cannot be saved due to a system error. Please try again.");
                    });
                } else {
                    $scope.showAlert(1, "Please select at least one coach to continue");
                }
            })

            $scope.openAddCoachingFeesForm = function () {
                $("#addCoachFees").show();
                $("#coachFeesFormTitle").text("Add Coaching Fees");
                $("#coachFeesFormSubmitTitle").text("ADD COACHING FEES +");
                $("#coachFeesId").val("");
                $("#coach_id").val("");
                $("#private_class_normal_fees").val("");
                $("#private_class_peak_fees").val("");
                $("#group_class_normal_fees").val("");
                $("#group_class_peak_fees").val("");
            }

            $scope.closeAddCoachFeesForm = function () {
                $("#addCoachFees").hide();
            }

            $scope.addCoachFees = function () {
                $http({
                    method: 'post',
                    url: apiUrl + 'addUpdateCoachFees',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    data: $("#coachFeesForm").serialize() + "&" + $.param({
                        club_id: localStorage.getItem("club_id")
                    }),
                }).then(function success(res) {
                    if (res.data.status) {
                        $scope.getIndividualCoachingFees();
                        $scope.closeAddCoachFeesForm();
                        if ($("#coachFeesId").val() != '') {
                            $scope.showAlert(2, "Coaching fees have been updated successfully.", "Coaching Fees Updated");
                        } else {
                            $scope.showAlert(2, "Coaching fees have been added successfully.", "Coaching Fees Added");
                        }

                    } else {
                        $scope.showAlert(1, res.data.message);
                    }
                }, function error(response) {
                    $scope.showAlert(1, "Changes cannot be saved due to a system error. Please try again.");
                });
            }

            $scope.editCoachFees = function (coachFeesId) {
                $http({
                    method: 'post',
                    url: apiUrl + 'getCoachingFees',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    data: $.param({
                        id: coachFeesId
                    }),
                }).then(function success(res) {
                    if (res.data.status) {
                        $scope.openAddCoachingFeesForm();
                        $("#coachFeesFormTitle").text("Edit Coaching Fees");
                        $("#coachFeesFormSubmitTitle").text("SAVE CHANGES");
                        $("#coachFeesId").val(res.data.data.CoachFees.id);
                        $("#coach_id").val(res.data.data.CoachFees.coach_id);
                        $("#private_class_normal_fees").val(res.data.data.CoachFees.private_class_normal_fees);
                        $("#private_class_peak_fees").val(res.data.data.CoachFees.private_class_peak_fees);
                        $("#group_class_normal_fees").val(res.data.data.CoachFees.group_class_normal_fees);
                        $("#group_class_peak_fees").val(res.data.data.CoachFees.group_class_peak_fees);
                    } else {
                        $scope.showAlert(1, res.data.message);
                    }
                }, function error(response) {
                    $scope.showAlert(1, "Changes cannot be saved due to a system error. Please try again.");
                });
            }

            $scope.getAllCoaches = function () {
                $http({
                    method: 'post',
                    url: apiUrl + 'coachesByClubId',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    data: $.param({
                        club_id: localStorage.getItem("club_id")
                    }),
                }).then(function success(res) {
                    if (res.data.status) {
                        $scope.coaches = res.data.data;
                    } else {
                        //$scope.showAlert(1, "Coaches can not found");
                    }
                }, function error(response) {
                    //$scope.showAlert(1, response);
                });
            }

            $scope.checkURL = function ($e) {
                var string = $($e.target).val();
                if (!~string.indexOf("http")) {
                    string = "http://" + string;
                }
                $($e.target).val(string);
                return $e;
            }

        })

var showClubPicture = function (input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('.club-profile-img img:first')
                    .attr('src', e.target.result);
        };
        reader.readAsDataURL(input.files[0]);
    }
}

var saveClubPicture = function (input) {
    var formData = new FormData();
    formData.append("id", localStorage.getItem("club_id"));
    formData.append("image", input.files[0]);
    $.ajax({
        type: 'post',
        url: apiUrl + 'addClubPictures',
        data: formData,
        contentType: false, // NEEDED, DON'T OMIT THIS (requires jQuery 1.6+)
        processData: false,
    }).then(function success(res) {
        if (res.status) {
            showAlert(2, "Your photo(s) have been uploaded successfully.", "Upload successful");
            angular.element(document.getElementById('ClubSetupCtrl')).scope().getClubProfile();
        } else {
            showAlert(1, res.message);
        }
    }, function error(response) {
        showAlert(1, "Changes cannot be saved due to a system error. Please try again.");
    });
}

