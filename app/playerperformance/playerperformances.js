'use strict';
angular.module('myApp.playerperformance', ['ngRoute'])

        .config(['$routeProvider', function ($routeProvider) {
                $routeProvider.when('/playerperformance', {
                    templateUrl: 'playerperformance/playerperformance.html',
                    controller: 'PlayerPerformanceCtrl'
                });
            }])



        .controller('PlayerPerformanceCtrl', function ($scope, $http, $timeout, $compile, $rootScope, $templateCache, $filter) {

            $scope.uploadUrl = uploadUrl;
            var j = 0;
            var table;
            var clubBookingSessionId = 0;
            if ($rootScope.searchData != undefined) {
                var searchData = $rootScope.searchData;
            } else {
                var searchData = '';
            }
            $scope.search = {};

            $scope.$on('$viewContentLoaded', function () {
                if(searchData==''){
                    $scope.contentLoading(1);
                }else{
                    $scope.contentLoading(2);
                }
                
            });
            
            $scope.currentClasses = function(){
                $scope.contentLoading(1);
                $("#currentClassList").addClass("active");
                $("#pastClassList").removeClass("active");
            }
            
            $scope.pastClasses = function(){
                $scope.contentLoading(2);
                $("#currentClassList").removeClass("active");
                $("#pastClassList").addClass("active");
            }
            
            $scope.counterPlayer = 0;

            $scope.contentLoading = function (type) {
                j++;
                if (j > 1) {
                    table.destroy();
                }
                table = $('#classListTable').DataTable({
                    "autoWidth": false,

                    "ajax": {
                        "destroy": true,
                        "url": apiUrl + 'allClasses',
                        "type": 'post',
                        "headers": {
                            'Content-Type': 'application/x-www-form-urlencoded'
                        },
                        "data": {
                            "coach_id": localStorage.getItem("coach_id"),
                            "searchData": searchData,
                            "type": type
                        },
                        "dataSrc": function (json) {
                            console.log(json);
                            var fData = [];
                            if (json.status) {
                                $.each(json.data, function (index, val) {
                                    console.log(val);
                                    var name = (val.ClubBookingSession.class_name != '') ? "&nbsp;" + val.ClubBookingSession.class_name : "&nbsp;" + val.ClubBookingSession.booker_name;
                                    var date = val.ClubBookingSession.book_date;
                                    var feedbacks = val.Feedback;
                                    var details = " <a href='javascript:' ng-click='openClassDetails(" + val.ClubBookingSession.id + ", " + val.ClubBookingSession.class_type_id + ")'><img src='images/edit.png'/></a>";
                                    var res = {
                                        name: name,
                                        date: date,
                                        feedbacks: feedbacks,
                                        details: details
                                    }
                                    fData.push(res);
                                });
                            }


                            return fData;

                        }
                    }, "fnDrawCallback": function () {
                        var html = '<img src="images/left-arrow.png"/>';
                        $("#classListTable_previous").html(html);
                        var html = '<img src="images/right-arrow.png"/>';
                        $("#classListTable_next").html(html);
                        $("#classListTable_info").remove();
                        $("#classListTable_filter").remove();
                        $compile($("#classListTable"))($scope);
                    },
                    "pageLength": 5,
                    "columns": [
                        {"data": "name"},
                        {"data": "date"},
                        {"data": "feedbacks"},
                        {"data": "details"}
                    ]


                });
            }





            $scope.openClassDetails = function (session_id, class_type_id) {
                $http({
                    method: 'post',
                    url: apiUrl + 'allGroupclasses',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    data: $.param({
                        group_class_id: session_id,
                        class_type_id: class_type_id
                    }),
                }).then(function success(res) {
                    
                    if (res.data.status) {
                        $rootScope.displayType = 2;
                        $rootScope.session_id = session_id;
                        $rootScope.class_type_id = class_type_id;
                        $("#rightSideBarGroupClassDetails").show();
                        $rootScope.groupClassDeatilData = res.data.data;
                        $rootScope.totalFeedback = 0;
                        $rootScope.totalJoinGroupClass = 0;
                        if (res.data.data.JoinGroupClass.length > 0) {
                            $.each(res.data.data.JoinGroupClass, function (i, v) {
                                $rootScope.totalFeedback += v.PlayerFeedback.length;
                                $rootScope.totalJoinGroupClass++;
                            });
                        }

                    } else {
                        $scope.showAlert(1, res.data.message);
                    }
                }, function error(response) {
                    $scope.showAlert(1, response);
                });
            }

            $scope.closeRightSideBarAddGroupClass = function () {
                $("#rightSideBarGroupClassDetails").hide();
            }

            $scope.closeRightSideBarFeedback = function () {
                $("#rightSideBarFeedback").hide();
            }

            $scope.showFeedbackPage = function (session_id, player_id) {
                clubBookingSessionId = session_id;
                $http({
                    method: 'post',
                    url: apiUrl + 'getPlayerInfo',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    data: $.param({
                        playerId: player_id
                    }),
                }).then(function success(res) {
                    if (res.data.status) {
                        $("#rightSideBarFeedback").show();
                        $rootScope.playerDetails = res.data.data;
                        $("#firstUtr span.gray-box").css("background-color","#737373");
                        $(".slide-image").removeClass("hide").css("left","-5px");
                        $("input[name='utr_level']").val(0);
                        var totalRating = 0;
                        $rootScope.avgRating = 0;
                        if(res.data.data[0].PlayerFeedback.length>0){
                            for(var i=0; i<res.data.data[0].PlayerFeedback.length; i++){
                                totalRating = totalRating+res.data.data[0].PlayerFeedback.current_rating;
                            }
                            if(totalRating>0){
                                $rootScope.avgRating = totalRating/res.data.data[0].PlayerFeedback.length;
                            }
                        }
                    } else {
                        $scope.showAlert(1, res.data.message);
                    }
                }, function error(response) {
                    $scope.showAlert(1, response);
                });
            }

            var rating = 1;
            $scope.minus = function () {
                if (rating > 1) {
                    rating--;
                    $(".urtRating").html('<span><span class="rating-text">New Rating</span>'+rating+'</span>');
                    $("#current_rating").val(rating);
                    $scope.setUtr(rating);
                }
            }

            $scope.plus = function () {
                if (rating < 16) {
                    rating++;
                    $(".urtRating").html('<span><span class="rating-text">New Rating</span>'+rating+'</span>');
                    $("#current_rating").val(rating);
                    $scope.setUtr(rating);
                }
            }

            $scope.saveFeedback = function (player_id, session_id) {
                $http({
                    method: 'post',
                    url: apiUrl + 'savePlayerFeedback',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    data: $("#playerFeedback").serialize() + "&" + $.param({
                        club_booking_id: session_id,
                        player_id: player_id,
                        user_id: localStorage.getItem("coach_id"),
                        class_type_id: $rootScope.class_type_id
                    }),
                }).then(function success(res) {
                    if (res.data.status) {
                        $scope.closeRightSideBarFeedback();
                        $scope.closeRightSideBarAddGroupClass();
                        $scope.showAlert(2, "You have successfully provided feedback. They will be notified accordingly.", "Feedback saved");
                    } else {
                        $scope.showAlert(1, res.data.message);
                    }
                }, function error(response) {
                    $scope.showAlert(1, response);
                });
            }
            
            $scope.clearSearch = function(){
                searchData = '';
                $("#backToClasses").addClass("hide");
                $scope.currentClasses();
            }

            $scope.searchBookings = function () {
                searchData = {};
                searchData = $scope.search;
                searchData.bookDate = $("#searchDate").val();
                $("#backToClasses").removeClass("hide");
                $scope.closeRightSideBarSearch();
                $scope.contentLoading();
            }
            
            $scope.setTraits = function(id, num){
                for(var i=1; i<=5; i++){
                    $("#"+id+i).removeClass("current-rating");
                }
                for(var i=1; i<=num; i++){
                    $("#"+id+i).addClass("current-rating");
                }
                $("input[name='"+id+"_trait']").val(num);
                $("#"+id+"_rating").text(num+"/5");
            }
            
            $scope.setUtr = function(utrVal){
                var ev = $("#utr"+utrVal);
                var utrPoint = ($(ev).position().left-5)+"px";
                $(".gray-box").css("background-color","#CFCFCF");
                $(".slider-no").css("color","#CFCFCF");
                console.log($(ev).siblings()[0]);
                $(ev).find("span:first").css("color","#6dccc1");
                $(ev).find("span:nth-child(2)").css("background-color","#737373");
                $(".slide-image").removeClass("hide").css("left",utrPoint);
                $("input[name='utr_level']").val(utrVal);
                $(".urtRating").html('<span><span class="rating-text">New Rating</span>'+utrVal+'</span>');
                $("#current_rating").val(utrVal);
                rating = utrVal;
                $("#utrNo").text(rating);
            }
            
            $scope.showPlayerCommentNext = function(){
                if($rootScope.counterPlayer<($rootScope.playerInfo.PlayerFeedback.length-1)){
                    var date = new Date($rootScope.playerInfo.PlayerFeedback[$rootScope.counterPlayer].created+' UTC');
                    date.toString();
                    $rootScope.playerFeedbackDate = date;
                    $rootScope.counterPlayer++;
                }
            }
            
            $scope.showPlayerCommentBack = function(){
                if($rootScope.counterPlayer>0){
                    var date = new Date($rootScope.playerInfo.PlayerFeedback[$rootScope.counterPlayer].created+' UTC');
                    date.toString();
                    $rootScope.playerFeedbackDate = date;
                    $rootScope.counterPlayer--;
                }
            }

        })
        
        