'use strict';

angular.module('myApp.register', ['ngRoute'])
	.config(['$routeProvider', function ($routeProvider) {
		$routeProvider.when('/register', {
			templateUrl: 'register/register.html',
			controller: 'RegisterCtrl'
		}).when('/clubregister', {
			templateUrl: 'register/clubregister.html',
			controller: 'ClubregisterCtrl'
		}).when('/coachregister', {
			templateUrl: 'register/coachregister.html',
			controller: 'CoachregisterCtrl'
		});
}])
.controller('RegisterCtrl', function ($scope, $http, $location, $rootScope) {	
	if($(window).width() < 642){
		$('.first').css('height',($(window).height()/2));
		$('.second').css('height',($(window).height()/2));
	} else {
		$('.first').css('height',$(document).height());
		$('.second').css('height',$(document).height());
	}
	
	$scope.$on('$viewContentLoaded', function () {
		$http({
			method: 'post',
			url: apiUrl + 'getAllCountries',
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded'
			},
		}).then(function success(res) {
			if (res.data.status) {
				$scope.countries = res.data.data;
			} else {
				$scope.countries = [];
				$scope.showAlert(1, res.data.message);
			}
		}, function error(response) {
			$scope.showAlert(1, response);
		});				
		//$scope.contentLoading(1);
	});
			
	$('#regClub').css('display','none');
	$('#regCoach').css('display','none');
	
	$scope.show_club_reg = function () {
		window.location.href = '#!/clubregister';
	}	
	
	$scope.hide_club_reg = function(){
		$('#regClub').css('display','none');
	}
	
	$scope.hide_coach_reg = function(){
		$('#regCoach').css('display','none');
	}
	
	$scope.addCoachGender = function(gender){
		$('.coach_gender_btn').removeClass('selected');
		$('#coach_gender').val(gender);
		$('#'+gender+'_gender').addClass('selected');
	}
	
	
	/* club add form submit */
	$scope.submitClubForm = function () {
		$http({
			method: 'post',
			url: apiUrl + 'addclub',
			dataType:'json',
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded'
			},
			data: $.param({
				coachForm: $('#club_reg').serialize()
			}),
		}).then(function success(res) {
			$scope.showAlert(2, res.data.message,'Club Add');
			if(res.data.status){
				$('#regClub').css('display','none');
			}
			window.location.href = '#!/login';
			
		}, function error(response) {
			$scope.showAlert(1, response);
		});	
	}
	
	
	$scope.show_coach_reg = function () {
		window.location.href = '#!/coachregister';
	}
	
	$scope.show_coach_availability = function () {
		$('#avabilityForm')[0].reset();
		$('#add_more_time').html('');
		$('#myAvailability').modal();
	}
	
	
	$scope.mondaySchedule = [];
	$scope.tuesdaySchedule = [];
	$scope.wednesdaySchedule = [];
	$scope.thursdaySchedule = [];
	$scope.fridaySchedule = [];
	$scope.saturdaySchedule = [];
	$scope.sundaySchedule = [];
	
	$scope.addAvalCoch = function(){
		var schduleData = [];
		var start_time = '';
		var end_time = '';
		
		$('#add_more_time > div').each(function(){
			var sd = $(this).find('.startDate').val();
			var st = $(this).find('.startTime').val();
			
			if(sd != '' && st != ''){
				start_time = sd+':'+st;	
			}
			
			var ed = $(this).find('.endDate').val();
			var et = $(this).find('.endTime').val();
			
			if(ed != '' && et != ''){
				end_time = ed+':'+et;	
			}
			
			if(start_time != '' && end_time != ''){
				schduleData.push(start_time+' - '+end_time);	
			}
		});
		
		
		$('#myAvailability input[type="checkbox"]:checked').each(function(){
			if(schduleData.length > 0){
				if($(this).attr('id') == 'monday'){
					$scope.mondaySchedule.push(schduleData);
				}

				if($(this).attr('id') == 'tuesday'){
					$scope.tuesdaySchedule.push(schduleData);
				}

				if($(this).attr('id') == 'wednesday'){
					$scope.wednesdaySchedule.push(schduleData);
				}

				if($(this).attr('id') == 'thursday'){
					$scope.thursdaySchedule.push(schduleData);
				}

				if($(this).attr('id') == 'friday'){
					$scope.fridaySchedule.push(schduleData);
				}

				if($(this).attr('id') == 'saturday'){
					$scope.saturdaySchedule.push(schduleData);
				}

				if($(this).attr('id') == 'sunday'){
					$scope.sundaySchedule.push(schduleData);
				}
				
				$('.avl_delete').css('display','block');
			}
		});
		
		$('#myAvailability').modal('hide');
	};
	
	
	$scope.editAvalCoch = function(){
		var data_index = $('#data_index').val();
		var day_week = $('#day_week').val();
		
		var start_time = '';
		var end_time = '';
		var f_time = '';
		
		var sd = $('#avabilityEditForm .startDate').val();
		var st = $('#avabilityEditForm .startTime').val();

		if(sd !== '' && st !== ''){
			start_time = sd+':'+st;	
		}

		var ed = $('#avabilityEditForm .endDate').val();
		var et = $('#avabilityEditForm .endTime').val();

		if(ed !== '' && et !== ''){
			end_time = ed+':'+et;	
		}

		if(start_time !== '' && end_time !== ''){
			f_time = start_time+' - '+end_time;
		}
		
		if(day_week === 'monday'){
			var mondatDataEdit = [];
			$($scope.mondaySchedule).each(function (i, val) {
				$(val).each(function (j, valData) {				
					if (j != data_index) {
						mondatDataEdit.push(valData);
					} else {
						mondatDataEdit.push(f_time);	
					}				
				});
			});
			var mondatDataEditVal = [];
			mondatDataEditVal.push(mondatDataEdit);
			$scope.mondaySchedule = mondatDataEditVal;
		}
		
		if(day_week === 'tuesday'){
			var tuesdayDataEdit = [];
			$($scope.tuesdaySchedule).each(function (i, val) {
				$(val).each(function (j, valData) {				
					if (j != data_index) {
						tuesdayDataEdit.push(valData);
					} else {
						tuesdayDataEdit.push(f_time);	
					}				
				});
			});
			var tuesdayDataEditVal = [];
			tuesdayDataEditVal.push(tuesdayDataEdit);
			$scope.tuesdaySchedule = tuesdayDataEditVal;
		}
		
		if(day_week === 'wednesday'){
			var wednesdayDataEdit = [];
			$($scope.wednesdaySchedule).each(function (i, val) {
				$(val).each(function (j, valData) {				
					if (j != data_index) {
						wednesdayDataEdit.push(valData);
					} else {
						wednesdayDataEdit.push(f_time);	
					}				
				});
			});
			var wednesdayDataEditVal = [];
			wednesdayDataEdit.push(wednesdayDataEdit);
			$scope.wednesdaySchedule = wednesdayDataEdit;
		}
		
		
		if(day_week === 'thursday'){
			var thursdayDataEdit = [];
			$($scope.thursdaySchedule).each(function (i, val) {
				$(val).each(function (j, valData) {				
					if (j != data_index) {
						thursdayDataEdit.push(valData);
					} else {
						thursdayDataEdit.push(f_time);	
					}				
				});
			});
			var thursdayDataEditVal = [];
			thursdayDataEditVal.push(thursdayDataEdit);
			$scope.thursdaySchedule = thursdayDataEditVal;
		}
		
		if(day_week === 'friday'){
			var fridayDataEdit = [];
			$($scope.fridaySchedule).each(function (i, val) {
				$(val).each(function (j, valData) {				
					if (j != data_index) {
						fridayDataEdit.push(valData);
					} else {
						fridayDataEdit.push(f_time);	
					}				
				});
			});
			var fridayDataEditVal = [];
			fridayDataEditVal.push(fridayDataEdit);
			$scope.fridaySchedule = fridayDataEditVal;
		}
		
		if(day_week === 'saturday'){
			var saturdayDataEdit = [];
			$($scope.saturdaySchedule).each(function (i, val) {
				$(val).each(function (j, valData) {				
					if (j != data_index) {
						saturdayDataEdit.push(valData);
					} else {
						saturdayDataEdit.push(f_time);	
					}				
				});
			});
			var saturdayDataEditVal = [];
			saturdayDataEditVal.push(saturdayDataEdit);
			$scope.mondaySchedule = saturdayDataEditVal;
		}
		
		if(day_week === 'sunday'){
			var sundayDataEdit = [];
			$($scope.sundaySchedule).each(function (i, val) {
				$(val).each(function (j, valData) {				
					if (j != data_index) {
						sundayDataEdit.push(valData);
					} else {
						sundayDataEdit.push(f_time);	
					}				
				});
			});
			var sundayDataEditVal = [];
			sundayDataEditVal.push(sundayDataEdit);
			$scope.mondaySchedule = sundayDataEditVal;
		}
		
		$('#myAvailabilityEdit').modal('hide');
		
	};
	
	
	
	$scope.deleteAvltime = function(){
		var mondatDataEdit = [];
		$($scope.mondaySchedule).each(function (i, val) {
			$(val).each(function (j, valData) {	
				var checkedLgth = $('.monday_class:checked[data-index="'+j+'"]').length;
				if (checkedLgth == 0) {
					mondatDataEdit.push(valData);
				}				
			});
		});
		var mondatDataEditVal = [];
		mondatDataEditVal.push(mondatDataEdit);
		$scope.mondaySchedule = mondatDataEditVal;

		var tuesdayDataEdit = [];
		$($scope.tuesdaySchedule).each(function (i, val) {
			$(val).each(function (j, valData) {	
				var checkedLgth = $('.tuesday_class:checked[data-index="'+j+'"]').length;
				if (checkedLgth == 0) {
					tuesdayDataEdit.push(valData);
				}				
			});
		});
		var tuesdayDataEditVal = [];
		tuesdayDataEditVal.push(tuesdayDataEdit);
		$scope.tuesdaySchedule = tuesdayDataEditVal;
		
		var wednesdayDataEdit = [];
		$($scope.wednesdaySchedule).each(function (i, val) {
			$(val).each(function (j, valData) {	
				var checkedLgth = $('.wednesday_class:checked[data-index="'+j+'"]').length;
				if (checkedLgth == 0) {
					wednesdayDataEdit.push(valData);
				}				
			});
		});
		var wednesdayDataEditVal = [];
		wednesdayDataEditVal.push(wednesdayDataEdit);
		$scope.wednesdaySchedule = wednesdayDataEditVal;
		
		
		var thursdayDataEdit = [];
		$($scope.thursdaySchedule).each(function (i, val) {
			$(val).each(function (j, valData) {	
				var checkedLgth = $('.thursday_class:checked[data-index="'+j+'"]').length;
				if (checkedLgth == 0) {
					thursdayDataEdit.push(valData);
				}				
			});
		});
		var thursdayDataEditVal = [];
		thursdayDataEditVal.push(thursdayDataEdit);
		$scope.thursdaySchedule = thursdayDataEditVal;
		
		
		var fridayDataEdit = [];
		$($scope.fridaySchedule).each(function (i, val) {
			$(val).each(function (j, valData) {	
				var checkedLgth = $('.friday_class:checked[data-index="'+j+'"]').length;
				if (checkedLgth == 0) {
					fridayDataEdit.push(valData);
				}				
			});
		});
		var fridayDataEditVal = [];
		fridayDataEditVal.push(fridayDataEdit);
		$scope.fridaySchedule = fridayDataEditVal;
		
		
		var saturdayDataEdit = [];
		$($scope.saturdaySchedule).each(function (i, val) {
			$(val).each(function (j, valData) {	
				var checkedLgth = $('.saturday_class:checked[data-index="'+j+'"]').length;
				if (checkedLgth == 0) {
					saturdayDataEdit.push(valData);
				}				
			});
		});
		var saturdayDataEditVal = [];
		saturdayDataEditVal.push(saturdayDataEdit);
		$scope.saturdaySchedule = saturdayDataEditVal;
		
		
		var sundayDataEdit = [];
		$($scope.sundaySchedule).each(function (i, val) {
			$(val).each(function (j, valData) {	
				var checkedLgth = $('.sunday_class:checked[data-index="'+j+'"]').length;
				if (checkedLgth == 0) {
					sundayDataEdit.push(valData);
				}				
			});
		});
		var sundayDataEditVal = [];
		sundayDataEditVal.push(sundayDataEdit);
		$scope.sundaySchedule = sundayDataEditVal;
	};
	
	var i = 0;
	var j = 1;
	$scope.add_moreTime = function () {
		if(j == 1){
			var t_html = '<div>';			
				t_html += '<label>Start Time</label>'
				t_html += '<select class="startDate"><option value="">Hour</option><option value="1">1 AM</option><option value="2">2 AM</option><option value="3">3 AM</option><option value="4">4 AM</option><option value="5">5 AM</option><option value="6">6 AM</option><option value="7">7 AM</option><option value="8">8 AM</option><option value="9">9 AM</option><option value="10">10 AM</option><option value="11">11 AM</option><option value="12">12 PM</option><option value="1">1 PM</option><option value="2">2 PM</option><option value="3">3 PM</option><option value="4">4 PM</option><option value="5">5 PM</option><option value="6">6 PM</option><option value="7">7 PM</option><option value="8">8 PM</option><option value="9">9 PM</option><option value="10">10 PM</option><option value="11">11 PM</option><option value="12">12 AM</option></select>'
				
				t_html += '<select class="startTime"><option value="">Minute</option><option value="15">15</option><option value="30">30</option><option value="45">45</option><option value="00">00</option></select>'
				
				t_html += '<label>End Time</label>'
				t_html += '<select class="endDate"><option value="">Hour</option><option value="1">1 AM</option><option value="2">2 AM</option><option value="3">3 AM</option><option value="4">4 AM</option><option value="5">5 AM</option><option value="6">6 AM</option><option value="7">7 AM</option><option value="8">8 AM</option><option value="9">9 AM</option><option value="10">10 AM</option><option value="11">11 AM</option><option value="12">12 PM</option><option value="1">1 PM</option><option value="2">2 PM</option><option value="3">3 PM</option><option value="4">4 PM</option><option value="5">5 PM</option><option value="6">6 PM</option><option value="7">7 PM</option><option value="8">8 PM</option><option value="9">9 PM</option><option value="10">10 PM</option><option value="11">11 PM</option><option value="12">12 AM</option></select>'
				
				t_html += '<select class="endTime"><option value="">Minute</option><option value="15">15</option><option value="30">30</option><option value="45">45</option><option value="00">00</option></select>'
			t_html += '</div>'
		}
		else{
			var t_html = '<div>';			
				t_html += '<label>Start Time '+j+' </label>'
				t_html += '<select class="startDate"><option value="">Hour</option><option value="1">1 AM</option><option value="2">2 AM</option><option value="3">3 AM</option><option value="4">4 AM</option><option value="5">5 AM</option><option value="6">6 AM</option><option value="7">7 AM</option><option value="8">8 AM</option><option value="9">9 AM</option><option value="10">10 AM</option><option value="11">11 AM</option><option value="12">12 PM</option><option value="1">1 PM</option><option value="2">2 PM</option><option value="3">3 PM</option><option value="4">4 PM</option><option value="5">5 PM</option><option value="6">6 PM</option><option value="7">7 PM</option><option value="8">8 PM</option><option value="9">9 PM</option><option value="10">10 PM</option><option value="11">11 PM</option><option value="12">12 AM</option></select>'
				
				t_html += '<select class="startTime"><option value="">Minute</option><option value="15">15</option><option value="30">30</option><option value="45">45</option><option value="00">00</option></select>'
				
				t_html += '<label>End Time '+j+'</label>'
				t_html += '<select class="endDate"><option value="">Hour</option><option value="1">1 AM</option><option value="2">2 AM</option><option value="3">3 AM</option><option value="4">4 AM</option><option value="5">5 AM</option><option value="6">6 AM</option><option value="7">7 AM</option><option value="8">8 AM</option><option value="9">9 AM</option><option value="10">10 AM</option><option value="11">11 AM</option><option value="12">12 PM</option><option value="1">1 PM</option><option value="2">2 PM</option><option value="3">3 PM</option><option value="4">4 PM</option><option value="5">5 PM</option><option value="6">6 PM</option><option value="7">7 PM</option><option value="8">8 PM</option><option value="9">9 PM</option><option value="10">10 PM</option><option value="11">11 PM</option><option value="12">12 AM</option></select>'
				
				t_html += '<select class="endTime"><option value="">Minute</option><option value="15">15</option><option value="30">30</option><option value="45">45</option><option value="00">00</option></select>'
			t_html += '</div>'	
		}
		$('#add_more_time').append(t_html);
		
		j++;
		i++;
	}
	
	var ayexp = 1;
	$scope.add_year_exp = function(){
		var sel_options = '';
		var num = new Date();
		num = num.getFullYear();
		for (var i = 1980; i <= num; i++) {
			sel_options += '<option value="'+i+'">'+i+'</option>';
		}
		var exp_html = '<div class="coach_experirnce_year_div" id="coach_experirnce_year_rrmv_'+ayexp+'">';
			exp_html += '<a class="cacoaching_year_rmv nevyblue" href="javascript:void(0);" onclick="rmvCoachingYearExp('+ayexp+')"><i class="fa fa-minus" aria-hidden="true"></i></a><select name="coach_from_year['+ayexp+']" class="form-control" required>'+sel_options+'</select><select name="coach_to_year['+ayexp+']" class="form-control" required>'+sel_options+'</select><select name="coach_experirnce_field['+ayexp+']" id="coach_experirnce_field" class="form-control"><option value="PTR">PTR</option><option value="USPTA">USPTA</option><option value="ITPA">ITPA</option><option value="GPTCA">GPTCA</option></select>';
		exp_html += '</div>';
		$('#coach_experirnce_div').append(exp_html);
		ayexp++;

	}
	
	var ca = 1;
	$scope.add_ach = function(){
		var ca_html = '<div class="coach_achienement" id="coach_achienement_'+ca+'">';
			ca_html += '<a class="coach_achienement_rmv nevyblue" href="javascript:void(0);" onclick="rmvCoachingAchi('+ca+')"><i class="fa fa-minus" aria-hidden="true"></i></a><select name="coach_achievement['+ca+']" id="coach_achievement" class="form-control"><option value="">Select an achievement</option><option value="achievement 1">achievement 1</option><option value="achievement 2">achievement 2</option></select><input type="text" name="coach_achievement_detail['+ca+']" id="coach_achievement" class="form-control"  />';
		ca_html += '</div>';
		$('#coach_achievement_div').append(ca_html);
		ca++;
	}
	
	var cace = 1;
	$scope.coach_add_coaching_exp = function(){
		var cace_html = '<div class="cacoaching_exp" id="rmv_coach_'+cace+'">';
			cace_html += '<a class="cacoaching_exp_rmv nevyblue" href="javascript:void(0);" onclick="rmvCoachingExp('+cace+')"><i class="fa fa-minus" aria-hidden="true"></i></a><select class="form-control" id="coach_exp_from" name="coach_exp_from['+cace+']" required><option value="">Year</option><option value="1">1</option><option value="2">2</option><option value="3">3</option><option value="4">4</option><option value="5">5</option><option value="6">6</option><option value="7">7</option><option value="8">8</option><option value="9">9</option><option value="10">10</option></select><select class="form-control" id="coach_exp_to" name="coach_exp_to['+cace+']" required><option value="">Year</option><option value="1">1</option><option value="2">2</option><option value="3">3</option><option value="4">4</option><option value="5">5</option><option value="6">6</option><option value="7">7</option><option value="8">8</option><option value="9">9</option><option value="10">10</option></select><input type="text" class="form-control" id="coach_exp_to" name="coach_location['+cace+']" required>';
		cace_html += '</div>';
		$('#cacoaching_exp').append(cace_html);
		cace++;
	}
	
	var coach_certi = 1;
	$scope.addCerti = function(){
		var cer_html = '<div class="coach_certification" id="coach_certification_'+coach_certi+'">';
			cer_html += '<a class="coach_certification_rmv nevyblue" href="javascript:void(0);" onclick="rmvCoachingCerti('+coach_certi+')"><i class="fa fa-minus" aria-hidden="true"></i></a><select name="coach_certification['+coach_certi+']" id="coach_certification" class="form-control" required><option value="">Select a certification</option><option value="certification 1">certification 1</option><option value="certification 2">certification 2</option></select>';
		cer_html += '</div>';
		
		$('#coach_certi').append(cer_html);
		coach_certi++;
	}

	
	/* coach add form submit */
	$scope.submitCoachForm = function () {		
		var cpass = $('#cpwd').val();
		var pas = $('#password').val();
		var chk = false;
		if(pas != '' && cpass != '' && pas == cpass){
			chk = true;
		}
		else{
			$scope.showAlert(1, 'Password and confirm password are not match!');
		}
		
		if(chk){
			$http({
				method: 'post',
				url: apiUrl + 'addcoach',
				dataType:'json',
				headers: {
					'Content-Type': 'application/x-www-form-urlencoded'
				},
				data: $.param({
					coachForm: $('#coach_reg').serialize()
				})
			}).then(function success(res) {
				$scope.showAlert(2, res.data.message,'Coach Register');
				if(res.data.status){
					$('#regCoach').css('display','none');
				}
				window.location.href = '#!/login';
				
			}, function error(response) {
				$scope.showAlert(1, response);
			});	
		}
	}
		
})
.controller('ClubregisterCtrl', function ($scope, $http, $location, $rootScope) {	
	if($(window).width() < 642){
		$('.first').css('height',($(window).height()/2));
		$('.second').css('height',($(window).height()/2));
	} else {
		$('.first').css('height',$(document).height());
		$('.second').css('height',$(document).height());
	}
	
	/*angular.element(document.querySelectorAll("input.num")).on("input", function(event) {  
		console.log(event.keyCode);
		return false;
	});*/
	
	$scope.$on('$viewContentLoaded', function () {
		$http({
			method: 'post',
			url: apiUrl + 'getAllCountries',
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded'
			},
		}).then(function success(res) {
			if (res.data.status) {
				$scope.countries = res.data.data;
			} else {
				$scope.countries = [];
				$scope.showAlert(1, res.data.message);
			}
		}, function error(response) {
			$scope.showAlert(1, response);
		});				
		//$scope.contentLoading(1);
	});
	initialize();		
	//$('#regClub').css('display','none');
	$('#regCoach').css('display','none');
	
	$scope.show_club_reg = function () {
		$('#club_reg')[0].reset();
		
		$('#regClub').css('display','block');
		$('#regCoach').css('display','none');
		/*Map Load*/
		initialize();
		//$('.club_reg').css('display','none');
	}	
	
	$scope.hide_club_reg = function(){
		$('#regClub').css('display','none');
	}
	
	$scope.hide_coach_reg = function(){
		$('#regCoach').css('display','none');
	}
	
	$scope.addCoachGender = function(gender){
		$('.coach_gender_btn').removeClass('selected');
		$('#coach_gender').val(gender);
		$('.'+gender+'_gender').addClass('selected');
	}
	
	
	/* club add form submit */
	$scope.submitClubForm = function () {
		$http({
			method: 'post',
			url: apiUrl + 'addclub',
			dataType:'json',
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded'
			},
			data: $.param({
				coachForm: $('#club_reg').serialize()
			}),
		}).then(function success(res) {
			$scope.showAlert(2, res.data.message,'Club Add');
			if(res.data.status){
				//$('#regClub').css('display','none');
				window.location.href = '#!/login';
			}			
		}, function error(response) {
			$scope.showAlert(1, response);
		});	
	}
	
	
	$scope.show_coach_reg = function () {
		$('#coach_reg')[0].reset();
		$('#regCoach').css('display','block');
		$('#regClub').css('display','none');
	}
	
	$scope.show_coach_availability = function () {
		$('#avabilityForm')[0].reset();
		$('#add_more_time').html('');
		$('#myAvailability').modal();
	}
	
	
	$scope.mondaySchedule = [];
	$scope.tuesdaySchedule = [];
	$scope.wednesdaySchedule = [];
	$scope.thursdaySchedule = [];
	$scope.fridaySchedule = [];
	$scope.saturdaySchedule = [];
	$scope.sundaySchedule = [];
	
	$scope.addAvalCoch = function(){
		var schduleData = [];
		var start_time = '';
		var end_time = '';
		
		$('#add_more_time > div').each(function(){
			var sd = $(this).find('.startDate').val();
			var st = $(this).find('.startTime').val();
			
			if(sd != '' && st != ''){
				start_time = sd+':'+st;	
			}
			
			var ed = $(this).find('.endDate').val();
			var et = $(this).find('.endTime').val();
			
			if(ed != '' && et != ''){
				end_time = ed+':'+et;	
			}
			
			if(start_time != '' && end_time != ''){
				schduleData.push(start_time+' - '+end_time);	
			}
		});
		
		
		$('#myAvailability input[type="checkbox"]:checked').each(function(){
			if(schduleData.length > 0){
				if($(this).attr('id') == 'monday'){
					$scope.mondaySchedule.push(schduleData);
				}

				if($(this).attr('id') == 'tuesday'){
					$scope.tuesdaySchedule.push(schduleData);
				}

				if($(this).attr('id') == 'wednesday'){
					$scope.wednesdaySchedule.push(schduleData);
				}

				if($(this).attr('id') == 'thursday'){
					$scope.thursdaySchedule.push(schduleData);
				}

				if($(this).attr('id') == 'friday'){
					$scope.fridaySchedule.push(schduleData);
				}

				if($(this).attr('id') == 'saturday'){
					$scope.saturdaySchedule.push(schduleData);
				}

				if($(this).attr('id') == 'sunday'){
					$scope.sundaySchedule.push(schduleData);
				}
				
				$('.avl_delete').css('display','block');
			}
		});
		
		$('#myAvailability').modal('hide');
	};
	
	
	$scope.editAvalCoch = function(){
		var data_index = $('#data_index').val();
		var day_week = $('#day_week').val();
		
		var start_time = '';
		var end_time = '';
		var f_time = '';
		
		var sd = $('#avabilityEditForm .startDate').val();
		var st = $('#avabilityEditForm .startTime').val();

		if(sd !== '' && st !== ''){
			start_time = sd+':'+st;	
		}

		var ed = $('#avabilityEditForm .endDate').val();
		var et = $('#avabilityEditForm .endTime').val();

		if(ed !== '' && et !== ''){
			end_time = ed+':'+et;	
		}

		if(start_time !== '' && end_time !== ''){
			f_time = start_time+' - '+end_time;
		}
		
		if(day_week === 'monday'){
			var mondatDataEdit = [];
			$($scope.mondaySchedule).each(function (i, val) {
				$(val).each(function (j, valData) {				
					if (j != data_index) {
						mondatDataEdit.push(valData);
					} else {
						mondatDataEdit.push(f_time);	
					}				
				});
			});
			var mondatDataEditVal = [];
			mondatDataEditVal.push(mondatDataEdit);
			$scope.mondaySchedule = mondatDataEditVal;
		}
		
		if(day_week === 'tuesday'){
			var tuesdayDataEdit = [];
			$($scope.tuesdaySchedule).each(function (i, val) {
				$(val).each(function (j, valData) {				
					if (j != data_index) {
						tuesdayDataEdit.push(valData);
					} else {
						tuesdayDataEdit.push(f_time);	
					}				
				});
			});
			var tuesdayDataEditVal = [];
			tuesdayDataEditVal.push(tuesdayDataEdit);
			$scope.tuesdaySchedule = tuesdayDataEditVal;
		}
		
		if(day_week === 'wednesday'){
			var wednesdayDataEdit = [];
			$($scope.wednesdaySchedule).each(function (i, val) {
				$(val).each(function (j, valData) {				
					if (j != data_index) {
						wednesdayDataEdit.push(valData);
					} else {
						wednesdayDataEdit.push(f_time);	
					}				
				});
			});
			var wednesdayDataEditVal = [];
			wednesdayDataEdit.push(wednesdayDataEdit);
			$scope.wednesdaySchedule = wednesdayDataEdit;
		}
		
		
		if(day_week === 'thursday'){
			var thursdayDataEdit = [];
			$($scope.thursdaySchedule).each(function (i, val) {
				$(val).each(function (j, valData) {				
					if (j != data_index) {
						thursdayDataEdit.push(valData);
					} else {
						thursdayDataEdit.push(f_time);	
					}				
				});
			});
			var thursdayDataEditVal = [];
			thursdayDataEditVal.push(thursdayDataEdit);
			$scope.thursdaySchedule = thursdayDataEditVal;
		}
		
		if(day_week === 'friday'){
			var fridayDataEdit = [];
			$($scope.fridaySchedule).each(function (i, val) {
				$(val).each(function (j, valData) {				
					if (j != data_index) {
						fridayDataEdit.push(valData);
					} else {
						fridayDataEdit.push(f_time);	
					}				
				});
			});
			var fridayDataEditVal = [];
			fridayDataEditVal.push(fridayDataEdit);
			$scope.fridaySchedule = fridayDataEditVal;
		}
		
		if(day_week === 'saturday'){
			var saturdayDataEdit = [];
			$($scope.saturdaySchedule).each(function (i, val) {
				$(val).each(function (j, valData) {				
					if (j != data_index) {
						saturdayDataEdit.push(valData);
					} else {
						saturdayDataEdit.push(f_time);	
					}				
				});
			});
			var saturdayDataEditVal = [];
			saturdayDataEditVal.push(saturdayDataEdit);
			$scope.mondaySchedule = saturdayDataEditVal;
		}
		
		if(day_week === 'sunday'){
			var sundayDataEdit = [];
			$($scope.sundaySchedule).each(function (i, val) {
				$(val).each(function (j, valData) {				
					if (j != data_index) {
						sundayDataEdit.push(valData);
					} else {
						sundayDataEdit.push(f_time);	
					}				
				});
			});
			var sundayDataEditVal = [];
			sundayDataEditVal.push(sundayDataEdit);
			$scope.mondaySchedule = sundayDataEditVal;
		}
		
		$('#myAvailabilityEdit').modal('hide');
		
	};
	
	
	
	$scope.deleteAvltime = function(){
		var mondatDataEdit = [];
		$($scope.mondaySchedule).each(function (i, val) {
			$(val).each(function (j, valData) {	
				var checkedLgth = $('.monday_class:checked[data-index="'+j+'"]').length;
				if (checkedLgth == 0) {
					mondatDataEdit.push(valData);
				}				
			});
		});
		var mondatDataEditVal = [];
		mondatDataEditVal.push(mondatDataEdit);
		$scope.mondaySchedule = mondatDataEditVal;

		var tuesdayDataEdit = [];
		$($scope.tuesdaySchedule).each(function (i, val) {
			$(val).each(function (j, valData) {	
				var checkedLgth = $('.tuesday_class:checked[data-index="'+j+'"]').length;
				if (checkedLgth == 0) {
					tuesdayDataEdit.push(valData);
				}				
			});
		});
		var tuesdayDataEditVal = [];
		tuesdayDataEditVal.push(tuesdayDataEdit);
		$scope.tuesdaySchedule = tuesdayDataEditVal;
		
		var wednesdayDataEdit = [];
		$($scope.wednesdaySchedule).each(function (i, val) {
			$(val).each(function (j, valData) {	
				var checkedLgth = $('.wednesday_class:checked[data-index="'+j+'"]').length;
				if (checkedLgth == 0) {
					wednesdayDataEdit.push(valData);
				}				
			});
		});
		var wednesdayDataEditVal = [];
		wednesdayDataEditVal.push(wednesdayDataEdit);
		$scope.wednesdaySchedule = wednesdayDataEditVal;
		
		
		var thursdayDataEdit = [];
		$($scope.thursdaySchedule).each(function (i, val) {
			$(val).each(function (j, valData) {	
				var checkedLgth = $('.thursday_class:checked[data-index="'+j+'"]').length;
				if (checkedLgth == 0) {
					thursdayDataEdit.push(valData);
				}				
			});
		});
		var thursdayDataEditVal = [];
		thursdayDataEditVal.push(thursdayDataEdit);
		$scope.thursdaySchedule = thursdayDataEditVal;
		
		
		var fridayDataEdit = [];
		$($scope.fridaySchedule).each(function (i, val) {
			$(val).each(function (j, valData) {	
				var checkedLgth = $('.friday_class:checked[data-index="'+j+'"]').length;
				if (checkedLgth == 0) {
					fridayDataEdit.push(valData);
				}				
			});
		});
		var fridayDataEditVal = [];
		fridayDataEditVal.push(fridayDataEdit);
		$scope.fridaySchedule = fridayDataEditVal;
		
		
		var saturdayDataEdit = [];
		$($scope.saturdaySchedule).each(function (i, val) {
			$(val).each(function (j, valData) {	
				var checkedLgth = $('.saturday_class:checked[data-index="'+j+'"]').length;
				if (checkedLgth == 0) {
					saturdayDataEdit.push(valData);
				}				
			});
		});
		var saturdayDataEditVal = [];
		saturdayDataEditVal.push(saturdayDataEdit);
		$scope.saturdaySchedule = saturdayDataEditVal;
		
		
		var sundayDataEdit = [];
		$($scope.sundaySchedule).each(function (i, val) {
			$(val).each(function (j, valData) {	
				var checkedLgth = $('.sunday_class:checked[data-index="'+j+'"]').length;
				if (checkedLgth == 0) {
					sundayDataEdit.push(valData);
				}				
			});
		});
		var sundayDataEditVal = [];
		sundayDataEditVal.push(sundayDataEdit);
		$scope.sundaySchedule = sundayDataEditVal;
	};
	
	var i = 0;
	var j = 1;
	$scope.add_moreTime = function () {
		if(j == 1){
			var t_html = '<div>';			
				t_html += '<label>Start Time</label>'
				t_html += '<select class="startDate"><option value="">Hour</option><option value="1">1 AM</option><option value="2">2 AM</option><option value="3">3 AM</option><option value="4">4 AM</option><option value="5">5 AM</option><option value="6">6 AM</option><option value="7">7 AM</option><option value="8">8 AM</option><option value="9">9 AM</option><option value="10">10 AM</option><option value="11">11 AM</option><option value="12">12 PM</option><option value="1">1 PM</option><option value="2">2 PM</option><option value="3">3 PM</option><option value="4">4 PM</option><option value="5">5 PM</option><option value="6">6 PM</option><option value="7">7 PM</option><option value="8">8 PM</option><option value="9">9 PM</option><option value="10">10 PM</option><option value="11">11 PM</option><option value="12">12 AM</option></select>'
				
				t_html += '<select class="startTime"><option value="">Minute</option><option value="15">15</option><option value="30">30</option><option value="45">45</option><option value="00">00</option></select>'
				
				t_html += '<label>End Time</label>'
				t_html += '<select class="endDate"><option value="">Hour</option><option value="1">1 AM</option><option value="2">2 AM</option><option value="3">3 AM</option><option value="4">4 AM</option><option value="5">5 AM</option><option value="6">6 AM</option><option value="7">7 AM</option><option value="8">8 AM</option><option value="9">9 AM</option><option value="10">10 AM</option><option value="11">11 AM</option><option value="12">12 PM</option><option value="1">1 PM</option><option value="2">2 PM</option><option value="3">3 PM</option><option value="4">4 PM</option><option value="5">5 PM</option><option value="6">6 PM</option><option value="7">7 PM</option><option value="8">8 PM</option><option value="9">9 PM</option><option value="10">10 PM</option><option value="11">11 PM</option><option value="12">12 AM</option></select>'
				
				t_html += '<select class="endTime"><option value="">Minute</option><option value="15">15</option><option value="30">30</option><option value="45">45</option><option value="00">00</option></select>'
			t_html += '</div>'
		}
		else{
			var t_html = '<div>';			
				t_html += '<label>Start Time '+j+' </label>'
				t_html += '<select class="startDate"><option value="">Hour</option><option value="1">1 AM</option><option value="2">2 AM</option><option value="3">3 AM</option><option value="4">4 AM</option><option value="5">5 AM</option><option value="6">6 AM</option><option value="7">7 AM</option><option value="8">8 AM</option><option value="9">9 AM</option><option value="10">10 AM</option><option value="11">11 AM</option><option value="12">12 PM</option><option value="1">1 PM</option><option value="2">2 PM</option><option value="3">3 PM</option><option value="4">4 PM</option><option value="5">5 PM</option><option value="6">6 PM</option><option value="7">7 PM</option><option value="8">8 PM</option><option value="9">9 PM</option><option value="10">10 PM</option><option value="11">11 PM</option><option value="12">12 AM</option></select>'
				
				t_html += '<select class="startTime"><option value="">Minute</option><option value="15">15</option><option value="30">30</option><option value="45">45</option><option value="00">00</option></select>'
				
				t_html += '<label>End Time '+j+'</label>'
				t_html += '<select class="endDate"><option value="">Hour</option><option value="1">1 AM</option><option value="2">2 AM</option><option value="3">3 AM</option><option value="4">4 AM</option><option value="5">5 AM</option><option value="6">6 AM</option><option value="7">7 AM</option><option value="8">8 AM</option><option value="9">9 AM</option><option value="10">10 AM</option><option value="11">11 AM</option><option value="12">12 PM</option><option value="1">1 PM</option><option value="2">2 PM</option><option value="3">3 PM</option><option value="4">4 PM</option><option value="5">5 PM</option><option value="6">6 PM</option><option value="7">7 PM</option><option value="8">8 PM</option><option value="9">9 PM</option><option value="10">10 PM</option><option value="11">11 PM</option><option value="12">12 AM</option></select>'
				
				t_html += '<select class="endTime"><option value="">Minute</option><option value="15">15</option><option value="30">30</option><option value="45">45</option><option value="00">00</option></select>'
			t_html += '</div>'	
		}
		$('#add_more_time').append(t_html);
		
		j++;
		i++;
	}
	
	var ayexp = 1;
	$scope.add_year_exp = function(){
		var exp_html = '<div class="coach_experirnce_year_div" id="coach_experirnce_year_rrmv_'+ayexp+'">';
			exp_html += '<a class="cacoaching_year_rmv nevyblue" href="javascript:void(0);" onclick="rmvCoachingYearExp('+ayexp+')"><i class="fa fa-minus" aria-hidden="true"></i></a><select name="coach_from_year['+ayexp+']" class="form-control" required><option value="">Select from year</option><option value="2014">2014</option><option value="2015">2015</option><option value="2016">2016</option><option value="2017">2017</option></select><select name="coach_to_year['+ayexp+']" class="form-control" required><option value="">Select to year</option><option value="2014">2014</option><option value="2015">2015</option><option value="2016">2016</option><option value="2017">2017</option></select><select name="coach_experirnce_field['+ayexp+']" id="coach_experirnce_field" class="form-control"><option value="">Select a field</option><option value="PTR">PTR</option><option value="USPTA">USPTA</option><option value="ITPA">ITPA</option><option value="GPTCA">GPTCA</option></select>';
		exp_html += '</div>';
		$('#coach_experirnce_div').append(exp_html);
		ayexp++;

	}
	
	var ca = 1;
	$scope.add_ach = function(){
		var ca_html = '<div class="coach_achienement" id="coach_achienement_'+ca+'">';
			ca_html += '<a class="coach_achienement_rmv nevyblue" href="javascript:void(0);" onclick="rmvCoachingAchi('+ca+')"><i class="fa fa-minus" aria-hidden="true"></i></a><select name="coach_achievement['+ca+']" id="coach_achievement" class="form-control"><option value="">Select an achievement</option><option value="achievement 1">achievement 1</option><option value="achievement 2">achievement 2</option></select><input type="text" name="coach_achievement_detail['+ca+']" id="coach_achievement" class="form-control"  />';
		ca_html += '</div>';
		$('#coach_achievement_div').append(ca_html);
		ca++;
	}
	
	var cace = 1;
	$scope.coach_add_coaching_exp = function(){
		var cace_html = '<div class="cacoaching_exp" id="rmv_coach_'+cace+'">';
			cace_html += '<a class="cacoaching_exp_rmv nevyblue" href="javascript:void(0);" onclick="rmvCoachingExp('+cace+')"><i class="fa fa-minus" aria-hidden="true"></i></a><select class="form-control" id="coach_exp_from" name="coach_exp_from['+cace+']" required><option value="">Year</option><option value="1">1</option><option value="2">2</option><option value="3">3</option><option value="4">4</option><option value="5">5</option><option value="6">6</option><option value="7">7</option><option value="8">8</option><option value="9">9</option><option value="10">10</option></select><select class="form-control" id="coach_exp_to" name="coach_exp_to['+cace+']" required><option value="">Year</option><option value="1">1</option><option value="2">2</option><option value="3">3</option><option value="4">4</option><option value="5">5</option><option value="6">6</option><option value="7">7</option><option value="8">8</option><option value="9">9</option><option value="10">10</option></select><input type="text" class="form-control" id="coach_exp_to" name="coach_location['+cace+']" required>';
		cace_html += '</div>';
		$('#cacoaching_exp').append(cace_html);
		cace++;
	}
	
	var coach_certi = 1;
	$scope.addCerti = function(){
		var cer_html = '<div class="coach_certification" id="coach_certification_'+coach_certi+'">';
			cer_html += '<a class="coach_certification_rmv nevyblue" href="javascript:void(0);" onclick="rmvCoachingCerti('+coach_certi+')"><i class="fa fa-minus" aria-hidden="true"></i></a><select name="coach_certification['+coach_certi+']" id="coach_certification" class="form-control" required><option value="">Select a certification</option><option value="certification 1">certification 1</option><option value="certification 2">certification 2</option></select>';
		cer_html += '</div>';
		
		$('#coach_certi').append(cer_html);
		coach_certi++;
	}

	
	/* coach add form submit */
	$scope.submitCoachForm = function () {		
		var cpass = $('#cpwd').val();
		var pas = $('#password').val();
		var chk = false;
		if(pas != '' && cpass != '' && pas == cpass){
			chk = true;
		}
		else{
			alert('Password and confirm password are not match!');
		}
		
		if(chk){
			$http({
				method: 'post',
				url: apiUrl + 'addcoach',
				dataType:'json',
				headers: {
					'Content-Type': 'application/x-www-form-urlencoded'
				},
				data: $.param({
					coachForm: $('#coach_reg').serialize()
				})
			}).then(function success(res) {
				$scope.showAlert(2, res.data.message,'Coach Register');
				if(res.data.status){
					$('#regCoach').css('display','none');
				}
				window.location.href = '#!/login';
				
			}, function error(response) {
				$scope.showAlert(1, response);
			});	
		}
	}
		
})
.controller('CoachregisterCtrl', function ($scope, $http, $location, $rootScope) {	
	if($(window).width() < 642){
		$('.first').css('height',($(window).height()/2));
		$('.second').css('height',($(window).height()/2));
	} else {
		$('.first').css('height',$(document).height());
		$('.second').css('height',$(document).height());
	}
	
	$scope.$on('$viewContentLoaded', function () {
		$http({
			method: 'post',
			url: apiUrl + 'getAllCountries',
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded'
			},
		}).then(function success(res) {
			if (res.data.status) {
				$scope.countries = res.data.data;
			} else {
				$scope.countries = [];
				$scope.showAlert(1, res.data.message);
			}
		}, function error(response) {
			$scope.showAlert(1, response);
		});				
		//$scope.contentLoading(1);
	});
			
	$('#regClub').css('display','none');
	//$('#regCoach').css('display','none');
	
	$scope.show_club_reg = function () {
		$('#club_reg')[0].reset();
		
		$('#regClub').css('display','block');
		$('#regCoach').css('display','none');
		/*Map Load*/
		initialize();
		//$('.club_reg').css('display','none');
	}	
	
	$scope.checkMinPlayer = function(checkMinPlayer){
		var min_coach_per_class = $('#min_coach_per_class').val();
		var coach_per_class = $('#coach_per_class').val();
		
		if(coach_per_class != '' && coach_per_class < min_coach_per_class){
			$scope.showAlert(1, 'Min no of player should be less then max no of player');
			$('#coach_per_class').val('');
		}
	}
	
	$scope.hide_club_reg = function(){
		$('#regClub').css('display','none');
	}
	
	$scope.hide_coach_reg = function(){
		$('#regCoach').css('display','none');
	}
	
	$scope.addCoachGender = function(gender){
		$('.coach_gender_btn').removeClass('selected');
		$('#coach_gender').val(gender);
		$('#'+gender+'_gender').addClass('selected');
	}
	
	
	/* club add form submit */
	$scope.submitClubForm = function () {
		$http({
			method: 'post',
			url: apiUrl + 'addclub',
			dataType:'json',
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded'
			},
			data: $.param({
				coachForm: $('#club_reg').serialize()
			}),
		}).then(function success(res) {
			$scope.showAlert(2, res.data.message,'Club Add');
			if(res.data.status){
				$('#regClub').css('display','none');
			}
			window.location.href = '#!/login';
			
		}, function error(response) {
			$scope.showAlert(1, response);
		});	
	}
	
	$scope.getYear = function () {
		var num = new Date();
		num = num.getFullYear();
		var group = [];
		for (var i = 1980; i <= num; i++) {
			group.push(i);
		}
		return group;
	}
	
	
	$scope.show_coach_reg = function () {
		$('#coach_reg')[0].reset();
		$('#regCoach').css('display','block');
		$('#regClub').css('display','none');
	}
	
	$scope.show_coach_availability = function () {
		$('#avabilityForm')[0].reset();
		$('#add_more_time').html('');
		$('#myAvailability').modal();
		$scope.add_moreTime();
		$scope.add_moreTime();
	}
	
	
	$scope.mondaySchedule = [];
	$scope.tuesdaySchedule = [];
	$scope.wednesdaySchedule = [];
	$scope.thursdaySchedule = [];
	$scope.fridaySchedule = [];
	$scope.saturdaySchedule = [];
	$scope.sundaySchedule = [];
	
	$scope.addAvalCoch = function(){
		var schduleData = [];
		var start_time = '';
		var end_time = '';
		
		$('#add_more_time > div').each(function(){
			var start_time = '';
			var end_time = '';
			var sd = $(this).find('.startDate').val();
			var st = $(this).find('.startTime').val();			
			var ed = $(this).find('.endDate').val();
			var et = $(this).find('.endTime').val();			
			if(sd != '' && st != '' && ed != '' && et != ''){
				start_time = sd+':'+st;	
				end_time = ed+':'+et;	
			}			
			if(start_time != '' && end_time != ''){
				schduleData.push(start_time+' - '+end_time);	
			}
		});
		
		
		$('#myAvailability input[type="checkbox"]:checked').each(function(){
			if(schduleData.length > 0){
				if($(this).attr('id') == 'monday'){
					$scope.mondaySchedule.push(schduleData);
				}

				if($(this).attr('id') == 'tuesday'){
					$scope.tuesdaySchedule.push(schduleData);
				}

				if($(this).attr('id') == 'wednesday'){
					$scope.wednesdaySchedule.push(schduleData);
				}

				if($(this).attr('id') == 'thursday'){
					$scope.thursdaySchedule.push(schduleData);
				}

				if($(this).attr('id') == 'friday'){
					$scope.fridaySchedule.push(schduleData);
				}

				if($(this).attr('id') == 'saturday'){
					$scope.saturdaySchedule.push(schduleData);
				}

				if($(this).attr('id') == 'sunday'){
					$scope.sundaySchedule.push(schduleData);
				}
				
				$('.avl_delete').css('display','block');
			}
		});
		
		$('#myAvailability').modal('hide');
	};
	
	
	$scope.editAvalCoch = function(){
		var data_index = parseInt($('#data_index').val());
		var day_week = $('#day_week').val();
		
		var start_time = '';
		var end_time = '';
		var f_time = '';
		
		var sd = $('#avabilityEditForm .startDate').val();
		var st = $('#avabilityEditForm .startTime').val();

		if(sd !== '' && st !== ''){
			start_time = sd+':'+st;	
		}

		var ed = $('#avabilityEditForm .endDate').val();
		var et = $('#avabilityEditForm .endTime').val();

		if(ed !== '' && et !== ''){
			end_time = ed+':'+et;	
		}

		if(start_time !== '' && end_time !== ''){
			f_time = start_time+' - '+end_time;
		}
		
		if(day_week === 'monday'){
			var mondatDataEdit = [];
			$($scope.mondaySchedule).each(function (i, val) {
				$(val).each(function (j, valData) {				
					if (j != data_index) {
						mondatDataEdit.push(valData);
					} else {
						mondatDataEdit.push(f_time);	
					}				
				});
			});
			var mondatDataEditVal = [];
			mondatDataEditVal.push(mondatDataEdit);
			$scope.mondaySchedule = mondatDataEditVal;
		}
		
		if(day_week === 'tuesday'){
			var tuesdayDataEdit = [];
			$($scope.tuesdaySchedule).each(function (i, val) {
				$(val).each(function (j, valData) {				
					if (j != data_index) {
						tuesdayDataEdit.push(valData);
					} else {
						tuesdayDataEdit.push(f_time);	
					}				
				});
			});
			var tuesdayDataEditVal = [];
			tuesdayDataEditVal.push(tuesdayDataEdit);
			$scope.tuesdaySchedule = tuesdayDataEditVal;
		}
		
		if(day_week === 'wednesday'){
			var wednesdayDataEdit = [];
			$($scope.wednesdaySchedule).each(function (i, val) {
				$(val).each(function (j, valData) {				
					if (j != data_index) {
						wednesdayDataEdit.push(valData);
					} else {
						wednesdayDataEdit.push(f_time);	
					}				
				});
			});
			var wednesdayDataEditVal = [];
			wednesdayDataEditVal.push(wednesdayDataEdit);
			$scope.wednesdaySchedule = wednesdayDataEditVal;
		}
		
		
		if(day_week === 'thursday'){
			var thursdayDataEdit = [];
			$($scope.thursdaySchedule).each(function (i, val) {
				$(val).each(function (j, valData) {				
					if (j != data_index) {
						thursdayDataEdit.push(valData);
					} else {
						thursdayDataEdit.push(f_time);	
					}				
				});
			});
			var thursdayDataEditVal = [];
			thursdayDataEditVal.push(thursdayDataEdit);
			$scope.thursdaySchedule = thursdayDataEditVal;
		}
		
		if(day_week === 'friday'){
			var fridayDataEdit = [];
			$($scope.fridaySchedule).each(function (i, val) {
				$(val).each(function (j, valData) {				
					if (j != data_index) {
						fridayDataEdit.push(valData);
					} else {
						fridayDataEdit.push(f_time);	
					}				
				});
			});
			var fridayDataEditVal = [];
			fridayDataEditVal.push(fridayDataEdit);
			$scope.fridaySchedule = fridayDataEditVal;
		}
		
		if(day_week === 'saturday'){
			var saturdayDataEdit = [];
			$($scope.saturdaySchedule).each(function (i, val) {
				$(val).each(function (j, valData) {				
					if (j != data_index) {
						saturdayDataEdit.push(valData);
					} else {
						saturdayDataEdit.push(f_time);	
					}				
				});
			});
			var saturdayDataEditVal = [];
			saturdayDataEditVal.push(saturdayDataEdit);
			$scope.saturdaySchedule = saturdayDataEditVal;
		}
		
		if(day_week === 'sunday'){
			var sundayDataEdit = [];
			$($scope.sundaySchedule).each(function (i, val) {
				$(val).each(function (j, valData) {				
					if (j != data_index) {
						sundayDataEdit.push(valData);
					} else {
						sundayDataEdit.push(f_time);	
					}				
				});
			});
			var sundayDataEditVal = [];
			sundayDataEditVal.push(sundayDataEdit);
			$scope.sundaySchedule = sundayDataEditVal;
		}
		
		$('#myAvailabilityEdit').modal('hide');
		
	};
	
	
	
	$scope.deleteAvltime = function(){
		var mondatDataEdit = [];
		$($scope.mondaySchedule).each(function (i, val) {
			$(val).each(function (j, valData) {	
				var checkedLgth = $('.monday_class:checked[data-index="'+j+'"]').length;
				if (checkedLgth == 0) {
					mondatDataEdit.push(valData);
				}				
			});
		});
		var mondatDataEditVal = [];
		mondatDataEditVal.push(mondatDataEdit);
		$scope.mondaySchedule = mondatDataEditVal;

		var tuesdayDataEdit = [];
		$($scope.tuesdaySchedule).each(function (i, val) {
			$(val).each(function (j, valData) {	
				var checkedLgth = $('.tuesday_class:checked[data-index="'+j+'"]').length;
				if (checkedLgth == 0) {
					tuesdayDataEdit.push(valData);
				}				
			});
		});
		var tuesdayDataEditVal = [];
		tuesdayDataEditVal.push(tuesdayDataEdit);
		$scope.tuesdaySchedule = tuesdayDataEditVal;
		
		var wednesdayDataEdit = [];
		$($scope.wednesdaySchedule).each(function (i, val) {
			$(val).each(function (j, valData) {	
				var checkedLgth = $('.wednesday_class:checked[data-index="'+j+'"]').length;
				if (checkedLgth == 0) {
					wednesdayDataEdit.push(valData);
				}				
			});
		});
		var wednesdayDataEditVal = [];
		wednesdayDataEditVal.push(wednesdayDataEdit);
		$scope.wednesdaySchedule = wednesdayDataEditVal;
		
		
		var thursdayDataEdit = [];
		$($scope.thursdaySchedule).each(function (i, val) {
			$(val).each(function (j, valData) {	
				var checkedLgth = $('.thursday_class:checked[data-index="'+j+'"]').length;
				if (checkedLgth == 0) {
					thursdayDataEdit.push(valData);
				}				
			});
		});
		var thursdayDataEditVal = [];
		thursdayDataEditVal.push(thursdayDataEdit);
		$scope.thursdaySchedule = thursdayDataEditVal;
		
		
		var fridayDataEdit = [];
		$($scope.fridaySchedule).each(function (i, val) {
			$(val).each(function (j, valData) {	
				var checkedLgth = $('.friday_class:checked[data-index="'+j+'"]').length;
				if (checkedLgth == 0) {
					fridayDataEdit.push(valData);
				}				
			});
		});
		var fridayDataEditVal = [];
		fridayDataEditVal.push(fridayDataEdit);
		$scope.fridaySchedule = fridayDataEditVal;
		
		
		var saturdayDataEdit = [];
		$($scope.saturdaySchedule).each(function (i, val) {
			$(val).each(function (j, valData) {	
				var checkedLgth = $('.saturday_class:checked[data-index="'+j+'"]').length;
				if (checkedLgth == 0) {
					saturdayDataEdit.push(valData);
				}				
			});
		});
		var saturdayDataEditVal = [];
		saturdayDataEditVal.push(saturdayDataEdit);
		$scope.saturdaySchedule = saturdayDataEditVal;
		
		
		var sundayDataEdit = [];
		$($scope.sundaySchedule).each(function (i, val) {
			$(val).each(function (j, valData) {	
				var checkedLgth = $('.sunday_class:checked[data-index="'+j+'"]').length;
				if (checkedLgth == 0) {
					sundayDataEdit.push(valData);
				}				
			});
		});
		var sundayDataEditVal = [];
		sundayDataEditVal.push(sundayDataEdit);
		$scope.sundaySchedule = sundayDataEditVal;
	};
	
	var i = 0;
	var j = 1;
	$scope.add_moreTime = function () {
		var j = parseInt($('#add_more_time > div').length) + 1
		if(j == 1){
			var t_html = '<div>';			
				t_html += '<label>Start Time</label>'
				t_html += '<select class="startDate"><option value="">Hour</option><option value="1">1 AM</option><option value="2">2 AM</option><option value="3">3 AM</option><option value="4">4 AM</option><option value="5">5 AM</option><option value="6">6 AM</option><option value="7">7 AM</option><option value="8">8 AM</option><option value="9">9 AM</option><option value="10">10 AM</option><option value="11">11 AM</option><option value="12">12 PM</option><option value="13">1 PM</option><option value="14">2 PM</option><option value="15">3 PM</option><option value="16">4 PM</option><option value="17">5 PM</option><option value="18">6 PM</option><option value="19">7 PM</option><option value="20">8 PM</option><option value="21">9 PM</option><option value="22">10 PM</option><option value="23">11 PM</option><option value="0">12 AM</option></select>'
				
				t_html += '<select class="startTime"><option value="">Minute</option><option value="15">15</option><option value="30">30</option><option value="45">45</option><option value="00">00</option></select>'
				
				t_html += '<label>End Time</label>'
				t_html += '<select class="endDate"><option value="">Hour</option><option value="1">1 AM</option><option value="2">2 AM</option><option value="3">3 AM</option><option value="4">4 AM</option><option value="5">5 AM</option><option value="6">6 AM</option><option value="7">7 AM</option><option value="8">8 AM</option><option value="9">9 AM</option><option value="10">10 AM</option><option value="11">11 AM</option><option value="12">12 PM</option><option value="13">1 PM</option><option value="14">2 PM</option><option value="15">3 PM</option><option value="16">4 PM</option><option value="17">5 PM</option><option value="18">6 PM</option><option value="19">7 PM</option><option value="20">8 PM</option><option value="21">9 PM</option><option value="22">10 PM</option><option value="23">11 PM</option><option value="0">12 AM</option></select>'
				
				t_html += '<select class="endTime"><option value="">Minute</option><option value="15">15</option><option value="30">30</option><option value="45">45</option><option value="00">00</option></select>'
			t_html += '</div>'
		}
		else{
			var t_html = '<div>';			
				t_html += '<label>Start Time '+j+' </label>'
				t_html += '<select class="startDate"><option value="">Hour</option><option value="1">1 AM</option><option value="2">2 AM</option><option value="3">3 AM</option><option value="4">4 AM</option><option value="5">5 AM</option><option value="6">6 AM</option><option value="7">7 AM</option><option value="8">8 AM</option><option value="9">9 AM</option><option value="10">10 AM</option><option value="11">11 AM</option><option value="12">12 PM</option><option value="13">1 PM</option><option value="14">2 PM</option><option value="15">3 PM</option><option value="16">4 PM</option><option value="17">5 PM</option><option value="18">6 PM</option><option value="19">7 PM</option><option value="20">8 PM</option><option value="21">9 PM</option><option value="22">10 PM</option><option value="23">11 PM</option><option value="0">12 AM</option></select>'
				
				t_html += '<select class="startTime"><option value="">Minute</option><option value="15">15</option><option value="30">30</option><option value="45">45</option><option value="00">00</option></select>'
				
				t_html += '<label>End Time '+j+'</label>'
				t_html += '<select class="endDate"><option value="">Hour</option><option value="1">1 AM</option><option value="2">2 AM</option><option value="3">3 AM</option><option value="4">4 AM</option><option value="5">5 AM</option><option value="6">6 AM</option><option value="7">7 AM</option><option value="8">8 AM</option><option value="9">9 AM</option><option value="10">10 AM</option><option value="11">11 AM</option><option value="12">12 PM</option><option value="13">1 PM</option><option value="14">2 PM</option><option value="15">3 PM</option><option value="16">4 PM</option><option value="17">5 PM</option><option value="18">6 PM</option><option value="19">7 PM</option><option value="20">8 PM</option><option value="21">9 PM</option><option value="22">10 PM</option><option value="23">11 PM</option><option value="0">12 AM</option></select>'
				
				t_html += '<select class="endTime"><option value="">Minute</option><option value="15">15</option><option value="30">30</option><option value="45">45</option><option value="00">00</option></select>'
			t_html += '</div>'	
		}
		$('#add_more_time').append(t_html);
		
		j++;
		i++;
	}
	
	var ayexp = 1;
	$scope.add_year_exp = function(){		
		var sel_options = '';
		var num = new Date();
		num = num.getFullYear();
		for (var i = 1980; i <= num; i++) {
			sel_options += '<option value="'+i+'">'+i+'</option>';
		}
		
		var exp_html = '<div class="coach_experirnce_year_div" id="coach_experirnce_year_rrmv_'+ayexp+'">';
			exp_html += '<a class="cacoaching_year_rmv nevyblue" href="javascript:void(0);" onclick="rmvCoachingYearExp('+ayexp+')"><i class="fa fa-minus" aria-hidden="true"></i></a><select name="coach_from_year['+ayexp+']" class="form-control" required>'+sel_options+'</select><select name="coach_to_year['+ayexp+']" class="form-control" required>'+sel_options+'</select><select name="coach_experirnce_field['+ayexp+']" id="coach_experirnce_field" class="form-control"><option value="">Select a field</option><option value="PTR">PTR</option><option value="USPTA">USPTA</option><option value="ITPA">ITPA</option><option value="GPTCA">GPTCA</option></select>';
		exp_html += '</div>';
		$('#coach_experirnce_div').append(exp_html);
		ayexp++;

	}
	
	var ca = 1;
	$scope.add_ach = function(){
		var ca_html = '<div class="coach_achienement" id="coach_achienement_'+ca+'">';
			ca_html += '<a class="coach_achienement_rmv nevyblue" href="javascript:void(0);" onclick="rmvCoachingAchi('+ca+')"><i class="fa fa-minus" aria-hidden="true"></i></a><select name="coach_achievement['+ca+']" id="coach_achievement" class="form-control"><option value="">Select an achievement</option><option value="achievement 1">achievement 1</option><option value="achievement 2">achievement 2</option></select><input type="text" name="coach_achievement_detail['+ca+']" id="coach_achievement" class="form-control"  />';
		ca_html += '</div>';
		$('#coach_achievement_div').append(ca_html);
		ca++;
	}
	
	var cace = 1;
	$scope.coach_add_coaching_exp = function(){
		var sel_options = '';
		var num = new Date();
		num = num.getFullYear();
		for (var i = 1980; i <= num; i++) {
			sel_options += '<option value="'+i+'">'+i+'</option>';
		}
		
		var cace_html = '<div class="cacoaching_exp" id="rmv_coach_'+cace+'">';
			cace_html += '<a class="cacoaching_exp_rmv nevyblue" href="javascript:void(0);" onclick="rmvCoachingExp('+cace+')"><i class="fa fa-minus" aria-hidden="true"></i></a><select class="form-control" id="coach_exp_from" name="coach_exp_from['+cace+']" required>'+sel_options+'</select><select class="form-control" id="coach_exp_to" name="coach_exp_to['+cace+']" required>'+sel_options+'</select><input type="text" class="form-control" id="coach_exp_to" name="coach_location['+cace+']" required>';
		cace_html += '</div>';
		$('#cacoaching_exp').append(cace_html);
		cace++;
	}
	
	var coach_certi = 1;
	$scope.addCerti = function(){
		var cer_html = '<div class="coach_certification" id="coach_certification_'+coach_certi+'">';
			cer_html += '<a class="coach_certification_rmv nevyblue" href="javascript:void(0);" onclick="rmvCoachingCerti('+coach_certi+')"><i class="fa fa-minus" aria-hidden="true"></i></a><select name="coach_certification['+coach_certi+']" id="coach_certification" class="form-control" required><option value="">Select a certification</option><option value="PTR">PTR</option><option value="USPTA">USPTA</option><option value="ITPA">ITPA</option><option value="GPTCA">GPTCA</option></select>';
		cer_html += '</div>';
		
		$('#coach_certi').append(cer_html);
		coach_certi++;
	}

	
	/* coach add form submit */
	$scope.submitCoachForm = function () {		
		var cpass = $('#cpwd').val();
		var pas = $('#password').val();
		var chk = false;
		if(pas != '' && cpass != '' && pas == cpass){
			chk = true;
		}
		else{
			$scope.showAlert(1, 'Password and confirm password are not match!');
		}
		if(parseInt($('#min_coach_per_class').val()) > parseInt($('#coach_per_class').val())){
			$scope.showAlert(1, 'Min no of players should be less then max no of players.');
			chk = false;
		}
		
		if(chk){
			$http({
				method: 'post',
				url: apiUrl + 'addcoach',
				dataType:'json',
				headers: {
					'Content-Type': 'application/x-www-form-urlencoded'
				},
				data: $.param({
					coachForm: $('#coach_reg').serialize()
				})
			}).then(function success(res) {
				$scope.showAlert(2, res.data.message,'Coach Register');
				if(res.data.status){
					//$('#regCoach').css('display','none');
					window.location.href = '#!/login';
				}
				
			}, function error(response) {
				$scope.showAlert(1, response);
			});	
		}
	}
		
});

var initialize = function () {
  var map;
  var position = new google.maps.LatLng(4.2105, 101.9758);    // set your own default location.
  var style = [
                    {
                        "featureType": "all",
                        "elementType": "labels.text.fill",
                        "stylers": [
                            {
                                "saturation": 36
                            },
                            {
                                "color": "#333333"
                            },
                            {
                                "lightness": 40
                            }
                        ]
                    },
                    {
                        "featureType": "all",
                        "elementType": "labels.text.stroke",
                        "stylers": [
                            {
                                "visibility": "on"
                            },
                            {
                                "color": "#ffffff"
                            },
                            {
                                "lightness": 16
                            }
                        ]
                    },
                    {
                        "featureType": "all",
                        "elementType": "labels.icon",
                        "stylers": [
                            {
                                "visibility": "off"
                            }
                        ]
                    },
                    {
                        "featureType": "administrative",
                        "elementType": "geometry.fill",
                        "stylers": [
                            {
                                "color": "#fefefe"
                            },
                            {
                                "lightness": 20
                            }
                        ]
                    },
                    {
                        "featureType": "administrative",
                        "elementType": "geometry.stroke",
                        "stylers": [
                            {
                                "color": "#fefefe"
                            },
                            {
                                "lightness": 17
                            },
                            {
                                "weight": 1.2
                            }
                        ]
                    },
                    {
                        "featureType": "landscape",
                        "elementType": "geometry",
                        "stylers": [
                            {
                                "color": "#ffffff"
                            },
                            {
                                "lightness": 20
                            }
                        ]
                    },
                    {
                        "featureType": "poi",
                        "elementType": "geometry",
                        "stylers": [
                            {
                                "color": "#f1f1f1"
                            },
                            {
                                "lightness": 21
                            }
                        ]
                    },
                    {
                        "featureType": "road.highway",
                        "elementType": "geometry.fill",
                        "stylers": [
                            {
                                "color": "#dedede"
                            },
                            {
                                "lightness": 17
                            }
                        ]
                    },
                    {
                        "featureType": "road.highway",
                        "elementType": "geometry.stroke",
                        "stylers": [
                            {
                                "color": "#dedede"
                            },
                            {
                                "lightness": 29
                            },
                            {
                                "weight": 0.2
                            }
                        ]
                    },
                    {
                        "featureType": "road.arterial",
                        "elementType": "geometry",
                        "stylers": [
                            {
                                "color": "#dedede"
                            },
                            {
                                "lightness": 18
                            }
                        ]
                    },
                    {
                        "featureType": "road.local",
                        "elementType": "geometry",
                        "stylers": [
                            {
                                "color": "#ffffff"
                            },
                            {
                                "lightness": 16
                            }
                        ]
                    },
                    {
                        "featureType": "transit",
                        "elementType": "geometry",
                        "stylers": [
                            {
                                "color": "#f2f2f2"
                            },
                            {
                                "lightness": 19
                            }
                        ]
                    },
                    {
                        "featureType": "water",
                        "elementType": "geometry",
                        "stylers": [
                            {
                                "color": "#a0d6d1"
                            },
                            {
                                "lightness": 17
                            }
                        ]
                    }
                ];
  var myOptions = {
	zoom: 13,
	center: position,
	zoomControl: false,
	mapTypeControl: false,
	scaleControl: false,
	streetViewControl: false,
	rotateControl: false,
	fullscreenControl: false,
	disableDefaultUI: true,
	styles: style
  };
  var map = new google.maps.Map(document.getElementById("map-canvas"), myOptions);
  // We send a request to search for the location of the user.  
  // If that location is found, we will zoom/pan to this place, and set a marker
  navigator.geolocation.getCurrentPosition(locationFound, locationNotFound);

  function locationFound(position) {
	// we will zoom/pan to this place, and set a marker
	var location = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
	// var accuracy = position.coords.accuracy;

	map.setCenter(location);
	var marker = new google.maps.Marker({
		position: {lat: position.coords.latitude, lng: position.coords.longitude},
		icon: uploadUrl + "marker.png",
		draggable: true,
		map: map,
		title: "You are here! Drag the marker to the exact location."
	});
	// set the value an value of the <input>
	updateInput(location.lat(), location.lng());

	// Add a "drag end" event handler
	google.maps.event.addListener(marker, 'dragend', function() {
	  updateInput(this.position.lat(), this.position.lng());
	});


  }

  function locationNotFound() {
	var location = new google.maps.LatLng(4.2105, 101.9758);
	// var accuracy = position.coords.accuracy;

	map.setCenter(location);
	var marker = new google.maps.Marker({
		position: {lat: 4.2105, lng: 101.9758},
		icon: uploadUrl + "marker.png",
		draggable: true,
		map: map,
		title: "You are here! Drag the marker to the exact location."
	});
	// set the value an value of the <input>
	updateInput(location.lat(), location.lng());

	// Add a "drag end" event handler
	google.maps.event.addListener(marker, 'dragend', function() {
	  updateInput(this.position.lat(), this.position.lng());
	});
  }

}

function updateInput(lat, lng) {
  document.getElementById("my_location").value = lat + ',' + lng;
}