'use strict';

angular.module('myApp.coaches', ['ngRoute'])

    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/coaches', {
            templateUrl: 'coaches/coaches.html',
            controller: 'CoachesCtrl'
        });

    }])




    .controller('CoachesCtrl', function ($scope, $http, $timeout, $compile, $rootScope, $templateCache, $filter) {
        console.log("coaches ctrl");

        var table;
        var k = 0;

        var table1;
        var j = 0;

        $scope.coachId = 0;


        $scope.$on('$viewContentLoaded', function () {
            $scope.showCoachList();
        });


        $scope.contentLoading = function (type) {
            k++;
            if (k > 1) {
                table.destroy();
            }
            table = $('#coachListTable').DataTable({
                "autoWidth": false,

                "ajax": {
                    "destroy": true,
                    "url": apiUrl + 'allCoaches',
                    "type": 'post',
                    "headers": {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    "data": {
                        "club_id": localStorage.getItem("club_id"),
                        "type": type
                    },
                    "dataSrc": function (json) {
                        console.log(json);
                        var fData = [];
                        if (json.status) {
                            $.each(json.data, function (index, val) {
                                console.log(val);
                                var cimage = "<img src='" + uploadUrl + val.Coach.profile_picture + "' style='width:50px' />";
                                var cname = val.Coach.name;
                                var position = val.Coach.position;
                                $.each(val.Club, function (index1, val1) {
                                    if(val1.CoachRelation.club_id==localStorage.getItem("club_id")){
                                        position = val1.CoachRelation.designation;
                                    }
                                });
                                
                                var experiance = val.Coach.experiance;
                                if (type == 2) {
                                    var rating = $filter('formatDate')(val.Coach.created);
                                } else {
                                    var rating = val.Coach.current_rating;
                                }

                                var details = " <a href='javascript:' ng-click='showCoachProfileCoaches(" + val.Coach.id + ", " + type + ")'><img src='images/ahed-arrow.png'/></a>";
                                var res = {
                                    cimage: cimage,
                                    cname: cname,
                                    position: position,
                                    experiance: experiance,
                                    rating: rating,
                                    details: details
                                }
                                fData.push(res);
                            });
                        }


                        return fData;

                    }
                }, "fnDrawCallback": function () {
                    var html = '<img src="images/left-arrow.png"/>';
                    $("#coachListTable_previous").html(html);
                    var html = '<img src="images/right-arrow.png"/>';
                    $("#coachListTable_next").html(html);
                    $("#coachListTable_info").remove();
                    $("#coachListTable_filter label input").attr("placeholder","Search by coach name");
                    if (type == 2) {
                        $("#coachRating").text("Date of Application");
                    } else {
                        $("#coachRating").text("Rating");
                    }
                    $compile($("#coachListTable"))($scope);
                    $compile($("#coachListTable_previous"))($scope);
                    $compile($("#coachListTable_next"))($scope);
                },
                "pageLength": 5,
                "language": {
                    "search": ""
                },
                "columns": [
                    { "data": "cimage" },
                    { "data": "cname" },
                    { "data": "position" },
                    { "data": "experiance" },
                    { "data": "rating" },
                    { "data": "details" }
                ]


            });
             $("#coachListTable" ).wrap( "<div class='table-rasponsive'></div>" );
        }

        $scope.contentLoadingCoachProfile = function (type, coachId) {
            j++;
            if (j > 1) {
                table1.destroy();
            }
            table1 = $('#coachProfileTable').DataTable({
                "autoWidth": false,

                "ajax": {
                    "destroy": true,
                    "url": apiUrl + 'upcomingClubBookings',
                    "type": 'post',
                    "headers": {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    "data": {
                        "club_id": localStorage.getItem("club_id"),
                        "type": type,
                        "coach_id": coachId
                    },
                    "dataSrc": function (json) {
                        console.log(json);
                        var fData = [];
                        if (json.status) {
                            $.each(json.data, function (index, val) {
                                console.log(val);
                                var book_date = val.ClubBooking.book_date;
                                var book_time = val.ClubBooking.book_start_time + " " + val.ClubBooking.book_end_time;
                                var class_type = val.ClassType.name;
                                var booker_name = val.ClubBooking.booker_name;
                                var details = " <a href='javascript:' ng-click='showBookingDetails(" + val.ClubBooking.id + ")'><img src='images/ahed-arrow.png'/></a>";
                                var res = {
                                    book_date: book_date,
                                    book_time: book_time,
                                    class_type: class_type,
                                    booker_name: booker_name,
                                    details: details
                                }
                                fData.push(res);
                            });
                        }


                        return fData;

                    }
                }, "fnDrawCallback": function () {
                    var html = '<img src="images/left-arrow.png"/>';
                    $("#coachProfileTable_previous").html(html);
                    var html = '<img src="images/right-arrow.png"/>';
                    $("#coachProfileTable_next").html(html);
                    $("#coachProfileTable_info").remove();
                    $("#coachProfileTable_filter").remove();
                    $compile($("#coachProfileTable"))($scope);
                    $compile($("#coachProfileTable_previous"))($scope);
                    $compile($("#coachProfileTable_next"))($scope);
                },
                "pageLength": 5,
                "columns": [
                    { "data": "book_date" },
                    { "data": "book_time" },
                    { "data": "class_type" },
                    {"data": "booker_name"},
                    { "data": "details" }
                ]


            });

        }

        $scope.showCoachProfileCoaches = function (id, type) {
            $scope.coachId = id;
            $http({
                method: 'post',
                url: apiUrl + 'CoachProfile',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: $.param({
                    coach_id: id,
                    club_id: localStorage.getItem("club_id")
                }),
            }).then(function success(res) {
                if (res.data.status) {
                    
                    $rootScope.clubId = localStorage.getItem("club_id");
                    $rootScope.coachDetails = res.data.data.Coach;
                    $rootScope.uploadUrl = uploadUrl;
                    $rootScope.coachImage = uploadUrl + res.data.data.Coach.profile_picture;
                    $rootScope.coachAge = $scope.calculateAge(res.data.data.Coach.dob);
                    $rootScope.userDetails = res.data.data.User;
                    $rootScope.countryDetails = res.data.data.Country;
                    $rootScope.coachAvailableHours = res.data.data.CoachAvailableHour;
                    $rootScope.coachExperiance = res.data.data.CoachExperiance;
                    $rootScope.coachCertificate = res.data.data.CoachCertificate;
                    $rootScope.coachReview = res.data.data.CoachReview;
                    $rootScope.clubs = res.data.data.Club;
                    $rootScope.reviewLimit = 2;
                    var totalRating = 0;
                    for (var i = 0; i < (res.data.data.CoachReview.length); i++) {
                        totalRating += parseFloat(res.data.data.CoachReview[i].rating_star);
                    }
                    $rootScope.avgRating = totalRating / res.data.data.CoachReview.length;
                    $rootScope.totalPlayer = res.data.data.CoachReview.length;
                    $rootScope.coachDetails.created = new Date($rootScope.coachDetails.created);
                    $timeout(function () {
                        var template = $("#coachProfile").html();
                        var $html = $('<div />', { html: template });
                        $html.find('.box-main-inner').removeClass("hide");
                        $html.find('#1a').attr("id", "1aa");
                        $html.find('#2a').attr("id", "2aa");
                        $html.find('#3a').attr("id", "3aa");
                        $html.find('#4a').attr("id", "4aa");
                        $html.find('#example').attr("id", "coachProfileTable");
                        $html.find('#upcomingClass').attr("id", "upcomingClassCoachProfile");
                        $html.find('#pastClass').attr("id", "pastClassCoachProfile");
                        $html.find('#loadMoreReviews').attr("id", "loadMoreReviewsCoachProfile");
                        template = $html.html();
                        $("#commonPopup").html(template);
                        $compile($("#upcomingClassCoachProfile"))($scope);
                        $compile($("#pastClassCoachProfile"))($scope);
                        $compile($("#loadMoreReviewsCoachProfile"))($scope);
                        $compile($(".approveProfileDiv"))($scope);
                        if (type == 2) {
                            $(".approveProfileDiv").show();
                        } else {
                            $(".approveProfileDiv").hide();
                        }
                        $(".overlay").removeClass("hide");
                        $("#commonPopup").removeClass("hide");
                        $scope.showUpcomingClass();
                    }, 100);
                } else {
                    $scope.showAlert(1, res.data.message);
                }
            }, function error(response) {
                $scope.showAlert(1, JSON.stringify(response));
            });


        }

        $scope.coachProfileAction = function (type, coachId) {
            $rootScope.coachId = coachId;
            if (type == 1) {
                $("#coachProfileAcceptModalPopup").modal('show');
            } else {
                $("#coachProfileRejectModalPopup").modal('show');
            }
        }

        $scope.approveCoachProfile = function (coachId) {
            $http({
                method: 'post',
                url: apiUrl + 'coachProfileAction',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: $.param({
                    id: coachId,
                    position: $("#coachDesignation").val(),
                    salary: $("#coachSalary").val(),
                    commission: $("#coachCommission").val(),
                    remarks: $("#coachRemarks").val(),
                    is_approved: 1,
                    "club_id": localStorage.getItem("club_id")
                }),
            }).then(function success(res) {
                $("#coachProfileAcceptModalPopup").modal('hide');
                $scope.showAlert(2, "Coach profile approved");
                window.location.reload(true);
            }, function error(response) {
                $scope.showAlert(1, response);
            });
        }

        $scope.rejectCoachProfile = function (coachId) {
            $http({
                method: 'post',
                url: apiUrl + 'coachProfileAction',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: $.param({
                    id: coachId,
                    reason: $("#coachReason").val(),
                    is_approved: 3
                }),
            }).then(function success(res) {
                $("#coachProfileRejectModalPopup").modal('hide');
                $scope.showAlert(2, "Coach profile rejected");
                window.location.reload(true);
            }, function error(response) {
                $scope.showAlert(1, response);
            });
        }

        $scope.showMoreReviews = function () {
            $scope.reviewLimit += 1;
            $timeout(function () {
                var template = $("#2a").html();
                var $html = $('<div />', { html: template });
                $html.find('#loadMoreReviews').attr("id", "loadMoreReviewsCoachProfile");
                template = $html.html();
                $("#2aa").html(template);
                $compile($("#loadMoreReviewsCoachProfile"))($scope);
                if ($scope.reviewLimit >= $scope.coachReview.length) {
                    $timeout(function () {
                        $("#loadMoreReviewsCoachProfile").hide();
                    }, 100);
                }
            }, 100);
        }



        $scope.showUpcomingClass = function () {
            $("#upcomingClassCoachProfile").addClass("active");
            $("#pastClassCoachProfile").removeClass("active");
            $scope.contentLoadingCoachProfile(1, $scope.coachId);
        }

        $scope.showPastClass = function () {
            $("#upcomingClassCoachProfile").removeClass("active");
            $("#pastClassCoachProfile").addClass("active");
            $scope.contentLoadingCoachProfile(2, $scope.coachId);
        }


        $scope.showCoachList = function () {
            if (!$("#coachList").hasClass("active")) {
                $("#coachList").addClass("active");
                $("#coachApplications").removeClass("active");
                $scope.contentLoading(1);
            }
        }

        $scope.showCoachApplications = function () {
            if (!$("#coachApplications").hasClass("active")) {
                $("#coachList").removeClass("active");
                $("#coachApplications").addClass("active");
                $scope.contentLoading(2);
            }
        }

        $scope.showBookingDetails = function (bookingId) {
            $http({
                method: 'post',
                url: apiUrl + 'getEventDetails',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: $.param({
                    booking_id: bookingId
                }),
            }).then(function success(res) {
                res = res.data;
                $("#coachBookingModalPopup").modal('show');
                if(res.data.UserDetail.Player!=undefined){
                    $rootScope.bookerName = res.data.UserDetail.Player.first_name + " " + res.data.UserDetail.Player.last_name;
                }else{
                    $rootScope.bookerName = '';
                }
                $rootScope.playerContactNo = res.data.UserDetail.Player.phone;
                $rootScope.playerEmail = res.data.UserDetail.User.email;
                $rootScope.teamNumber = res.data.ClubBooking.team_number;
                $rootScope.remoteUrl = apiUrl + 'getSearchPlayers/' + $scope.teamNumber + '/';
                $rootScope.bookingId = bookingId;
                $rootScope.totalPlayer = res.data.Team.length;
                $rootScope.teams = res.data.Team;
                $rootScope.coachName = res.data.Coach.name;
                $rootScope.subCoachName = res.data.SubCoach.name;
                $rootScope.startTime = res.data.ClubBooking.book_start_time;
                $rootScope.endTime = res.data.ClubBooking.book_end_time;
                $rootScope.courtBookingFee = res.data.ClubBooking.court_booking_fee;
                $rootScope.coachBookingFee = res.data.ClubBooking.coach_booking_fee;
                $rootScope.subTotal = res.data.ClubBooking.sub_total;
                $rootScope.taxFee = res.data.ClubBooking.tax_fee;
                $rootScope.taxAmount = res.data.ClubBooking.tax_amount;
                $rootScope.grandTotal = res.data.ClubBooking.grand_total;
                $rootScope.paymentMode = res.data.ClubBooking.payment_mode;
                $rootScope.clubMembershipId = res.data.Club.membership_id;
                $rootScope.userAppId = res.data.ClubBooking.user_id;
                $rootScope.paymentDate = res.data.ClubBooking.payment_date;
                $rootScope.cardType = res.data.Transaction.card_type;
                $rootScope.cardNumber = res.data.Transaction.card_number;
                $rootScope.cardName = res.data.Transaction.card_name;
                var edate = new Date(res.data.ClubBooking.book_date + " " + res.data.ClubBooking.book_start_time);
                var eedate = new Date(res.data.ClubBooking.book_date + " " + res.data.ClubBooking.book_end_time);
                $rootScope.eventPlayTime = moment(edate).format('h:mma') + " - " + moment(eedate).format('h:mma');
                $rootScope.courtName = res.data.Court.name;
                $rootScope.start = res.data.ClubBooking.book_date;
                $rootScope.eventDate = moment(edate).format('Do MMMM YYYY');
                $rootScope.eventDay = moment(edate).format('dddd');
                $rootScope.bookingNumber = res.data.ClubBooking.prefix_booking_number + ("0000" + res.data.ClubBooking.booking_number).slice(-6);
                $rootScope.matchType = res.data.ClassType.name;
                $rootScope.courtName = res.data.Court.name;
                $rootScope.picUrl = uploadUrl;
            }, function error(response) {
                $scope.showAlert(1, JSON.stringify(response));
            });
        }


    })

var closeCoachProfile = function () {
    $("#commonPopup").html('');
    $(".overlay").addClass("hide");
    $("#commonPopup").addClass("hide");
}

var showCoachProfileTab = function (id) {
    $("#" + id).show();
    $("#" + id).addClass("active");
    if (id == '1aa') {
        $("#2aa, #3aa, #4aa").hide();
        $("#2aa, #3aa, #4aa").removeClass("active");
    } else if (id == '2aa') {
        $("#1aa, #3aa, #4aa").hide();
        $("#1aa, #3aa, #4aa").removeClass("active");
    } else if (id == '3aa') {
        $("#1aa, #2aa, #4aa").hide();
        $("#1aa, #2aa, #4aa").removeClass("active");
    } else if (id == '4aa') {
        $("#1aa, #2aa, #3aa").hide();
        $("#1aa, #2aa, #3aa").removeClass("active");
    }
}

var openBookingDetails = function (bookingId) {
    angular.element(document.getElementById('cochesCtrl')).scope().showBookingDetails(bookingId);
}

$('#coachListTable').DataTable( {
    responsive: true
} );
