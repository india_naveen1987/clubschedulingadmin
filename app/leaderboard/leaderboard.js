'use strict';

angular.module('myApp.leaderboard', ['ngRoute']).config(['$routeProvider', function ($routeProvider) {
	$routeProvider.when('/leaderboard', {
		templateUrl: 'leaderboard/leaderboard.html',
		controller: 'LeaderboardCtrl'
	});
}])
.controller('LeaderboardCtrl', function ($scope, $http, $timeout, $compile, $rootScope, $filter) {
	console.log('Leaderboard Loading...');
	
	
	 $scope.$on('$viewContentLoaded', function () {
		$http({
			method: 'post',
			url: apiUrl + 'getCountry',
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded'
			},
			data: $.param({
				club_id: localStorage.getItem("club_id"),
			}),
		}).then(function success(res) {
			if (res.data.status) {
				localStorage.setItem('country_id', res.data.data.country_id);
				localStorage.setItem('country_name', res.data.data.country_name);
				$scope.country_name = res.data.data.country_name;
			} 
		}, function error(response) {
			$scope.showAlert(1, response);
		});			
		 
		$scope.contentLoading(1);
	});
	
	
	
	$scope.contentLoading = function(page_no){
		$http({
			method: 'post',
			url: apiUrl + 'getLeaderboard',
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded'
			},
			data: $.param({
				page_no: page_no,
			}),
		}).then(function success(res) {
			if (res.data.status) {
				var allLeaderboard = [];
				$scope.imgUrl = uploadUrl;
				$scope.allLeaderboard = res.data.data;
				$scope.totalPages = 1;
				$scope.currentPage = 1;
				if (res.data.totalPages) {
					$scope.totalPages = parseInt(res.data.totalPages);
					$scope.currentPage = parseInt(res.data.currentPage);
					$scope.perPageData = parseInt(res.data.perPageData);
				}
			} else {
				$scope.showAlert(1, res.data.message);
			}
		}, function error(response) {
			$scope.showAlert(1, response);
		});
	};
	
	$scope.contentLoadingCountry = function(page_no){
		$http({
			method: 'post',
			url: apiUrl + 'getCountryLeaderboard',
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded'
			},
			data: $.param({
				page_no: page_no,
				country_id: localStorage.getItem("country_id"),
			}),
		}).then(function success(res) {
			if (res.data.status) {
				var countryLeaderboard = [];
				$scope.imgUrl = uploadUrl;
				$scope.countryLeaderboard = res.data.data;
				$scope.country_totalPages = 1;
				$scope.country_currentPage = 1;
				if (res.data.totalPages) {
					$scope.country_totalPages = parseInt(res.data.totalPages);
					$scope.country_currentPage = parseInt(res.data.currentPage);
					$scope.country_perPageData = parseInt(res.data.perPageData);
				}
			} else {
				$scope.showAlert(1, res.data.message);
			}
		}, function error(response) {
			$scope.showAlert(1, response);
		});
	};
	
	$scope.globaltop = function () {
		$('#1a').show();
		$('#2a').hide();
		$('.globaltop').css('opacity',1);
		$('.countrytop').css('opacity',0.5);
		$scope.contentLoading(1);
	};
	
	$scope.countrytop = function () {
		$('#1a').hide();
		$('#2a').show();
		$('.countrytop').css('opacity',1);
		$('.globaltop').css('opacity',0.5);
		$scope.contentLoadingCountry(1);
	};
	
	$scope.showPlayerInfoData = function (player_id) {
		alert(player_id);
	};
	
	$scope.showPlayerInfoData = function (id) {
		$http({
			method: 'post',
			url: apiUrl + 'memberprofile',
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded'
			},
			data: $.param({
				memberId: id
			}),
		}).then(function success(res) {					
			if (res.data.status) {
				$scope.userprofileImg = uploadUrl+res.data.data.Player.profile_picture;
				$scope.user_first_name = res.data.data.Player.first_name;
				$scope.user_dob = res.data.data.Player.dob;
				$scope.user_email = res.data.data.User.email;
				$scope.user_contact = res.data.data.Player.phone;
				
				var address_1 = '';
				
				if(res.data.data.Player.address_1 != 'null'){
					address_1 = res.data.data.Player.address_1;
				}
				
				var address_2 = '';
				if(res.data.data.Player.address_2 != 'null'){
					address_2 = res.data.data.Player.address_2;
				}
				
				$scope.user_address = address_1+', '+address_2;
				$scope.user_postcode = res.data.data.Player.postal_code;
				$scope.user_city = res.data.data.Player.city;
				$scope.user_country = res.data.data.Player.country;
				
				$('#myModal').modal();
			} 
			else {
				$scope.showAlert(1, res.data.message);
			}
		}, function error(response) {
			$scope.showAlert(1, JSON.stringify(response));
		});


	}
	

	$scope.range = function (min, max, step) {
		step = step || 1;
		var input = [];
		for (var i = min; i <= max; i += step) {
			input.push(i);
		}
		return input;
	};
	
});