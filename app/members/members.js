'use strict';
angular.module('myApp.myprofile', ['ngRoute'])

        .config(['$routeProvider', function ($routeProvider) {
                $routeProvider.when('/myprofile', {
                    templateUrl: 'myprofile/myprofile.html',
                    controller: 'MyProfileCtrl'
                });
            }])



        .controller('MyProfileCtrl', function ($scope, $http, $timeout, $compile, $rootScope, $templateCache, $filter) {
            console.log("my profile ctrl");
            $scope.remoteUrl = '';
            $scope.uploadUrl = uploadUrl;
            var j = 0;
            var table1;

            $scope.$on('$viewContentLoaded', function () {
                $timeout(function () {
                    $("#myProfileMenu li a").each(function (i, v) {
                        var cid = $(this).text();
                        console.log(cid);
                        cid = cid.replace(" ", "_");
                        $("#" + cid).hide();
                    });
                    $("#Login_Details").show();
                    var userDetails = JSON.parse(localStorage.getItem('user'));
                    $("#email").val(userDetails.email);
                    $("#password").val('');
                    $("#confirmpassword").val('');
                }, 500);
                $http({
                    method: 'post',
                    url: apiUrl + 'getAllCountries',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                }).then(function success(res) {
                    if (res.data.status) {
                        $scope.countries = res.data.data;
                    } else {
                        $scope.countries = [];
                        $scope.showAlert(1, res.data.message);
                    }
                }, function error(response) {
                    $scope.showAlert(1, response);
                });
            });
            $scope.saveLoginDetails = function () {
                if ($("#password").val().length < 8) {
                    $scope.showAlert(1, "Password should be atleast 8 characters long");
                } else if ($("#confirmpassword").val().length < 8) {
                    $scope.showAlert(1, "Confirm password should be atleast 8 characters long");
                } else if ($("#confirmpassword").val() != $("#password").val()) {
                    $scope.showAlert(1, "Both passwords does not match");
                } else {
                    $http({
                        method: 'post',
                        url: apiUrl + 'updateLoginDetails',
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded'
                        },
                        data: $.param({
                            id: localStorage.getItem('user_id'),
                            password: $("#password").val()
                        }),
                    }).then(function success(res) {
                        if (res.data.status) {
                            $scope.showAlert(2, "Login credentials saved");
                        } else {
                            $scope.showAlert(1, res.data.message);
                        }
                    }, function error(response) {
                        $scope.showAlert(1, response);
                    });
                }
            }



            $scope.openMenuItem = function (menu, ev) {
                $("#myProfileMenu li").removeClass("active-li");
                $(ev.target).parent("li").addClass("active-li");
                $("#myProfileMenu li a").each(function (i, v) {
                    var cid = $(this).text();
                    cid = cid.replace(" ", "_");
                    $("#" + cid).hide();
                });
                var eid = $(ev.target).text();
                eid = eid.replace(" ", "_");
                $("#" + eid).show();
                if (eid == 'Personal_Details') {
                    $scope.personalDetails();
                } else if (eid == 'Rating') {
                    $scope.getRatings();
                } else if (eid == 'Joined_Club') {
                    $scope.getJoinedClubs();
                } else if (eid == 'CV') {
                    $scope.getCV();
                }
            }

            $scope.personalDetails = function () {
                $http({
                    method: 'post',
                    url: apiUrl + 'CoachProfile',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    data: $.param({
                        coach_id: localStorage.getItem("coach_id")
                    }),
                }).then(function success(res) {
                    if (res.data.status) {
                        $rootScope.Coach = res.data.data.Coach;
                        var name = $rootScope.Coach.name.split(" ");
                        $rootScope.Coach.first_name = name[0];
                        $rootScope.Coach.last_name = name[1];
                        $("#country_id option[value='" + res.data.data.Country.id + "']").attr("selected", true);
                        $("#min_age_group option[value='" + res.data.data.Coach.min_age_group + "']").attr("selected", true);
                        $("#max_age_group option[value='" + res.data.data.Coach.max_age_group + "']").attr("selected", true);
                        $scope.setGender(res.data.data.Coach.gender);
                    } else {
                        $scope.showAlert(1, res.data.message);
                    }
                }, function error(response) {
                    $scope.showAlert(1, response);
                });
            }

            $scope.getRatings = function () {
                $http({
                    method: 'post',
                    url: apiUrl + 'CoachProfile',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    data: $.param({
                        coach_id: localStorage.getItem("coach_id")
                    }),
                }).then(function success(res) {
                    if (res.data.status) {
                        $rootScope.Coach = res.data.data.Coach;
                    } else {
                        $scope.showAlert(1, res.data.message);
                    }
                }, function error(response) {
                    $scope.showAlert(1, response);
                });
            }

            $scope.getJoinedClubs = function () {
                $http({
                    method: 'post',
                    url: apiUrl + 'CoachProfile',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    data: $.param({
                        coach_id: localStorage.getItem("coach_id")
                    }),
                }).then(function success(res) {
                    if (res.data.status) {
                        $scope.joinedClubs = res.data.data;
                    } else {
                        $scope.showAlert(1, res.data.message);
                    }
                }, function error(response) {
                    $scope.showAlert(1, response);
                });
            }

            $scope.showCoachProfile = function () {
                $scope.coachId = localStorage.getItem("coach_id");
                $http({
                    method: 'post',
                    url: apiUrl + 'CoachProfile',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    data: $.param({
                        coach_id: $scope.coachId
                    }),
                }).then(function success(res) {
                    if (res.data.status) {
                        $scope.coachDetails = res.data.data.Coach;
                        $scope.uploadUrl = uploadUrl;
                        $scope.coachImage = uploadUrl + res.data.data.Coach.profile_picture;
                        $scope.coachAge = $scope.calculateAge(res.data.data.Coach.dob);
                        $scope.userDetails = res.data.data.User;
                        $scope.countryDetails = res.data.data.Country;
                        $scope.coachAvailableHours = res.data.data.CoachAvailableHour;
                        $scope.coachExperiance = res.data.data.CoachExperiance;
                        $scope.coachCertificate = res.data.data.CoachCertificate;
                        $scope.coachReview = res.data.data.CoachReview;
                        $scope.reviewLimit = 2;
                        $scope.clubDetails = res.data.data.Club;
                        var totalRating = 0;
                        for (var i = 0; i < (res.data.data.CoachReview.length); i++) {
                            totalRating += parseFloat(res.data.data.CoachReview[i].rating_star);
                        }
                        $scope.avgRating = totalRating / res.data.data.CoachReview.length;
                        $scope.totalPlayer = res.data.data.CoachReview.length;
                        $scope.coachDetails.created = new Date($scope.coachDetails.created);
                        $timeout(function () {
                            var template = $("#coachProfile").html();
                            var $html = $('<div />', {html: template});
                            $html.find('.box-main-inner').removeClass("hide");
                            $html.find('#1a').attr("id", "1aa");
                            $html.find('#2a').attr("id", "2aa");
                            $html.find('#3a').attr("id", "3aa");
                            $html.find('#4a').attr("id", "4aa");
                            $html.find('#example').attr("id", "coachProfileTable");
                            $html.find('#upcomingClass').attr("id", "upcomingClassCoachProfile");
                            $html.find('#pastClass').attr("id", "pastClassCoachProfile");
                            $html.find('#loadMoreReviews').attr("id", "loadMoreReviewsCoachProfile");
                            template = $html.html();
                            $("#commonPopup").html(template);
                            $compile($("#upcomingClassCoachProfile"))($scope);
                            $compile($("#pastClassCoachProfile"))($scope);
                            $compile($("#loadMoreReviewsCoachProfile"))($scope);
                            $compile($(".approveProfileDiv"))($scope);

                            $(".overlay").removeClass("hide");
                            $("#commonPopup").removeClass("hide");
                            $scope.showUpcomingClass();
                        }, 100);
                    } else {
                        $scope.showAlert(1, res.data.message);
                    }
                }, function error(response) {
                    $scope.showAlert(1, JSON.stringify(response));
                });


            }

            $scope.contentLoadingCoachProfile = function (type, coachId) {
                j++;
                if (j > 1) {
                    table1.destroy();
                }
                table1 = $('#coachProfileTable').DataTable({
                    "autoWidth": false,

                    "ajax": {
                        "destroy": true,
                        "url": apiUrl + 'upcomingClubBookings',
                        "type": 'post',
                        "headers": {
                            'Content-Type': 'application/x-www-form-urlencoded'
                        },
                        "data": {
                            "type": type,
                            "coach_id": coachId
                        },
                        "dataSrc": function (json) {
                            console.log(json);
                            var fData = [];
                            if (json.status) {
                                $.each(json.data, function (index, val) {
                                    console.log(val);
                                    var book_date = val.ClubBooking.book_date;
                                    var book_time = val.ClubBooking.book_start_time + " " + val.ClubBooking.book_end_time;
                                    var class_type = val.ClassType.name;
                                    var details = " <a href='javascript:' onClick='openBookingDetails(" + val.ClubBooking.id + ")'><img src='images/ahed-arrow.png'/></a>";
                                    var res = {
                                        book_date: book_date,
                                        book_time: book_time,
                                        class_type: class_type,
                                        details: details
                                    }
                                    fData.push(res);
                                });
                            }


                            return fData;

                        }
                    }, "fnDrawCallback": function () {
                        var html = '<img src="images/left-arrow.png"/>';
                        $("#coachProfileTable_previous").html(html);
                        var html = '<img src="images/right-arrow.png"/>';
                        $("#coachProfileTable_next").html(html);
                        $("#coachProfileTable_info").remove();
                        $("#coachProfileTable_filter").remove();
                    },
                    "pageLength": 5,
                    "columns": [
                        {"data": "book_date"},
                        {"data": "book_time"},
                        {"data": "class_type"},
                        {"data": "details"}
                    ]


                });

            }

            $scope.showUpcomingClass = function () {
                $("#upcomingClassCoachProfile").addClass("active");
                $("#pastClassCoachProfile").removeClass("active");
                $scope.contentLoadingCoachProfile(1, $scope.coachId);
            }

            $scope.showPastClass = function () {
                $("#upcomingClassCoachProfile").removeClass("active");
                $("#pastClassCoachProfile").addClass("active");
                $scope.contentLoadingCoachProfile(2, $scope.coachId);
            }

            $scope.getAgeGroup = function (num) {
                var group = [];
                for (var i = 8; i < num; i++) {
                    group.push(i);
                }
                return group;
            }

            $scope.getYear = function () {
                var num = new Date();
                num = num.getFullYear();
                var group = [];
                for (var i = 1980; i <= num; i++) {
                    group.push(i);
                }
                return group;
            }

            $scope.openImagePicker = function (id) {
                $("#" + id).trigger("click");
            }

            $scope.setGender = function (gender) {
                if (gender == 1) {
                    $("#coachMale").addClass("genderActive");
                    $("#coachFemale").removeClass("genderActive");
                } else if (gender == 2) {
                    $("#coachMale").removeClass("genderActive");
                    $("#coachFemale").addClass("genderActive");
                }
                $("#coachGender").val(gender);
            }

            $scope.updatePersonalDetails = function () {
                console.log($("#updatePersonalDetailsForm").serialize());
                var form = $("#updatePersonalDetailsForm")[0]; // You need to use standard javascript object here
                var formData = new FormData(form);
                formData.append("id", localStorage.getItem("coach_id"));
                $.ajax({
                    type: 'post',
                    url: apiUrl + 'updateCoachProfile',
                    data: formData,
                    contentType: false, // NEEDED, DON'T OMIT THIS (requires jQuery 1.6+)
                    processData: false,
                }).then(function success(res) {
                    if (res.status) {
                        $scope.personalDetails();
                        $scope.showAlert(2, "Coach profile updated");
                    } else {
                        $scope.showAlert(1, res.message);
                    }
                }, function error(response) {
                    $scope.showAlert(1, response);
                });
            }

            $scope.saveRatings = function () {
                var form = $("#Rating")[0]; // You need to use standard javascript object here
                var formData = new FormData(form);
                formData.append("id", localStorage.getItem("coach_id"));
                $.ajax({
                    type: 'post',
                    url: apiUrl + 'updateCoachProfile',
                    data: formData,
                    contentType: false, // NEEDED, DON'T OMIT THIS (requires jQuery 1.6+)
                    processData: false,
                }).then(function success(res) {
                    if (res.status) {
                        $scope.getRatings();
                        $scope.showAlert(2, "Rating / Ranking saved");
                    } else {
                        $scope.showAlert(1, res.message);
                    }
                }, function error(response) {
                    $scope.showAlert(1, response);
                });
            }

            $scope.showBookingDetails = function (bookingId) {
                $http({
                    method: 'post',
                    url: apiUrl + 'getEventDetails',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    data: $.param({
                        booking_id: bookingId
                    }),
                }).then(function success(res) {
                    res = res.data;
                    $("#coachBookingModalPopup").modal('show');
                    $rootScope.bookerName = res.data.UserDetail.Player.first_name + " " + res.data.UserDetail.Player.last_name;
                    $rootScope.playerContactNo = res.data.UserDetail.Player.phone;
                    $rootScope.playerEmail = res.data.UserDetail.User.email;
                    $rootScope.teamNumber = res.data.ClubBooking.team_number;
                    $rootScope.remoteUrl = apiUrl + 'getSearchPlayers/' + $scope.teamNumber + '/';
                    $rootScope.bookingId = bookingId;
                    $rootScope.totalPlayer = res.data.Team.length;
                    $rootScope.teams = res.data.Team;
                    $rootScope.coachName = res.data.Coach.name;
                    $rootScope.subCoachName = res.data.SubCoach.name;
                    $rootScope.startTime = res.data.ClubBooking.book_start_time;
                    $rootScope.endTime = res.data.ClubBooking.book_end_time;
                    $rootScope.courtBookingFee = res.data.ClubBooking.court_booking_fee;
                    $rootScope.coachBookingFee = res.data.ClubBooking.coach_booking_fee;
                    $rootScope.subTotal = res.data.ClubBooking.sub_total;
                    $rootScope.taxFee = res.data.ClubBooking.tax_fee;
                    $rootScope.taxAmount = res.data.ClubBooking.tax_amount;
                    $rootScope.grandTotal = res.data.ClubBooking.grand_total;
                    $rootScope.paymentMode = res.data.ClubBooking.payment_mode;
                    $rootScope.clubMembershipId = res.data.Club.membership_id;
                    $rootScope.userAppId = res.data.ClubBooking.user_id;
                    $rootScope.paymentDate = res.data.ClubBooking.payment_date;
                    $rootScope.cardType = res.data.Transaction.card_type;
                    $rootScope.cardNumber = res.data.Transaction.card_number;
                    $rootScope.cardName = res.data.Transaction.card_name;
                    var edate = new Date(res.data.ClubBooking.book_date + " " + res.data.ClubBooking.book_start_time);
                    var eedate = new Date(res.data.ClubBooking.book_date + " " + res.data.ClubBooking.book_end_time);
                    $rootScope.eventPlayTime = moment(edate).format('h:mma') + " - " + moment(eedate).format('h:mma');
                    $rootScope.courtName = res.data.Court.name;
                    $rootScope.start = res.data.ClubBooking.book_date;
                    $rootScope.eventDate = moment(edate).format('Do MMMM YYYY');
                    $rootScope.eventDay = moment(edate).format('dddd');
                    $rootScope.bookingNumber = res.data.ClubBooking.prefix_booking_number + ("0000" + res.data.ClubBooking.booking_number).slice(-6);
                    $rootScope.matchType = res.data.ClassType.name;
                    $rootScope.courtName = res.data.Court.name;
                    $rootScope.picUrl = uploadUrl;
                }, function error(response) {
                    $scope.showAlert(1, JSON.stringify(response));
                });
            }

            var e = 0;
            $scope.addMoreExperience = function () {
                e++;
                var html = $(".experienceDiv:first").clone();
                var $html = $('<div />', {html: html});
                $html.find('select[name="coachExperience[0][from_year]"]').attr("name", "coachExperience[" + e + "][from_year]");
                $html.find('select[name="coachExperience[0][to_year]"]').attr("name", "coachExperience[" + e + "][to_year]");
                $html.find('input[name="coachExperience[0][place]"]').attr("name", "coachExperience[" + e + "][place]");
                html = $html.html();
                $("#extraExperiences").append(html);
                if (e > 0) {
                    $("#removeExperience").show();
                }
            }

            $scope.removeExperience = function () {
                e--;
                $("#extraExperiences div.experienceDiv:last").remove();
                if (e == 0) {
                    $("#removeExperience").hide();
                }
            }

            var c = 0;
            $scope.addMoreCertification = function () {
                c++;
                var html = $(".certificationDiv:first").clone();
                var $html = $('<div />', {html: html});
                $html.find('input[name="coachCertificates[0][name]"]').attr("name", "coachCertificates[" + c + "][name]");
                $html.find('input[name="coachCertificates[0][attachment]"]').attr("name", "coachCertificates[" + c + "][attachment]");
                $html.find('img[name="coachCertificates[0][image]"]').attr("name", "coachCertificates[" + c + "][image]");
                $html.find('img[name="coachCertificates[' + c + '][image]"]').attr("src", "");
                html = $html.html();
                $("#extraCertifications").append(html);
                if (c > 0) {
                    $("#removeCertification").show();
                }
            }

            $scope.removeCertification = function () {
                c--;
                $("#extraCertifications div.certificationDiv:last").remove();
                if (c == 0) {
                    $("#removeCertification").hide();
                }
            }

            $scope.getCV = function () {
                $http({
                    method: 'post',
                    url: apiUrl + 'CoachProfile',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    data: $.param({
                        coach_id: localStorage.getItem("coach_id")
                    }),
                }).then(function success(res) {
                    if (res.data.status) {
                        $("#extraExperiences").html('');
                        $("#extraCertifications").html('');
                        $('input[name="experiance"]').val(res.data.data.Coach.experiance);
                        if (res.data.data.CoachExperiance.length > 0) {
                            $.each(res.data.data.CoachExperiance, function (i, v) {
                                if (i > 0) {
                                    $scope.addMoreExperience();
                                }
                                $('select[name="coachExperience[' + i + '][from_year]"]').val(v.from_year);
                                $('select[name="coachExperience[' + i + '][to_year]"]').val(v.to_year);
                                $('input[name="coachExperience[' + i + '][place]"]').val(v.place);
                            });
                        }
                        if (res.data.data.CoachCertificate.length > 0) {
                            $.each(res.data.data.CoachCertificate, function (i, v) {
                                if (i > 0) {
                                    $scope.addMoreCertification();
                                }
                                $('input[name="coachCertificates[' + i + '][name]"]').val(v.name);
                                $('img[name="coachCertificates[' + i + '][image]"]').attr("src", uploadUrl + v.attachement);
                            });
                        }
                    } else {
                        $scope.showAlert(1, res.message);
                    }
                }, function error(response) {
                    $scope.showAlert(1, response);
                });
            }

            $scope.saveCv = function () {
                var form = $("#CV")[0]; // You need to use standard javascript object here
                var formData = new FormData(form);
                formData.append("id", localStorage.getItem("coach_id"));
                $.ajax({
                    type: 'post',
                    url: apiUrl + 'saveCv',
                    data: formData,
                    contentType: false, // NEEDED, DON'T OMIT THIS (requires jQuery 1.6+)
                    processData: false,
                }).then(function success(res) {
                    if (res.status) {
                        $scope.getCV();
                        $scope.showAlert(2, "CV updated");

                    } else {
                        $scope.showAlert(1, res.message);
                    }
                }, function error(response) {
                    $scope.showAlert(1, response);
                });
            }

            $scope.openApplyClub = function () {
                $("#applyClub").show();
                $scope.remoteUrl = apiUrl + 'searchClubs/';
            }

            $scope.closeApplyClub = function () {
                $("#applyClub").hide();
            }

            $scope.openDayTimingBlock = function (id) {
                if ($("#" + id).hasClass("hide")) {
                    $("#" + id).removeClass("hide");
                    $("#" + id + " select").each(function () {
                        $(this).attr("required", true);
                    });
                } else {
                    $("#" + id).addClass("hide");
                    $("#" + id + " select").each(function () {
                        $(this).removeAttr("required");
                    });
                }

            }

            $scope.applyToClub = function () {
                $http({
                    method: 'post',
                    url: apiUrl + 'applyToClub',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    data: $("#applyToClub").serialize() + "&" + $.param({
                        coach_id: localStorage.getItem("coach_id")
                    }),
                }).then(function success(res) {
                    if (res.data.status) {
                        $scope.closeApplyClub();
                        $scope.showAlert(2, "Coach availability is applied to the club", "Applied To Club");
                    } else {
                        $scope.showAlert(1, res.message);
                    }
                }, function error(response) {
                    $scope.showAlert(1, response);
                });
            }
            
            $scope.selectedClubInfo = function(obj){
                console.log(obj);
                $("#clubId").val(obj.originalObject.id);
            }

        })

var showCoachPicture = function (input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('.club-profile-img img:first')
                    .attr('src', e.target.result);
        };
        reader.readAsDataURL(input.files[0]);
    }
}

var openBookingDetails = function (bookingId) {
    angular.element(document.getElementById('MyProfileCtrl')).scope().showBookingDetails(bookingId);
}