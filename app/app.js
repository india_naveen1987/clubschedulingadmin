'use strict';
//var apiUrl = "http://192.168.1.19/tennis/users/";
//var uploadUrl = "http://192.168.1.19/tennis/uploads/";
//var apiUrl = "http://13.228.195.165/tbwa/users/";
//var uploadUrl = "http://13.228.195.165/tbwa/uploads/";
var apiUrl = "http://localhost/tennis/users/";
var uploadUrl = "http://localhost/tennis/uploads/";
var db = '';
var calendar = '';
var notificationAjax = '';
Stripe.setPublishableKey('pk_test_g7NBeukxcCGLBRpBUXgiS66R'); //pk_test_6pRNASCoBOKtIshFeQd4XMUh
// Declare app level module which depends on views, and components

angular.module('myApp', [
    'ngRoute',
    'myApp.login',
    'myApp.dashboard',
    'myApp.bookings',
    'myApp.coaches',
    'myApp.clubsetup',
    'myApp.myprofile',
    'myApp.playerperformance',
    'myApp.promotions',
    'myApp.version',
    'ui.calendar',
    'kendo.directives',
    'datePicker',
    'angucomplete-alt',
    'myApp.groupclasses',
    'myApp.clubmembers',
    'myApp.register',
    'myApp.leaderboard',
    'myApp.reports'
]).
    config(function ($locationProvider, $routeProvider, $httpProvider) {
        $locationProvider.hashPrefix('!');
        $httpProvider.interceptors.push('myInterceptor');
        $routeProvider.otherwise({ redirectTo: '/login' });
        if (localStorage.getItem("user_id") === null || localStorage.getItem("user_id") === 0 || localStorage.getItem("user_id") === '') {
            window.location.href = '#!/login';
        }
    })

    .factory('myInterceptor',
    function ($q, $rootScope) {
        var interceptor = {
            'request': function (config) {
                $("#mydiv").show();
                // Successful request method
                return config; // or $q.when(config);
            },
            'response': function (response) {
                $("#mydiv").hide();
                // successful response
                return response; // or $q.when(config);
            },
            'requestError': function (rejection) {
                $("#mydiv").hide();
                // an error happened on the request
                // if we can recover from the error
                // we can return a new request
                // or promise
                return response; // or new promise
                // Otherwise, we can reject the next
                // by returning a rejection
                // return $q.reject(rejection);
            },
            'responseError': function (rejection) {
                $("#mydiv").hide();
                // an error happened on the request
                // if we can recover from the error
                // we can return a new response
                // or promise
                return rejection; // or new promise
                // Otherwise, we can reject the next
                // by returning a rejection
                // return $q.reject(rejection);
            }
        };
        return interceptor;
    })

    .run(function ($rootScope, $templateCache) {
        $rootScope.$on('$routeChangeStart', function (event, next, current) {
            if (typeof (current) !== 'undefined') {
                $templateCache.remove(current.templateUrl);
            }
        });
    })

    .directive('compileMe', function ($compile) {
        return {
            template: '',
            scope: {
                htmlToBind: '=',
                variables: '='
            },
            link: function (scope, element, attrs) {
                var content = angular.element(scope.htmlToBind);
                var compiled = $compile(content);
                element.append(content);
                compiled(scope);
            }
        };
    })



    .directive("addExternalHtml", function ($templateRequest, $compile) {
        return {
            scope: {
                htmlToBind: '=',
                variables: '='
            },
            link: function (scope, element, attrs) {
                scope.name = attrs.foo;
                scope.show = false;
                $templateRequest("foo.html").then(function (html) {
                    element.append($compile(html)(scope));
                });
            }
        };
    })

    .filter('formatTime', function ($filter) {
        return function (time, format) {
            var parts = time.split(':');
            var date = new Date(0, 0, 0, parts[0], parts[1], parts[2]);
            return $filter('date')(date, format || 'h:mm a');
        }
    })

    .filter('formatTimeHour', function ($filter) {
        return function (time, format) {
            var parts = time.split(':');
            var date = new Date(0, 0, 0, parts[0], parts[1], parts[2]);
            return $filter('date')(date, format || 'h');
        }
    })

    .filter('formatTimeAm', function ($filter) {
        return function (time, format) {
            var parts = time.split(':');
            var date = new Date(0, 0, 0, parts[0], parts[1], parts[2]);
            return $filter('date')(date, format || 'a');
        }
    })

    .filter('formatDropDownTime', function ($filter) {
        return function (time, format) {
            var date = new Date(0, 0, 0, time, 0, 0);
            return $filter('date')(date, format || 'h a');
        }
    })

    .filter('formatDate', function ($filter) {
        return function (date, format) {
            var parts = date.split(' ');
            var date = new Date(parts[0]);
            return $filter('date')(date, format || 'dd MM yyyy');
        }
    })

    .filter('showDayName', function ($filter) {
        return function (day) {
            if (day == 0) {
                var dayName = 'Sunday';
            } else if (day == 1) {
                var dayName = 'Monday';
            } else if (day == 2) {
                var dayName = 'Tuesday';
            } else if (day == 3) {
                var dayName = 'Wednesday';
            } else if (day == 4) {
                var dayName = 'Thursday';
            } else if (day == 5) {
                var dayName = 'Friday';
            } else if (day == 6) {
                var dayName = 'Saturday';
            }
            return dayName;
        }
    })

    .filter('daydiff', function ($filter) {
        return function (second, first) {
            var current = new Date();
            var first = new Date(first);
            var timeDiff = Math.round(current.getTime() - first.getTime());
            var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
            if (diffDays <= 0) {
                return 0;
            } else {
                first = current;
            }
            var second = new Date(second);
            var diff = Math.round(((second - first) / (1000 * 60 * 60 * 24)) + 1);
            if (diff <= 0) {
                return 0;
            } else {
                return diff
            }
        }

    })


    .controller('myAppCtrl', function ($scope, $location, $rootScope, $http, $timeout, uiCalendarConfig, $compile, $filter) {
        if (localStorage.getItem("user_id") === null || localStorage.getItem("user_id") === 0 || localStorage.getItem("user_id") === '') {
            window.location.href = '#!/login';
        }
        //db = window.openDatabase("Ludo", "1.0", "Ludo", 2 * 1024 * 1024);
        $rootScope.role_id = localStorage.getItem('role_id');
        $rootScope.userName = localStorage.getItem('userName');
        $scope.remoteUrl = '';
        $scope.addFormPlayers = [];
        $scope.search = {};
        $scope.totalRatingRange = [];
        $scope.cuurentMenu = '';
        $scope.minDate = moment(new Date());
        for (var i = 1; i <= 16; i++) {
            $scope.totalRatingRange.push(i);
        }

        var slider = '';
        var k = 0;
        var table1;
        var j = 0;
        var table2;
        $scope.uploadUrl = uploadUrl;

        $scope.goToPage = function (page) {
            window.location.href = '#!/' + page;
            window.location.reload(true);
        }

        $scope.showAlert = function (type, message, title) {
            $("#errMsg").text(message);
            if (title != '' && title != undefined) {
                $("#errTitle").text(title);
            } else if (type == 1) {
                $("#errTitle").text("Error");
            } else {
                $("#errTitle").text("Changes saved");
            }
            $(".overlay").removeClass("hide");
            $("#boxAlert").removeClass("hide");
            //alert(message);
        }




        $scope.calculateAge = function (birthday) { // birthday is a date
            birthday = new Date(birthday);
            var ageDifMs = Date.now() - birthday.getTime();
            var ageDate = new Date(ageDifMs); // miliseconds from epoch
            return Math.abs(ageDate.getUTCFullYear() - 1970);
        }

        $scope.closeErrPopup = function () {
            $(".overlay").addClass("hide");
            $(".box-main").addClass("hide");
        }

        $scope.showConfirm = function (msg, id, func, title) {
            $(".overlay").removeClass("hide");
            $("#boxConfirm").removeClass("hide");
            if (title != '') {
                $("#errConfirmTitle").text(title);
            } else {
                $("#errConfirmTitle").text("Confirm");
            }
            $("#errConfirmMsg").text(msg);
            $("#boxConfirm").attr("func-name", func.toString());
            $("#boxConfirm").attr("func-id", id);
            /*if (confirm(msg)) {
             func(id);
             }*/
        }

        $scope.getConfirm = function () {
            var funcName = $("#boxConfirm").attr("func-name");
            var funcId = $("#boxConfirm").attr("func-id");
            $scope.$broadcast(funcName, { id: funcId })
            $(".overlay").addClass("hide");
            $(".box-main").addClass("hide");
        }

        $scope.logOut = function () {
            $rootScope.userName = '';
            localStorage.removeItem('user_id');
            clearInterval(notificationAjax);
            window.location.href = '#!/login';
        }

        $scope.openAddForm = function () {
            $scope.openAddCoachBox();
            $scope.teamNumber = 0;
            $scope.addFormPlayers = [];
            $scope.remoteUrl = apiUrl + 'getSearchPlayers/' + $scope.teamNumber + '/' + localStorage.getItem('club_id') + '/3/';
            $scope.remoteUrlBookerInfo = apiUrl + 'getSearchPlayers/' + $scope.teamNumber + '/' + localStorage.getItem('club_id') + '/2/';
            $scope.closeAllForms('rightSideBarAddForm');
            $scope.steams = [];
            $scope.teams = [];
            $("#addEventBooking input").each(function () {
                $(this).val('');
            });
            $("#addEventBooking select").each(function () {
                $(this).val('');
            });

            var endDate = new Date();
            endDate.setDate(endDate.getDate() + parseInt($rootScope.generalSettings.advance_booking));
            $("#bookAddDate").datepicker({
                autoclose: true,
                startDate: new Date(),
                endDate: endDate
            });
            //$scope.rightSideBarAddForm = true;
            //$("#rightSideBarAddForm").animate({ width: 'toggle' }, 350);
            ////$(".app-content-body").addClass("sideBarOn");
            ////$(".button-bottom").addClass("sideBarOn");

            $("#chooseForm option[value='2']").attr("selected", false);
            $("#chooseForm option[value='1']").attr("selected", true);
            $("#chooseForm").val(1);
            $rootScope.courtFee = 0;
            $rootScope.taxRate = 0;
            $rootScope.totalCoachFee = 0;
            $rootScope.subTotal = 0;
            $rootScope.taxAmt = 0;
            $rootScope.grandTotal = 0;
            $rootScope.totalClassPlayer = 0;
            $rootScope.perHeadFee = 0;
        }

        $scope.openAddCoachBox = function () {
            $("#collapseAddOne").collapse('toggle');
            if ($("#coachAddInfoSign").hasClass("reguler-plus")) {
                $("#coachAddInfoSign").removeClass("reguler-plus").addClass("reguler-minus");
            } else {
                $("#coachAddInfoSign").removeClass("reguler-minus").addClass("reguler-plus");
            }
        }

        $scope.openAddPlayerBox = function () {
            $("#collapseAddTwo").collapse('toggle');
            if ($("#playerAddInfoSign").hasClass("reguler-plus")) {
                $("#playerAddInfoSign").removeClass("reguler-plus").addClass("reguler-minus");
            } else {
                $("#playerAddInfoSign").removeClass("reguler-minus").addClass("reguler-plus");
            }
        }



        $scope.selectedBookerInfo = function (item) {
            console.log(item);

            //$("#AddMembers_value").blur();
            $http({
                method: 'post',
                url: apiUrl + 'getPlayerInfo',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: $.param({
                    playerId: item.originalObject.id
                }),
            }).then(function success(res) {
                if (res.data.status) {
                    console.log(res.data.data[0])
                    $("#playerAddFirstName").val(res.data.data[0].Player.first_name);
                    $("#playerAddLastName").val(res.data.data[0].Player.last_name);
                    $("#playerAddContactNo").val(res.data.data[0].Player.phone);
                    $("#playerAddEmail").val(res.data.data[0].User.email);
                    $("#playerEditFirstName").val(res.data.data[0].Player.first_name);
                    $("#playerEditLastName").val(res.data.data[0].Player.last_name);
                    $("#playerEditContactNo").val(res.data.data[0].Player.phone);
                    $("#playerEditEmail").val(res.data.data[0].User.email);
                    $("#AddMembers_value").val('');
                } else {
                    $scope.showAlert(1, "Can not found information");
                }
            }, function error(response) {
                // do nothing
            });

        }

        $scope.selectedAddMember = function (item) {
            //console.log(item);

            /*if ($("#totalAddPlayer").val() != '') { */
            if ((parseInt($("#totalAddPlayer").val()) - 1) > $scope.addFormPlayers.length) {
                console.log(item);

                $http({
                    method: 'post',
                    url: apiUrl + 'getPlayerInfo',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    data: $.param({
                        playerId: item.originalObject.id,
                        playersIds: $scope.addFormPlayers
                    }),
                }).then(function success(res) {
                    if (res.data.status) {
                        $("#AddAppMembers_value").val('');
                        //$('#AddAppMembers_value').blur();
                        $('#AddAppMembers_value').on("blur", function () {
                            $timeout(function () {
                                $(this).val('');
                            }, 1000);
                        });

                        if ($.inArray(item.originalObject.id, $scope.addFormPlayers) == -1) {
                            $scope.addFormPlayers.push(item.originalObject.id);
                            $scope.teams = res.data.data;
                            console.log($scope.addFormPlayers);
                            console.log($scope.teams);
                            console.log($scope.addFormPlayers.join(","))
                        } else {
                            $scope.showAlert(1, "This player already added");
                        }
                    } else {
                        $scope.showAlert(1, "Can not add player");
                    }


                }, function error(response) {
                    // do nothing
                });
            } else {
                $scope.showAlert(1, "Only " + (parseInt($("#totalAddPlayer").val()) - 1) + " players can be add");
            }

        }

        $scope.closeRightSideBarAddForm = function () {
            $("#rightSideBarAddForm").hide();
            //$scope.rightSideBarAddForm = false;
            //$(".app-content-body").removeClass("sideBarOn");
            //$(".button-bottom").removeClass("sideBarOn");
        }

        $scope.removeAddPlayer = function (id) {
            $scope.showConfirm("Are you sure1?", id, "confirmRemoveAddPlayer");
        }

        $scope.$on('confirmRemoveAddPlayer', function (ev, args) {
            var index = $scope.addFormPlayers.indexOf(args.id);
            $scope.addFormPlayers.splice(index, 1);
            $("#teamAdd" + args.id).remove();
            $("#steamAdd" + args.id).remove();
        });
        $scope.addEventBooking = function () {
            console.log($scope.addFormPlayers);
            if (parseInt($("#totalAddPlayer").val()) > 1 && ($scope.addFormPlayers.length == 0 || $scope.addFormPlayers == '')) {
                $scope.showAlert(1, "Please add app users");
                return false;
            } else if (($scope.addFormPlayers.length) < (parseInt($("#totalAddPlayer").val()) - 1)) {
                $scope.showAlert(1, "App users must be equal to total number of players");
                return false;
            } else if ($("#totalAddPlayer").val() == '') {
                $("#totalAddPlayer").val(1);
            }
            if ($scope.addFormPlayers != '') {
                var params = $.param({
                    book_date: $("#bookAddDate").val(),
                    court_id: $("#eventAddCourt").val(),
                    start_hours: $("#eventAddStartHours").val(),
                    start_minutes: $("#eventAddStartMinutes").val(),
                    end_hours: $("#eventAddEndHours").val(),
                    end_minutes: $("#eventAddEndMinutes").val(),
                    first_name: $("#playerAddFirstName").val(),
                    last_name: $("#playerAddLastName").val(),
                    phone: $("#playerAddContactNo").val(),
                    booked_contact: $("#playerAddContactNo").val(),
                    email: $("#playerAddEmail").val(),
                    booked_email: $("#playerAddEmail").val(),
                    coach_id: $("#eventAddCoach").val(),
                    sub_coach_id: $("#eventAddSubCoach").val(),
                    club_id: localStorage.getItem('club_id'),
                    user_id: localStorage.getItem('user_id'),
                    player_ids: $scope.addFormPlayers.join(","),
                    no_player: $("#totalAddPlayer").val(),
                    promo_code: $("#playerAddPromoCode").val()
                });
            } else {
                var params = $.param({
                    book_date: $("#bookAddDate").val(),
                    court_id: $("#eventAddCourt").val(),
                    start_hours: $("#eventAddStartHours").val(),
                    start_minutes: $("#eventAddStartMinutes").val(),
                    end_hours: $("#eventAddEndHours").val(),
                    end_minutes: $("#eventAddEndMinutes").val(),
                    first_name: $("#playerAddFirstName").val(),
                    last_name: $("#playerAddLastName").val(),
                    phone: $("#playerAddContactNo").val(),
                    booked_contact: $("#playerAddContactNo").val(),
                    email: $("#playerAddEmail").val(),
                    booked_email: $("#playerAddEmail").val(),
                    coach_id: $("#eventAddCoach").val(),
                    sub_coach_id: $("#eventAddSubCoach").val(),
                    club_id: localStorage.getItem('club_id'),
                    user_id: localStorage.getItem('user_id'),
                    no_player: $("#totalAddPlayer").val(),
                    promo_code: $("#playerAddPromoCode").val()
                });
            }

            $http({
                method: 'post',
                url: apiUrl + 'bookClub',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: params,
            }).then(function success(res) {
                if (res.data.status) {
                    //$scope.showAlert(2, "Booking details added!");
                    $scope.openDetailsConfirm(res.data.data);
                } else {
                    if (res.data.title != undefined) {
                        $scope.showAlert(1, res.data.message, res.data.title);
                    } else {
                        $scope.showAlert(1, res.data.message);
                    }

                }


            }, function error(response) {
                $scope.showAlert(1, "Can not add booking details!");
            });
        }

        $scope.openDetailsConfirm = function (bookingId) {
            $http({
                method: 'post',
                url: apiUrl + 'getEventSessionDetails',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: $.param({
                    booking_id: bookingId
                }),
            }).then(function success(res) {
                res = res.data;
                $scope.closeAllForms('rightSideBarBookingDetailsConfirm');
                $scope.bookerName = res.data.ClubBookingSession.booked_first_name + " " + res.data.ClubBookingSession.booked_last_name;
                $scope.playerContactNo = res.data.ClubBookingSession.booked_contact;
                $scope.playerEmail = res.data.ClubBookingSession.booked_email;
                $scope.teamNumber = res.data.ClubBookingSession.team_number;
                $scope.remoteUrl = apiUrl + 'getSearchPlayers/' + $scope.teamNumber + '/' + localStorage.getItem('club_id') + '/1/';
                $scope.bookingId = bookingId;
                $scope.totalPlayer = res.data.ClubBookingSession.no_player;
                $scope.teams = [];
                $scope.teams = res.data.Team;
                $scope.coachName = res.data.Coach.name;
                $scope.subCoachName = res.data.SubCoach.name;
                $scope.startTime = res.data.ClubBookingSession.book_start_time;
                $scope.endTime = res.data.ClubBookingSession.book_end_time;

                $scope.clubMembershipId = res.data.Club.membership_id;
                $scope.userAppId = res.data.ClubBookingSession.user_id;
                var edate = new Date(res.data.ClubBookingSession.book_date + " " + res.data.ClubBookingSession.book_start_time);
                var eedate = new Date(res.data.ClubBookingSession.book_date + " " + res.data.ClubBookingSession.book_end_time);
                $scope.eventPlayTime = moment(edate).format('h:mma') + " - " + moment(eedate).format('h:mma');
                $scope.courtName = res.data.Court.name;
                $scope.start = res.data.ClubBookingSession.book_date;
                $scope.eventDate = moment(edate).format('Do MMMM YYYY');
                $scope.eventDay = moment(edate).format('dddd');
                $scope.bookingNumber = res.data.ClubBookingSession.prefix_booking_number + ("0000" + res.data.ClubBookingSession.booking_number).slice(-6);
                $scope.matchType = res.data.ClassType.name;
                $scope.nonMemberFees = res.data.ClubBookingSession.non_member_fee;
                $scope.picUrl = uploadUrl;
                if (res.feesArray != '') {
                    $rootScope.coachBookingFee = res.feesArray.total_coach_fees;
                    $rootScope.courtBookingFee = res.feesArray.total_court_fees;
                    $rootScope.promoDiscount = res.promoPrice;
                    $rootScope.subTotal = ($rootScope.coachBookingFee + $rootScope.courtBookingFee + parseFloat(res.data.ClubBookingSession.non_member_fee)) - $rootScope.promoDiscount;
                    $rootScope.taxFee = res.feesArray.tax_rate;
                    $rootScope.taxAmount = ($rootScope.subTotal * $rootScope.taxFee) / 100;
                    $rootScope.grandTotal = $rootScope.subTotal + $rootScope.taxAmount;
                } else {
                    $rootScope.courtBookingFee = res.data.ClubBookingSession.court_booking_fee;
                    $rootScope.coachBookingFee = res.data.ClubBookingSession.coach_booking_fee;
                    $rootScope.subTotal = res.data.ClubBookingSession.sub_total;
                    $rootScope.taxFee = res.data.ClubBookingSession.tax_fee;
                    $rootScope.taxAmount = res.data.ClubBookingSession.tax_amount;
                    $rootScope.grandTotal = res.data.ClubBookingSession.grand_total;
                    $rootScope.promoDiscount = 0;
                }
            }, function error(response) {
                $scope.showAlert(1, response);
            });
        }



        $scope.closeRightSideBarBookingDetailsConfirm = function () {
            $scope.closeAllForms('rightSideBarAddForm');
            $("#chooseForm option[value='2']").attr("selected", false);
            $("#chooseForm option[value='1']").attr("selected", true);
        }

        $scope.makePayment = function (type, bookingId, grandTotal) {

            if (type == 1 || type == 3) {
                $http({
                    method: 'post',
                    url: apiUrl + 'makePayment',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    data: $.param({
                        booking_id: bookingId,
                        payment_type: type
                    }),
                }).then(function success(res) {
                    res = res.data;
                    if (res.status) {
                        $scope.bookingConfirm(res.data);
                    } else {
                        $scope.showAlert(1, res.message);
                    }
                }, function error(response) {
                    $scope.showAlert(1, response);
                });
            } else if (type == 2) {
                $scope.closeAllForms('rightSideBarPaymentForm');
                $scope.bookingId = bookingId;
                $scope.grandTotal = grandTotal;
            }
        }

        $scope.confirmCardPayment = function (bookingId) {
            var $form = $('#payment-form');
            console.log(Stripe)
            Stripe.card.createToken($form, function (error, response) {
                console.log(error);
                console.log(response);
                if (error != 200) {
                    $scope.showAlert(1, response.error.message);
                } else {
                    $http({
                        method: 'post',
                        url: apiUrl + 'makePayment',
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded'
                        },
                        data: $.param({
                            booking_id: bookingId,
                            payment_type: 2,
                            card_type: response.card.brand,
                            card_number: response.card.last4,
                            card_name: response.card.name
                        }),
                    }).then(function success(res) {
                        res = res.data;
                        if (res.status) {
                            $scope.paymentGateway(response, bookingId);
                        } else {
                            $scope.showAlert(1, res.message);
                        }
                    }, function error(response) {
                        $scope.showAlert(1, response);
                    });
                }
            });
            return false;
        }

        $scope.paymentGateway = function (payment, bookingId, type, diffamount) {
            if (type == undefined) {
                type = 1;
            }
            if (diffamount == undefined) {
                diffamount = 0;
            }
            $http({
                method: 'post',
                url: apiUrl + 'chargeCard',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: $.param({
                    booking_id: bookingId,
                    payment: payment,
                    type: type,
                    diffamount: diffamount
                }),
            }).then(function success(res) {
                res = res.data;
                if (res.status) {
                    $scope.bookingConfirm(res.data);
                } else {
                    $scope.showAlert(1, res.message);
                }
            }, function error(response) {
                $scope.showAlert(1, response);
            });
        }

        $scope.bookingConfirm = function (bookingId) {
            $http({
                method: 'post',
                url: apiUrl + 'getEventDetails',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: $.param({
                    booking_id: bookingId
                }),
            }).then(function success(res) {
                res = res.data;
                $scope.closeAllForms('rightSideBarPrint');
                $scope.bookerName = res.data.ClubBooking.booked_first_name + " " + res.data.ClubBooking.booked_last_name;
                $scope.playerContactNo = res.data.ClubBooking.booked_contact;
                $scope.playerEmail = res.data.ClubBooking.booked_email;
                $scope.teamNumber = res.data.ClubBooking.team_number;
                $scope.remoteUrl = apiUrl + 'getSearchPlayers/' + $scope.teamNumber + '/' + localStorage.getItem('club_id') + '/1/';
                $scope.bookingId = bookingId;
                $scope.totalPlayer = res.data.ClubBooking.no_player;
                $scope.teams = res.data.Team;
                $scope.coachName = res.data.Coach.name;
                $scope.subCoachName = res.data.SubCoach.name;
                $scope.startTime = res.data.ClubBooking.book_start_time;
                $scope.endTime = res.data.ClubBooking.book_end_time;
                $scope.paymentMode = res.data.ClubBooking.payment_mode;
                $scope.clubMembershipId = res.data.Club.membership_id;
                $scope.userAppId = res.data.ClubBooking.user_id;
                $scope.paymentDate = res.data.ClubBooking.payment_date;
                $scope.paymentMode = res.data.ClubBooking.payment_mode;
                $scope.cardType = res.data.Transaction.card_type;
                $scope.cardNumber = res.data.Transaction.card_number;
                $scope.cardName = res.data.Transaction.card_name;
                var edate = new Date(res.data.ClubBooking.book_date + " " + res.data.ClubBooking.book_start_time);
                var eedate = new Date(res.data.ClubBooking.book_date + " " + res.data.ClubBooking.book_end_time);
                $scope.eventPlayTime = moment(edate).format('h:mma') + " - " + moment(eedate).format('h:mma');
                $scope.courtName = res.data.Court.name;
                $scope.start = res.data.ClubBooking.book_date;
                $scope.eventDate = moment(edate).format('Do MMMM YYYY');
                $scope.eventDay = moment(edate).format('dddd');
                $scope.bookingNumber = res.data.ClubBooking.prefix_booking_number + ("0000" + res.data.ClubBooking.booking_number).slice(-6);
                $scope.matchType = res.data.ClassType.name;
                $scope.courtName = res.data.Court.name;
                $scope.picUrl = uploadUrl;
                $scope.clubName = res.data.Club.name;
                $scope.clubAddress = res.data.Club.address_1 + ", " + res.data.Club.address_2;
                $scope.clubContact = res.data.Club.contact;
                $scope.clubProfilePicture = res.data.Club.profile_picture;
                $scope.nonMemberFees = res.data.ClubBooking.non_member_fee;
                if (res.feesArray != '') {
                    $scope.coachBookingFee = res.feesArray.total_coach_fees;
                    $scope.courtBookingFee = res.feesArray.total_court_fees;
                    $scope.promoDiscount = res.promoPrice;
                    $scope.subTotal = ($scope.coachBookingFee + $scope.courtBookingFee + parseInt(res.data.ClubBooking.non_member_fee)) - $scope.promoDiscount;
                    $scope.taxFee = res.feesArray.tax_rate;
                    $scope.taxAmount = ($scope.subTotal * $scope.taxFee) / 100;
                    $scope.grandTotal = $scope.subTotal + $scope.taxAmount;
                } else {
                    $scope.courtBookingFee = res.data.ClubBooking.court_booking_fee;
                    $scope.coachBookingFee = res.data.ClubBooking.coach_booking_fee;
                    $scope.subTotal = res.data.ClubBooking.sub_total;
                    $scope.taxFee = res.data.ClubBooking.tax_fee;
                    $scope.taxAmount = res.data.ClubBooking.tax_amount;
                    $scope.grandTotal = res.data.ClubBooking.grand_total;
                    $scope.promoDiscount = 0;
                }
                $scope.$broadcast("setCalendar", { id: localStorage.getItem('club_id'), court_id: '', type: 2 });
            }, function error(response) {
                $scope.showAlert(1, response);
            });
        }

        $scope.closeRightSideBarPaymentForm = function () {
            $scope.closeAllForms('rightSideBarBookingDetailsConfirm');
        }

        $scope.closeRightSideBarPrint = function () {
            $("#rightSideBarPrint").hide();
            //$(".app-content-body").removeClass("sideBarOn");
            //$(".button-bottom").removeClass("sideBarOn");
        }

        $scope.openSearchBox = function () {
            $scope.closeAllForms('rightSideBarSearch');
            $("#searchBookings input").each(function () {
                $(this).val('');
            });
            $("#searchDate").datepicker({
                autoclose: true,
            });
            //$("#rightSideBarSearch").animate({ width: 'toggle' }, 350);
            //$(".app-content-body").addClass("sideBarOn");
            //$(".button-bottom").addClass("sideBarOn");
        }

        $scope.closeRightSideBarSearch = function () {
            $("#rightSideBarSearch").hide();
            //$(".app-content-body").removeClass("sideBarOn");
            //$(".button-bottom").removeClass("sideBarOn");
        }

        $scope.searchBookings = function () {
            console.log($scope.search);
            $rootScope.searchData = '';
            $rootScope.searchData = $scope.search;
            $rootScope.searchData.bookDate = $("#searchDate").val();
            if (localStorage.getItem('role_id') > 2) {
                window.location.href = "#!/bookings";
            } else {
                if ($location.absUrl().split('#!/')[1] == 'playerperformance') {
                    $scope.contentLoading(2);
                } else {
                    window.location.href = "#!/playerperformance";
                }
            }

        }



        $scope.getEventDetails = function (bookingId) {
            $http({
                method: "POST",
                url: apiUrl + "getEventDetails",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: $.param({ booking_id: bookingId }),
            }).then(function success(eventResponse) {
                var edate = new Date(eventResponse.data.data.ClubBooking.book_date);
                $scope.eventDate = moment(edate).format('Do MMMM YYYY');
                $scope.eventDay = moment(edate).format('dddd');
                var edate = new Date(eventResponse.data.data.ClubBooking.book_date + " " + eventResponse.data.data.ClubBooking.book_start_time);
                $scope.startTimeHour = moment(edate).format('h');
                $scope.startTimeAM = moment(edate).format('a');
                var eedate = new Date(eventResponse.data.data.ClubBooking.book_date + " " + eventResponse.data.data.ClubBooking.book_end_time);
                $scope.eventPlayTime = moment(edate).format('h:mma') + " - " + moment(eedate).format('h:mma');
                $scope.matchType = eventResponse.data.data.ClassType.name;
                $scope.courtName = eventResponse.data.data.Court.name;
                $scope.bookingNumber = eventResponse.data.data.ClubBooking.prefix_booking_number + ("0000" + eventResponse.data.data.ClubBooking.booking_number).slice(-6);
                $scope.teams = eventResponse.data.data.Team;
                $scope.picUrl = uploadUrl;
                $scope.bookingId = eventResponse.data.data.ClubBooking.id;
            }, function error(eventResponse) {
                $scope.showAlert(1, 'Can not get event details!');
            });
        }

        $scope.closeRightSideBarDetails = function () {
            $("#rightSideBarDetails").hide();
        }

        $scope.selectedMemberShip = function (item) {
            $http({
                method: 'post',
                url: apiUrl + 'addTeamMember',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: $.param({
                    teamNumber: $scope.teamNumber,
                    playerId: item.originalObject.id
                }),
            }).then(function success(res) {
                if (res.data.status) {
                    if (parseInt($("#totalPlayer").val()) > $scope.teams.length) {
                        $scope.steams = res.data.data;
                        $("#members_value").val('');
                        $("#AddMembers_value").val('');
                    } else {
                        $scope.showAlert(1, "Only " + $("#totalPlayer").val() + " players can be add");
                    }
                } else {
                    $scope.showAlert(1, "Can not add player");
                }
            }, function error(response) {
                // do nothing
            });
        }

        $scope.selectedMember = function (item) {
            $http({
                method: 'post',
                url: apiUrl + 'addTeamMember',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: $.param({
                    teamNumber: $scope.teamNumber,
                    playerId: item.originalObject.id
                }),
            }).then(function success(res) {
                if (res.data.status) {
                    $scope.teamNumber = res.data.team_number;
                    if (parseInt($("#totalPlayer").val()) > $scope.teams.length) {
                        $scope.teams = res.data.data;
                        $("#members_value").val('');
                        $("#AddMembers_value").val('');
                    } else {
                        $scope.showAlert(1, "Only " + $("#totalPlayer").val() + " players can be add");
                    }
                } else {
                    $scope.showAlert(1, "Can not add player");
                }
            }, function error(response) {
                // do nothing
            });


        }

        $scope.updateEventBooking = function (bookingId) {
            $http({
                method: 'post',
                url: apiUrl + 'updateClubBook',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: $.param({
                    book_date: $("#bookDate").val(),
                    court_id: $("#eventCourt").val(),
                    start_hours: $("#eventStartHours").val(),
                    start_minutes: $("#eventStartMinutes").val(),
                    end_hours: $("#eventEndHours").val(),
                    end_minutes: $("#eventEndMinutes").val(),
                    first_name: $("#playerEditFirstName").val(),
                    last_name: $("#playerEditLastName").val(),
                    phone: $("#playerEditContactNo").val(),
                    email: $("#playerEditEmail").val(),
                    coach_id: $("#eventCoach").val(),
                    sub_coach_id: $("#eventSubCoach").val(),
                    no_player: $("#totalPlayer").val(),
                    booking_id: bookingId,
                    team_number: $scope.teamNumber
                }),
            }).then(function success(res) {
                if (res.data.status) {
                    $scope.showAlert(2, "Your booking has been made successfully.", "Booking Successful");
                    $scope.closeRightSideBarDetails();
                } else {
                    if (res.data.message == 'makePayment') {
                        var diff = res.data.data;
                        $scope.openUpdateDetailsConfirm(bookingId, diff);
                        /*$rootScope.bookingId = bookingId;
                         $rootScope.grandTotal = res.data.data;
                         $rootScope.court_id = $("#eventCourt").val();
                         $rootScope.book_date = $("#bookDate").val();
                         $rootScope.coach_id = $("#eventCoach").val();
                         $rootScope.no_player = $("#totalPlayer").val();
                         $rootScope.start_hours = $("#eventStartHours").val();
                         $rootScope.start_minutes = $("#eventStartMinutes").val();
                         $rootScope.end_hours = $("#eventEndHours").val();
                         $rootScope.end_minutes = $("#eventEndMinutes").val();
                         $(".overlay").removeClass("hide");
                         $("#rightSideBarPaymentFormPopup.box-main").removeClass("hide");*/
                    } else {
                        $scope.showAlert(1, res.data.message);
                    }
                }


            }, function error(response) {
                $scope.showAlert(1, "Can not update booking details!");
            });
        }

        $scope.openUpdateDetailsConfirm = function (bookingId, diff) {
            $http({
                method: 'post',
                url: apiUrl + 'getEventDetails',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: $.param({
                    booking_id: bookingId
                }),
            }).then(function success(res) {
                res = res.data;
                $scope.closeAllForms('rightSideBarBookingDetailsConfirmUpdate');
                $scope.bookerName = res.data.ClubBooking.booked_first_name + " " + res.data.ClubBooking.booked_last_name;
                $scope.playerContactNo = res.data.ClubBooking.booked_contact;
                $scope.playerEmail = res.data.ClubBooking.booked_email;
                $scope.teamNumber = res.data.ClubBooking.team_number;
                $scope.remoteUrl = apiUrl + 'getSearchPlayers/' + $scope.teamNumber + '/' + localStorage.getItem('club_id') + '/1/';
                $scope.bookingId = bookingId;
                $scope.totalPlayer = res.data.ClubBooking.no_player;
                $scope.teams = [];
                $scope.teams = res.data.Team;
                $scope.coachName = res.data.Coach.name;
                $scope.subCoachName = res.data.SubCoach.name;
                $scope.startTime = res.data.ClubBooking.book_start_time;
                $scope.endTime = res.data.ClubBooking.book_end_time;

                $scope.clubMembershipId = res.data.Club.membership_id;
                $scope.userAppId = res.data.ClubBooking.user_id;
                var edate = new Date(res.data.ClubBooking.book_date + " " + res.data.ClubBooking.book_start_time);
                var eedate = new Date(res.data.ClubBooking.book_date + " " + res.data.ClubBooking.book_end_time);
                $scope.eventPlayTime = moment(edate).format('h:mma') + " - " + moment(eedate).format('h:mma');
                $scope.courtName = res.data.Court.name;
                $scope.start = res.data.ClubBooking.book_date;
                $scope.eventDate = moment(edate).format('Do MMMM YYYY');
                $scope.eventDay = moment(edate).format('dddd');
                $scope.bookingNumber = res.data.ClubBooking.prefix_booking_number + ("0000" + res.data.ClubBooking.booking_number).slice(-6);
                $scope.matchType = res.data.ClassType.name;
                $scope.picUrl = uploadUrl;
                $rootScope.courtBookingFee = res.data.ClubBooking.court_booking_fee;
                $rootScope.coachBookingFee = res.data.ClubBooking.coach_booking_fee;
                $rootScope.subTotal = res.data.ClubBooking.sub_total;
                $rootScope.taxFee = res.data.ClubBooking.tax_fee;
                $rootScope.taxAmount = res.data.ClubBooking.tax_amount;
                $rootScope.grandTotal = res.data.ClubBooking.grand_total;
                $rootScope.promoDiscount = 0;
                $rootScope.diffTotal = diff;
                $rootScope.nonMemberFees = res.data.ClubBooking.non_member_fee;
            }, function error(response) {
                $scope.showAlert(1, response);
            });
        }

        $scope.makePaymentUpdate = function (type, bookingId, grandTotal) {
            if (type == 1 || type == 3) {

                $http({
                    method: 'post',
                    url: apiUrl + 'makePaymentUpdate',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    data: $.param({
                        booking_id: bookingId,
                        payment_type: type
                    }),
                }).then(function success(res) {
                    res = res.data;
                    if (res.status) {
                        $scope.showAlert(2, "Your booking has been made successfully.", "Booking Successful");
                        $scope.closeRightSideBarBookingDetailsConfirmUpdate();
                        $scope.closeRightSideBarDetails();
                    } else {
                        $scope.showAlert(1, res.message);
                    }
                }, function error(response) {
                    $scope.showAlert(1, response);
                });
            } else if (type == 2) {
                $scope.closeAllForms('rightSideBarPaymentFormUpdate');
                $scope.bookingId = bookingId;
                $scope.grandTotal = grandTotal;
            }
        }

        $scope.closeRightSideBarBookingDetailsConfirmUpdate = function () {
            $("#rightSideBarBookingDetailsConfirmUpdate").hide();
        }

        $scope.closeRightSideBarPaymentFormUpdate = function () {
            $("#rightSideBarPaymentFormUpdate").hide();
        }

        $scope.confirmCardPaymentUpdate = function (bookingId) {
            var $form = $('#payment-form-update');
            console.log(Stripe)
            Stripe.card.createToken($form, function (error, response) {
                console.log(error);
                console.log(response);
                if (error != 200) {
                    $scope.showAlert(1, response.error.message);
                } else {
                    $http({
                        method: 'post',
                        url: apiUrl + 'makePaymentUpdate',
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded'
                        },
                        data: $.param({
                            booking_id: bookingId,
                            payment_type: 2,
                            card_type: response.card.brand,
                            card_number: response.card.last4,
                            card_name: response.card.name
                        }),
                    }).then(function success(res) {
                        res = res.data;
                        if (res.status) {
                            $scope.paymentGateway(response, bookingId, 2, $rootScope.diffTotal);
                        } else {
                            $scope.showAlert(1, res.message);
                        }
                    }, function error(response) {
                        $scope.showAlert(1, response);
                    });
                }
            });
            return false;
        }

        $scope.removePlayer = function (id) {
            $scope.showConfirm("Are you sure?", id, "confirmRemovePlayer");
        }

        $scope.$on('confirmRemovePlayer', function (ev, args) {
            var id = args.id;
            $http({
                method: 'post',
                url: apiUrl + 'removeTeamMember',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: $.param({
                    id: id,
                    teamNumber: $scope.teamNumber
                }),
            }).then(function success(res) {
                if (res.data.status) {
                    var index = $scope.teams.indexOf(id);
                    $scope.teams.splice(index, 1);
                    $("#team" + id).remove();
                    $("#steam" + id).remove();
                } else {
                    $scope.showAlert(1, res.data.message);
                }
            }, function error(response) {
                $scope.showAlert(1, response);
            });

        });
        $scope.getNumber = function (num) {
            return new Array(num);
        }

        $scope.getHoursMinutes = function (time) {
            var stime = time.split(":");
            var sh = stime[0];
            var sm = 0;
            if (stime <= 15) {
                sm = 15;
            } else if (stime <= 30) {
                sm = 30;
            } else if (stime <= 45) {
                sm = 45;
            } else {
                sm = 0;
            }
            return sh + ":" + sm;
        }

        $scope.openPlayerBox = function () {
            $("#collapseTwo").collapse('toggle');
            if ($("#playerInfoSign").hasClass("reguler-plus")) {
                $("#playerInfoSign").removeClass("reguler-plus").addClass("reguler-minus");
            } else {
                $("#playerInfoSign").removeClass("reguler-minus").addClass("reguler-plus");
            }
        }

        $scope.openCoachBox = function () {
            $("#collapseOne").collapse('toggle');
            if ($("#coachInfoSign").hasClass("reguler-plus")) {
                $("#coachInfoSign").removeClass("reguler-plus").addClass("reguler-minus");
            } else {
                $("#coachInfoSign").removeClass("reguler-minus").addClass("reguler-plus");
            }
        }



        $scope.getRightSideBarDetails = function (bookingId) {
            $http({
                method: 'post',
                url: apiUrl + 'getEventDetails',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: $.param({
                    booking_id: bookingId
                }),
            }).then(function success(res) {
                res = res.data;
                $scope.closeAllForms('rightSideBarBookingDetails');
                $scope.playerBookerName = res.data.ClubBooking.booked_first_name + " " + res.data.ClubBooking.booked_last_name;
                $scope.playerContactNo = res.data.ClubBooking.booked_contact;
                $scope.playerEmail = res.data.ClubBooking.booked_email;
                $scope.teamNumber = res.data.ClubBooking.team_number;
                $scope.remoteUrl = apiUrl + 'getSearchPlayers/' + $scope.teamNumber + '/' + localStorage.getItem('club_id') + '/1/';
                $scope.bookingId = bookingId;
                $scope.totalPlayer = res.data.ClubBooking.no_player;
                $scope.teams = res.data.Team;
                $scope.coachName = res.data.Coach.name;
                $scope.subCoachName = res.data.SubCoach.name;
                $scope.startTime = res.data.ClubBooking.book_start_time;
                $scope.endTime = res.data.ClubBooking.book_end_time;
                $scope.courtBookingFee = res.data.ClubBooking.court_booking_fee;
                $scope.coachBookingFee = res.data.ClubBooking.coach_booking_fee;
                $scope.subTotal = res.data.ClubBooking.sub_total;
                $scope.taxFee = res.data.ClubBooking.tax_fee;
                $scope.taxAmount = res.data.ClubBooking.tax_amount;
                $scope.grandTotal = res.data.ClubBooking.grand_total;
                $scope.paymentMode = res.data.ClubBooking.payment_mode;
                $scope.clubMembershipId = res.data.Club.membership_id;
                $scope.userAppId = res.data.ClubBooking.user_id;
                $scope.paymentDate = res.data.ClubBooking.payment_date;
                $scope.cardType = res.data.Transaction.card_type;
                $scope.cardNumber = res.data.Transaction.card_number;
                $scope.cardName = res.data.Transaction.card_name;
                var edate = new Date(res.data.ClubBooking.book_date + " " + res.data.ClubBooking.book_start_time);
                var eedate = new Date(res.data.ClubBooking.book_date + " " + res.data.ClubBooking.book_end_time);
                $scope.eventPlayTime = moment(edate).format('h:mma') + " - " + moment(eedate).format('h:mma');
                $scope.courtName = res.data.Court.name;
                $scope.start = res.data.ClubBooking.book_date;
                $scope.eventDate = moment(edate).format('Do MMMM YYYY');
                $scope.eventDay = moment(edate).format('dddd');
                $scope.bookingNumber = res.data.ClubBooking.prefix_booking_number + ("0000" + res.data.ClubBooking.booking_number).slice(-6);
                $scope.matchType = res.data.ClassType.name;
                $scope.courtName = res.data.Court.name;
                $scope.picUrl = uploadUrl;
            }, function error(response) {
                $scope.showAlert(1, response);
            });
        }

        $scope.closeRightSideBarBookingDetails = function () {
            $("#rightSideBarBookingDetails").hide();
        }

        $scope.cancelBooking = function () {
            $scope.closeAllForms('rightSideBarCancelBooking');
            $("#reasonText").val('');
        }

        $scope.closeRightSideBarCancelBooking = function () {
            $scope.closeAllForms('rightSideBarBookingDetails');
        }

        $scope.confirmCancelBooking = function () {
            if ($("#reasonText").val() == '') {
                $scope.showAlert(1, "Please enter reason");
            } else {
                $http({
                    method: 'post',
                    url: apiUrl + 'deleteClubBooking',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    data: $.param({
                        booking_id: $scope.bookingId,
                        user_id: localStorage.getItem('user_id'),
                        reason: $("#reasonText").val()
                    }),
                }).then(function success(res) {
                    if (res.status) {
                        $("#upcomingBookings").removeClass("active");
                        $scope.$broadcast("setCalendar", { id: localStorage.getItem('club_id'), court_id: localStorage.getItem('court_id'), type: 2 });
                        $scope.$broadcast("showUpcomingBookings");
                        $scope.showAlert(2, "This booking has been cancelled successfully. All players involved will be notified. ", "Booking Cancelled");
                        $scope.closeRightSideBarCancelBooking();
                        $scope.closeRightSideBarBookingDetails();
                        $scope.closeRightSideBar();
                    }
                }, function error(response) {
                    $scope.showAlert(1, response);
                });
            }
        }

        $scope.printReciept = function () {
            /*alert("here")
             var myWindow = $("#rightSideBarPrint").html();
             window.print();*/
            //Get the HTML of div
            /*    var divElements = document.getElementById("rightSideBarPrint").innerHTML;
             //Get the HTML of whole page
             var oldPage = document.body.innerHTML;
             
             //Reset the page's HTML with div's HTML only
             document.body.innerHTML =
             "<html><head><title></title></head><body>" +
             divElements + "</body>";
             
             //Print Page
             window.print();
             
             //Restore orignal HTML
             document.body.innerHTML = oldPage; */
            var divToPrint = document.getElementById('rightSideBarPrint');
            var popupWin = window.open('', '_blank', 'width=400,height=400');
            //popupWin.document.open();
            popupWin.document.write(
                /*'<html>'+
                '<head>'+
                '<link rel="stylesheet" href="css/style.css">'+
                '<link rel="stylesheet" href="css/fontawesome.css">'+
            
                '<link rel="stylesheet" href="css/font-awesome.min.css">'+
                '<link rel="stylesheet" href="css/bootstrap.css">'+*/
                '<body>' + divToPrint.outerHTML + '</head></html>'
            );
            popupWin.print();
            popupWin.close();

        }



        $scope.getCourtFee = function ($court) {
            $http({
                method: 'post',
                url: apiUrl + 'getCourtDetails',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: $.param({
                    court_id: $court,
                }),
            }).then(function success(res) {
                if (res.data.status) {
                    console.log(res.data)
                    $scope.courtFee = parseFloat(res.data.data.Court.rate);
                    $scope.taxRate = parseFloat(res.data.data.Club.tax_rate);
                    if ($scope.totalCoachFee != undefined) {
                        $scope.subTotal = $scope.totalCoachFee + $scope.courtFee;
                    } else {
                        $scope.subTotal = $scope.courtFee;
                    }
                    if ($scope.subTotal != undefined) {
                        $scope.taxAmt = ($scope.subTotal * $scope.taxRate) / 100;
                    } else {
                        $scope.taxAmt = 0;
                    }
                    $scope.grandTotal = parseFloat($scope.subTotal) + parseFloat($scope.taxAmt);
                    $scope.perHeadFee = $scope.grandTotal / $scope.totalClassPlayer;
                } else {
                    $scope.showAlert(1, res.data.message);
                }
            }, function error(response) {
                $scope.showAlert(1, response);
            });
        }

        $scope.getCoachFee = function ($coach) {
            if ($("#classStartHour").val() == "") {
                $("#classCoach").val("");
                $scope.showAlert(1, "Please choose class start hour");
            } else if ($("#classStartMinute").val() == "") {
                $("#classCoach").val("");
                $scope.showAlert(1, "Please choose class start minute");
            } else if ($("#classEndHour").val() == "") {
                $("#classCoach").val("");
                $scope.showAlert(1, "Please choose class end hour");
            } else if ($("#classEndMinute").val() == "") {
                $("#classCoach").val("");
                $scope.showAlert(1, "Please choose class end minute");
            } else {
                $http({
                    method: 'post',
                    url: apiUrl + 'getCoachRate',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    data: $.param({
                        coach_id: $coach,
                        book_date: $("#classBookDate").val(),
                        start_hours: $("#classStartHour").val(),
                        start_minutes: $("#classStartMinute").val(),
                        end_hours: $("#classEndHour").val(),
                        end_minutes: $("#classEndMinute").val()
                    }),
                }).then(function success(res) {
                    if (res.data.status) {
                        console.log(res.data);
                        $scope.totalCoachFee = parseFloat(res.data.data);
                        if ($scope.courtFee != undefined) {
                            $scope.subTotal = $scope.totalCoachFee; // + $scope.courtFee
                        } else {
                            $scope.subTotal = $scope.totalCoachFee;
                        }
                        if ($scope.taxRate != undefined) {
                            $scope.taxAmt = ($scope.subTotal * $scope.taxRate) / 100;
                        } else {
                            $scope.taxAmt = 0;
                        }
                        $scope.grandTotal = parseFloat($scope.subTotal) + parseFloat($scope.taxAmt);
                        $scope.perHeadFee = $scope.grandTotal / $scope.totalClassPlayer;
                        //$scope.courtFee = res.data.data.Court.rate;
                    } else {
                        $scope.showAlert(1, res.data.message, res.data.title);
                    }
                }, function error(response) {
                    $scope.showAlert(1, response);
                });
            }
        }

        $scope.addGroupClass = function () {
            $http({
                method: 'post',
                url: apiUrl + 'getGroupClassRate',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: $.param({
                    book_date: $("#classBookDate").val(),
                    court_id: $("#classCourtNo").val(),
                    start_hours: $("#classStartHour").val(),
                    start_minutes: $("#classStartMinute").val(),
                    end_hours: $("#classEndHour").val(),
                    end_minutes: $("#classEndMinute").val(),
                    no_player: $("#classTotalPlayer").val(),
                    coach_id: $("#classCoach").val()
                }),
            }).then(function success(res) {
                if (res.data.status) {
                    $rootScope.groupCounter = 0;
                    console.log(res.data);
                    $rootScope.courtFee = parseFloat(res.data.feesArray.total_court_fees);
                    $rootScope.taxRate = parseFloat(res.data.feesArray.tax_rate);
                    $rootScope.totalCoachFee = parseFloat(res.data.feesArray.total_coach_fees);
                    $rootScope.subTotal = $rootScope.totalCoachFee + $rootScope.courtFee;
                    $rootScope.taxAmt = ($rootScope.subTotal * $rootScope.taxRate) / 100;
                    $rootScope.grandTotal = parseFloat($rootScope.subTotal) + parseFloat($rootScope.taxAmt);
                    $rootScope.totalClassPlayer = parseInt($("#classTotalPlayer").val())
                    $rootScope.perHeadFee = $rootScope.grandTotal / $rootScope.totalClassPlayer;
                    $scope.showConfirm("Do you want to create this class?", 0, "confirmCreateGroupClass", "Verify Price");
                } else {
                    $scope.showAlert(1, res.data.message);
                }
            }, function error(response) {
                $scope.showAlert(1, response);
            });



        }

        $scope.$on('confirmCreateGroupClass', function (ev, args) {
            $rootScope.groupCounter++;
            if ($rootScope.groupCounter == 1) {
                var isClubMember = $("#isClubMember").is(":checked") ? 1 : 2;
                var ageGroup = $("#classPlayerAgeGroup").val().split("-");
                $http({
                    method: 'post',
                    url: apiUrl + 'addGroupClass',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    data: $.param({
                        is_club_member: isClubMember,
                        class_name: $("#className").val(),
                        class_description: $("#classDescription").val(),
                        book_date: $("#classBookDate").val(),
                        court_id: $("#classCourtNo").val(),
                        start_hours: $("#classStartHour").val(),
                        start_minutes: $("#classStartMinute").val(),
                        end_hours: $("#classEndHour").val(),
                        end_minutes: $("#classEndMinute").val(),
                        min_player_rating: $("#classPlayerMinRating").val(),
                        max_player_rating: $("#classPlayerMaxRating").val(),
                        min_player_age: ageGroup[0],
                        max_player_age: ageGroup[1],
                        no_player: $("#classTotalPlayer").val(),
                        coach_id: $("#classCoach").val(),
                        sub_coach_id: $("#classSubCoach").val(),
                        court_booking_fee: $rootScope.courtFee,
                        coach_booking_fee: $rootScope.totalCoachFee,
                        sub_total: $rootScope.subTotal,
                        tax_amount: $rootScope.taxAmt,
                        tax_fee: $rootScope.taxRate,
                        grand_total: $rootScope.grandTotal,
                        club_id: localStorage.getItem('club_id'),
                        user_id: localStorage.getItem('user_id')
                    }),
                }).then(function success(res) {
                    if (res.data.status) {
                        //$scope.showAlert(2, 'Your class has been created successfully. To view upcoming group classes, please go to Group Classes', 'Class Created');
                        $scope.groupClassbooked(res.data.data);
                        $scope.$broadcast("setCalendar", { id: localStorage.getItem('club_id'), court_id: '', type: 2 });
                    } else {
                        $scope.showAlert(1, res.data.message);
                    }
                }, function error(response) {
                    $scope.showAlert(1, response);
                });
            }

        });

        $scope.groupClassbooked = function (bookingId) {
            $http({
                method: 'post',
                url: apiUrl + 'getEventSessionDetails',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: $.param({
                    booking_id: bookingId
                }),
            }).then(function success(res) {
                res = res.data;
                $scope.closeAllForms('rightSideBarGroupClassBooked');
                $scope.bookingId = bookingId;
                $scope.totalPlayer = res.data.ClubBookingSession.no_player;
                $scope.teams = res.data.Team;
                $scope.coachName = res.data.Coach.name;
                $scope.subCoachName = res.data.SubCoach.name;
                $scope.startTime = res.data.ClubBookingSession.book_start_time;
                $scope.endTime = res.data.ClubBookingSession.book_end_time;
                $scope.courtBookingFee = res.data.ClubBookingSession.court_booking_fee;
                $scope.coachBookingFee = res.data.ClubBookingSession.coach_booking_fee;
                $scope.subTotal = res.data.ClubBookingSession.sub_total;
                $scope.taxFee = res.data.ClubBookingSession.tax_fee;
                $scope.taxAmount = res.data.ClubBookingSession.tax_amount;
                $scope.grandTotal = res.data.ClubBookingSession.grand_total;
                var edate = new Date(res.data.ClubBookingSession.book_date + " " + res.data.ClubBookingSession.book_start_time);
                var eedate = new Date(res.data.ClubBookingSession.book_date + " " + res.data.ClubBookingSession.book_end_time);
                $scope.eventPlayTime = moment(edate).format('h:mma') + " - " + moment(eedate).format('h:mma');
                $scope.courtName = res.data.Court.name;
                $scope.start = res.data.ClubBookingSession.book_date;
                $scope.eventDate = moment(edate).format('Do MMMM YYYY');
                $scope.eventDay = moment(edate).format('dddd');
                $scope.bookingNumber = res.data.ClubBookingSession.prefix_booking_number + ("0000" + res.data.ClubBookingSession.booking_number).slice(-6);
                $scope.matchType = res.data.ClassType.name;
                $scope.courtName = res.data.Court.name;
                $scope.clubName = res.data.Club.name;
                $scope.clubAddress = res.data.Club.address;
                $scope.clubContact = res.data.Club.contact;
                $scope.clubProfilePicture = res.data.Club.profile_picture;
            }, function error(response) {
                $scope.showAlert(1, response);
            });
        }

        $scope.closeRightSideBarAddGroupClassCommon = function () {
            $("#rightSideBarAddGroupClass").hide();
            //$(".app-content-body").removeClass("sideBarOn");
            //$(".button-bottom").removeClass("sideBarOn");
        }

        $scope.closeRightSideBarGroupClassBooked = function () {
            $("#rightSideBarGroupClassBooked").hide();
            //$(".app-content-body").removeClass("sideBarOn");
            //$(".button-bottom").removeClass("sideBarOn");
        }

        $scope.setClassTotalPlayer = function (classPlayer) {
            $scope.totalClassPlayer = classPlayer;
            $scope.perHeadFee = $scope.grandTotal / $scope.totalClassPlayer;
        }

        $scope.closeAllForms = function (id) {
            $("#rightSideBarAddForm").hide();
            $("#rightSideBarBookingDetailsConfirm").hide();
            $("#rightSideBarPaymentForm").hide();
            $("#rightSideBarPrint").hide();
            $("#rightSideBarSearch").hide();
            $("#rightSideBar").hide();
            $("#rightSideBarDetails").hide();
            $("#rightSideBarBookingDetails").hide();
            $("#rightSideBarCancelBooking").hide();
            $("#rightSideBarAddGroupClass").hide();
            $("#rightSideBarGroupClassBooked").hide();
            $("#rightSideBarBlockDate").hide();
            $("#rightSideBarGroupClassDetails").hide();
            $("#rightSideBarBookingDetailsConfirmUpdate").hide();
            $("#rightSideBarPaymentFormUpdate").hide();
            $("#rightSideBarMultiple").hide();
            $("#" + id).show();
        }



        $scope.openNotifications = function () {
            $http({
                method: 'post',
                url: apiUrl + 'getUnreadNotifications',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: $.param({
                    user_id: localStorage.getItem('user_id'),
                    grouped: true
                }),
            }).then(function success(res) {
                if (res.data.status) {
                    $scope.uploadUrl = uploadUrl;
                    $scope.notifications = res.data.data;
                } else {
                    $scope.notifications = [];
                }
                $timeout(function () {
                    $scope.roleId = localStorage.getItem('role_id');
                    $(".overlay").removeClass("hide");
                    $("#notificationsMainDiv.box-main").removeClass("hide");
                    /*var template = $("#notificationsMainDiv").html();
                     var $html = $('<div />', {html: template});
                     $html.find('.box-main-inner').removeClass("hide");
                     $html.find('#1a').attr("id", "1aa");
                     $html.find('#2a').attr("id", "2aa");
                     $html.find('#3a').attr("id", "3aa");
                     $html.find('#4a').attr("id", "4aa");
                     $html.find('#5a').attr("id", "5aa");
                     template = $html.html();
                     $("#commonPopup").html(template);
                     $(".overlay").removeClass("hide");
                     $("#commonPopup").removeClass("hide");
                     $compile($(".ahed-div"))($scope);*/
                }, 100);
            }, function error(response) {
                $scope.showAlert(1, response);
            });
        }

        $scope.showNotificationDetail = function (id, type, nid) {
            $http({
                method: 'post',
                url: apiUrl + 'setReadNotification',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: $.param({
                    id: nid,
                    user_id: localStorage.getItem('user_id')
                }),
            }).then(function success(res) {
                if (res.data.status) {
                    closeNotifications();
                    if (localStorage.getItem('role_id') == 3) {
                        if (type == 1 || type == 9) {
                            var currentPage = $location.path();
                            if (currentPage == '/bookings') {
                                $scope.getRightSideBarDetails(id);
                            } else {
                                window.location.href = '#!/bookings';
                                $timeout(function () {
                                    var currentPage = $location.path();
                                    if (currentPage == '/bookings') {
                                        $scope.getRightSideBarDetails(id);
                                    }
                                }, 2000);
                            }
                        } else if (type == 10) {
                            var currentPage = $location.path();
                            if (currentPage == '/coaches') {
                                showCoachProfileCoaches(id, 2);
                            } else {
                                window.location.href = '#!/coaches';
                                $timeout(function () {
                                    var currentPage = $location.path();
                                    if (currentPage == '/coaches') {
                                        showCoachProfileCoaches(id, 2);
                                    }
                                }, 2000);
                            }
                        }
                    } else if (localStorage.getItem('role_id') == 2) {
                        if (type == 1 || type == 9) {
                            var currentPage = $location.path();
                            if (currentPage == '/playerperformance') {
                                openClassDetails(id);
                            } else {
                                window.location.href = '#!/playerperformance';
                                $timeout(function () {
                                    var currentPage = $location.path();
                                    if (currentPage == '/playerperformance') {
                                        openClassDetails(id);
                                    }
                                }, 2000);
                            }
                        }
                    }

                } else {
                    $scope.showAlert(1, res.data.message);
                }
            }, function error(response) {
                $scope.showAlert(1, response);
            });
        }

        $scope.openDetails = function (bid) {
            $http({
                method: 'post',
                url: apiUrl + 'getEventDetails',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: $.param({
                    booking_id: bid
                }),
            }).then(function success(res) {
                $scope.teams = [];
                $scope.steams = [];
                $scope.teamNumber = 0;
                $("#updateEventBooking input").each(function () {
                    $(this).val('');
                });
                $("#updateEventBooking select").each(function () {
                    $(this).val('');
                });
                $("#updateEventBooking textarea").each(function () {
                    $(this).val('');
                });
                res = res.data;
                $scope.closeAllForms('rightSideBarDetails');
                $scope.openCoachBox();
                $("#bookDate").datepicker({
                    autoclose: true
                });
                //$("#rightSideBarDetails").animate({ width: 'toggle' }, 350);
                //$(".app-content-body").addClass("sideBarOn");
                //$(".button-bottom").addClass("sideBarOn");
                $("#playerEditFirstName").val(res.data.ClubBooking.booked_first_name);
                $("#playerEditLastName").val(res.data.ClubBooking.booked_last_name);
                //$scope.playerFirstName = res.data.ClubBooking.booked_first_name;
                //$scope.playerLastName = res.data.ClubBooking.booked_last_name;
                $("#playerEditContactNo").val(res.data.ClubBooking.booked_contact);
                $("#playerEditEmail").val(res.data.ClubBooking.booked_email);
                //$scope.playerEmail = res.data.ClubBooking.booked_email;
                $scope.teamNumber = res.data.ClubBooking.team_number;
                $scope.remoteUrl = apiUrl + 'getSearchPlayers/' + $scope.teamNumber + '/' + localStorage.getItem('club_id') + '/2/';
                $scope.bookingId = bid;
                if (res.data.Team.length == 0) {
                    $("#totalPlayer").val('');
                } else {
                    $("#totalPlayer").val(res.data.Team.length);
                }
                $scope.teams = res.data.Team;
                $scope.steams = res.data.Team;
                $timeout(function () {
                    if (res.data.ClubBooking.coach_id == 0) {
                        $("#eventCoach").val('');
                    } else {
                        $("#eventCoach").val(res.data.ClubBooking.coach_id);
                    }
                    if (res.data.ClubBooking.sub_coach_id == 0) {
                        $("#eventSubCoach").val('');
                    } else {
                        $("#eventSubCoach").val(res.data.ClubBooking.sub_coach_id);
                    }
                }, 1000);

                var startTime = res.data.ClubBooking.book_start_time;
                startTime = $scope.getHoursMinutes(startTime);
                startTime = startTime.split(":");
                $timeout(function () {
                    $("#eventStartHours").val(startTime[0]);
                }, 1000);
                if (startTime[1] == 0) {
                    startTime[1] = "00";
                }
                $("#eventStartMinutes").val(startTime[1]);
                var endTime = res.data.ClubBooking.book_end_time;
                endTime = $scope.getHoursMinutes(endTime);
                endTime = endTime.split(":");
                $timeout(function () {
                    $("#eventEndHours").val(endTime[0]);
                }, 1000);
                if (endTime[1] == 0) {
                    endTime[1] = "00";
                }
                $("#eventEndMinutes").val(endTime[1]);
                if (res.data.ClubBooking.court_id == 0 || $("#eventCourt option[value='" + res.data.ClubBooking.court_id + "']").length == 0) {
                    $("#eventCourt").val('');
                } else {
                    $("#eventCourt").val(res.data.ClubBooking.court_id);
                }
                //$scope.start = res.data.ClubBooking.book_date;
                $("#bookDate").datepicker("update", new Date(res.data.ClubBooking.book_date));
                $scope.setAvailableTimings('eventCourt', 'bookDate')
                $scope.bookerInfo = res.data.bookerInfo;
                //$scope.totalPlayer = res.data.Team.length;
            }, function error(response) {
                // do nothing
            });
        }

        /*$scope.getRightSideBarDetails = function (bookingId) {
         $http({
         method: 'post',
         url: apiUrl + 'getEventDetails',
         headers: {
         'Content-Type': 'application/x-www-form-urlencoded'
         },
         data: $.param({
         booking_id: bookingId
         }),
         }).then(function success(res) {
         res = res.data;
         $scope.closeAllForms('rightSideBarBookingDetails');
         //$("#rightSideBarBookingDetails").animate({ width: 'toggle' }, 350);
         ////$(".app-content-body").addClass("sideBarOn");
         ////$(".button-bottom").addClass("sideBarOn");
         $scope.bookerName = res.data.UserDetail.Player.first_name + " " + res.data.UserDetail.Player.last_name;
         $scope.playerContactNo = res.data.UserDetail.Player.phone;
         $scope.playerEmail = res.data.UserDetail.User.email;
         $scope.teamNumber = res.data.ClubBooking.team_number;
         $scope.remoteUrl = apiUrl + 'getSearchPlayers/' + $scope.teamNumber + '/';
         $scope.bookingId = bookingId;
         $scope.totalPlayer = res.data.Team.length;
         $scope.teams = res.data.Team;
         $scope.steams = res.data.Team;
         $scope.coachName = res.data.Coach.name;
         $scope.subCoachName = res.data.SubCoach.name;
         $scope.startTime = res.data.ClubBooking.book_start_time;
         $scope.endTime = res.data.ClubBooking.book_end_time;
         $scope.courtBookingFee = res.data.ClubBooking.court_booking_fee;
         $scope.coachBookingFee = res.data.ClubBooking.coach_booking_fee;
         $scope.subTotal = res.data.ClubBooking.sub_total;
         $scope.taxFee = res.data.ClubBooking.tax_fee;
         $scope.taxAmount = res.data.ClubBooking.tax_amount;
         $scope.grandTotal = res.data.ClubBooking.grand_total;
         $scope.paymentMode = res.data.ClubBooking.payment_mode;
         $scope.clubMembershipId = res.data.Club.membership_id;
         $scope.userAppId = res.data.ClubBooking.user_id;
         $scope.paymentDate = res.data.ClubBooking.payment_date;
         $scope.cardType = res.data.Transaction.card_type;
         $scope.cardNumber = res.data.Transaction.card_number;
         $scope.cardName = res.data.Transaction.card_name;
         var edate = new Date(res.data.ClubBooking.book_date + " " + res.data.ClubBooking.book_start_time);
         var eedate = new Date(res.data.ClubBooking.book_date + " " + res.data.ClubBooking.book_end_time);
         $scope.eventPlayTime = moment(edate).format('h:mma') + " - " + moment(eedate).format('h:mma');
         $scope.courtName = res.data.Court.name;
         $scope.start = res.data.ClubBooking.book_date;
         $scope.eventDate = moment(edate).format('Do MMMM YYYY');
         $scope.eventDay = moment(edate).format('dddd');
         $scope.bookingNumber = res.data.ClubBooking.prefix_booking_number + ("0000" + res.data.ClubBooking.booking_number).slice(-6);
         $scope.matchType = res.data.ClassType.name;
         $scope.courtName = res.data.Court.name;
         $scope.picUrl = uploadUrl;
         }, function error(response) {
         // do nothing
         });
         
         }*/

        $scope.closeRightSideBarSearchAppUser = function () {
            $("#rightSideBarSearchAppUser").hide();
        }

        $scope.closeRightSideBarSearchAppEditUser = function () {
            $("#rightSideBarSearchAppEditUser").hide();
        }

        $scope.openSearchAppForm = function () {
            if ($("#totalAddPlayer").val() != '' && parseInt($("#totalAddPlayer").val()) > 1) {
                $scope.remoteUrl = apiUrl + 'getSearchPlayers/0/' + localStorage.getItem('club_id') + "/2/";
                $("#rightSideBarSearchAppUser").show();
                $("#AddAppMembers_value").val('');
            } else if (parseInt($("#totalAddPlayer").val()) == 1) {
                $scope.showAlert(1, "Please select the number of players more than one to add app users.", "Number of players required");
            } else {
                $scope.showAlert(1, "Please select the number of players before entering their details.", "Number of players required");
            }

        }

        $scope.openSearchAppFormAdd = function () {
            if ($("#totalAddPlayer").val() != '') {
                var playerEmail = $("#playerAddEmail").val();
                $scope.remoteUrl = apiUrl + 'getSearchPlayersAdd/' + playerEmail + '/' + localStorage.getItem('club_id');
                $("#rightSideBarSearchAppUser").show();
                $scope.teams = [];
            } else {
                $scope.showAlert(1, "Please select the number of players before entering their details.", "Number of players required");
            }

        }

        $scope.openSearchAppEditForm = function () {
            if ($("#totalPlayer").val() != '' && parseInt($("#totalPlayer").val()) > 1) {
                //$scope.remoteUrl = apiUrl + 'getSearchPlayers/0/' + localStorage.getItem('club_id')+"/2/";
                $("#rightSideBarSearchAppEditUser").show();
                //$scope.teams = [];
            } else if (parseInt($("#totalPlayer").val()) == 1) {
                $scope.showAlert(1, "Please select the number of players more than one to add app users.", "Number of players required");
            } else {
                $scope.showAlert(1, "Please select the number of players before entering their details.", "Number of players required");
            }

        }

        $scope.addSelectedMembers = function () {
            if (parseInt($("#totalAddPlayer").val()) >= $scope.addFormPlayers.length) {
                $http({
                    method: 'post',
                    url: apiUrl + 'getPlayerInfo',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    data: $.param({
                        playersIds: $scope.addFormPlayers
                    }),
                }).then(function success(res) {
                    if (res.data.status) {
                        $scope.steams = res.data.data;
                        $scope.closeRightSideBarSearchAppUser();
                    } else {
                        $scope.showAlert(1, "Can not add player");
                    }
                }, function error(response) {
                    // do nothing
                });
            } else {
                $scope.showAlert(1, "Can not add more than " + $("#totalAddPlayer").val() + " players");
            }

        }

        $scope.addSelectedEditMembers = function () {
            var teamIds = [];
            $.each($scope.teams, function (i, v) {
                teamIds.push(v.Player.id);
            });
            $http({
                method: 'post',
                url: apiUrl + 'getPlayerInfo',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: $.param({
                    playersIds: teamIds
                }),
            }).then(function success(res) {
                if (res.data.status) {
                    $scope.steams = res.data.data;
                    $scope.closeRightSideBarSearchAppEditUser();
                } else {
                    $scope.showAlert(1, "Can not add player");
                }
            }, function error(response) {
                // do nothing
            });
        }


        $scope.setAvailableTimings = function (courtId, bookDateId) {
            if ($("#" + bookDateId).val() == '') {
                $("#" + courtId + " option[value='']").attr('selected', true);
                $scope.showAlert(1, "Before deciding on a time, please choose a date first.", "Select a date");

                return false;
            } else {
                var bookDate = $("#" + bookDateId).val();
            }
            $http({
                method: 'post',
                url: apiUrl + 'checkCourtAvailableTimings',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: $.param({
                    book_date: bookDate,
                    club_id: localStorage.getItem('club_id'),
                    court_id: $("#" + courtId).val()
                }),
            }).then(function success(res) {
                if (res.data.status) {
                    $rootScope.allAvailableCoaches = res.data.data.Coaches;
                    if (res.data.data.CourtStartTime.length > 0) {
                        var startTime = res.data.data.CourtStartTime[0];
                        var startHours = startTime.split(":");
                    } else {
                        var startTime = 0;
                        var startHours = [0];
                    }
                    if (res.data.data.CourtEndTime.length > 0) {
                        var endTime = res.data.data.CourtEndTime[0];
                        var endHours = endTime.split(":");
                    } else {
                        var endTime = 0;
                        var endHours = [0];
                    }

                    $rootScope.startHours = [];
                    $rootScope.endHours = [];
                    for (var i = parseInt(startHours[0]); i <= parseInt(endHours[0]); i++) {
                        if (i < parseInt(endHours[0])) {
                            $rootScope.startHours.push(i);
                            $rootScope.endHours.push(i);
                        }
                    }
                    console.log($rootScope.startHours)
                } else {
                    $scope.showAlert(1, res.data.message, 'ERROR!');
                }
            }, function error(response) {
                $scope.showAlert(1, "Some error occurred", "ERROR!");
            });
        }

        $scope.checkPromoCode = function (ev) {
            if ($(ev.target).val() != '') {
                if ($("#playerAddEmail").val() == '') {
                    $(ev.target).val('');
                    $scope.showAlert(1, "Booker email address is manadatory");
                    return false;
                }
                $http({
                    method: 'post',
                    url: apiUrl + 'getPromoCheck',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    data: $.param({
                        promo_name: $(ev.target).val(),
                        user_id: localStorage.getItem('user_id'),
                        email: $("#playerAddEmail").val(),
                        is_calculate: 0,
                        club_id: localStorage.getItem('club_id')
                    }),
                }).then(function success(res) {
                    if (res.data.status) {
                        $scope.showAlert(2, "Promotion is successfully added", "Promotion added");
                    } else {
                        $(ev.target).val('');
                        $scope.showAlert(1, res.data.message);
                    }
                }, function error(response) {
                    $(ev.target).val('');
                    $scope.showAlert(1, "Some error occurred");
                });
            }
        }

        $scope.showPlayerProfile = function (player_id) {
            $http({
                method: 'post',
                url: apiUrl + 'getPlayerInfo',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: $.param({
                    playerId: player_id
                }),
            }).then(function success(res) {
                if (res.data.status) {
                    $("#playerProfileOverlay").removeClass("hide");
                    $("#playerProfileMainDiv").removeClass("hide");
                    $rootScope.counterPlayer = 0;

                    $rootScope.playerInfo = res.data.data[0];
                    if ($rootScope.playerInfo.PlayerFeedback.length > 0) {
                        var date = new Date($rootScope.playerInfo.PlayerFeedback[$rootScope.counterPlayer].created + ' UTC');
                        date.toString();
                        //alert($rootScope.playerInfo.PlayerFeedback[$rootScope.counterPlayer].created+" / "+date);
                        $rootScope.playerFeedbackDate = date;//$rootScope.playerInfo.PlayerFeedback[$rootScope.counterPlayer].created;
                    } else {
                        $rootScope.playerFeedbackDate = '';
                    }

                    $rootScope.picUrl = uploadUrl;
                    $rootScope.reviewLimit = 2;
                    var totalRating = 0;
                    for (var i = 0; i < (res.data.data[0].Review.length); i++) {
                        if (res.data.data[0].Review[i].receiver_id == player_id) {
                            totalRating += parseFloat(res.data.data[0].Review[i].rate);
                        }

                    }
                    if (totalRating > 0) {
                        $rootScope.avgRating = totalRating / res.data.data[0].Review.length;
                    } else {
                        $rootScope.avgRating = 0;
                    }
                    if (totalRating > 0) {
                        $rootScope.totalPlayer = res.data.data[0].Review.length;
                    } else {
                        $rootScope.totalPlayer = 0;
                    }
                    var totalRating = 0;
                    $rootScope.forhead_trait = 0;
                    $rootScope.backhand_trait = 0;
                    $rootScope.slice_trait = 0;
                    $rootScope.net_game_trait = 0;
                    $rootScope.serve_trait = 0;
                    $rootScope.point_play_trait = 0;
                    for (var i = 0; i < (res.data.data[0].PlayerFeedback.length); i++) {
                        totalRating += parseFloat(res.data.data[0].PlayerFeedback[i].current_rating);
                        $rootScope.forhead_trait += parseFloat(res.data.data[0].PlayerFeedback[i].forhead_trait);
                        $rootScope.backhand_trait += parseFloat(res.data.data[0].PlayerFeedback[i].backhand_trait);
                        $rootScope.slice_trait += parseFloat(res.data.data[0].PlayerFeedback[i].slice_trait);
                        $rootScope.net_game_trait += parseFloat(res.data.data[0].PlayerFeedback[i].net_game_trait);
                        $rootScope.serve_trait += parseFloat(res.data.data[0].PlayerFeedback[i].serve_trait);
                        $rootScope.point_play_trait += parseFloat(res.data.data[0].PlayerFeedback[i].point_play_trait);
                    }
                    $rootScope.avgPlayerRating = totalRating / res.data.data[0].PlayerFeedback.length;
                    $rootScope.forhead_trait = ($rootScope.forhead_trait / (res.data.data[0].PlayerFeedback.length * 5)) * 100;
                    $rootScope.backhand_trait = ($rootScope.backhand_trait / (res.data.data[0].PlayerFeedback.length * 5)) * 100;
                    $rootScope.slice_trait = ($rootScope.slice_trait / (res.data.data[0].PlayerFeedback.length * 5)) * 100;
                    $rootScope.net_game_trait = ($rootScope.net_game_trait / (res.data.data[0].PlayerFeedback.length * 5)) * 100;
                    $rootScope.serve_trait = ($rootScope.serve_trait / (res.data.data[0].PlayerFeedback.length * 5)) * 100;
                    $rootScope.point_play_trait = ($rootScope.point_play_trait / (res.data.data[0].PlayerFeedback.length * 5)) * 100;
                    $timeout(function () {
                        $(".bx-wrapper").remove();
                        if (slider == '') {
                            if ($rootScope.playerInfo.Badge.length > 0) {
                                slider = $('.slider1').bxSlider({
                                    slideWidth: 200,
                                    minSlides: 2,
                                    maxSlides: 3,
                                    slideMargin: 10
                                });
                            }

                        } else {
                            slider.destroySlider();
                            if ($rootScope.playerInfo.Badge.length > 0) {
                                slider = $('.slider1').bxSlider({
                                    slideWidth: 200,
                                    minSlides: 2,
                                    maxSlides: 3,
                                    slideMargin: 10
                                });
                            }
                        }
                    }, 1000);
                    $rootScope.player_id = player_id;
                    $scope.showUpcomingPlay();
                } else {
                    $scope.showAlert(1, res.data.message);
                }
            }, function error(response) {
                $scope.showAlert(1, response);
            });
        }

        $scope.showPlayerCommentNext = function () {
            if ($rootScope.counterPlayer < ($rootScope.playerInfo.PlayerFeedback.length - 1)) {
                var date = new Date($rootScope.playerInfo.PlayerFeedback[$rootScope.counterPlayer].created + ' UTC');
                date.toString();
                $rootScope.playerFeedbackDate = date;
                $rootScope.counterPlayer++;
            }
        }

        $scope.showPlayerCommentBack = function () {
            if ($rootScope.counterPlayer > 0) {
                var date = new Date($rootScope.playerInfo.PlayerFeedback[$rootScope.counterPlayer].created + ' UTC');
                date.toString();
                $rootScope.playerFeedbackDate = date;
                $rootScope.counterPlayer--;
            }
        }

        $scope.closePlayerProfile = function () {
            $("#playerProfileOverlay").addClass("hide");
            $("#playerProfileMainDiv").addClass("hide");
        }

        $scope.showPlayerProfileTab = function (id) {
            $("#" + id).show();
            $("#" + id).addClass("active");
            if (id == '1a') {
                $("#2a, #3a, #4a").hide();
                $("#2a, #3a, #4a").removeClass("active");
            } else if (id == '2a') {
                $("#1a, #3a, #4a").hide();
                $("#1a, #3a, #4a").removeClass("active");
            } else if (id == '3a') {
                $("#1a, #2a, #4a").hide();
                $("#1a, #2a, #4a").removeClass("active");
            } else if (id == '4a') {
                $("#1a, #2a, #3a").hide();
                $("#1a, #2a, #3a").removeClass("active");
            }
        }

        $scope.showMoreReviews = function () {
            $rootScope.reviewLimit += 2;
            $timeout(function () {
                if ($rootScope.reviewLimit >= $rootScope.playerInfo.Review.length) {
                    $timeout(function () {
                        $("#loadMoreReviews").hide();
                    }, 100);
                }
            }, 100);
        }

        $scope.showUpcomingPlay = function () {
            $("#upcomingPlay").addClass("active");
            $("#pastPlay").removeClass("active");
            $scope.contentLoadingPlayerProfile(1, $rootScope.player_id);
        }

        $scope.showPastPlay = function () {
            $("#upcomingPlay").removeClass("active");
            $("#pastPlay").addClass("active");
            $scope.contentLoadingPlayerProfile(2, $rootScope.player_id);
        }

        $scope.contentLoadingPlayerProfile = function (type, player_id) {
            k++;
            if (k > 1) {
                table1.destroy();
            }
            table1 = $('#playerProfileTable').DataTable({
                "autoWidth": false,

                "ajax": {
                    "destroy": true,
                    "url": apiUrl + 'allClasses',
                    "type": 'post',
                    "headers": {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    "data": {
                        "coach_id": localStorage.getItem("coach_id"),
                        type: type,
                        player_id: player_id
                    },
                    "dataSrc": function (json) {
                        console.log(json);
                        var fData = [];
                        if (json.status) {
                            $.each(json.data, function (index, val) {
                                console.log(val);
                                var book_date = val.ClubBookingSession.book_date;
                                var book_time = $filter('formatTime')(val.ClubBookingSession.book_start_time) + " - " + $filter('formatTime')(val.ClubBookingSession.book_end_time);
                                var class_type = val.ClassType.name;
                                var details = " <a href='javascript:' ng-click='openBookingDetails(" + val.ClubBookingSession.id + ")'><img src='images/ahed-arrow.png'/></a>";
                                var res = {
                                    book_date: book_date,
                                    book_time: book_time,
                                    class_type: class_type,
                                    details: details
                                }
                                fData.push(res);
                            });
                        }


                        return fData;

                    }
                }, "fnDrawCallback": function () {
                    var html = '<img src="images/left-arrow.png"/>';
                    $("#playerProfileTable_previous").html(html);
                    var html = '<img src="images/right-arrow.png"/>';
                    $("#playerProfileTable_next").html(html);
                    $("#playerProfileTable_info").remove();
                    $("#playerProfileTable_filter").remove();
                    $compile($("#playerProfileTable"))($scope);
                },
                "pageLength": 5,
                "columns": [
                    { "data": "book_date" },
                    { "data": "book_time" },
                    { "data": "class_type" },
                    { "data": "details" }
                ]


            });
        }

        $scope.openBookingDetails = function (bookingId) {
            $http({
                method: 'post',
                url: apiUrl + 'getEventSessionDetails',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: $.param({
                    booking_id: bookingId,
                    requestType: 1
                }),
            }).then(function success(res) {
                res = res.data;
                if (res.status) {
                    $("#coachBookingModalPopup").modal('show');
                    //if (res.data.UserDetail.length > 0) {
                    if (res.data.UserDetail.Player != undefined) {
                        $rootScope.bookerName = res.data.UserDetail.Player.first_name + " " + res.data.UserDetail.Player.last_name;
                        $rootScope.playerContactNo = res.data.UserDetail.Player.phone;
                    } else {
                        $rootScope.bookerName = res.data.UserDetail.Club.name;
                        $rootScope.playerContactNo = '';
                    }

                    $rootScope.playerEmail = res.data.UserDetail.User.email;
                    /*} else {
                        $rootScope.bookerName = '';
                        $rootScope.playerContactNo = '';
                        $rootScope.playerEmail = '';
                    }*/

                    $rootScope.teamNumber = res.data.ClubBookingSession.team_number;
                    $rootScope.remoteUrl = apiUrl + 'getSearchPlayers/' + $scope.teamNumber + '/';
                    $rootScope.bookingId = bookingId;
                    $rootScope.totalPlayer = res.data.Team.length;
                    $rootScope.teams = res.data.Team;
                    $rootScope.coachName = res.data.Coach.name;
                    $rootScope.subCoachName = res.data.SubCoach.name;
                    $rootScope.startTime = res.data.ClubBookingSession.book_start_time;
                    $rootScope.endTime = res.data.ClubBookingSession.book_end_time;
                    $rootScope.courtBookingFee = res.data.ClubBookingSession.court_booking_fee;
                    $rootScope.coachBookingFee = res.data.ClubBookingSession.coach_booking_fee;
                    $rootScope.subTotal = res.data.ClubBookingSession.sub_total;
                    $rootScope.promoDiscount = res.promoPrice;
                    $rootScope.nonMemberFees = res.data.ClubBookingSession.non_member_fee;
                    $rootScope.taxFee = res.data.ClubBookingSession.tax_fee;
                    $rootScope.taxAmount = res.data.ClubBookingSession.tax_amount;
                    $rootScope.grandTotal = res.data.ClubBookingSession.grand_total;
                    $rootScope.paymentMode = res.data.ClubBookingSession.payment_mode;
                    $rootScope.clubMembershipId = res.data.Club.membership_id;
                    $rootScope.userAppId = res.data.ClubBookingSession.user_id;
                    $rootScope.paymentDate = res.data.ClubBookingSession.payment_date;
                    $rootScope.cardType = res.data.Transaction.card_type;
                    $rootScope.cardNumber = res.data.Transaction.card_number;
                    $rootScope.cardName = res.data.Transaction.card_name;
                    var edate = new Date(res.data.ClubBookingSession.book_date + " " + res.data.ClubBookingSession.book_start_time);
                    var eedate = new Date(res.data.ClubBookingSession.book_date + " " + res.data.ClubBookingSession.book_end_time);
                    $rootScope.eventPlayTime = moment(edate).format('h:mma') + " - " + moment(eedate).format('h:mma');
                    $rootScope.courtName = res.data.Court.name;
                    $rootScope.start = res.data.ClubBookingSession.book_date;
                    $rootScope.eventDate = moment(edate).format('Do MMMM YYYY');
                    $rootScope.eventDay = moment(edate).format('dddd');
                    $rootScope.bookingNumber = res.data.ClubBookingSession.prefix_booking_number + ("0000" + res.data.ClubBookingSession.booking_number).slice(-6);
                    $rootScope.matchType = res.data.ClassType.name;
                    $rootScope.courtName = res.data.Court.name;
                    $rootScope.picUrl = uploadUrl;
                } else {
                    $scope.showAlert(1, res.message);
                }

            }, function error(response) {
                $scope.showAlert(1, JSON.stringify(response));
            });
        }

        $scope.showCoachProfile = function (id) {
            $rootScope.coachId = id;
            $http({
                method: 'post',
                url: apiUrl + 'CoachProfile',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: $.param({
                    coach_id: $scope.coachId,
                    club_id: localStorage.getItem("club_id")
                }),
            }).then(function success(res) {
                if (res.data.status) {
                    $rootScope.coachDetails = res.data.data.Coach;
                    $rootScope.uploadUrl = uploadUrl;
                    $rootScope.coachImage = uploadUrl + res.data.data.Coach.profile_picture;
                    $rootScope.coachAge = $scope.calculateAge(res.data.data.Coach.dob);
                    $rootScope.userDetails = res.data.data.User;
                    $rootScope.countryDetails = res.data.data.Country;
                    $rootScope.coachAvailableHours = res.data.data.CoachAvailableHour;
                    $rootScope.coachExperiance = res.data.data.CoachExperiance;
                    $rootScope.coachCertificate = res.data.data.CoachCertificate;
                    $rootScope.coachReview = res.data.data.CoachReview;
                    $rootScope.reviewLimit = 2;
                    $rootScope.clubDetails = res.data.data.Club;
                    var totalRating = 0;
                    for (var i = 0; i < (res.data.data.CoachReview.length); i++) {
                        totalRating += parseFloat(res.data.data.CoachReview[i].rating_star);
                    }
                    $rootScope.avgRating = totalRating / res.data.data.CoachReview.length;
                    $rootScope.totalPlayer = res.data.data.CoachReview.length;
                    $rootScope.coachDetails.created = new Date($scope.coachDetails.created);
                    $("#coachProfileMainDiv.box-main").removeClass("hide");
                    $(".overlay").removeClass("hide");
                    $scope.showUpcomingClass();
                    /*$timeout(function () {
                     var template = $("#coachProfile").html();
                     var $html = $('<div />', {html: template});
                     $html.find('.box-main-inner').removeClass("hide");
                     $html.find('#1a').attr("id", "1aa");
                     $html.find('#2a').attr("id", "2aa");
                     $html.find('#3a').attr("id", "3aa");
                     $html.find('#4a').attr("id", "4aa");
                     $html.find('#example').attr("id", "coachProfileTable");
                     $html.find('#upcomingClass').attr("id", "upcomingClassCoachProfile");
                     $html.find('#pastClass').attr("id", "pastClassCoachProfile");
                     $html.find('#loadMoreReviews').attr("id", "loadMoreReviewsCoachProfile");
                     template = $html.html();
                     $("#commonPopup").html(template);
                     $compile($("#upcomingClassCoachProfile"))($scope);
                     $compile($("#pastClassCoachProfile"))($scope);
                     $compile($("#loadMoreReviewsCoachProfile"))($scope);
                     $compile($(".approveProfileDiv"))($scope);
                     
                     $(".overlay").removeClass("hide");
                     $("#commonPopup").removeClass("hide");
                     $scope.showUpcomingClass();
                     }, 100);*/
                } else {
                    $scope.showAlert(1, res.data.message);
                }
            }, function error(response) {
                $scope.showAlert(1, JSON.stringify(response));
            });


        }

        $scope.showUpcomingClass = function () {
            $("#upcomingClassCoachProfile").addClass("active");
            $("#pastClassCoachProfile").removeClass("active");
            $scope.contentLoadingCoachProfile(1, $rootScope.coachId);
        }

        $scope.showPastClass = function () {
            $("#upcomingClassCoachProfile").removeClass("active");
            $("#pastClassCoachProfile").addClass("active");
            $scope.contentLoadingCoachProfile(2, $rootScope.coachId);
        }

        $scope.showMoreReviews = function () {
            $scope.reviewLimit += 1;
            $timeout(function () {
                var template = $("#2a").html();
                var $html = $('<div />', { html: template });
                $html.find('#loadMoreReviews').attr("id", "loadMoreReviewsCoachProfile");
                template = $html.html();
                $("#2aa").html(template);
                $compile($("#loadMoreReviewsCoachProfile"))($scope);
                if ($scope.reviewLimit >= $scope.coachReview.length) {
                    $timeout(function () {
                        $("#loadMoreReviewsCoachProfile").hide();
                    }, 100);
                }
            }, 100);
        }

        $scope.contentLoadingCoachProfile = function (type, coachId) {
            j++;
            if (j > 1) {
                table2.destroy();
            }
            table2 = $('#coachProfileTable').DataTable({
                "autoWidth": false,

                "ajax": {
                    "destroy": true,
                    "url": apiUrl + 'upcomingClubBookings',
                    "type": 'post',
                    "headers": {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    "data": {
                        "type": type,
                        "coach_id": coachId
                    },
                    "dataSrc": function (json) {
                        console.log(json);
                        var fData = [];
                        if (json.status) {
                            $.each(json.data, function (index, val) {
                                console.log(val);
                                var book_date = val.ClubBooking.book_date;
                                var book_time = val.ClubBooking.book_start_time + " " + val.ClubBooking.book_end_time;
                                var class_type = val.ClassType.name;
                                var booker_name = val.ClubBooking.booker_name;
                                var details = " <a href='javascript:' onClick='openCoachBookingDetails(" + val.ClubBooking.id + ")'><img src='images/ahed-arrow.png'/></a>";
                                var res = {
                                    book_date: book_date,
                                    book_time: book_time,
                                    class_type: class_type,
                                    booker_name: booker_name,
                                    details: details
                                }
                                fData.push(res);
                            });
                        }


                        return fData;

                    }
                }, "fnDrawCallback": function () {
                    var html = '<img src="images/left-arrow.png"/>';
                    $("#coachProfileTable_previous").html(html);
                    var html = '<img src="images/right-arrow.png"/>';
                    $("#coachProfileTable_next").html(html);
                    $("#coachProfileTable_info").remove();
                    $("#coachProfileTable_filter").remove();
                },
                "pageLength": 5,
                "columns": [
                    { "data": "book_date" },
                    { "data": "book_time" },
                    { "data": "class_type" },
                    { "data": "booker_name" },
                    { "data": "details" }
                ]


            });

        }

        $scope.makeNumberArray = function (num) {
            var marray = new Array();
            for (var i = 1; i <= num; i++) {
                marray.push(i);
            }
            return marray;
        }

        $scope.matchJoinResponse = function (id, playerId, userResp) {
            $http({
                method: 'post',
                url: apiUrl + 'setReadNotification',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: $.param({
                    id: id,
                    user_id: localStorage.getItem('user_id')
                }),
            }).then(function success(res) {
                if (res.data.status) {
                    closeNotifications();
                    $http({
                        method: 'post',
                        url: apiUrl + 'joinUpcomingEvent',
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded'
                        },
                        data: $.param({
                            notification_id: id,
                            player_id: playerId,
                            acceptance_response: userResp
                        }),
                    }).then(function success(res) {
                        if (res.data.status) {
                            $scope.showAlert(2, "Request accepted.");
                        } else {
                            $scope.showAlert(1, res.data.message);
                        }
                    }, function error(response) {
                        $scope.showAlert(1, JSON.stringify(response));
                    });

                } else {
                    $scope.showAlert(1, res.data.message);
                }
            }, function error(response) {
                $scope.showAlert(1, response);
            });

        }

        $scope.cancelClass = function (classId) {
            $http({
                method: 'post',
                url: apiUrl + 'cancelClassWithRefundNew',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: $.param({
                    group_class_id: classId,
                    user_id: localStorage.getItem('user_id')
                }),
            }).then(function success(res) {
                if (res.data.status) {
                    //$scope.closeRightSideBarAddGroupClass();
                    $scope.showAlert(2, "Class has been cancelled");
                    window.location.reload(true);
                    //$scope.contentLoading(1);
                } else {
                    $scope.showAlert(1, res.data.message);
                }
            }, function error(response) {
                $scope.showAlert(1, JSON.stringify(response));
            });
        }

        $scope.closeRightSideBarAddGroupClass = function () {
            $("#rightSideBarAddGroupClass").hide();
            $("#rightSideBarEditGroupClass").hide();
            $("#rightSideBarGroupClassDetails").hide();
            $("#rightSideBarSearchGroupClass").hide();
            $("#rightSideBarAddPlayerToClassForm").hide();
            $('#rightSideBarPaymentForm').hide();
            $('#rightSideBarPaymentFormPlayer').hide();
            //$(".app-content-body").removeClass("sideBarOn");
            //$(".button-bottom").removeClass("sideBarOn");
        }

        $scope.closeRightSideBarPaymentFormPlayer = function () {

            $('#rightSideBarPaymentFormPlayer').hide();
            //$(".app-content-body").removeClass("sideBarOn");
            //$(".button-bottom").removeClass("sideBarOn");
        }


        $scope.selectedAddPlayerClass = function (item) {
            var no_players = $('#no_players_data').val();
            var no_join_players = $('.playersInfoData').length;
            
            if (no_join_players < no_players) {
                var checkExistData = 0;
                $($rootScope.ajaxplayers).each(function (i, val) {
                    if (val.Player.id == item.originalObject.id) {
                        checkExistData = 1;
                    }
                });
                if (checkExistData == 0) {
                    $http({
                        method: 'post',
                        url: apiUrl + 'getPlayerInfo',
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded'
                        },
                        data: $.param({
                            playerId: item.originalObject.id,
                            playersIds: $scope.addFormPlayers
                        }),
                    }).then(function success(res) {
                        if (res.data.status) {
                            var val = res.data.data;
                            if (val.length > 0) {
                                $('#clubAppUserFirstName').val(val[0].Player.first_name);
                                $('#clubAppUserSurname').val(val[0].Player.last_name);
                                $('#clubAppUserContectNo').val(val[0].Player.phone);
                                $('#clubAppUserEmail').val(val[0].User.email);
                                $('#clubAppUserIdForJoin').val(val[0].Player.id);
                            }
                        } else {
                            $scope.showAlert(1, "Can not add player");
                        }
                    }, function error(response) {
                        // do nothing
                    });
                } else {
                    $scope.showAlert(1, "Already Joined");
                }
            } else {
                $scope.showAlert(1, "Class full");
            }

        }

        $scope.addPlayerToJoinClass = function () {
            var playerId = $('#clubAppUserIdForJoin').val();
            if (playerId > 0 && playerId != '') {
                $http({
                    method: 'post',
                    url: apiUrl + 'getPlayerInfo',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    data: $.param({
                        playerId: playerId,
                        playersIds: $scope.addFormPlayers
                    }),
                }).then(function success(res) {
                    if (res.data.status) {
                        var val = res.data.data;
                        if (val.length > 0) {
                            $timeout(function () {
                                $("#clubMemberSearch_value").val('');
                                $("#clubAppUserSearch_value").val('');
                            }, 500);

                            //alert($("#clubAppUserSearch_value").val());
                            $rootScope.ajaxplayers.push(val[0]);
                            $('#clubAppUserFirstName, #clubAppUserSurname, #clubAppUserContectNo, #clubAppUserEmail, #clubAppUserIdForJoin').val('');
                            var noofplayers = parseInt($('#noofplayers').val()) + 1;
                            var perplayer_price = $('#perplayer_price').val();
                            $rootScope.noofplayers = noofplayers;
                            $rootScope.totalmountChargesub = noofplayers * perplayer_price;
                            var promotionPrice = 0;
                            $rootScope.totalmountCharge = $rootScope.totalmountChargesub - promotionPrice;
                            $('#clubMemberSearch_value, #clubAppUserSearch_value').val();
                        }
                    }
                }, function error(response) {
                    // do nothing
                });
            }
        }

        $scope.removePlayerFromClassAjax = function (player) {
            $('.ajaxaddedData.playerBlok_' + player).remove();
            var allplayers = [];
            $($rootScope.ajaxplayers).each(function (i, val) {
                if (val.Player.id != player) {
                    allplayers.push(val);
                }
            });
            $rootScope.ajaxplayers = allplayers;
            var noofplayers = parseInt($('#noofplayers').val()) - 1;
            var perplayer_price = $('#perplayer_price').val();
            $rootScope.noofplayers = noofplayers;
            $rootScope.totalmountChargesub = noofplayers * perplayer_price;
            var promotionPrice = 0;
            $rootScope.totalmountCharge = $rootScope.totalmountChargesub - promotionPrice;
            return;
        }

        $scope.cashPaymentToJaoinClass = function (payment_type) {
            var no_players = $('#no_players_data').val();
            var no_join_players = $('.playersInfoData').length;
            var forJoinClass = $('.selectedTOAddClass').length;
            var classId = $('#classIdForJoin').val();
            if (forJoinClass > 0) {
                var playersIds = [];
                $('.selectedTOAddClass').each(function () {
                    playersIds.push($(this).val());
                });
                $http({
                    method: 'post',
                    url: apiUrl + 'setClassJoinPlayers',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    data: $.param({
                        group_class_id: classId,
                        payment_type: payment_type,
                        playersIds: playersIds,
                        amount: $rootScope.groupClassDeatilData.ClubBookingSession.grand_total / $rootScope.groupClassDeatilData.ClubBookingSession.no_player
                    }),
                }).then(function success(res) {
                    if (res.data.status) {
                        $scope.closeRightSideBarAddGroupClass();
                        $scope.showAlert(2, res.data.message);
                        //$scope.contentLoading(1);
                    } else {
                        $scope.showAlert(1, res.data.message);
                    }
                }, function error(response) {
                    $scope.showAlert(1, JSON.stringify(response));
                });
            } else {
                $scope.showAlert(1, 'Please add a minimum of 1 player to proceed.');
            }
        }

        $scope.makePaymentJoin = function (type) {
            var no_players = $('#no_players_data').val();
            var no_join_players = $('.playersInfoData').length;
            var forJoinClass = $('.selectedTOAddClass').length;
            var classId = $('#classIdForJoin').val();
            if (forJoinClass > 0) {
                var playersIds = [];
                $('.selectedTOAddClass').each(function () {
                    playersIds.push($(this).val());
                });
            } else {
                $scope.showAlert(1, 'Please add a minimum of 1 player to proceed.');
                return false;
            }
            $http({
                method: 'post',
                url: apiUrl + 'checkAvailableClassJoinPlayers',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: $.param({
                    group_class_id: classId,
                    playersIds: playersIds
                }),
            }).then(function success(res) {
                if (res.data.status) {
                    $scope.closeRightSideBarAddGroupClass();
                    $('#rightSideBarPaymentFormPlayer').show();
                    $scope.bookingId = classId;
                    $scope.grandTotal = $rootScope.totalmountCharge;
                } else {
                    $scope.showAlert(1, res.data.message);
                }
            }, function error(response) {
                $scope.showAlert(1, JSON.stringify(response));
            });


        }

        $scope.confirmCardPaymentPlayer = function (bookingId) {
            var $form = $('#payment-form-player');
            //console.log(Stripe)
            Stripe.card.createToken($form, function (error, response) {
                //console.log(error);
                //console.log(response);
                if (error != 200) {
                    $scope.showAlert(1, response.error.message);
                } else {
                    $http({
                        method: 'post',
                        url: apiUrl + 'makePaymentAddplayer',
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded'
                        },
                        data: $.param({
                            booking_id: bookingId,
                            payment_type: 2,
                            card_type: response.card.brand,
                            booking_type: 2,
                            card_number: response.card.last4,
                            card_name: response.card.name
                        }),
                    }).then(function success(res) {
                        res = res.data;
                        if (res.status) {
                            $scope.paymentGatewayPlayer(response, bookingId);
                        } else {
                            $scope.showAlert(1, res.message);
                        }
                    }, function error(response) {
                        $scope.showAlert(1, response);
                    });
                }
            });
            return false;

        }

        $scope.paymentGatewayPlayer = function (payment, bookingId) {
            $http({
                method: 'post',
                url: apiUrl + 'chargeCardAddplayer',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: $.param({
                    booking_id: bookingId,
                    payment: payment,
                    amount: $rootScope.totalmountCharge
                }),
            }).then(function success(res) {
                res = res.data;
                if (res.status) {
                    //console.log(res);
                    $scope.bookingConfirmPlayer(res.transction_id.Transaction.id);
                } else {
                    $scope.showAlert(1, res.message);
                }
            }, function error(response) {
                $scope.showAlert(1, response);
            });
        }

        $scope.bookingConfirmPlayer = function (bookingId) {
            var no_players = $('#no_players_data').val();
            var no_join_players = $('.playersInfoData').length;
            var forJoinClass = $('.selectedTOAddClass').length;
            var classId = $('#classIdForJoin').val();
            var playersIds = [];
            $('.selectedTOAddClass').each(function () {
                playersIds.push($(this).val());
            });
            $http({
                method: 'post',
                url: apiUrl + 'setClassJoinPlayers',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: $.param({
                    group_class_id: classId,
                    payment_type: 2,
                    playersIds: playersIds,
                    transction_id: bookingId,
                    amount: $rootScope.groupClassDeatilData.ClubBookingSession.grand_total / $rootScope.groupClassDeatilData.ClubBookingSession.no_player
                }),
            }).then(function success(res) {
                if (res.data.status) {
                    $scope.closeRightSideBarAddGroupClass();
                    $scope.showAlert(2, res.data.message);
                } else {
                    $scope.showAlert(1, res.data.message);
                }
            }, function error(response) {
                $scope.showAlert(1, JSON.stringify(response));
            });
        }

        $scope.openRightSideBarAddPlayerToClass = function (classId) {

            $http({
                method: 'post',
                url: apiUrl + 'getClassJoinData',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: $.param({
                    group_class_id: classId
                }),
            }).then(function success(res) {
                //console.log(res.data);
                if (res.data.status) {
                    $scope.closeRightSideBarAddGroupClass();
                    $("#rightSideBarAddPlayerToClassForm").show();
                    $("#rightSideBarAddPlayerToClassForm input").each(function () {
                        $(this).val('');
                    });
                    $("#rightSideBarAddPlayerToClassForm select").each(function () {
                        $(this).val('');
                    });
                    $('.ajaxaddedData').remove();
                    //$(".app-content-body").addClass("sideBarOn");
                    //$(".button-bottom").addClass("sideBarOn");
                    $('#clubMemberSearch_value, #clubAppUserSearch_value, #clubAppUserFirstName, #clubAppUserSurname, #clubAppUserContectNo, #clubAppUserEmail, #clubAppUserIdForJoin').val('');
                    var val = res.data.data;
                    var edate = new Date(val.ClubBookingSession.book_date + " " + val.ClubBookingSession.book_start_time);
                    val.ClubBookingSession.book_start_time = edate;
                    var edate = new Date(val.ClubBookingSession.book_date + " " + val.ClubBookingSession.book_end_time);
                    val.ClubBookingSession.book_end_time = edate;
                    //console.log(val);
                    $rootScope.groupClassDeatilData = val;
                    $rootScope.picUrl = uploadUrl;
                    $rootScope.players = res.data.players;
                    $rootScope.ajaxplayers = [];
                    $rootScope.remoteUrl = apiUrl + 'getSearchPlayersForClass/' + val.ClubBookingSession.id + '/1/';
                    $rootScope.clubremoteUrl = apiUrl + 'getSearchPlayersForClass/' + val.ClubBookingSession.id + '/2/';
                    $rootScope.noofplayers = 0;
                    $rootScope.totalmountChargesub = 0;
                    $rootScope.classIdForJoin = val.ClubBookingSession.id;
                    
                    var promotionPrice = 0;
                    $rootScope.totalmountCharge = $rootScope.totalmountChargesub - promotionPrice;
                    $("#no_players_data").val($rootScope.groupClassDeatilData.ClubBookingSession.no_player);
                    $("#perplayer_price").val($rootScope.groupClassDeatilData.ClubBookingSession.grand_total / $rootScope.groupClassDeatilData.ClubBookingSession.no_player);
                    $("#noofplayers").val($rootScope.noofplayers);
                    $("#totalmountCharge").val($rootScope.totalmountCharge);
                    $("#classIdForJoin").val($rootScope.classIdForJoin);
                } else {
                    $scope.showAlert(1, res.data.message);
                }
            }, function error(response) {
                $scope.showAlert(1, JSON.stringify(response));
            });
        }

        $scope.updateEventBookingNew = function (bookingId) {
            $http({
                method: 'post',
                url: apiUrl + 'checkClubBook',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: $.param({
                    book_date: $("#bookDate").val(),
                    court_id: $("#eventCourt").val(),
                    start_hours: $("#eventStartHours").val(),
                    start_minutes: $("#eventStartMinutes").val(),
                    end_hours: $("#eventEndHours").val(),
                    end_minutes: $("#eventEndMinutes").val(),
                    first_name: $("#playerEditFirstName").val(),
                    last_name: $("#playerEditLastName").val(),
                    phone: $("#playerEditContactNo").val(),
                    email: $("#playerEditEmail").val(),
                    coach_id: $("#eventCoach").val(),
                    sub_coach_id: $("#eventSubCoach").val(),
                    no_player: $("#totalPlayer").val(),
                    booking_id: bookingId,
                    team_number: $scope.teamNumber
                }),
            }).then(function success(res) {
                if (res.data.status) {
                    var updatedBookingDetails = {
                        book_date: $("#bookDate").val(),
                        court_id: $("#eventCourt").val(),
                        start_hours: $("#eventStartHours").val(),
                        start_minutes: $("#eventStartMinutes").val(),
                        end_hours: $("#eventEndHours").val(),
                        end_minutes: $("#eventEndMinutes").val(),
                        first_name: $("#playerEditFirstName").val(),
                        last_name: $("#playerEditLastName").val(),
                        phone: $("#playerEditContactNo").val(),
                        email: $("#playerEditEmail").val(),
                        coach_id: $("#eventCoach").val(),
                        sub_coach_id: $("#eventSubCoach").val(),
                        no_player: $("#totalPlayer").val(),
                        booking_id: bookingId,
                        team_number: $scope.teamNumber,
                        feesArray: res.data.feesArray,
                        promoPrice: res.data.promoPrice
                    };
                    localStorage.setItem("updatedBookingDetails", JSON.stringify(updatedBookingDetails));
                    $scope.openUpdateDetailsConfirmNew(bookingId);
                } else {
                    $scope.showAlert(1, res.data.message);
                }
            }, function error(response) {
                $scope.showAlert(1, "Can not update booking details!");
            });
        }

        $scope.openUpdateDetailsConfirmNew = function (bookingId) {
            var updatedBookingDetails = $.parseJSON(localStorage.getItem("updatedBookingDetails"));
            $http({
                method: 'post',
                url: apiUrl + 'getEventDetails',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: $.param({
                    booking_id: bookingId,
                    coach_id: updatedBookingDetails.coach_id,
                    sub_coach_id: updatedBookingDetails.sub_coach_id,
                    start_hours: updatedBookingDetails.start_hours,
                    start_minutes: updatedBookingDetails.start_minutes,
                    end_hours: updatedBookingDetails.end_hours,
                    end_minutes: updatedBookingDetails.end_minutes,
                    book_date: updatedBookingDetails.book_date,
                    court_id: updatedBookingDetails.court_id,
                    team_number: updatedBookingDetails.team_number
                }),
            }).then(function success(res) {
                res = res.data;
                $scope.closeAllForms('rightSideBarBookingDetailsConfirmUpdate');
                $scope.bookerName = updatedBookingDetails.first_name + " " + updatedBookingDetails.last_name;
                $scope.playerContactNo = updatedBookingDetails.phone;
                $scope.playerEmail = updatedBookingDetails.email;
                $scope.teamNumber = updatedBookingDetails.team_number;
                $scope.remoteUrl = apiUrl + 'getSearchPlayers/' + $scope.teamNumber + '/' + localStorage.getItem('club_id') + '/1/';
                $scope.bookingId = bookingId;
                $scope.totalPlayer = updatedBookingDetails.no_player;
                $scope.teams = [];
                $scope.teams = res.data.Team;
                $scope.coachName = res.Coach.name;
                $scope.subCoachName = res.SubCoach.name;
                $scope.startTime = res.StartTime;
                $scope.endTime = res.EndTime;

                $scope.clubMembershipId = res.data.Club.membership_id;
                $scope.userAppId = res.data.ClubBooking.user_id;
                var edate = new Date(res.BookDate + " " + $scope.startTime);
                var eedate = new Date(res.BookDate + " " + $scope.endTime);
                $scope.eventPlayTime = moment(edate).format('h:mma') + " - " + moment(eedate).format('h:mma');
                $scope.courtName = res.Court.name;
                $scope.start = res.BookDate;
                $scope.eventDate = moment(edate).format('Do MMMM YYYY');
                $scope.eventDay = moment(edate).format('dddd');
                $scope.bookingNumber = res.data.ClubBooking.prefix_booking_number + ("0000" + res.data.ClubBooking.booking_number).slice(-6);
                $scope.matchType = res.ClassType.name;
                $scope.picUrl = uploadUrl;
                $rootScope.courtBookingFee = parseFloat(updatedBookingDetails.feesArray.total_court_fees);
                $rootScope.coachBookingFee = parseFloat(updatedBookingDetails.feesArray.total_coach_fees);
                var subTotal = ($rootScope.courtBookingFee+$rootScope.coachBookingFee+parseFloat(updatedBookingDetails.feesArray.total_non_member_fees)) - parseFloat(updatedBookingDetails.promoPrice);
                var taxAmount = (subTotal*parseFloat(updatedBookingDetails.feesArray.tax_rate))/100;
                $rootScope.subTotal = subTotal;
                $rootScope.taxFee = parseFloat(updatedBookingDetails.feesArray.tax_rate);
                $rootScope.taxAmount = taxAmount;
                $rootScope.grandTotal = $rootScope.subTotal+$rootScope.taxAmount;
                $rootScope.promoDiscount = res.promoPrice;
                var diff =  $rootScope.grandTotal - res.data.ClubBooking.grand_total;
                if(diff>0){
                    $rootScope.diffTotal = diff;
                }else{
                    $rootScope.diffTotal = 0;
                }
                updatedBookingDetails.diffTotal = diff;
                localStorage.setItem("updatedBookingDetails",JSON.stringify(updatedBookingDetails));
                $rootScope.nonMemberFees = updatedBookingDetails.feesArray.total_non_member_fees;
            }, function error(response) {
                $scope.showAlert(1, response);
            });
        }

        $scope.makePaymentUpdateNew = function(paymentMode){
            var updatedBookingDetails = $.parseJSON(localStorage.getItem("updatedBookingDetails"));
            updatedBookingDetails.paymentMode = paymentMode;
            localStorage.setItem("updatedBookingDetails",JSON.stringify(updatedBookingDetails));
            if(paymentMode==1 || paymentMode==3){
                $scope.bookingUpdateNew();
            }else{
                $scope.closeAllForms('rightSideBarPaymentFormUpdate');
                $scope.bookingId = updatedBookingDetails.booking_id;
                $scope.grandTotal = updatedBookingDetails.diffTotal;
            }
        }

        $scope.confirmCardPaymentUpdateNew = function () {
            var $form = $('#payment-form-update');
            console.log(Stripe)
            Stripe.card.createToken($form, function (error, response) {
                console.log(error);
                console.log(response);
                if (error != 200) {
                    $scope.showAlert(1, response.error.message);
                } else {
                    var updatedBookingDetails = $.parseJSON(localStorage.getItem("updatedBookingDetails"));
                    $http({
                        method: 'post',
                        url: apiUrl + 'makePaymentUpdate',
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded'
                        },
                        data: $.param({
                            booking_id: updatedBookingDetails.booking_id,
                            payment_type: 2,
                            card_type: response.card.brand,
                            card_number: response.card.last4,
                            card_name: response.card.name
                        }),
                    }).then(function success(res) {
                        res = res.data;
                        if (res.status) {
                            $scope.paymentGatewayNew(response, updatedBookingDetails.booking_id, 2, updatedBookingDetails.diffTotal);
                        } else {
                            $scope.showAlert(1, res.message);
                        }
                    }, function error(response) {
                        $scope.showAlert(1, response);
                    });
                }
            });
            return false;
        }

        $scope.paymentGatewayNew = function (payment, bookingId, type, diffamount) {
            if (type == undefined) {
                type = 1;
            }
            if (diffamount == undefined) {
                diffamount = 0;
            }
            $http({
                method: 'post',
                url: apiUrl + 'chargeCard',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: $.param({
                    booking_id: bookingId,
                    payment: payment,
                    type: type,
                    diffamount: diffamount
                }),
            }).then(function success(res) {
                res = res.data;
                if (res.status) {
                    $scope.bookingUpdateNew();
                } else {
                    $scope.showAlert(1, res.message);
                }
            }, function error(response) {
                $scope.showAlert(1, response);
            });
        }

        $scope.bookingUpdateNew = function () {
            var updatedBookingDetails = $.parseJSON(localStorage.getItem("updatedBookingDetails"));
            $http({
                method: 'post',
                url: apiUrl + 'updateClubBook',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: $.param(updatedBookingDetails),
            }).then(function success(res) {
                res = res.data;
                if (res.status) {
                    $scope.showAlert(2, "Your booking has been updated successfully.", "Booking Successful");
                    $scope.closeRightSideBarDetails();
                    $scope.closeRightSideBarPaymentFormUpdate();
                    localStorage.removeItem("updatedBookingDetails");
                } else {
                    $scope.showAlert(1, res.message);
                }
            }, function error(response) {
                $scope.showAlert(1, response);
            });
        }

        $scope.$on('$viewContentLoaded', function () {
            $timeout(function () {
                var pageId = $location.path().replace("/", "");
                $(".sidebar-nav-inner li").removeClass("active");
                $("#" + pageId + "Menu").addClass("active");
                $(".datepicker").datepicker({
                    autoclose: true,
                    startDate: new Date()
                });
                $(".datepickerend").datepicker({
                    autoclose: true,
                    endDate: new Date()
                });
                $.fn.dataTable.ext.errMode = 'none';
            }, 1000);
            $scope.openMenu = function () {
                $("#wrapper").addClass("active");
                $("#sidebar-wrapper").removeClass("active");
            };
            $scope.closeMenu = function () {
                $("#sidebar-wrapper").addClass("active");
                $("#wrapper").removeClass("active");
            };
            $scope.closeRightSideBar = function () {
                $("#rightSideBar").hide();
                //$("#rightSideBar").animate({ width: 'toggle' }, 350);
                //$(".app-content-body").removeClass("sideBarOn");
                //$(".button-bottom").removeClass("sideBarOn");
            };

            $http({
                method: 'post',
                url: apiUrl + 'getGeneralSettings',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
            }).then(function success(res) {
                if (res.data.status) {
                    $rootScope.generalSettings = res.data.data.Setting;
                    var paymentMethods = $rootScope.generalSettings.payment_method.split(",");
                    $rootScope.paymentMethods = paymentMethods;
                }
            }, function error(response) {
                // no comments
            });
        });
    });

var openCoachBookingDetails = function (bookingId) {
    angular.element(document.getElementById('appCtrl')).scope().openBookingDetails(bookingId);
}

var closeCoachProfileCommon = function () {
    $(".overlay").addClass("hide");
    $("#coachProfileMainDiv").addClass("hide");
}

var showForm = function (ev) {
    if (ev == 1) {
        $("#addEventBooking input").each(function () {
            $(this).val('');
        });
        $("#addEventBooking select").each(function () {
            $(this).val('');
        });
        $("#rightSideBarAddForm").show();
        $("#rightSideBarAddGroupClass").hide();
        $("#chooseForm option[value='2']").attr("selected", false);
        $("#chooseForm option[value='1']").attr("selected", true);
        $("#chooseForm").val(1)
        //$("#chooseFormSchedule").val(1);
    } else if (ev == 2) {
        $("#addGroupClass input").each(function () {
            $(this).val('');
        });
        $("#addGroupClass select").each(function () {
            $(this).val('');
        });
        $("#addGroupClass textarea").each(function () {
            $(this).val('');
        });
        $("#rightSideBarAddForm").hide();
        $("#rightSideBarAddGroupClass").show();
        $("#chooseForm option[value='2']").attr("selected", true);
        $("#chooseForm option[value='1']").attr("selected", false);
        //$("#chooseFormGroupClass").val(2);
    }
    //$("#chooseForm").val(ev);
}

var checkMainCoach = function (type) {
    if (type == 1) {
        if ($("#eventAddSubCoach").val() != '') {
            if ($("#eventAddCoach").val() == '') {
                $("#eventAddSubCoach").val('');
                showAlert(1, "Before selecting an associate coach, please choose a main coach first.", "Select a main coach");
            } else if ($("#eventAddCoach").val() == $("#eventAddSubCoach").val()) {
                $("#eventAddSubCoach").val('');
                showAlert(1, "The same coach cannot be selected as both the main and associated coach. Please select a different coach for either field to continue.");
            }
        }
        if ($("#eventSubCoach").val() != '') {
            if ($("#eventCoach").val() == '') {
                $("#eventSubCoach").val('');
                showAlert(1, "Before selecting an associate coach, please choose a main coach first.", "Select a main coach");
            } else if ($("#eventCoach").val() == $("#eventSubCoach").val()) {
                $("#eventSubCoach").val('');
                showAlert(1, "The same coach cannot be selected as both the main and associated coach. Please select a different coach for either field to continue.");
            }
        }
    } else if (type == 2) {
        if ($("#classSubCoach").val() != '') {
            if ($("#classCoach").val() == '') {
                $("#classSubCoach").val('');
                showAlert(1, "Before selecting an associate coach, please choose a main coach first.", "Select a main coach");
            } else if ($("#classCoach").val() == $("#classSubCoach").val()) {
                $("#classSubCoach").val('');
                showAlert(1, "The same coach cannot be selected as both the main and associated coach. Please select a different coach for either field to continue.");
            }
        }
    }

}

var showAlert = function (type, message, title) {
    $("#errMsg").text(message);
    if (title != '' && title != undefined) {
        $("#errTitle").text(title);
    } else if (type == 1) {
        $("#errTitle").text("ERROR");
    } else {
        $("#errTitle").text("Changes saved!!");
    }
    $(".overlay").removeClass("hide");
    $("#boxAlert").removeClass("hide");
}


var showNotificationTab = function (id) {
    $("#" + id).show();
    $("#" + id).addClass("active");
    if (id == 'n1') {
        $("#n2, #n3, #n4, #n5").hide();
        $("#n2, #n3, #n4, #n5").removeClass("active");
    } else if (id == 'n2') {
        $("#n1, #n3, #n4, #n5").hide();
        $("#n1, #n3, #n4, #n5").removeClass("active");
    } else if (id == 'n3') {
        $("#n1, #n2, #n4, #n5").hide();
        $("#n1, #n2, #n4, #n5").removeClass("active");
    } else if (id == 'n4') {
        $("#n1, #n2, #n3, #n5").hide();
        $("#n1, #n2, #n3, #n5").removeClass("active");
    } else if (id == 'n5') {
        $("#n1, #n2, #n3, #n4").hide();
        $("#n1, #n2, #n3, #n4").removeClass("active");
    }
}

var closeNotifications = function () {
    $(".overlay").addClass("hide");
    $(".box-main").addClass("hide");
}

var getNotifications = function () {
    $.ajax({
        method: 'post',
        url: apiUrl + 'getUnreadNotifications',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
        data: $.param({
            user_id: localStorage.getItem('user_id'),
            grouped: true
        }),
    }).then(function success(res) {
        if (res.status) {
            var booking = res.data.booking.length;
            var coach = res.data.coach.length;
            var matchJoinRequest = res.data.matchJoinRequest.length;
            $(".noti-count").text(booking + coach + matchJoinRequest);
        } else {
            $(".noti-count").text(0);
        }
    }, function error(response) {
        //showAlert(1, response);
    });
}

var filterInput = function (event) {
    console.log(event)
    var keyCode = ('which' in event) ? event.which : event.keyCode;
    var isNotWanted = (keyCode == 69 || keyCode == 101);
    return !isNotWanted;
}

var showCoachProfileTab = function (id) {
    $("#" + id).show();
    $("#" + id).addClass("active");
    if (id == '1aa') {
        $("#2aa, #3aa, #4aa").hide();
        $("#2aa, #3aa, #4aa").removeClass("active");
    } else if (id == '2aa') {
        $("#1aa, #3aa, #4aa").hide();
        $("#1aa, #3aa, #4aa").removeClass("active");
    } else if (id == '3aa') {
        $("#1aa, #2aa, #4aa").hide();
        $("#1aa, #2aa, #4aa").removeClass("active");
    } else if (id == '4aa') {
        $("#1aa, #2aa, #3aa").hide();
        $("#1aa, #2aa, #3aa").removeClass("active");
    }
}

var closeRightSideBarPaymentFormPopup = function () {
    $(".overlay").addClass("hide");
    $("#rightSideBarPaymentFormPopup.box-main").addClass("hide");
}

var showCoachProfileCoaches = function (id, type) {
    angular.element(document.getElementById('cochesCtrl')).scope().showCoachProfileCoaches(id, type);
}

var openClassDetails = function (id) {
    angular.element(document.getElementById('playerPerformanceCtrl')).scope().openClassDetails(id);
}