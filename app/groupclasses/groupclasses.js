'use strict';

angular.module('myApp.groupclasses', ['ngRoute'])

    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/groupclasses', {
            templateUrl: 'groupclasses/groupclasses.html',
            controller: 'GroupclassesCtrl'
        });
    }])

    .controller('GroupclassesCtrl', function ($scope, $http, $timeout, $compile, $rootScope, $templateCache) {
        //console.log('Group Classes Loading...');
        if ($rootScope.searchData != undefined) {
            var searchData = $rootScope.searchData;
        } else {
            var searchData = '';
        }
        $scope.$on('$viewContentLoaded', function () {
            $scope.contentLoading(1);
        });


        $scope.addGroupClass = function () {
            var currentForm = $('#addGroupClassForm');
            $http({
                method: 'post',
                url: apiUrl + 'getGroupClassRate',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: $.param({
                    book_date: currentForm.find("#classBookDate").val(),
                    court_id: currentForm.find("#classCourtNo").val(),
                    start_hours: currentForm.find("#classStartHour").val(),
                    start_minutes: currentForm.find("#classStartMinute").val(),
                    end_hours: currentForm.find("#classEndHour").val(),
                    end_minutes: currentForm.find("#classEndMinute").val(),
                    no_player: currentForm.find("#classTotalPlayer").val(),
                    coach_id: currentForm.find("#classCoach").val()
                }),
            }).then(function success(res) {
                if (res.data.status) {
                    $rootScope.groupCounter = 0;
                    console.log(res.data);
                    $rootScope.courtFeeAdd = parseFloat(res.data.feesArray.total_court_fees);
                    $rootScope.taxRateAdd = parseFloat(res.data.feesArray.tax_rate);
                    $rootScope.totalCoachFeeAdd = parseFloat(res.data.feesArray.total_coach_fees);
                    $rootScope.subTotalAdd = $rootScope.totalCoachFeeAdd + $rootScope.courtFeeAdd;
                    $rootScope.taxAmtAdd = ($rootScope.subTotalAdd * $rootScope.taxRateAdd) / 100;
                    $rootScope.grandTotalAdd = parseFloat($rootScope.subTotalAdd) + parseFloat($rootScope.taxAmtAdd);
                    $rootScope.totalClassPlayerAdd = parseInt(currentForm.find("#classTotalPlayer").val())
                    $rootScope.perHeadFeeAdd = $rootScope.grandTotalAdd / $rootScope.totalClassPlayerAdd;
                    $scope.showConfirm("Do you want to create this class?", 0, "confirmCreateGroupClassAdd", "Verify Price");
                } else {
                    $scope.showAlert(1, res.data.message);
                }
            }, function error(response) {
                $scope.showAlert(1, response);
            });
        }

        $scope.$on('confirmCreateGroupClassAdd', function (ev, args) {
            $rootScope.groupCounter++;
            if ($rootScope.groupCounter == 1) {
                var currentForm = $('#addGroupClassForm');
                var isClubMember = currentForm.find("#isClubMember").is(":checked") ? 1 : 2;
                var ageGroup = currentForm.find("#classPlayerAgeGroup").val().split("-");
                $http({
                    method: 'post',
                    url: apiUrl + 'addGroupClassByCurd',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    data: $.param({
                        is_club_member: isClubMember,
                        class_name: currentForm.find("#className").val(),
                        class_description: currentForm.find("#classDescription").val(),
                        book_date: currentForm.find("#classBookDate").val(),
                        court_id: currentForm.find("#classCourtNo").val(),
                        start_hours: currentForm.find("#classStartHour").val(),
                        start_minutes: currentForm.find("#classStartMinute").val(),
                        end_hours: currentForm.find("#classEndHour").val(),
                        end_minutes: currentForm.find("#classEndMinute").val(),
                        min_player_rating: currentForm.find("#classPlayerMinRating").val(),
                        max_player_rating: currentForm.find("#classPlayerMaxRating").val(),
                        min_player_age: ageGroup[0],
                        max_player_age: ageGroup[1],
                        no_player: currentForm.find("#classTotalPlayer").val(),
                        coach_id: currentForm.find("#classCoach").val(),
                        sub_coach_id: currentForm.find("#classSubCoach").val(),
                        court_booking_fee: $rootScope.courtFeeAdd,
                        coach_booking_fee: $rootScope.totalCoachFeeAdd,
                        sub_total: $rootScope.subTotalAdd,
                        tax_amount: $rootScope.taxAmtAdd,
                        tax_fee: $rootScope.taxRateAdd,
                        grand_total: $rootScope.grandTotalAdd,
                        club_id: localStorage.getItem('club_id'),
                        user_id: localStorage.getItem('user_id')
                    }),
                }).then(function success(res) {
                    if (res.data.status) {
                        $scope.groupClassbooked(res.data.data);
                        $scope.closeRightSideBarAddGroupClass();
                        $scope.contentLoading(1);
                        $scope.showAlert(2, 'Class created successfully');
                    } else {
                        $scope.showAlert(1, res.data.message);
                    }
                }, function error(response) {
                    $scope.showAlert(1, response);
                });
            }

        });


        $scope.contentLoading = function (page_no) {
            $http({
                method: 'post',
                url: apiUrl + 'allGroupclasses',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: $.param({
                    page_no: page_no,
                    club_id: localStorage.getItem("club_id"),
                }),
            }).then(function success(res) {
                //console.log(res.data.data);
                //console.log($scope);
                if (res.data.status) {
                    var groupClasses = [];
                    $.each(res.data.data, function (i, val) {
                        var edate = new Date(val.ClubBookingSession.book_date + " " + val.ClubBookingSession.book_start_time);
                        val.ClubBookingSession.book_start_time = edate;
                        var edate = new Date(val.ClubBookingSession.book_date + " " + val.ClubBookingSession.book_end_time);
                        val.ClubBookingSession.book_end_time = edate;
                        val.ClubBookingSession.class_description = val.ClubBookingSession.class_description.substring(0, 100);
                        val.JoinGroupClass = val.JoinGroupClass.length;
                        val.ClubBookingSession.grand_total_single = (parseFloat(val.ClubBookingSession.grand_total) / parseInt(val.ClubBookingSession.no_player)).toFixed(2)
                        groupClasses.push(val);
                    });
                    $rootScope.allCoaches = res.data.coaches;
                    $rootScope.courts = res.data.courts;
                    $scope.groupClasses = groupClasses;
                    $scope.totalPages = 1;
                    $scope.currentPage = 1;
                    if (res.data.totalPages) {
                        $scope.totalPages = res.data.totalPages;
                        $scope.currentPage = res.data.currentPage;
                    }
                } else {
                    $rootScope.allCoaches = res.data.coaches;
                    $rootScope.courts = res.data.courts;
                    //$scope.showAlert(1, res.data.message);
                }
            }, function error(response) {
                $scope.showAlert(1, JSON.stringify(response));
            });
        }

        $scope.getRightSideBarDetails = function (groupClassId) {
            $scope.closeRightSideBarAddGroupClass();
            $http({
                method: 'post',
                url: apiUrl + 'allGroupclasses',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: $.param({
                    group_class_id: groupClassId,
                    club_id: localStorage.getItem("club_id"),
                }),
            }).then(function success(res) {
                if (res.data.status) {
                    $rootScope.isDashboard = false;
                    $("#rightSideBarGroupClassDetails").show();
                    //$(".app-content-body").addClass("sideBarOn");
                    //$(".button-bottom").addClass("sideBarOn");
                    var val = res.data.data;
                    var edate = new Date(val.ClubBookingSession.book_date + " " + val.ClubBookingSession.book_start_time);
                    val.ClubBookingSession.book_start_time = edate;
                    var edate = new Date(val.ClubBookingSession.book_date + " " + val.ClubBookingSession.book_end_time);
                    val.ClubBookingSession.book_end_time = edate;
                    //console.log(val);
                    $rootScope.groupClassDeatilData = val;
                    $rootScope.picUrl = uploadUrl;
                    var totalActive = 0;
                    var totalActiveCharge = 0;
                    var totalCancel = 0;
                    var totalCancelCharge = 0;
                    $.each(val.JoinGroupClass, function (i, classData) {
                        if (classData.status == 1) {
                            totalActive = parseInt(totalActive) + 1;
                            totalActiveCharge = parseFloat(totalActiveCharge) + parseFloat(classData.amount);
                        } else {
                            totalCancel = parseInt(totalCancel) + 1;
                            totalCancelCharge = parseFloat(totalCancelCharge) + parseFloat(classData.total_cancellation_charge);
                        }
                    });
                    $rootScope.playersadded = res.data.players;
                    $rootScope.totalActive = totalActive;
                    $rootScope.totalActiveCharge = totalActiveCharge;
                    $rootScope.totalCancel = totalCancel;
                    $rootScope.totalCancelCharge = totalCancelCharge;
                    $rootScope.cancelClassCount = val.CancelClass.length;
                } else {
                    $scope.showAlert(1, res.data.message);
                }
            }, function error(response) {
                $scope.showAlert(1, JSON.stringify(response));
            });

        }

        $scope.showFormCreateGroupClassAdd = function () {
            $scope.closeRightSideBarAddGroupClass();
            $("#rightSideBarAddGroupClass").show();
            $("#rightSideBarAddGroupClass").find('select').prop('selectedIndex', 0);
            $("#rightSideBarAddGroupClass").find('input[type="text"], textarea').val('');
            $("#rightSideBarAddGroupClass").find('input[type="checkbox"]').prop('checked', false);
            $rootScope.courtFeeAdd = 0;
            $rootScope.totalCoachFeeAdd = 0;
            $rootScope.coachNameAdd = 0;
            $rootScope.subTotalAdd = 0;
            $rootScope.taxRateAdd = 0;
            $rootScope.taxAmtAdd = 0;
            $rootScope.grandTotalAdd = 0;
            $rootScope.totalClassPlayerAdd = 0;
            $rootScope.perHeadFeeAdd = 0;
        }

        $scope.getRightSideBarDetailsEdit = function (groupClassId) {
            $scope.closeRightSideBarAddGroupClass();
            $http({
                method: 'post',
                url: apiUrl + 'allGroupclasses',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: $.param({
                    group_class_id: groupClassId,
                    club_id: localStorage.getItem("club_id"),
                }),
            }).then(function success(res) {
                if (res.data.status) {
                    var editClass = $("#rightSideBarEditGroupClass");
                    editClass.show();
                    //$(".app-content-body").addClass("sideBarOn");
                    //$(".button-bottom").addClass("sideBarOn");
                    var val = res.data.data;
                    var book_start_time = val.ClubBookingSession.book_start_time.split(':');
                    var book_end_time = val.ClubBookingSession.book_end_time.split(':');
                    var edate = new Date(val.ClubBookingSession.book_date + " " + val.ClubBookingSession.book_start_time);
                    val.ClubBookingSession.book_start_time = edate;
                    var edate = new Date(val.ClubBookingSession.book_date + " " + val.ClubBookingSession.book_end_time);
                    val.ClubBookingSession.book_end_time = edate;
                    if (val.ClubBookingSession.is_club_member == 1) {
                        editClass.find('#isClubMember').prop('checked', true);
                    } else {
                        editClass.find('#isClubMember').prop('checked', false);
                    }
                    editClass.find('#className').val(val.ClubBookingSession.class_name);
                    editClass.find('#classDescription').val(val.ClubBookingSession.class_description);
                    editClass.find('#classBookDateEdit').val(val.ClubBookingSession.book_date);
                    editClass.find('#classStartHourEdit').val(book_start_time[0]);
                    editClass.find('#classStartMinuteEdit').val(book_start_time[1]);
                    editClass.find('#classEndHourEdit').val(book_end_time[0]);
                    editClass.find('#classEndMinuteEdit').val(book_end_time[1]);
                    editClass.find('#classPlayerMinRatingEdit').val(val.ClubBookingSession.min_player_rating);
                    editClass.find('#classPlayerMaxRatingEdit').val(val.ClubBookingSession.max_player_rating);
                    editClass.find('#classPlayerAgeGroup').val(val.ClubBookingSession.min_player_age + '-' + val.ClubBookingSession.max_player_age);
                    editClass.find('#classTotalPlayer').val(val.ClubBookingSession.no_player);
                    editClass.find('#groupClassId').val(val.ClubBookingSession.id);
                    $rootScope.groupClassDeatilData = val;
                    $rootScope.allCoaches = res.data.coaches;
                    $rootScope.courts = res.data.courts;
                    $rootScope.picUrl = uploadUrl;
                    $rootScope.perHeadFee = (val.ClubBookingSession.grand_total / val.ClubBookingSession.no_player).toFixed(4);
                    $rootScope.totalClassPlayer = val.ClubBookingSession.no_player;
                    $rootScope.grandTotal = val.ClubBookingSession.grand_total;
                    $rootScope.taxRate = val.ClubBookingSession.tax_fee;
                    $rootScope.taxAmt = val.ClubBookingSession.tax_amount;
                    $rootScope.subTotal = val.ClubBookingSession.sub_total;
                    $rootScope.coachName = val.Coach.name;
                    $rootScope.totalCoachFee = val.ClubBookingSession.coach_booking_fee;
                    $rootScope.courtFee = val.ClubBookingSession.court_booking_fee;
                    //console.log(val);
                    setTimeout(function () {
                        editClass.find('#classCoach').val(val.ClubBookingSession.coach_id);
                        editClass.find('#classSubCoach').val(val.ClubBookingSession.sub_coach_id);
                        editClass.find('#classCourtNo').val(val.ClubBookingSession.court_id);
                        editClass.find('#classStartHourEdit').val(parseInt(book_start_time[0]));
                        editClass.find('#classStartMinuteEdit').val((book_start_time[1]));
                        editClass.find('#classEndHourEdit').val(parseInt(book_end_time[0]));
                        editClass.find('#classEndMinuteEdit').val((book_end_time[1]));
                    }, 1000);
                } else {
                    $scope.showAlert(1, res.data.message);
                }
            }, function error(response) {
                $scope.showAlert(1, JSON.stringify(response));
            });

        }

        $scope.searchGroupClasses = function () {
            ////console.log($scope.search)
            searchData = $scope.search;
            searchData.classEndDate = $('#classEndDate').val();
            searchData.classStartDate = $('#classStartDate').val();
            searchData.club_id = localStorage.getItem("club_id");
            //alert(JSON.stringify($scope.search));
            var searchKeys = [];
            var startDate = '';
            var endDate = '';
            var startHour = '';
            var endHour = '';
            var startPrice = '';
            var endPrice = '';
            $.each(searchData, function (i, v) {
                if (i == 'className') {
                    var strSearch = v + " class";
                    searchKeys.push(strSearch);
                } else if (i == 'classCoachName') {
                    var strSearch = v + " coach";
                    searchKeys.push(strSearch);
                } else if (i == 'classStartDate' && v != '') {
                    startDate = v;
                } else if (i == 'classEndDate' && v != '') {
                    endDate = v;
                } else if (i == 'classStartHour') {
                    startHour = v;
                } else if (i == 'classEndHour') {
                    endHour = v;
                } else if (i == 'classPlayerRating') {
                    var strSearch = v + " Rating";
                    searchKeys.push(strSearch);
                } else if (i == 'classPlayerAgeGroup') {
                    var strSearch = v + " Age Group";
                    searchKeys.push(strSearch);
                } else if (i == 'classStartPrice') {
                    startPrice = v;
                } else if (i == 'classEndPrice') {
                    endPrice = v;
                } else if (i == 'classClubmembers') {
                    var strSearch = "Club Members Only";
                    searchKeys.push(strSearch);
                } else if (i == 'classHideFull') {
                    var strSearch = "Hide Full Class";
                    searchKeys.push(strSearch);
                }
            });
            if (startDate != '' && endDate != '') {
                searchKeys.push(startDate + " - " + endDate);
            } else if (startDate != '') {
                searchKeys.push(startDate);
            } else if (endDate != '') {
                searchKeys.push(endDate);
            }

            if (startHour != '' && endHour != '') {
                searchKeys.push(startHour + " - " + endHour + " Hours");
            } else if (startHour != '') {
                searchKeys.push(startHour);
            } else if (endHour != '') {
                searchKeys.push(endHour);
            }

            if (startPrice != '' && endPrice != '') {
                searchKeys.push(startPrice + " - " + endPrice + " Price");
            } else if (startPrice != '') {
                searchKeys.push(startPrice);
            } else if (endPrice != '') {
                searchKeys.push(endPrice);
            }
            //alert(searchKeys);
            ////console.log($('#classEndDate').val());
            $http({
                method: 'post',
                url: apiUrl + 'allGroupclassesSearch',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: $.param({
                    searchData: searchData
                }),
            }).then(function success(res) {
                //console.log(res.data.data);
                //console.log($scope);
                if (res.data.status) {
                    var groupClasses = [];
                    $.each(res.data.data, function (i, val) {
                        var edate = new Date(val.ClubBookingSession.book_date + " " + val.ClubBookingSession.book_start_time);
                        val.ClubBookingSession.book_start_time = edate;
                        var edate = new Date(val.ClubBookingSession.book_date + " " + val.ClubBookingSession.book_end_time);
                        val.ClubBookingSession.book_end_time = edate;
                        val.ClubBookingSession.class_description = val.ClubBookingSession.class_description.substring(0, 100);
                        val.JoinGroupClass = val.JoinGroupClass.length;
                        val.ClubBookingSession.grand_total_single = (parseFloat(val.ClubBookingSession.grand_total) / parseInt(val.ClubBookingSession.no_player)).toFixed(2)
                        groupClasses.push(val);
                    });
                    $scope.groupClasses = groupClasses;
                    $scope.totalPages = 1;
                    $scope.currentPage = 1;

                    var searchArray = searchKeys.join(",");
                    $("#groupClassFilterText").text(searchArray);
                    $("#groupClassFilter").removeClass("hide");
                    $scope.closeRightSideBarAddGroupClass();
                    $scope.$apply();
                } else {
                    $scope.showAlert(1, "Unable to find group class. Please try again.", "Not Found");
                }
            }, function error(response) {
                $scope.showAlert(1, "Unable to find group class. Please try again.", "System Error");
            });
        }

        $scope.editGroupClass = function () {
            var currentForm = $('#editGroupClassFrom');
            var isClubMember = currentForm.find("#isClubMember").is(":checked") ? 1 : 2;
            var ageGroup = currentForm.find("#classPlayerAgeGroup").val().split("-");
            $http({
                method: 'post',
                url: apiUrl + 'addGroupClassByCurd',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: $.param({
                    is_club_member: isClubMember,
                    id: currentForm.find("#groupClassId").val(),
                    class_name: currentForm.find("#className").val(),
                    class_description: currentForm.find("#classDescription").val(),
                    book_date: currentForm.find("#classBookDateEdit").val(),
                    court_id: currentForm.find("#classCourtNo").val(),
                    start_hours: currentForm.find("#classStartHourEdit").val(),
                    start_minutes: currentForm.find("#classStartMinuteEdit").val(),
                    end_hours: currentForm.find("#classEndHourEdit").val(),
                    end_minutes: currentForm.find("#classEndMinuteEdit").val(),
                    min_player_rating: currentForm.find("#classPlayerMinRatingEdit").val(),
                    max_player_rating: currentForm.find("#classPlayerMaxRatingEdit").val(),
                    min_player_age: ageGroup[0],
                    max_player_age: ageGroup[1],
                    no_player: currentForm.find("#classTotalPlayer").val(),
                    coach_id: currentForm.find("#classCoach").val(),
                    sub_coach_id: currentForm.find("#classSubCoach").val(),
                    court_booking_fee: $rootScope.courtFee,
                    coach_booking_fee: $rootScope.totalCoachFee,
                    sub_total: $rootScope.subTotal,
                    tax_amount: $rootScope.taxAmt,
                    tax_fee: $rootScope.taxRate,
                    grand_total: $rootScope.grandTotal,
                    club_id: localStorage.getItem('club_id'),
                    user_id: localStorage.getItem('user_id')
                }),
            }).then(function success(res) {
                if (res.data.status) {
                    $scope.closeRightSideBarAddGroupClass();
                    $scope.contentLoading(1);
                    $scope.showAlert(2, 'Class updated successfully');
                } else {
                    $scope.showAlert(1, res.data.message);
                }
            }, function error(response) {
                $scope.showAlert(1, response);
            });
        }

        $scope.getCourtEditFee = function ($court) {
            $http({
                method: 'post',
                url: apiUrl + 'getCourtDetails',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: $.param({
                    court_id: $court,
                }),
            }).then(function success(res) {
                if (res.data.status) {
                    console.log(res.data)
                    $rootScope.courtFee = parseFloat(res.data.data.Court.rate);
                    $rootScope.taxRate = parseFloat(res.data.data.Club.tax_rate);
                    if ($rootScope.totalCoachFee != undefined) {
                        $rootScope.subTotal = $rootScope.totalCoachFee + $rootScope.courtFee;
                    } else {
                        $rootScope.subTotal = $rootScope.courtFee;
                    }
                    if ($rootScope.subTotal != undefined && $rootScope.taxRate > 0) {
                        $rootScope.taxAmt = ($rootScope.subTotal * $rootScope.taxRate) / 100;
                    } else {

                        $rootScope.taxAmt = 0;
                    }
                    $rootScope.grandTotal = parseFloat($rootScope.subTotal) + parseFloat($rootScope.taxAmt);
                    $rootScope.perHeadFee = $rootScope.grandTotal / $rootScope.totalClassPlayer;
                } else {
                    $scope.showAlert(1, res.data.message);
                }
            }, function error(response) {
                $scope.showAlert(1, response);
            });
        }

        $scope.getCourtAddFee = function ($court) {
            $http({
                method: 'post',
                url: apiUrl + 'getCourtDetails',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: $.param({
                    court_id: $court,
                }),
            }).then(function success(res) {
                if (res.data.status) {
                    console.log(res.data)
                    $scope.courtFeeAdd = parseFloat(res.data.data.Court.rate);
                    $scope.taxRateAdd = parseFloat(res.data.data.Club.tax_rate);
                    if ($scope.totalCoachFeeAdd != undefined) {
                        $scope.subTotalAdd = $scope.totalCoachFeeAdd + $scope.courtFeeAdd;
                    } else {
                        $scope.subTotalAdd = $scope.courtFeeAdd;
                    }
                    if ($scope.subTotalAdd != undefined && $scope.taxRateAdd > 0) {
                        $scope.taxAmtAdd = ($scope.subTotalAdd * $scope.taxRateAdd) / 100;
                    } else {
                        $scope.taxAmtAdd = 0;
                    }
                    $scope.grandTotalAdd = parseFloat($scope.subTotalAdd) + parseFloat($scope.taxAmtAdd);
                    $scope.perHeadFeeAdd = $scope.grandTotalAdd / $scope.totalClassPlayerAdd;
                } else {
                    $scope.showAlert(1, res.data.message);
                }
            }, function error(response) {
                $scope.showAlert(1, response);
            });
        }

        $scope.getCoachAddFee = function ($coach) {
            if ($('#classCoach').val() != '' && $('#classCoach').val() == $('#classSubCoach').val()) {
                $("#classCoach").val('');
                showAlert(1, "Coach 1 and coach 2 can not be same");
            } else {
                if ($("#classStartHour").val() == "") {
                    $("#classCoach").val("");
                    $scope.showAlert(1, "Please choose class start hour");
                } else if ($("#classStartMinute").val() == "") {
                    $("#classCoach").val("");
                    $scope.showAlert(1, "Please choose class start minute");
                } else if ($("#classEndHour").val() == "") {
                    $("#classCoach").val("");
                    $scope.showAlert(1, "Please choose class end hour");
                } else if ($("#classEndMinute").val() == "") {
                    $("#classCoach").val("");
                    $scope.showAlert(1, "Please choose class end minute");
                } else {
                    $http({
                        method: 'post',
                        url: apiUrl + 'getCoachRate',
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded'
                        },
                        data: $.param({
                            coach_id: $coach,
                            book_date: $("#classBookDate").val(),
                            start_hours: $("#classStartHour").val(),
                            start_minutes: $("#classStartMinute").val(),
                            end_hours: $("#classEndHour").val(),
                            end_minutes: $("#classEndMinute").val()
                        }),
                    }).then(function success(res) {
                        if (res.data.status) {
                            //console.log(res.data);
                            $scope.totalCoachFeeAdd = parseFloat(res.data.data);
                            if ($scope.courtFeeAdd != undefined) {
                                $scope.subTotalAdd = $scope.totalCoachFeeAdd + $scope.courtFeeAdd;
                            } else {
                                $scope.subTotalAdd = $scope.totalCoachFeeAdd;
                            }
                            if ($scope.taxRateAdd != undefined && $scope.taxRateAdd > 0) {
                                $scope.taxAmtAdd = ($scope.subTotalAdd * $scope.taxRateAdd) / 100;
                            } else {
                                $scope.taxAmtAdd = 0;
                            }
                            $scope.grandTotalAdd = parseFloat($scope.subTotalAdd) + parseFloat($scope.taxAmtAdd);
                            $scope.perHeadFeeAdd = $scope.grandTotalAdd / $scope.totalClassPlayerAdd;
                            //$scope.courtFee = res.data.data.Court.rate;
                        } else {
                            $scope.showAlert(1, res.data.message);
                            $("#classCoach").prop('selectedIndex', 0);
                        }
                    }, function error(response) {
                        $scope.showAlert(1, response);
                        $("#classCoach").prop('selectedIndex', 0);
                    });
                }
            }
        }

        $scope.setClassTotalPlayerAdd = function (classPlayer) {
            $scope.totalClassPlayerAdd = classPlayer;
            $scope.perHeadFeeAdd = $scope.grandTotalAdd / $scope.totalClassPlayerAdd;
        }

        $scope.checkExistMainCoch = function ($coach) {
            if ($('.editCoachData').val() != '' && $('.editCoachData').val() == $('.editSubCoachData').val()) {
                $(".editSubCoachData").val('');
                showAlert(1, "Coach 1 and coach 2 can not be same");
            }
        }

        $scope.getCoachEditFee = function ($coach) {
            if ($('.editCoachData').val() != '' && $('.editCoachData').val() == $('.editSubCoachData').val()) {
                $(".editCoachData").val('');
                showAlert(1, "Coach 1 and coach 2 can not be same");
            } else {
                if ($("#classStartHourEdit").val() == "") {
                    $("#classCoachEdit").val("");
                    $scope.showAlert(1, "Please choose class start hour");
                } else if ($("#classStartMinuteEdit").val() == "") {
                    $("#classCoachEdit").val("");
                    $scope.showAlert(1, "Please choose class start minute");
                } else if ($("#classEndHourEdit").val() == "") {
                    $("#classCoachEdit").val("");
                    $scope.showAlert(1, "Please choose class end hour");
                } else if ($("#classEndMinuteEdit").val() == "") {
                    $("#classCoachEdit").val("");
                    $scope.showAlert(1, "Please choose class end minute");
                } else {
                    $http({
                        method: 'post',
                        url: apiUrl + 'getCoachRate',
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded'
                        },
                        data: $.param({
                            coach_id: $coach,
                            book_date: $("#classBookDateEdit").val(),
                            start_hours: $("#classStartHourEdit").val(),
                            start_minutes: $("#classStartMinuteEdit").val(),
                            end_hours: $("#classEndHourEdit").val(),
                            end_minutes: $("#classEndMinuteEdit").val()
                        }),
                    }).then(function success(res) {
                        if (res.data.status) {
                            //console.log(res.data);
                            $rootScope.totalCoachFee = parseFloat(res.data.data);
                            if ($rootScope.courtFee != undefined) {
                                $rootScope.subTotal = $rootScope.totalCoachFee + $rootScope.courtFee;
                            } else {
                                $rootScope.subTotal = $rootScope.totalCoachFee;
                            }
                            if ($rootScope.taxRate != undefined && $rootScope.taxRate > 0) {
                                $rootScope.taxAmt = ($rootScope.subTotal * $rootScope.taxRate) / 100;
                            } else {
                                $rootScope.taxAmt = 0;
                            }
                            $rootScope.grandTotal = parseFloat($rootScope.subTotal) + parseFloat($rootScope.taxAmt);
                            $rootScope.perHeadFee = $rootScope.grandTotal / $rootScope.totalClassPlayer;
                            //$scope.courtFee = res.data.data.Court.rate;
                        } else {
                            $scope.showAlert(1, res.data.message);
                            $("#classCoach").prop('selectedIndex', 0);
                        }
                    }, function error(response) {
                        $scope.showAlert(1, response);
                        $("#classCoach").prop('selectedIndex', 0);
                    });
                }
            }
        }

        $scope.setClassTotalEditPlayer = function (classPlayer) {
            $rootScope.totalClassPlayer = classPlayer;
            $rootScope.perHeadFee = $rootScope.grandTotal / $rootScope.totalClassPlayer;
        }







        $scope.removePlayerFromClass = function (player) {
            var classId = $('#classIdForJoin').val();
            $http({
                method: 'post',
                url: apiUrl + 'cancelClassWithRefundUser',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: $.param({
                    group_class_id: classId,
                    player_id: player
                }),
            }).then(function success(res) {
                if (res.data.status) {
                    $('.alreadyAdded.playerBlok_' + player).remove();
                    $scope.contentLoading(1);
                    $scope.showAlert(2, res.data.message);
                } else {
                    $scope.showAlert(1, res.data.message);
                }
            }, function error(response) {
                $scope.showAlert(1, JSON.stringify(response));
            });
            return;
        }



        $scope.makePaymentJoin = function (type) {
            var no_players = $('#no_players_data').val();
            var no_join_players = $('.playersInfoData').length;
            var forJoinClass = $('.selectedTOAddClass').length;
            var classId = $('#classIdForJoin').val();
            if (forJoinClass > 0) {
                var playersIds = [];
                $('.selectedTOAddClass').each(function () {
                    playersIds.push($(this).val());
                });
            } else {
                $scope.showAlert(1, 'Please add a minimum of 1 player to proceed.');
                return false;
            }
            $http({
                method: 'post',
                url: apiUrl + 'checkAvailableClassJoinPlayers',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: $.param({
                    group_class_id: classId,
                    playersIds: playersIds
                }),
            }).then(function success(res) {
                if (res.data.status) {
                    $scope.closeRightSideBarAddGroupClass();
                    $('#rightSideBarPaymentForm').show();
                    $scope.bookingId = classId;
                    $scope.grandTotal = $rootScope.totalmountCharge;
                } else {
                    $scope.showAlert(1, res.data.message);
                }
            }, function error(response) {
                $scope.showAlert(1, JSON.stringify(response));
            });


        }

        $scope.paymentGateway = function (payment, bookingId) {
            $http({
                method: 'post',
                url: apiUrl + 'chargeCardAddplayer',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: $.param({
                    booking_id: bookingId,
                    payment: payment,
                    amount: $rootScope.totalmountCharge
                }),
            }).then(function success(res) {
                res = res.data;
                if (res.status) {
                    //console.log(res);
                    $scope.bookingConfirm(res.transction_id.Transaction.id);
                } else {
                    $scope.showAlert(1, res.message);
                }
            }, function error(response) {
                $scope.showAlert(1, response);
            });
        }

        $scope.confirmCardPayment = function (bookingId) {
            var $form = $('#payment-form');
            //console.log(Stripe)
            Stripe.card.createToken($form, function (error, response) {
                //console.log(error);
                //console.log(response);
                if (error != 200) {
                    $scope.showAlert(1, response.error.message);
                } else {
                    $http({
                        method: 'post',
                        url: apiUrl + 'makePaymentAddplayer',
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded'
                        },
                        data: $.param({
                            booking_id: bookingId,
                            payment_type: 2,
                            card_type: response.card.brand,
                            booking_type: 2,
                            card_number: response.card.last4,
                            card_name: response.card.name
                        }),
                    }).then(function success(res) {
                        res = res.data;
                        if (res.status) {
                            $scope.paymentGateway(response, bookingId);
                        } else {
                            $scope.showAlert(1, res.message);
                        }
                    }, function error(response) {
                        $scope.showAlert(1, response);
                    });
                }
            });
            return false;

        }

        $scope.bookingConfirm = function (bookingId) {
            var no_players = $('#no_players_data').val();
            var no_join_players = $('.playersInfoData').length;
            var forJoinClass = $('.selectedTOAddClass').length;
            var classId = $('#classIdForJoin').val();
            var playersIds = [];
            $('.selectedTOAddClass').each(function () {
                playersIds.push($(this).val());
            });
            $http({
                method: 'post',
                url: apiUrl + 'setClassJoinPlayers',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: $.param({
                    group_class_id: classId,
                    payment_type: 2,
                    playersIds: playersIds,
                    transction_id: bookingId,
                    amount: $rootScope.groupClassDeatilData.ClubBookingSession.grand_total / $rootScope.groupClassDeatilData.ClubBookingSession.no_player
                }),
            }).then(function success(res) {
                if (res.data.status) {
                    $scope.closeRightSideBarAddGroupClass();
                    $scope.showAlert(2, res.data.message);
                    $scope.contentLoading(1);
                } else {
                    $scope.showAlert(1, res.data.message);
                }
            }, function error(response) {
                $scope.showAlert(1, JSON.stringify(response));
            });
        }



        $scope.cashPaymentToJaoinClass = function (payment_type) {
            var no_players = $('#no_players_data').val();
            var no_join_players = $('.playersInfoData').length;
            var forJoinClass = $('.selectedTOAddClass').length;
            var classId = $('#classIdForJoin').val();
            if (forJoinClass > 0) {
                var playersIds = [];
                $('.selectedTOAddClass').each(function () {
                    playersIds.push($(this).val());
                });
                $http({
                    method: 'post',
                    url: apiUrl + 'setClassJoinPlayers',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    data: $.param({
                        group_class_id: classId,
                        payment_type: payment_type,
                        playersIds: playersIds,
                        amount: $rootScope.groupClassDeatilData.ClubBookingSession.grand_total / $rootScope.groupClassDeatilData.ClubBookingSession.no_player
                    }),
                }).then(function success(res) {
                    if (res.data.status) {
                        $scope.closeRightSideBarAddGroupClass();
                        $scope.showAlert(2, res.data.message);
                        $scope.contentLoading(1);
                    } else {
                        $scope.showAlert(1, res.data.message);
                    }
                }, function error(response) {
                    $scope.showAlert(1, JSON.stringify(response));
                });
            } else {
                $scope.showAlert(1, 'Please add a minimum of 1 player to proceed.');
            }
        }

        $scope.cancelClass = function (classId) {
            $http({
                method: 'post',
                url: apiUrl + 'cancelClassWithRefundNew',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: $.param({
                    group_class_id: classId,
                    user_id: localStorage.getItem('user_id')
                }),
            }).then(function success(res) {
                if (res.data.status) {
                    $scope.closeRightSideBarAddGroupClass();
                    $scope.showAlert(2, "Class has been cancelled");
                    $scope.contentLoading(1);
                } else {
                    $scope.showAlert(1, res.data.message);
                }
            }, function error(response) {
                $scope.showAlert(1, JSON.stringify(response));
            });
        }

        $scope.openLeftSearchBar = function () {
            $("#rightSideBarSearchGroupClass").show();
            $("#rightSideBarSearchGroupClass").find('select').prop('selectedIndex', 0);
            $("#rightSideBarSearchGroupClass").find('input[type="text"]').val('');
            $("#rightSideBarSearchGroupClass").find('input[type="checkbox"]').prop('checked', false);
            /*var date = new Date();
             var classStartDate = date.setMonth(date.getMonth()-6);
             var month = date.getMonth()+1;
             $('#classStartDate').val(date.getFullYear()+'-'+(month<10?'0'+month:month)+'-01');
             var classEndDate = date.setMonth(date.getMonth() +12);
             var month = date.getMonth()+1;
             $('#classEndDate').val(date.getFullYear()+'-'+(month<10?'0'+month:month)+'-01');*/
        }

        $scope.range = function (min, max, step) {
            step = step || 1;
            var input = [];
            for (var i = min; i <= max; i += step) {
                input.push(i);
            }
            return input;
        }

        $scope.backToGroupClass = function () {
            window.location.reload(true);
        }

        $scope.searchGroupClass = function (keyword) {
            $http({
                method: 'post',
                url: apiUrl + 'allGroupclasses',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: $.param({
                    club_id: localStorage.getItem("club_id"),
                    keyword: keyword,
                    type: $("#search_by").val()
                }),
            }).then(function success(res) {
                //console.log(res.data.data);
                //console.log($scope);
                if (res.data.status) {
                    var groupClasses = [];
                    $.each(res.data.data, function (i, val) {
                        var edate = new Date(val.ClubBookingSession.book_date + " " + val.ClubBookingSession.book_start_time);
                        val.ClubBookingSession.book_start_time = edate;
                        var edate = new Date(val.ClubBookingSession.book_date + " " + val.ClubBookingSession.book_end_time);
                        val.ClubBookingSession.book_end_time = edate;
                        val.ClubBookingSession.class_description = val.ClubBookingSession.class_description.substring(0, 100);
                        val.JoinGroupClass = val.JoinGroupClass.length;
                        val.ClubBookingSession.grand_total_single = (parseFloat(val.ClubBookingSession.grand_total) / parseInt(val.ClubBookingSession.no_player)).toFixed(2)
                        groupClasses.push(val);
                    });
                    $rootScope.allCoaches = res.data.coaches;
                    $rootScope.courts = res.data.courts;
                    $scope.groupClasses = groupClasses;
                    $scope.totalPages = 1;
                    $scope.currentPage = 1;
                    if (res.data.totalPages) {
                        $scope.totalPages = res.data.totalPages;
                        $scope.currentPage = res.data.currentPage;
                    }
                } else {
                    $rootScope.allCoaches = res.data.coaches;
                    $rootScope.courts = res.data.courts;
                    //$scope.showAlert(1, res.data.message);
                }
            }, function error(response) {
                $scope.showAlert(1, JSON.stringify(response));
            });
        }

        $scope.sortGroupClass = function (sorttype) {
            $http({
                method: 'post',
                url: apiUrl + 'allGroupclasses',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: $.param({
                    club_id: localStorage.getItem("club_id"),
                    sorttype: sorttype
                }),
            }).then(function success(res) {
                //console.log(res.data.data);
                //console.log($scope);
                if (res.data.status) {
                    var groupClasses = [];
                    $.each(res.data.data, function (i, val) {
                        var edate = new Date(val.ClubBookingSession.book_date + " " + val.ClubBookingSession.book_start_time);
                        val.ClubBookingSession.book_start_time = edate;
                        var edate = new Date(val.ClubBookingSession.book_date + " " + val.ClubBookingSession.book_end_time);
                        val.ClubBookingSession.book_end_time = edate;
                        val.ClubBookingSession.class_description = val.ClubBookingSession.class_description.substring(0, 100);
                        val.JoinGroupClass = val.JoinGroupClass.length;
                        val.ClubBookingSession.grand_total_single = (parseFloat(val.ClubBookingSession.grand_total) / parseInt(val.ClubBookingSession.no_player)).toFixed(2)
                        groupClasses.push(val);
                    });
                    $rootScope.allCoaches = res.data.coaches;
                    $rootScope.courts = res.data.courts;
                    $scope.groupClasses = groupClasses;
                    $scope.totalPages = 1;
                    $scope.currentPage = 1;
                    if (res.data.totalPages) {
                        $scope.totalPages = res.data.totalPages;
                        $scope.currentPage = res.data.currentPage;
                    }
                } else {
                    $rootScope.allCoaches = res.data.coaches;
                    $rootScope.courts = res.data.courts;
                    //$scope.showAlert(1, res.data.message);
                }
            }, function error(response) {
                $scope.showAlert(1, JSON.stringify(response));
            });
        }
    })

