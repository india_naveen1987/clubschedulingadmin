'use strict';

angular.module('myApp.bookings', ['ngRoute'])

    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/bookings', {
            templateUrl: 'bookings/bookings.html',
            controller: 'BookingsCtrl'
        });
    }])

    .directive('csSelect', function () {
        return {
            require: '^stTable',
            template: '<input type="checkbox"/>',
            scope: {
                row: '=csSelect'
            },
            link: function (scope, element, attr, ctrl) {

                element.bind('change', function (evt) {
                    scope.$apply(function () {
                        ctrl.select(scope.row, 'multiple');
                    });
                });

                scope.$watch('row.isSelected', function (newValue, oldValue) {
                    if (newValue === true) {
                        element.parent().addClass('st-selected');
                    } else {
                        element.parent().removeClass('st-selected');
                    }
                });
            }
        };
    })

    .controller('BookingsCtrl', function ($scope, $http, $timeout, $compile, $rootScope, $filter) {
        console.log("bookings ctrl");
        console.log($rootScope.searchData);
        if ($rootScope.searchData != undefined) {
            var searchData = $rootScope.searchData;
        } else {
            var searchData = '';
        }
        var table;
        var k = 0;
        $scope.picUrl = uploadUrl;
        $scope.search = {};


        $scope.$on('$viewContentLoaded', function () {
            $http({
                method: 'post',
                url: apiUrl + 'clubProfile',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: $.param({
                    id: localStorage.getItem('club_id'),
                }),
            }).then(function success(res) {
                res = res.data;
                console.log(res);
                $scope.courts = res.data.Court;
                $scope.allCoaches = res.data.Coach;
                $rootScope.maxPrivatePlayers = res.data.Club.private_class_max_players;
                $rootScope.maxGroupPlayers = res.data.Club.group_class_max_players;
                if (res.data.ClubOperatingHour.length == 0) {
                    $scope.clubMinTime = "09:00:00";
                    $scope.clubMaxTime = "22:00:00";
                } else {
                    $scope.clubMinTime = res.data.ClubOperatingHour[0].start_time;
                    $scope.clubMaxTime = res.data.ClubOperatingHour[0].end_time;
                }
                console.log($scope.clubMinTime);
            }, function error(response) {
                // do nothing
            });
            if (searchData != '') {
                $scope.contentLoading(1);
            } else {
                $scope.showUpcomingBookings();
            }
        });


        $scope.contentLoading = function (type) {
            k++;
            if (k > 1) {
                table.destroy();
            }
            table = $('#example').DataTable({
                "autoWidth": false,
                
                "ajax": {
                    "destroy": true,
                    "url": apiUrl + 'upcomingClubBookings',
                    "type": 'post',
                    "headers": {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    "data": {
                        "club_id": localStorage.getItem("club_id"),
                        "type": type,
                        "searchData": searchData
                    },
                    "dataSrc": function (json) {
                        console.log(json);
                        var fData = [];
                        if (json.status) {
                            $.each(json.data, function (index, val) {
                                console.log(val);
                                var checkdelete = "<input type='checkbox' id='check" + val.ClubBooking.id + "' class='checkselect'  />&nbsp;&nbsp;";
                                var book_date = val.ClubBooking.book_date;
                                var book_time = $filter('formatTime')(val.ClubBooking.book_start_time) + " - " + $filter('formatTime')(val.ClubBooking.book_end_time);
                                var court = val.Court.name;
                                var courtSort = val.Court.name;
                                var booking_number = val.ClubBooking.prefix_booking_number + " - " + val.ClubBooking.booking_number;
                                var booker_name = val.ClubBooking.booker_name;
                                var details = " <a href='javascript:' ng-click='openDetails(" + val.ClubBooking.id + ")'><img style='margin-right:30px' class='ancher-img' src='images/edit.png'/></a><a href='javascript:' ng-click='getRightSideBarDetails(" + val.ClubBooking.id + ")'><img src='images/ahed-arrow.png'/></a>";
                                var res = {
                                    checkdelete: checkdelete,
                                    book_date: book_date,
                                    book_time: book_time,
                                    court: court,
                                    courtSort: courtSort,
                                    booking_number: booking_number,
                                    booker_name: booker_name,
                                    details: details
                                }
                                fData.push(res);
                            });
                        }


                        return fData;
                        /*for (var i = 0, ien = json.length; i < ien; i++) {
                            json[i][0] = '<a href="/message/' + json[i][0] + '>View message</a>';
                        }

                        {
                            "_": "Court "+Court.name,
                            "sort": "Court.name"
                            };
                        return json;*/
                    }
                }, "fnDrawCallback": function () {
                    var html = '<a href="javascript:" class="form-control" id="deleteBookings" ng-click="deleteBookings()"><img src="images/delete.png"/></a>';
                    $("#example_info").html(html);
                    var html = '<img src="images/left-arrow.png"/>';
                    $("#example_previous").html(html);
                    var html = '<img src="images/right-arrow.png"/>';
                    $("#example_next").html(html);
                    $("#example_filter label input").attr("placeholder"," Type here to search");
                    if(searchData!=''){
                        $("#example_filter").find(".groupclassBack").remove("div.groupclassBack");
                        $("#example_filter").append('<div class="groupclassBack" ng-click="backToBookings()"><a href="javascript:void(0)"><i class="ion-android-arrow-back"></i>&nbsp;BACK TO BOOKINGS</a></div>');
                    }
                    $compile($("#example"))($scope);
                    $compile($("#example_info"))($scope);
                    $compile($("#example_previous"))($scope);
                    $compile($("#example_next"))($scope);
                    $compile($("#example_filter"))($scope);
                },
                "columns": [
                    { "data": "checkdelete"},
                    { "data": "book_date" },
                    { "data": "book_time" },
                    {
                        "data": {
                            _: "court",
                            sort: "courtSort"
                        }
                    },
                    { "data": "booking_number" },
                    { "data": "booker_name"},
                    { "data": "details" }
                ],
                "pageLength": 5
                

            });

        }

        $scope.backToBookings = function(){
            window.location.reload(true);
        }

        $scope.searchBookings = function () {
            $scope.search.bookDate = $("#searchDate").val();
            console.log($scope.search)
            searchData = $scope.search;
            $scope.contentLoading(1);
        }

        $scope.closeRightSideBarBookingDetails = function () {
            $("#rightSideBarBookingDetails").hide();
            //$(".app-content-body").removeClass("sideBarOn");
            //$(".button-bottom").removeClass("sideBarOn");
        }

        $scope.deleteBookings = function () {
            var n = $('input:checkbox[id^="check"]:checked').length;
            if (n == 0) {
                $scope.showAlert(1, "Please select bookings", "ERROR");
            } else {
                $scope.showConfirm("Would you like to delete the following booking(s)?", 0, "removeBookings", "Are you sure?");
            }

        }

        $scope.$on('removeBookings', function (ev, args) {
            var ids = [];
            $('input:checkbox[id^="check"]:checked').each(function () {
                ids.push($(this).attr("id").replace("check", ""));

            });
            if (ids.length > 0) {
                $http({
                    method: 'post',
                    url: apiUrl + 'deleteClubBooking',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    data: $.param({
                        ids: ids,
                        user_id: localStorage.getItem('user_id')
                    }),
                }).then(function success(res) {
                    $scope.showAlert(2, "The following booking(s) have been deleted successfully.", "Successful");
                    $("#upcomingBookings").removeClass("active");
                    $scope.showUpcomingBookings();
                    //window.location.reload();
                }, function error(response) {
                    $scope.showAlert(1, response);
                });
            }
        })

        $scope.$on('showUpcomingBookings', function () {
            if (!$("#upcomingBookings").hasClass("active") && searchData == '') {
                $("#upcomingBookings").addClass("active");
                $("#archievedBookings").removeClass("active");
                $scope.contentLoading(1);
            } else if (searchData != '') {
                searchData = '';
                $scope.contentLoading(1);
            }
        });

        $scope.showUpcomingBookings = function () {
            if (!$("#upcomingBookings").hasClass("active") && searchData == '') {
                $("#upcomingBookings").addClass("active");
                $("#archievedBookings").removeClass("active");
                $scope.contentLoading(1);
            } else if (searchData != '') {
                searchData = '';
                $scope.contentLoading(1);
            }
        }

        $scope.showArcheivedBookings = function () {
            if (!$("#archievedBookings").hasClass("active") && searchData == '') {
                $("#upcomingBookings").removeClass("active");
                $("#archievedBookings").addClass("active");
                $scope.contentLoading(2);
            } else if (searchData != '') {
                searchData = '';
                $scope.contentLoading(2);
            }
        }

        $scope.clearSearch = function () {
            searchData = '';
            $("#upcomingBookings").removeClass("active");
            $scope.showUpcomingBookings();
        }

        $scope.selectDeselectAll = function () {
            if ($("#checkAll").is(":checked")) {
                $(".checkselect").prop("checked", true);
            } else {
                $(".checkselect").prop("checked", false);
            }
        }

        



        $scope.closeRightSideBarDetails = function () {
            $("#rightSideBarDetails").hide();
            //$(".app-content-body").removeClass("sideBarOn");
            //$(".button-bottom").removeClass("sideBarOn");
        }



    })