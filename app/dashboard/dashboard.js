'use strict';

angular.module('myApp.dashboard', ['ngRoute'])

        .config(['$routeProvider', function ($routeProvider) {
                $routeProvider.when('/dashboard', {
                    templateUrl: 'dashboard/dashboard.html',
                    controller: 'DashboardCtrl'
                });
            }])

        .controller('DashboardCtrl', function ($scope, $http, $filter, uiCalendarConfig, $timeout, $rootScope, $compile) {
            $(".background-div").css("height", $(window).height());


            $scope.eventSources = [];
            $scope.allCoaches = [];

            $scope.selectedCourts = [];

            $scope.teamNumber = 0;

            $scope.clubMinTime = '';
            $scope.clubMaxTime = '';
            $scope.picUrl = uploadUrl;
            if (localStorage.getItem('role_id') == 3) {
                $scope.club_id = localStorage.getItem('club_id');
            } else if (localStorage.getItem('role_id') == 2) {
                $scope.coach_id = localStorage.getItem('coach_id');
            }


            $scope.payment = {};
            var viewName = '';
            var viewDate = '';
            var kendoCurrent = '';
            var kendoView = '';




            $scope.$on('$viewContentLoaded', function () {
                if (localStorage.getItem('role_id') == 3) {
                    $http({
                        method: 'post',
                        url: apiUrl + 'clubProfile',
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded'
                        },
                        data: $.param({
                            id: localStorage.getItem('club_id'),
                        }),
                    }).then(function success(res) {
                        res = res.data;
                        console.log(res);
                        $scope.courts = res.data.Court;
                        $scope.allCoaches = res.data.Coach;
                        $rootScope.maxPrivatePlayers = res.data.Club.private_class_max_players;
						$rootScope.maxGroupPlayers = res.data.Club.group_class_max_players;
                        if (res.data.ClubOperatingHour.length == 0) {
                            $scope.clubMinTime = "09:00:00";
                            $scope.clubMaxTime = "22:00:00";
                        } else {
                            $scope.clubMinTime = res.data.ClubOperatingHour[0].start_time;
                            $scope.clubMaxTime = res.data.ClubOperatingHour[0].end_time;
                        }
                        console.log($scope.clubMinTime);
                        $scope.$broadcast("setCalendar", {id: localStorage.getItem('club_id'), court_id: '', type: 1, class_type_id: ''});
                    }, function error(response) {
                        // do nothing
                    });
                } else if (localStorage.getItem('role_id') == 2) {
                    $http({
                        method: 'post',
                        url: apiUrl + 'getAllCourtsByCoachId',
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded'
                        },
                        data: $.param({
                            id: localStorage.getItem('coach_id'),
                        }),
                    }).then(function success(res) {
                        res = res.data;
                        console.log(res);
                        $scope.courts = res.data;
                        $scope.clubMinTime = "01:00:00";
                        $scope.clubMaxTime = "23:59:59";
                        $scope.$broadcast("setCalendar", {id: localStorage.getItem('coach_id'), court_id: '', type: 1, class_type_id: ''});
                    }, function error(response) {
                        // do nothing
                    });
                }

                $scope.role_id = localStorage.getItem('role_id');
                var $form = $('#payment-form');

            });

            $scope.setCalendarFlow = function (club_id, court_id, type) {
                var class_type_id = '';
                var courtIds = [];
                $.each($scope.selectedCourts, function (j, k) {
                    courtIds.push(k.id);
                });
                if (courtIds.length == 0) {
                    courtIds = '';
                }
                if (localStorage.getItem('role_id') == 3) {
                    var coach_id = $("#coachId").val();
                    //alert(JSON.stringify({id: club_id, court_id: courtIds, type: type, coach_id: coach_id, class_type_id: class_type_id}));
                    $scope.$broadcast("setCalendar", {id: club_id, court_id: courtIds, type: type, coach_id: coach_id, class_type_id: class_type_id});
                } else if (localStorage.getItem('role_id') == 2) {
                    $scope.$broadcast("setCalendar", {id: club_id, court_id: courtIds, type: type, class_type_id: class_type_id});
                }
            }

            $scope.setCalendarFlowByCoach = function (club_id, coach_id, type) {
                var court_id = $("#courtId").val();
                var class_type_id = '';
                $scope.$broadcast("setCalendar", {id: club_id, court_id: court_id, type: type, coach_id: coach_id, class_type_id: class_type_id});
            }

            $scope.setCalendarFlowByClassType = function (id, ev, club_id, type) {
                if (localStorage.getItem('role_id') == 3) {
                    var coach_id = $("#coachId").val();
                    var court_id = $("#courtId").val();
                    var class_type_id = id;
                    if ($(ev.target).hasClass("calendarfiltertype")) {
                        $(ev.target).removeClass("calendarfiltertype").addClass("calendarfiltertypedisabled");
                        $scope.$broadcast("setCalendar", {id: club_id, court_id: court_id, type: type, coach_id: coach_id, class_type_id: class_type_id});
                    } else {
                        $(ev.target).removeClass("calendarfiltertypedisabled").addClass("calendarfiltertype");
                        $scope.$broadcast("setCalendar", {id: club_id, court_id: court_id, type: type, coach_id: coach_id, class_type_id: ''});
                    }
                } else if (localStorage.getItem('role_id') == 2) {
                    var court_id = $("#courtId").val();
                    var class_type_id = id;
                    if ($(ev.target).hasClass("calendarfiltertypecoach")) {
                        $(ev.target).removeClass("calendarfiltertypecoach").addClass("calendarfiltertypedisabledcoach");
                        $scope.$broadcast("setCalendar", {id: club_id, court_id: court_id, type: type, class_type_id: class_type_id});
                    } else {
                        $(ev.target).removeClass("calendarfiltertypedisabledcoach").addClass("calendarfiltertypecoach");
                        $scope.$broadcast("setCalendar", {id: club_id, court_id: court_id, type: type, class_type_id: ''});
                    }
                }

            }


            $scope.showCurrentDayView = function () {
                var value = new Date();

                calendar.navigate(moment(value), "month");
                $timeout(function () {
                    uiCalendarConfig.calendars['myCalendar'].fullCalendar('changeView', 'agendaDay');
                    uiCalendarConfig.calendars['myCalendar'].fullCalendar('gotoDate', moment(value));
                }, 500);
            }

            $scope.$on('setCalendar', function (ev, args) {
                var id = args.id;
                var court_id = args.court_id;
                var coach_id = args.coach_id;
                var class_type_id = args.class_type_id;
                var type = args.type;
                //alert(id+":"+court_id)
                if (type == 2) {
                    var view = uiCalendarConfig.calendars['myCalendar'].fullCalendar('getView');
                    var date = uiCalendarConfig.calendars['myCalendar'].fullCalendar('getDate');
                    //var date = $("#calendar").fullCalendar('getDate');
                    //var month_int = date.getMonth();
                    console.log(moment(date).format('MM'));
                    console.log(view.name);
                    viewName = view.name;
                    viewDate = moment(date);
                    kendoCurrent = calendar.current();
                    kendoView = calendar.view();
                    console.log(kendoCurrent);
                    console.log(kendoView);
                    calendar.destroy();
                    $("#kendoCalendar").empty();
                }
                if (localStorage.getItem("role_id") == 3) {
                    var params = $.param({
                        id: id,
                        court_id: court_id,
                        coach_id: coach_id,
                        class_type_id: class_type_id,
                        role_id: 3
                    })
                } else if (localStorage.getItem("role_id") == 2) {
                    var params = $.param({
                        id: id,
                        court_id: court_id,
                        coach_id: coach_id,
                        class_type_id: class_type_id,
                        role_id: 2
                    })
                }
                $http({
                    method: 'post',
                    url: apiUrl + 'clubProfile',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    data: params,
                }).then(function success(res) {
                    console.log(res)
                    res = res.data;
                    var scheduleList = [];
                    if (res.status) {
                        var resString = JSON.stringify(res.data);
                        resString = resString.replace(/book_start_time/g, 'start');
                        resString = resString.replace(/book_end_time/g, 'end');
                        var schedules = JSON.parse(resString);
                        console.log(schedules);
                        $.each(schedules, function (index, obj) {
                            var schedule = {};
                            console.log(obj);
                            if (obj.ClubBooking.class_type_id != 10) {
                                var date = obj.ClubBooking.book_date;
                                schedule.start = date + 'T' + obj.ClubBooking.start;
                                schedule.end = date + 'T' + obj.ClubBooking.end;
                                schedule.type = obj.ClassType.name;
                                schedule.classTypeId = obj.ClassType.id;
                                schedule.coachId = obj.Coach.id;
                                schedule.coachName = obj.Coach.name != null ? obj.Coach.name : '';
                                schedule.title = schedule.type + '\n' + schedule.coachName;
                                schedule.id = obj.ClubBooking.id;
                                schedule.data = obj;
                                if (obj.ClassType.id >= 2 && obj.ClassType.id <= 5) {
                                    schedule.textColor = '#FFFFFF';
                                    //schedule.rendering = 'background';
                                    schedule.backgroundColor = '#7DB9CB';
                                } else if (obj.ClassType.id == 6 || obj.ClassType.id == 1 || obj.ClassType.id == 7) {
                                    schedule.textColor = '#FFFFFF';
                                    schedule.backgroundColor = '#FF994B';
                                }
                            } else {
                                var date = obj.ClubBooking.date;
                                schedule.start = date + 'T' + obj.ClubBooking.start_time;
                                schedule.end = date + 'T' + obj.ClubBooking.end_time;
                                schedule.type = 'BlockDate';
                                schedule.classTypeId = obj.ClubBooking.class_type_id;
                                schedule.coachId = obj.ClubBooking.coach_id;
                                schedule.coachName = obj.ClubBooking.coach_name;
                                schedule.title = schedule.type + '\n' + schedule.coachName;
                                schedule.id = obj.ClubBooking.id;
                                schedule.data = obj;
                                schedule.textColor = '#000';
                                schedule.backgroundColor = '#E6E6E6';
                            }

                            scheduleList.push(schedule);
                        });
                        console.log(scheduleList)
                        $scope.uiConfig = {
                            calendar: {
                                editable: false,
                                selectable: true,
                                selectHelper: true,
                                allDaySlot: false,
                                slotDuration: '00:15:00',
                                eventBorderColor: 'transparent',
                                eventBackgroundColor: 'transparent',
                                minTime: $scope.clubMinTime,
                                maxTime: $scope.clubMaxTime,
                                eventOverlap: function (stillEvent, movingEvent) {
                                    return false;
                                },
                                eventLimit: true,
                                
                                header: {
                                    left: 'title,prev,next',
                                    center: '',
                                    right: 'month agendaWeek agendaDay'
                                },
                                events: scheduleList,
                                aspectRatio: 2,
                                height: "parent",
                                dayClick: function (date, jsEvent, view) {
                                    console.log(jsEvent);
                                    //alert('Clicked on: ' + date.format());
                                    //alert('Coordinates: ' + jsEvent.pageX + ',' + jsEvent.pageY);
                                    //alert('Current view: ' + view.name);
                                    var events = uiCalendarConfig.calendars['myCalendar'].fullCalendar('clientEvents');

                                    //console.log(events)
                                    var currentEvent = false;

                                    for (var i = 0; i < events.length; i++) {
                                        //console.log(date.format());
                                        //console.log(events[i].start.format("YYYY-MM-DD"));
                                        if (date.format() == events[i].start.format("YYYY-MM-DD")) {
                                            console.log(date.format())
                                            currentEvent = true;
                                            break;
                                        }
                                    }
                                    if(currentEvent && localStorage.getItem('role_id') == 3){
                                        $scope.getEventDetailsByDate(date.format());
                                    }else{
                                        if (localStorage.getItem('role_id') == 3) {
                                            $scope.openAddForm();
                                            $("#bookAddDate").datepicker("update", new Date(date.format()));
                                        } else if (localStorage.getItem('role_id') == 2) {
                                            $scope.openBlockDate();
                                            $("#coachBlockDate").datepicker("update", new Date(date.format()));
                                        }
                                    }
                                },
                                viewRender: function (view, element) {
                                    console.log(view);
                                    console.log(element);
                                    console.log(calendar);
                                    console.log($("#kendoCalendar"))
                                    if (view.name == 'month') {
                                        var currentDate = uiCalendarConfig.calendars['myCalendar'].fullCalendar('getDate');
                                        //alert(new Date(currentDate)+" = "+calendar.current())
                                        //alert(moment(calendar.current()).format("MM")+" = "+moment(currentDate).format("MM"))
                                        if (moment(calendar.current()).format("MM") != moment(currentDate).format("MM")) {

                                            calendar.navigate(currentDate, view.name);
                                        }

                                    }
                                    $("#cdayview").remove();
                                    var html = '<img id="cdayview" ng-click="showCurrentDayView()" class="inactive-image" src="images/calander1.png" style="width:20px;height:20px" />';
                                    $(".fc-right").append(html);
                                    $compile($(".fc-right"))($scope);
                                    if (view.name == 'agendaWeek') {
                                        /*var dayName = moment(currentDate).format("ddd").toLowerCase();
                                         $(".fc-" + dayName).css("text-decoration", "underline");
                                         $(".fc-" + dayName).css("font-weight", "bold");*/
                                    }

                                    if (view.name == 'agendaDay') {
                                        console.log(event);
                                    }
                                },
                                eventClick: function (calEvent, jsEvent, view) {
                                    console.log(calEvent);
                                    if (localStorage.getItem('role_id') == 3) {
                                        if (calEvent.classTypeId < 6 || calEvent.classTypeId > 6) {
                                            if (calEvent.classTypeId != 10) {
                                                $scope.closeAllForms('rightSideBar');
                                                $scope.getEventDetails(calEvent.id);
                                            }
                                        } else if (calEvent.classTypeId == 6) {
                                            $scope.getRightSideBarGroupClassDetails(calEvent.id);
                                        } else {
                                            return false;
                                        }
                                    } else if (localStorage.getItem('role_id') == 2) {
                                        if (localStorage.getItem('coach_id') == calEvent.coachId) {
                                            if (calEvent.classTypeId != 10) {
                                                $scope.openClassDetails(calEvent.id, calEvent.classTypeId);
                                            }
                                        }
                                    }

                                },
                                eventRender: function (event, element, view) {
                                    if (view.name == 'agendaWeek') {
                                        console.log(event);
                                        console.log(element);
                                        //$('.fc-col' + event.start.getDay()).not('.fc-widget-header').css('background-color', 'blue');
                                    }

                                    if (view.name == 'agendaDay') {
                                        console.log(event);
                                    }
                                }
                            }
                        };
                        // kendo ui calendar
                        var eventDates = [];
                        $.each(scheduleList, function (index, obj) {
                            var cdate = new Date(obj.start);
                            cdate.setTime(cdate.getTime() + (cdate.getTimezoneOffset() * 60 * 1000));
                            console.log(cdate)
                            var fdate = +new Date(cdate.getFullYear(), cdate.getMonth(), cdate.getDate());
                            console.log(fdate)
                            eventDates.push(fdate);
                        });
                        console.log(eventDates);
                        $("#kendoCalendar").kendoCalendar({
                            value: new Date(),
                            dates: eventDates,
                            weekNumber: false,
                            month: {
                                // template for dates in month view
                                content: '# if ($.inArray(+data.date, data.dates) != -1) { #' +
                                        '<div class="exhibition"' +
                                        '">#= data.value #</div>' +
                                        '# } else { #' +
                                        '#= data.value #' +
                                        '# } #'
                            },
                            footer: false,
                            change: function () {
                                var value = this.value();
                                console.log(moment(value));
                                uiCalendarConfig.calendars['myCalendar'].fullCalendar('changeView', 'agendaDay');
                                uiCalendarConfig.calendars['myCalendar'].fullCalendar('gotoDate', moment(value));
                                var cdate = new Date();
                                cdate = moment(cdate).format("YYYY-MM-DD");
                                value = moment(value).format("YYYY-MM-DD");
                                if (cdate != value) {
                                    $("#cdayview").removeClass("inactive-image");
                                    $("#cdayview").attr("ng-click", "showCurrentDayView()");
                                    $compile($(".fc-right"))($scope);
                                } else {
                                    $("#cdayview").addClass("inactive-image");
                                    $("#cdayview").removeAttr("onClick");
                                }
                            },
                            navigate: function () {
                                var view = this.view();
                                console.log(view.name); //name of the current view

                                var current = this.current();
                                console.log(current); //currently focused date
                                uiCalendarConfig.calendars['myCalendar'].fullCalendar('changeView', view.name);
                                uiCalendarConfig.calendars['myCalendar'].fullCalendar('gotoDate', moment(current));
                                $("#cdayview").removeClass("inactive-image");
                            }
                        });
                        calendar = $("#kendoCalendar").data("kendoCalendar");
                        if (viewName != '') {
                            $timeout(function () {
                                calendar.navigate(kendoCurrent, kendoView.name);
                                $timeout(function () {
                                    uiCalendarConfig.calendars['myCalendar'].fullCalendar('changeView', viewName);
                                    uiCalendarConfig.calendars['myCalendar'].fullCalendar('gotoDate', moment(viewDate).format("YYYY-MM-DD"));
                                }, 500);
                            }, 500);
                        }
                    }
                }, function error(response) {
                    // do nothing
                });
            });


            $scope.editRightSideBarDetails = function (bookingId) {
                $http({
                    method: 'post',
                    url: apiUrl + 'getEventDetails',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    data: $.param({
                        booking_id: bookingId
                    }),
                }).then(function success(res) {
                    res = res.data;
                    $scope.closeAllForms('rightSideBarDetails');
                    $scope.openCoachBox();
                    $scope.playerFirstName = res.data.ClubBooking.booked_first_name;
                    $scope.playerLastName = res.data.ClubBooking.booked_last_name;
                    $scope.playerContactNo = res.data.ClubBooking.booked_contact;
                    $scope.playerEmail = res.data.ClubBooking.booked_email;
                    $scope.teamNumber = res.data.ClubBooking.team_number;
                    $scope.remoteUrl = apiUrl + 'getSearchPlayers/' + $scope.teamNumber + '/';
                    $scope.bookingId = bookingId;
                    $("#totalPlayer").val(res.data.Team.length);
                    $scope.teams = res.data.Team;
                    $("#eventCoach").val(res.data.ClubBooking.coach_id);
                    $("#eventSubCoach").val(res.data.ClubBooking.sub_coach_id);
                    var startTime = res.data.ClubBooking.book_start_time;
                    startTime = $scope.getHoursMinutes(startTime);
                    startTime = startTime.split(":");
                    $("#eventStartHours").val(startTime[0]);
                    if (startTime[1] == 0) {
                        startTime[1] = "00";
                    }
                    $("#eventStartMinutes").val(startTime[1]);
                    var endTime = res.data.ClubBooking.book_end_time;
                    endTime = $scope.getHoursMinutes(endTime);
                    endTime = endTime.split(":");
                    $("#eventEndHours").val(endTime[0]);
                    if (endTime[1] == 0) {
                        endTime[1] = "00";
                    }
                    $("#eventEndMinutes").val(endTime[1]);
                    $("#eventCourt").val(res.data.ClubBooking.court_id);
                    //$scope.start = res.data.ClubBooking.book_date;
                    alert(res.data.ClubBooking.book_date)
                    $("#bookDate").datepicker("update", new Date(res.data.ClubBooking.book_date));
                    //$scope.totalPlayer = res.data.Team.length;
                }, function error(response) {
                    // do nothing
                });

            }

            $scope.openBlockDate = function () {
                $scope.closeAllForms('rightSideBarBlockDate');
                $("#addBlockDate input").each(function () {
                    $(this).val('');
                });
                $("#addBlockDate select").each(function () {
                    $(this).val('');
                });
                $("#addBlockDate textarea").each(function () {
                    $(this).val('');
                });
            }

            $scope.closeRightSideBarBlockDate = function () {
                $("#rightSideBarBlockDate").hide();
            }

            $scope.addBlockDate = function () {
                var userStartTime = new Date($("input[name=date]").val()+" "+$("select[name=start_hours]").val()+":"+$("select[name=start_minutes]").val()+":00");
                
                var d = new Date();
                //alert(userStartTime+" / "+d);
                if(userStartTime<d){
                    $scope.showAlert(1,"Block time can not smaller from current time");
                    return false;
                }
                $http({
                    method: 'post',
                    url: apiUrl + 'saveBlockDate',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    data: $("#addBlockDate").serialize() + "&" + $.param({
                        id: localStorage.getItem('coach_id')
                    }),
                }).then(function success(res) {
                    if (res.data.status) {
                        $scope.closeRightSideBarBlockDate();
                        $scope.showAlert(2, "These dates have been blocked successfully. Classes will not be scheduled on these selected dates.", "Date Blocked");
                    } else {
                        $scope.showAlert(1, res.data.message);
                    }
                }, function error(response) {
                    $scope.showAlert(1, JSON.stringify(response));
                });
            }

            $scope.getRightSideBarGroupClassDetails = function (groupClassId) {
                $http({
                    method: 'post',
                    url: apiUrl + 'allGroupclasses',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    data: $.param({
                        group_class_id: groupClassId,
                        club_id: localStorage.getItem("club_id"),
                    }),
                }).then(function success(res) {
                    console.log(res);
                    if (res.data.status) {
                        $rootScope.isDashboard = true;
                        $scope.closeAllForms("rightSideBarGroupClassDetails");
                        var val = res.data.data;
                        var edate = new Date(val.ClubBookingSession.book_date + " " + val.ClubBookingSession.book_start_time);
                        val.ClubBookingSession.book_start_time = edate;
                        var edate = new Date(val.ClubBookingSession.book_date + " " + val.ClubBookingSession.book_end_time);
                        val.ClubBookingSession.book_end_time = edate;
                        //console.log(val);
                        $rootScope.groupClassDeatilData = val;
                        $rootScope.picUrl = uploadUrl;
                        var totalActive = 0;
                        var totalActiveCharge = 0;
                        var totalCancel = 0;
                        var totalCancelCharge = 0;
                        $.each(val.JoinGroupClass, function (i, classData) {
                            if (classData.status == 1) {
                                totalActive = parseInt(totalActive) + 1;
                                totalActiveCharge = parseFloat(totalActiveCharge) + parseFloat(classData.amount);
                            } else {
                                totalCancel = parseInt(totalCancel) + 1;
                                totalCancelCharge = parseFloat(totalCancelCharge) + parseFloat(classData.total_cancellation_charge);
                            }
                        });
                        $rootScope.playersadded = res.data.players;
                        $rootScope.totalActive = totalActive;
                        $rootScope.totalActiveCharge = totalActiveCharge;
                        $rootScope.totalCancel = totalCancel;
                        $rootScope.totalCancelCharge = totalCancelCharge;
                        $rootScope.cancelClassCount = val.CancelClass.length;
                    } else {
                        $scope.showAlert(1, res.data.message);
                    }
                }, function error(response) {
                    $scope.showAlert(1, JSON.stringify(response));
                });

            }

            $scope.closeRightSideBarAddGroupClass = function () {
                $("#rightSideBarGroupClassDetails").hide();
            }

            $scope.openClassDetails = function (session_id, class_type_id) {
                $http({
                    method: 'post',
                    url: apiUrl + 'allGroupclasses',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    data: $.param({
                        group_class_id: session_id,
                        class_type_id: class_type_id
                    }),
                }).then(function success(res) {
                    if (res.data.status) {
                        $rootScope.displayType = 1;
                        $rootScope.session_id = session_id;
                        $scope.closeAllForms('rightSideBarGroupClassDetails');
                        $rootScope.groupClassDeatilData = res.data.data;
                        $rootScope.totalFeedback = 0;
                        $rootScope.totalJoinGroupClass = 0;
                        if (res.data.data.JoinGroupClass.length > 0) {
                            $.each(res.data.data.JoinGroupClass, function (i, v) {
                                $rootScope.totalFeedback += v.PlayerFeedback.length;
                                $rootScope.totalJoinGroupClass++;
                            });
                        }
                    } else {
                        $scope.showAlert(1, res.data.message);
                    }
                }, function error(response) {
                    $scope.showAlert(1, response);
                });
            }

            $scope.setCourtSelection = function (courtId) {
                $.each($scope.courts, function (i, v) {
                    if (localStorage.getItem("role_id") > 2) {
                        if (v.id == courtId) {
                            var flag = 0;
                            $.each($scope.selectedCourts, function (j, k) {
                                if (k.id == courtId) {
                                    flag = 1;
                                }
                            });
                            if (flag == 0) {
                                $scope.selectedCourts.push(v);
                                console.log($scope.selectedCourts);
                                if (localStorage.getItem("role_id") > 2) {
                                    $scope.setCalendarFlow($scope.club_id, courtId, 2);
                                } else {
                                    $scope.setCalendarFlow($scope.coach_id, courtId, 2);
                                }
                            } else {
                                $scope.showAlert(1, "This court is already added", "Already Added");
                            }
                        }
                    } else {
                        if (v.Court.id == courtId) {
                            var flag = 0;
                            $.each($scope.selectedCourts, function (j, k) {
                                if (k.id == courtId) {
                                    flag = 1;
                                }
                            });
                            if (flag == 0) {
                                $scope.selectedCourts.push(v.Court);
                                console.log($scope.selectedCourts);
                                if (localStorage.getItem("role_id") > 2) {
                                    $scope.setCalendarFlow($scope.club_id, courtId, 2);
                                } else {
                                    $scope.setCalendarFlow($scope.coach_id, courtId, 2);
                                }
                            } else {
                                $scope.showAlert(1, "This court is already added", "Already Added");
                            }
                        }
                    }

                });

            }

            $scope.removeSelectedCourt = function (id) {
                $scope.showConfirm("Do you want to delete this court selection?", id, "confirmRemoveSelectedCourt", "Delete Court Selection");
            }

            $scope.getEventDetailsByDate = function(bookDate){
                $http({
                    method: "POST",
                    url: apiUrl + "getEventDetailsByDate",
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    data: $.param({ bookDate: bookDate, club_id: localStorage.getItem("club_id") }),
                }).then(function success(eventResponse) {
                    eventResponse = eventResponse.data;
                    if(eventResponse.status){
                        $scope.closeAllForms('rightSideBarMultiple');
                        var edate = new Date(eventResponse.data[0].ClubBooking.book_date);
                        $scope.eventDate = moment(edate).format('Do MMMM YYYY');
                        $scope.eventDay = moment(edate).format('dddd');
                        $scope.events = eventResponse.data;
                        $rootScope.bookDate = eventResponse.data[0].ClubBooking.book_date;
                        $rootScope.bookStartTime = eventResponse.data[0].ClubBooking.book_start_time;
                    }else{
                        $scope.showAlert(1, 'Can not get events.');
                    }
                }, function error(eventResponse) {
                    $scope.showAlert(1, 'Can not get events.');
                });
            }

            $scope.isDiffrentBookingTime = function(bookDate, startTime){
                var edate = new Date($rootScope.bookDate + " " + $rootScope.bookStartTime);
                var previousStartHour = moment(edate).format('h');
                var eedate = new Date(bookDate + " " + startTime);
                var currentStartHour = moment(eedate).format('h');
                $rootScope.bookDate = bookDate;
                $rootScope.bookStartTime = startTime;
                if(previousStartHour==currentStartHour){
                    return false;
                }else{
                    return true;
                }
            }

            $scope.closeRightSideBarMultiple = function(){
                $("#rightSideBarMultiple").hide();
            }

            $scope.$on('confirmRemoveSelectedCourt', function (ev, args) {
                var index = (args.id);
                $("#selectedCourt" + index).remove();
                if ($scope.selectedCourts.length > 0) {
                    $.each($scope.selectedCourts, function (j, k) {
                        if (k.id == index) {
                            var s = $scope.selectedCourts.indexOf(k);
                            $scope.selectedCourts.splice(s, 1);
                            if ($scope.selectedCourts.length == 0) {
                                $("#courtId").val('');
                            }
                            if (localStorage.getItem("role_id") > 2) {
                                $scope.setCalendarFlow($scope.club_id, k.id, 2);
                            } else {
                                $scope.setCalendarFlow($scope.coach_id, k.id, 2);
                            }
                        }
                    });
                }
            });

        });

