'use strict';

angular.module('myApp.reports', ['ngRoute'])

    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/reports', {
            templateUrl: 'reports/reports.html',
            controller: 'ReportsCtrl'
        });
        $routeProvider.when('/coaches-reports', {
            templateUrl: 'reports/coaches-reports.html',
            controller: 'CoachesReportsCtrl'
        });
        $routeProvider.when('/general-reports', {
            templateUrl: 'reports/general-reports.html',
            controller: 'GeneralReportsCtrl'
        });
        $routeProvider.when('/reviews', {
            templateUrl: 'reports/reviews.html',
            controller: 'ReviewsCtrl'
        });
    }])

    .controller('ReportsCtrl', function ($scope, $http, $timeout, $compile, $rootScope, $templateCache) {
        $scope.startDate = '';
        $scope.endDate = '';
        $scope.promotionName = '';
        $scope.courtId = 0;
        $scope.paymentMode = 0;
        $scope.allPromo = false;
        $scope.userName = '';
        $scope.isAppBooking = 0;
        $scope.bookingType = 0;
        $rootScope.paymentModes = [1,2,3];
        
        $scope.showBookingsTab = function (activeTab, tabStartName, totalTabs) {
            for (var i = 1; i <= totalTabs; i++) {
                $("#" + tabStartName + i).hide();
                $("#" + tabStartName + i + "footer").addClass("hide");
                $("#" + tabStartName + i + "download").addClass("hide");
            }
            $("#" + activeTab).show();
            $("#" + activeTab + "footer").removeClass("hide");
            $("#" + activeTab + "download").removeClass("hide");
            if(activeTab=='tab3'){
                $scope.getPromotionReports();
            }else if(activeTab=='tab1'){
                $scope.getFeesReports();
            }
        }

        $scope.getPromotionReports = function () {
            if($scope.startDate==0){
                $scope.startDate = '';
            }
            if($scope.endDate==0){
                $scope.endDate = '';
            }
            if($scope.promotionName==0){
                $scope.promotionName = '';
            }
            $http({
                method: 'post',
                url: apiUrl + 'getPromotionReports',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: $.param({
                    club_id: localStorage.getItem('club_id'),
                    startDate: $scope.startDate,
                    endDate: $scope.endDate,
                    promotionName: $scope.promotionName
                }),
            }).then(function success(res) {
                if (res.data.status) {
                    $scope.promotionsReport = res.data.data;
                }
            }, function error(response) {
                // no comments
            });
        }

        $scope.filterByPromoName = function(keyword){
            $scope.promotionName = keyword;
            $scope.getPromotionReports();
        }

        $scope.downloadPromotionReport = function(){
            if($scope.startDate==''){
                $scope.startDate = 0;
            }
            if($scope.endDate==''){
                $scope.endDate = 0;
            }
            if($scope.promotionName==''){
                $scope.promotionName = 0;
            }
            window.open(apiUrl+"downloadPromoList/"+localStorage.getItem('club_id')+"/"+$scope.startDate+"/"+$scope.endDate+"/"+$scope.promotionName+".xlsx");
        }

        $scope.filterByCourt = function (cid, str) {
            $("#fees-reports-courts-filter").text(str);
            $scope.courtId = cid;
            $scope.getFeesReports();
        }

        $scope.filterByFeesType = function (pid, str) {
            $("#fees-reports-feestype-filter").text(str);
            $scope.paymentMode = pid;
            $scope.getFeesReports();
        }

        $scope.filterByPromo = function(keyword){
            $scope.promotionName = keyword;
            $scope.getFeesReports();
        }

        $scope.filterByAllPromo = function(event){
            $scope.allPromo = event;
            $scope.getFeesReports();
        }

        $scope.filterByUser = function(keyword){
            $scope.userName = keyword;
            $scope.getFeesReports();
        }

        $scope.filterByApp = function(type,event){
            if($(event.currentTarget).hasClass("reportsfiltertypedisabled")){
                $(event.currentTarget).removeClass("reportsfiltertypedisabled");
                $scope.isAppBooking = type;
            }else{
                $(event.currentTarget).addClass("reportsfiltertypedisabled");
                $scope.isAppBooking = 0;
            }
            $scope.getFeesReports();
        }

        $scope.downloadCancelReport = function(){
            if($scope.startDate==''){
                $scope.startDate = 0;
            }
            if($scope.endDate==''){
                $scope.endDate = 0;
            }
            if($scope.promotionName==''){
                $scope.promotionName = 0;
            }
            if($scope.userName==''){
                $scope.userName = 0;
            }
            window.open(apiUrl+"downloadBookingList/"+localStorage.getItem('club_id')+"/"+$scope.courtId+"/"+$scope.paymentMode+"/"+$scope.startDate+"/"+$scope.endDate+"/"+$scope.promotionName+"/"+$scope.allPromo+"/"+$scope.userName+"/"+$scope.isAppBooking+"/"+$scope.bookingType+"/3.xlsx");
        }

        $scope.downloadBookingReport = function(){
            if($scope.startDate==''){
                $scope.startDate = 0;
            }
            if($scope.endDate==''){
                $scope.endDate = 0;
            }
            if($scope.promotionName==''){
                $scope.promotionName = 0;
            }
            if($scope.userName==''){
                $scope.userName = 0;
            }
            window.open(apiUrl+"downloadBookingList/"+localStorage.getItem('club_id')+"/"+$scope.courtId+"/"+$scope.paymentMode+"/"+$scope.startDate+"/"+$scope.endDate+"/"+$scope.promotionName+"/"+$scope.allPromo+"/"+$scope.userName+"/"+$scope.isAppBooking+"/"+$scope.bookingType+"/2.xlsx");
        }

        $scope.downloadCourtUtilizationReport = function(){
            if($scope.startDate==''){
                $scope.startDate = 0;
            }
            if($scope.endDate==''){
                $scope.endDate = 0;
            }
            if($scope.promotionName==''){
                $scope.promotionName = 0;
            }
            if($scope.userName==''){
                $scope.userName = 0;
            }
            window.open(apiUrl+"downloadBookingList/"+localStorage.getItem('club_id')+"/"+$scope.courtId+"/"+$scope.paymentMode+"/"+$scope.startDate+"/"+$scope.endDate+"/"+$scope.promotionName+"/"+$scope.allPromo+"/"+$scope.userName+"/"+$scope.isAppBooking+"/"+$scope.bookingType+"/1.xlsx");
        }

        $scope.filterByBookingType = function(type,event){
            if($(event.currentTarget).hasClass("reportsfiltertypedisabled")){
                $(event.currentTarget).removeClass("reportsfiltertypedisabled");
                if(type==1){
                    $("#commonFeesBreakdown").addClass("hide");
                    $("#blueFeesBreakdown").addClass("hide");
                    $("#orangeFeesBreakdown").removeClass("hide");
                    $("#commonCourtUtilization").addClass("hide");
                    $("#blueCourtUtilization").addClass("hide");
                    $("#orangeCourtUtilization").removeClass("hide");
                    $("#commonCancellations").addClass("hide");
                    $("#blueCancellations").addClass("hide");
                    $("#orangeCancellations").removeClass("hide");
                    $("#blueFilter").addClass("reportsfiltertypedisabled");
                    $scope.bookingType = 1;
                }else if(type==2){
                    $("#commonFeesBreakdown").addClass("hide");
                    $("#blueFeesBreakdown").removeClass("hide");
                    $("#orangeFeesBreakdown").addClass("hide");
                    $("#commonCourtUtilization").addClass("hide");
                    $("#blueCourtUtilization").removeClass("hide");
                    $("#orangeCourtUtilization").addClass("hide");
                    $("#commonCancellations").addClass("hide");
                    $("#blueCancellations").removeClass("hide");
                    $("#orangeCancellations").addClass("hide");
                    $("#orangeFilter").addClass("reportsfiltertypedisabled");
                    $scope.bookingType = 2;
                }
                
            }else{
                $(event.currentTarget).addClass("reportsfiltertypedisabled");
                $("#commonFeesBreakdown").removeClass("hide");
                $("#blueFeesBreakdown").addClass("hide");
                $("#orangeFeesBreakdown").addClass("hide");
                $("#commonCourtUtilization").removeClass("hide");
                $("#blueCourtUtilization").addClass("hide");
                $("#orangeCourtUtilization").addClass("hide");
                $("#commonCancellations").removeClass("hide");
                $("#blueCancellations").addClass("hide");
                $("#orangeCancellations").addClass("hide");
                $scope.bookingType = 0;
            }
            $scope.getFeesReports();
        }

        $scope.getFeesReports = function () {
            if($scope.startDate==0){
                $scope.startDate = '';
            }
            if($scope.endDate==0){
                $scope.endDate = '';
            }
            if($scope.promotionName==0){
                $scope.promotionName = '';
            }
            if($scope.userName==0){
                $scope.userName = '';
            }
            $http({
                method: 'post',
                url: apiUrl + 'getFeesReports',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: $.param({
                    club_id: localStorage.getItem('club_id'),
                    courtId: $scope.courtId,
                    paymentMode: $scope.paymentMode,
                    startDate: $scope.startDate,
                    endDate: $scope.endDate,
                    promotionName: $scope.promotionName,
                    allPromo: $scope.allPromo,
                    userName: $scope.userName,
                    isAppBooking: $scope.isAppBooking,
                    bookingType: $scope.bookingType
                }),
            }).then(function success(res) {
                if (res.data.status) {
                    $scope.feesReport = res.data.data;
                    $scope.totalCourtAvailableHours = $scope.feesReport.totalCourtAvailableHours;
                    $scope.totalPrivateBookingAvailableHours = $scope.feesReport.totalPrivateBookingAvailableHours;
                    $scope.totalPrivateBookingPer = (($scope.totalPrivateBookingAvailableHours/$scope.totalCourtAvailableHours) * 100).toFixed(2);
                    $scope.totalGroupBookingPer = (($scope.feesReport.totalGroupBookingAvailableHours/$scope.totalCourtAvailableHours) * 100).toFixed(2);
                    $scope.totalCancelBookingPer = (($scope.feesReport.totalCancellationHours/$scope.totalCourtAvailableHours) * 100).toFixed(2);
                    $scope.totalUtilisation = (parseFloat($scope.totalPrivateBookingPer)+parseFloat($scope.totalGroupBookingPer)+parseFloat($scope.totalCancelBookingPer)).toFixed(2);
                    if($scope.feesReport.totalPrivateCancellationHours>0){
                        $scope.totalPrivateCancelPer = (($scope.feesReport.totalPrivateCancellationHours/$scope.feesReport.totalCancellationHours) * 100).toFixed(2);
                    }else{
                        $scope.totalPrivateCancelPer = 0;
                    }
                    if($scope.feesReport.totalGroupCancellationHours>0){
                        $scope.totalGroupCancelPer = (($scope.feesReport.totalGroupCancellationHours/$scope.feesReport.totalCancellationHours) * 100).toFixed(2);
                    }else{
                        $scope.totalGroupCancelPer = 0;
                    }
                    if($scope.feesReport.totalMatchPlayAvailableHours>0){
                        $scope.totalMatchPlayAvailablePer = (($scope.feesReport.totalMatchPlayAvailableHours/$scope.feesReport.totalCourtAvailableHours) * 100).toFixed(2);
                    }else{
                        $scope.totalMatchPlayAvailablePer = 0;
                    }
                    if($scope.feesReport.totalPractisePlayAvailableHours>0){
                        $scope.totalPractisePlayAvailablePer = (($scope.feesReport.totalPractisePlayAvailableHours/$scope.feesReport.totalCourtAvailableHours) * 100).toFixed(2);
                    }else{
                        $scope.totalPractisePlayAvailablePer = 0;
                    }
                    if($scope.feesReport.totalMatchPlayCancellationHours>0){
                        $scope.totalMatchPlayCancelPer = (($scope.feesReport.totalMatchPlayCancellationHours/$scope.feesReport.totalCancellationHours) * 100).toFixed(2);
                    }else{
                        $scope.totalMatchPlayCancelPer = 0;
                    }
                    if($scope.feesReport.totalPractisePlayCancellationHours>0){
                        $scope.totalPractisePlayCancelPer = (($scope.feesReport.totalPractisePlayCancellationHours/$scope.feesReport.totalCancellationHours) * 100).toFixed(2);
                    }else{
                        $scope.totalPractisePlayCancelPer = 0;
                    }
                    //alert($scope.totalMatchPlayCancelPer+":"+$scope.totalPractisePlayCancelPer)
                    $scope.totalCancelUtilisation = parseFloat($scope.totalPrivateCancelPer)+parseFloat($scope.totalGroupCancelPer);
                    $scope.totalBlueCancelUtilisation = parseFloat($scope.totalMatchPlayCancelPer)+parseFloat($scope.totalPractisePlayCancelPer);
                    if(myDoughnutChart!=undefined){
                        myDoughnutChart.destroy();
                    }
                    if($scope.bookingType==0){
                        var myDoughnutChart = new Chart(document.getElementById("courtChart").getContext("2d"), {
                            type: 'doughnut',
                            data: {
                                //labels: ["Private Classes", "Group Classes", "Cancellations"],
                                datasets: [{
                                    data: [$scope.totalPrivateBookingPer, $scope.totalGroupBookingPer, $scope.totalCancelBookingPer],
                                    backgroundColor: [
                                        '#7db9cb',
                                        '#ff9f56',
                                        '#d60c0c'
                                        
                                    ],
                                    borderColor:  [
                                        '#7db9cb',
                                        '#ff9f56',
                                        '#d60c0c'
                                        
                                    ],
                                    borderWidth: 1
                                }]
                            },
                            options: {
                                cutoutPercentage: 80,
                                elements: {
                                    center: {
                                        text: $scope.totalUtilisation+"%",
                              color: '#000', // Default is #000000
                              fontStyle: "Khand', sans-serif",
                              sidePadding: 20 // Defualt is 20 (as a percentage)
                                    }
                                },
                                tooltips: {
                                    enabled: false
                                }
                            }
                        });
                    }
                    

                    if(myDoughnutChartOrange!=undefined){
                        myDoughnutChartOrange.destroy();
                    }

                    if($scope.bookingType==1){
                        var myDoughnutChartOrange = new Chart(document.getElementById("courtOrangeChart").getContext("2d"), {
                            type: 'doughnut',
                            data: {
                                //labels: ["Private Classes", "Group Classes", "Cancellations"],
                                datasets: [{
                                    data: [$scope.totalPrivateBookingPer, $scope.totalGroupBookingPer, $scope.totalCancelBookingPer],
                                    backgroundColor: [
                                        '#fcc44c',
                                        '#ff994b',
                                        '#d60c0c'
                                        
                                    ],
                                    borderColor:  [
                                        '#fcc44c',
                                        '#ff994b',
                                        '#d60c0c'
                                        
                                    ],
                                    borderWidth: 1
                                }]
                            },
                            options: {
                                cutoutPercentage: 80,
                                elements: {
                                    center: {
                                        text: $scope.totalUtilisation+"%",
                              color: '#000', // Default is #000000
                              fontStyle: "Khand', sans-serif",
                              sidePadding: 20 // Defualt is 20 (as a percentage)
                                    }
                                },
                                tooltips: {
                                    enabled: false
                                }
                            }
                        });
                    }
                    

                    if(myDoughnutChartBlue!=undefined){
                        myDoughnutChartBlue.destroy();
                    }
                    if($scope.bookingType==2){
                        var myDoughnutChartBlue = new Chart(document.getElementById("courtBlueChart").getContext("2d"), {
                            type: 'doughnut',
                            data: {
                                //labels: ["Private Classes", "Group Classes", "Cancellations"],
                                datasets: [{
                                    data: [$scope.totalMatchPlayAvailablePer, $scope.totalPractisePlayAvailablePer, $scope.totalCancelBookingPer],
                                    backgroundColor: [
                                        '#7db9cb',
                                        '#6dccc1',
                                        '#d60c0c'
                                        
                                    ],
                                    borderColor:  [
                                        '#7db9cb',
                                        '#6dccc1',
                                        '#d60c0c'
                                        
                                    ],
                                    borderWidth: 1
                                }]
                            },
                            options: {
                                cutoutPercentage: 80,
                                elements: {
                                    center: {
                                        text: $scope.totalUtilisation+"%",
                              color: '#000', // Default is #000000
                              fontStyle: "Khand', sans-serif",
                              sidePadding: 20 // Defualt is 20 (as a percentage)
                                    }
                                },
                                tooltips: {
                                    enabled: false
                                }
                            }
                        });
                    }
                    
                    
                    if(myDoughnutChartCancel!=undefined){
                        myDoughnutChartCancel.destroy();
                    }
                    if($scope.bookingType==0){
                        var myDoughnutChartCancel = new Chart(document.getElementById("cancelChart").getContext("2d"), {
                            type: 'doughnut',
                            data: {
                                //labels: ["Private Classes", "Group Classes", "Cancellations"],
                                datasets: [{
                                    data: [$scope.totalPrivateCancelPer, $scope.totalGroupCancelPer],
                                    backgroundColor: [
                                        '#7db9cb',
                                        '#ff9f56'
                                        
                                    ],
                                    borderColor:  [
                                        '#7db9cb',
                                        '#ff9f56'
                                        
                                    ],
                                    borderWidth: 1
                                }]
                            },
                            options: {
                                cutoutPercentage: 80,
                                elements: {
                                    center: {
                                        text: $scope.totalCancelUtilisation+"%",
                              color: '#000', // Default is #000000
                              fontStyle: "Khand', sans-serif",
                              sidePadding: 20 // Defualt is 20 (as a percentage)
                                    }
                                },
                                tooltips: {
                                    enabled: false
                                }
                            }
                        });
                    }
                    
                    if($scope.bookingType==1){
                        var myDoughnutChartOrangeCancel = new Chart(document.getElementById("cancelOrangeChart").getContext("2d"), {
                            type: 'doughnut',
                            data: {
                                //labels: ["Private Classes", "Group Classes", "Cancellations"],
                                datasets: [{
                                    data: [$scope.totalPrivateCancelPer, $scope.totalGroupCancelPer],
                                    backgroundColor: [
                                        '#7db9cb',
                                        '#ff9f56'
                                        
                                    ],
                                    borderColor:  [
                                        '#7db9cb',
                                        '#ff9f56'
                                        
                                    ],
                                    borderWidth: 1
                                }]
                            },
                            options: {
                                cutoutPercentage: 80,
                                elements: {
                                    center: {
                                        text: $scope.totalCancelUtilisation+"%",
                              color: '#000', // Default is #000000
                              fontStyle: "Khand', sans-serif",
                              sidePadding: 20 // Defualt is 20 (as a percentage)
                                    }
                                },
                                tooltips: {
                                    enabled: false
                                }
                            }
                        });
                    }

                    if($scope.bookingType==2){
                        var myDoughnutChartBlueCancel = new Chart(document.getElementById("cancelBlueChart").getContext("2d"), {
                            type: 'doughnut',
                            data: {
                                //labels: ["Private Classes", "Group Classes", "Cancellations"],
                                datasets: [{
                                    data: [$scope.totalMatchPlayCancelPer, $scope.totalPractisePlayCancelPer],
                                    backgroundColor: [
                                        '#7db9cb',
                                        '#ff9f56'
                                        
                                    ],
                                    borderColor:  [
                                        '#7db9cb',
                                        '#ff9f56'
                                        
                                    ],
                                    borderWidth: 1
                                }]
                            },
                            options: {
                                cutoutPercentage: 80,
                                elements: {
                                    center: {
                                        text: $scope.totalBlueCancelUtilisation+"%",
                              color: '#000', // Default is #000000
                              fontStyle: "Khand', sans-serif",
                              sidePadding: 20 // Defualt is 20 (as a percentage)
                                    }
                                },
                                tooltips: {
                                    enabled: false
                                }
                            }
                        });
                    }
                }
            }, function error(response) {
                // no comments
            });
        }

        $scope.$on('$viewContentLoaded', function () {
            $scope.getFeesReports();
            $("#promotions-reports-start-date").datepicker({
                autoclose: true
            }).on("changeDate", function (e) {
                console.log(e.date)
                $scope.startDate = moment(e.date).format('YYYY-MM-DD');
                $scope.getPromotionReports();
            });
            $("#promotions-reports-end-date").datepicker({
                autoclose: true
            }).on("changeDate", function (e) {
                console.log(e.date)
                $scope.endDate = moment(e.date).format('YYYY-MM-DD');
                $scope.getPromotionReports();
            });
            $("#fees-reports-start-date").datepicker({
                autoclose: true
            }).on("changeDate", function (e) {
                console.log(e.date)
                $scope.startDate = moment(e.date).format('YYYY-MM-DD');
                $scope.getFeesReports();
            });
            $("#fees-reports-end-date").datepicker({
                autoclose: true
            }).on("changeDate", function (e) {
                console.log(e.date)
                $scope.endDate = moment(e.date).format('YYYY-MM-DD');
                $scope.getFeesReports();
            });
            Chart.pluginService.register({
                beforeDraw: function (chart) {
                    if (chart.config.options.elements.center) {
                //Get ctx from string
                var ctx = chart.chart.ctx;
                
                        //Get options from the center object in options
                var centerConfig = chart.config.options.elements.center;
                  var fontStyle = centerConfig.fontStyle || 'Arial';
                        var txt = centerConfig.text.split('\n');
                var color = centerConfig.color || '#000';
                var sidePadding = centerConfig.sidePadding || 20;
                var sidePaddingCalculated = (sidePadding/100) * (chart.innerRadius * 2)
                //Start with a base font of 30px
                ctx.font = "30px " + fontStyle;
                
                        //Get the width of the string and also the width of the element minus 10 to give it 5px side padding
                var stringWidth = ctx.measureText(txt).width;
                var elementWidth = (chart.innerRadius * 2) - sidePaddingCalculated;
        
                // Find out how much the font can grow in width.
                var widthRatio = elementWidth / stringWidth;
                var newFontSize = Math.floor(30 * widthRatio);
                var elementHeight = (chart.innerRadius * 2);
        
                // Pick a new font size so it will not be larger than the height of label.
                var fontSizeToUse = Math.min(newFontSize, elementHeight);
        
                        //Set font settings to draw it correctly.
                ctx.textAlign = 'center';
                ctx.textBaseline = 'middle';
                var centerX = ((chart.chartArea.left + chart.chartArea.right) / 2);
                var centerY = ((chart.chartArea.top + chart.chartArea.bottom) / 2);
                ctx.font = fontSizeToUse+"px " + fontStyle;
                ctx.fillStyle = color;
                
                //Draw text in center
                ctx.fillText(txt, centerX, centerY);
                    }
                }
            });
        });
    })

    .controller('CoachesReportsCtrl', function ($scope, $http, $timeout, $compile, $rootScope, $templateCache) {
        $scope.startDate = '';
        $scope.endDate = '';
        $scope.gender = 0;
        $scope.minAgeGroup = 0;
        $scope.maxAgeGroup = 100;
        $scope.coachName = '';
        $scope.getCoachesReports = function () {
            if($scope.startDate==0){
                $scope.startDate = '';
            }
            if($scope.endDate==0){
                $scope.endDate = '';
            }
            if($scope.coachName==0){
                $scope.coachName = '';
            }
            $http({
                method: 'post',
                url: apiUrl + 'getCoachesReports',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: $.param({
                    club_id: localStorage.getItem('club_id'),
                    startDate: $scope.startDate,
                    endDate: $scope.endDate,
                    gender: $scope.gender,
                    minAgeGroup: $scope.minAgeGroup,
                    maxAgeGroup: $scope.maxAgeGroup,
                    coachName: $scope.coachName
                }),
            }).then(function success(res) {
                if (res.data.status) {
                    $scope.coachesReport = res.data.data;
                    var totalHours = $scope.coachesReport.totalAvailableHours;
                    $scope.privatePer = ($scope.coachesReport.totalPrivateClassHours/totalHours) * 100;
                    $scope.groupPer = ($scope.coachesReport.totalGroupClassHours/totalHours) * 100;
                    $scope.cancelPer = ($scope.coachesReport.totalCancelClassHours/totalHours) * 100;
                }
            }, function error(response) {
                // no comments
            });
        }

        $scope.filterByGender = function (gender, str) {
            $("#coaches-reports-gender-filter").text(str);
            $scope.gender = gender;
            $scope.getCoachesReports();
        }

        $scope.filterByAgeGroup = function (min, max, str) {
            $("#coaches-reports-age-group-filter").text(str);
            $scope.minAgeGroup = min;
            $scope.maxAgeGroup = max;
            $scope.getCoachesReports();
        }

        $scope.filterByCoachName = function(keyword){
            $scope.coachName = keyword;
            $scope.getCoachesReports();
        }

        $scope.downloadCoachList = function(){
            if($scope.startDate==''){
                $scope.startDate = 0;
            }
            if($scope.endDate==''){
                $scope.endDate = 0;
            }
            if($scope.coachName==''){
                $scope.coachName = 0;
            }
            window.open(apiUrl+"downloadCoachesList/"+localStorage.getItem('club_id')+"/"+$scope.minAgeGroup+"/"+$scope.maxAgeGroup+"/"+$scope.startDate+"/"+$scope.endDate+"/"+$scope.gender+"/"+$scope.coachName+"/1"+".xlsx");
        }

        $scope.downloadCoachUtilisationList = function(){
            if($scope.startDate==''){
                $scope.startDate = 0;
            }
            if($scope.endDate==''){
                $scope.endDate = 0;
            }
            if($scope.coachName==''){
                $scope.coachName = 0;
            }
            window.open(apiUrl+"downloadCoachesList/"+localStorage.getItem('club_id')+"/"+$scope.minAgeGroup+"/"+$scope.maxAgeGroup+"/"+$scope.startDate+"/"+$scope.endDate+"/"+$scope.gender+"/"+$scope.coachName+"/2"+".xlsx");
        }

        $scope.$on('$viewContentLoaded', function () {
            $scope.getCoachesReports();
            $("#coaches-reports-start-date").datepicker({
                autoclose: true
            }).on("changeDate", function (e) {
                console.log(e.date)
                $scope.startDate = moment(e.date).format('YYYY-MM-DD');
                $scope.getCoachesReports();
            });
            $("#coaches-reports-end-date").datepicker({
                autoclose: true
            }).on("changeDate", function (e) {
                console.log(e.date)
                $scope.endDate = moment(e.date).format('YYYY-MM-DD');
                $scope.getCoachesReports();
            });
        });
    })

    .controller('GeneralReportsCtrl', function ($scope, $http, $timeout, $compile, $rootScope, $templateCache) {
        $scope.minAgeGroup = 0;
        $scope.maxAgeGroup = 100;
        $scope.startDate = '';
        $scope.endDate = '';
        $scope.gender = 0;
        $scope.minRating = 0;
        $scope.maxRating = 16;
        $scope.getGeneralReports = function () {
            if($scope.startDate==0){
                $scope.startDate = '';
            }
            if($scope.endDate==0){
                $scope.endDate = '';
            }
            $http({
                method: 'post',
                url: apiUrl + 'getGeneralReports',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: $.param({
                    club_id: localStorage.getItem('club_id'),
                    minAgeGroup: $scope.minAgeGroup,
                    maxAgeGroup: $scope.maxAgeGroup,
                    startDate: $scope.startDate,
                    endDate: $scope.endDate,
                    gender: $scope.gender,
                    minRating: $scope.minRating,
                    maxRating: $scope.maxRating,
                }),
            }).then(function success(res) {
                if (res.data.status) {
                    $scope.generalReport = res.data.data;
                }
            }, function error(response) {
                // no comments
            });
        }

        $scope.filterByAgeGroup = function (min, max, str) {
            $("#general-reports-age-group-filter").text(str);
            $scope.minAgeGroup = min;
            $scope.maxAgeGroup = max;
            $scope.getGeneralReports();
        }

        $scope.filterByRating = function (min, max, str) {
            $("#general-reports-rating-filter").text(str);
            $scope.minRating = min;
            $scope.maxRating = max;
            $scope.getGeneralReports();
        }

        $scope.filterByGender = function (gender, str) {
            $("#general-reports-gender-filter").text(str);
            $scope.gender = gender;
            $scope.getGeneralReports();
        }

        $scope.downloadClubMemberList = function(){
            if($scope.startDate==''){
                $scope.startDate = 0;
            }
            if($scope.endDate==''){
                $scope.endDate = 0;
            }
            window.open(apiUrl+"downloadClubMembersList/"+localStorage.getItem('club_id')+"/"+$scope.minAgeGroup+"/"+$scope.maxAgeGroup+"/"+$scope.startDate+"/"+$scope.endDate+"/"+$scope.gender+"/"+$scope.minRating+"/"+$scope.maxRating+"/1"+".xlsx");
        }

        $scope.downloadNewRegistration = function(){
            if($scope.startDate==''){
                $scope.startDate = 0;
            }
            if($scope.endDate==''){
                $scope.endDate = 0;
            }
            window.open(apiUrl+"downloadClubMembersList/"+localStorage.getItem('club_id')+"/"+$scope.minAgeGroup+"/"+$scope.maxAgeGroup+"/"+$scope.startDate+"/"+$scope.endDate+"/"+$scope.gender+"/"+$scope.minRating+"/"+$scope.maxRating+"/2"+".xlsx");
        }

        $scope.$on('$viewContentLoaded', function () {
            $scope.getGeneralReports();
            $("#general-reports-start-date").datepicker({
                autoclose: true
            }).on("changeDate", function (e) {
                console.log(e.date)
                $scope.startDate = moment(e.date).format('YYYY-MM-DD');
                $scope.getGeneralReports();
            });
            $("#general-reports-end-date").datepicker({
                autoclose: true
            }).on("changeDate", function (e) {
                console.log(e.date)
                $scope.endDate = moment(e.date).format('YYYY-MM-DD');
                $scope.getGeneralReports();
            });
        });
    })

    .controller('ReviewsCtrl', function ($scope, $http, $timeout, $compile, $rootScope, $templateCache) {

    })
