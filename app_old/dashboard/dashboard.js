'use strict';

angular.module('myApp.dashboard', ['ngRoute'])

        .config(['$routeProvider', function ($routeProvider) {
                $routeProvider.when('/dashboard', {
                    templateUrl: 'dashboard/dashboard.html',
                    controller: 'DashboardCtrl'
                });
            }])

        .controller('DashboardCtrl', function ($scope, $http, $filter, uiCalendarConfig) {
            $(".background-div").css("height", $(window).height());
            $scope.rightSideBar = false;
            $scope.rightSideBarDetails = false;
            $scope.eventSources = [];
            $scope.allCoaches = [];
            $scope.teamNumber = 0;
            $scope.remoteUrl = '';
            $scope.clubMinTime = '';
            $scope.clubMaxTime = '';
            $scope.picUrl = uploadUrl;
            $scope.club_id = localStorage.getItem('club_id');
            $scope.updateEventBooking = function (bookingId) {
                $http({
                    method: 'post',
                    url: apiUrl + 'updateClubBook',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    data: $.param({
                        book_date: $("#bookDate").val(),
                        court_id: $("#eventCourt").val(),
                        start_hours: $("#eventStartHours").val(),
                        start_minutes: $("#eventStartMinutes").val(),
                        end_hours: $("#eventEndHours").val(),
                        end_minutes: $("#eventEndMinutes").val(),
                        first_name: $scope.playerFirstName,
                        last_name: $scope.playerLastName,
                        phone: $scope.playerContactNo,
                        email: $scope.playerEmail,
                        coach_id: $("#eventCoach").val(),
                        sub_coach_id: $("#eventSubCoach").val(),
                        booking_id: bookingId
                    }),
                }).then(function success(res) {
                    if (res.data.status) {
                        $scope.showAlert(2, "Booking details updated!");
                        $scope.closeRightSideBarDetails();
                    } else {
                        $scope.showAlert(1, res.data.message);
                    }


                }, function error(response) {
                    $scope.showAlert(1, "Can not update booking details!");
                });
            }
            $scope.selectedMember = function (item) {
                $http({
                    method: 'post',
                    url: apiUrl + 'addTeamMember',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    data: $.param({
                        teamNumber: $scope.teamNumber,
                        playerId: item.originalObject.id
                    }),
                }).then(function success(res) {

                    if (res.data.status) {
                        $scope.teams = res.data.data;
                        $("#members_value").val('');
                    } else {
                        $scope.showAlert(1, "Can not add player");
                    }


                }, function error(response) {
                    // do nothing
                });

            }
            $http({
                method: 'post',
                url: apiUrl + 'clubProfile',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: $.param({
                    id: localStorage.getItem('club_id'),
                }),
            }).then(function success(res) {
                res = res.data;
                console.log(res);
                $scope.courts = res.data.Court;
                $scope.allCoaches = res.data.Coach;
                $scope.clubMinTime = res.data.ClubOperatingHour[0].start_time;
                $scope.clubMaxTime = res.data.ClubOperatingHour[0].end_time;
                console.log($scope.clubMinTime);
            }, function error(response) {
                // do nothing
            });
            $scope.setCalendar = function (id, court_id, type) {
                //alert(id+":"+court_id)
                if (type == 2) {
                    calendar.destroy();
                    $("#kendoCalendar").empty();
                }
                $http({
                    method: 'post',
                    url: apiUrl + 'clubProfile',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    data: $.param({
                        id: id,
                        court_id: court_id
                    }),
                }).then(function success(res) {
                    console.log(res)
                    res = res.data;
                    var scheduleList = [];
                    if (res.status)
                    {
                        var resString = JSON.stringify(res.data);
                        resString = resString.replace(/book_start_time/g, 'start');
                        resString = resString.replace(/book_end_time/g, 'end');
                        var schedules = JSON.parse(resString);

                        $.each(schedules, function (index, obj) {
                            var schedule = {};
                            var date = obj.ClubBooking.book_date;
                            schedule.start = date + 'T' + obj.ClubBooking.start;
                            schedule.end = date + 'T' + obj.ClubBooking.end;
                            schedule.type = obj.ClassType.name;
                            schedule.coachId = obj.Coach.id;
                            schedule.coachName = obj.Coach.name;
                            schedule.title = schedule.type + '\n' + schedule.coachName;
                            schedule.id = obj.ClubBooking.id;
                            schedule.data = obj;
                            if (obj.ClassType.id == 1)
                            {
                                schedule.color = '#23E6D5';
                            } else if (obj.ClassType.id == 2)
                            {
                                schedule.color = '#89E625';
                            } else if (obj.ClassType.id == 5)
                            {
                                schedule.color = '#E66630';
                            } else {
                                schedule.color = '#89E625';
                            }
                            scheduleList.push(schedule);
                        });
                        console.log(scheduleList)
                        $scope.uiConfig = {
                            calendar: {
                                editable: true,
                                selectable: true,
                                selectHelper: true,
                                allDaySlot: false,
                                slotDuration: '00:15:00',
                                minTime: $scope.clubMinTime,
                                maxTime: $scope.clubMaxTime,
                                eventOverlap: function (stillEvent, movingEvent) {
                                    return false;
                                },
                                header: {
                                    left: 'prev,next',
                                    center: 'title',
                                    right: 'month agendaWeek agendaDay'
                                },
                                events: scheduleList,
                                aspectRatio: 2,
                                eventClick: function (calEvent, jsEvent, view) {

                                    /*alert('Event: ' + calEvent.title + 'ID: ' + calEvent.id);
                                     alert('Coordinates: ' + jsEvent.pageX + ',' + jsEvent.pageY);
                                     alert('View: ' + view.name);*/
                                    $scope.rightSideBar = true;
                                    $("#rightSideBar").animate({width: 'toggle'}, 350);
                                    if ($(".app-content-body").hasClass("sideBarOn")) {
                                        $(".app-content-body").removeClass("sideBarOn");
                                        $(".button-bottom").removeClass("sideBarOn");
                                    } else {
                                        $(".app-content-body").addClass("sideBarOn");
                                        $(".button-bottom").addClass("sideBarOn");
                                    }
                                    //window.resizeBy(0,0);
                                    // profile
                                    $http({
                                        method: "POST",
                                        url: apiUrl + "getEventDetails",
                                        headers: {
                                            'Content-Type': 'application/x-www-form-urlencoded'
                                        },
                                        data: $.param({booking_id: calEvent.id}),
                                    }).then(function success(eventResponse) {
                                        var edate = new Date(eventResponse.data.data.ClubBooking.book_date);
                                        $scope.eventDate = moment(edate).format('Do MMMM YYYY');
                                        $scope.eventDay = moment(edate).format('dddd');
                                        var edate = new Date(eventResponse.data.data.ClubBooking.book_date + " " + eventResponse.data.data.ClubBooking.book_start_time);
                                        $scope.startTimeHour = moment(edate).format('h');
                                        $scope.startTimeAM = moment(edate).format('a');
                                        var eedate = new Date(eventResponse.data.data.ClubBooking.book_date + " " + eventResponse.data.data.ClubBooking.book_end_time);
                                        $scope.eventPlayTime = moment(edate).format('h:mma') + " - " + moment(eedate).format('h:mma');
                                        $scope.matchType = eventResponse.data.data.ClassType.name;
                                        $scope.courtName = eventResponse.data.data.Court.name;
                                        $scope.bookingNumber = eventResponse.data.data.ClubBooking.prefix_booking_number + ("0000" + eventResponse.data.data.ClubBooking.booking_number).slice(-6);
                                        $scope.teams = eventResponse.data.data.Team;
                                        $scope.picUrl = uploadUrl;
                                        $scope.bookingId = eventResponse.data.data.ClubBooking.id;
                                    }, function error(eventResponse) {
                                        $scope.showAlert('error', 'Can not get event details!');
                                    });
                                }
                            }
                        };
                        // kendo ui calendar
                        var eventDates = [];
                        $.each(scheduleList, function (index, obj) {
                            var cdate = new Date(obj.start);
                            cdate.setTime(cdate.getTime() + (cdate.getTimezoneOffset() * 60 * 1000));
                            console.log(cdate)
                            var fdate = +new Date(cdate.getFullYear(), cdate.getMonth(), cdate.getDate());
                            console.log(fdate)
                            eventDates.push(fdate);
                        });
                        console.log(eventDates);
                        $("#kendoCalendar").kendoCalendar({
                            value: new Date(),
                            dates: eventDates,
                            weekNumber: false,
                            month: {
                                // template for dates in month view
                                content: '# if ($.inArray(+data.date, data.dates) != -1) { #' +
                                        '<div class="exhibition"' +
                                        '">#= data.value #</div>' +
                                        '# } else { #' +
                                        '#= data.value #' +
                                        '# } #'
                            },
                            footer: false,
                            change: function () {
                                var value = this.value();
                                console.log(value);
                                //console.log(uiCalendarConfig.calendars['myCalendar'].fullCalendar('gotoDate',moment('2017-04-11')));
                                // uiCalendarConfig.calendars['myCalendar'].fullCalendar('gotoDate',moment(value)); //moment(value).format('YYYY-MM-DD')
                                uiCalendarConfig.calendars['myCalendar'].fullCalendar('changeView', 'agendaDay');
                                uiCalendarConfig.calendars['myCalendar'].fullCalendar('gotoDate', moment(value));
                            }
                        });
                        calendar = $("#kendoCalendar").data("kendoCalendar");

                        /*$scope.calendar = {
                         value: new Date(),
                         dates: eventDates,
                         weekNumber: false,
                         month: {
                         // template for dates in month view
                         content: '# if ($.inArray(+data.date, data.dates) != -1) { #' +
                         '<div class="exhibition"' +
                         '">#= data.value #</div>' +
                         '# } else { #' +
                         '#= data.value #' +
                         '# } #'
                         },
                         footer: false,
                         change: function () {
                         var value = this.value();
                         console.log(value);
                         //console.log(uiCalendarConfig.calendars['myCalendar'].fullCalendar('gotoDate',moment('2017-04-11')));
                         // uiCalendarConfig.calendars['myCalendar'].fullCalendar('gotoDate',moment(value)); //moment(value).format('YYYY-MM-DD')
                         uiCalendarConfig.calendars['myCalendar'].fullCalendar('changeView', 'agendaDay');
                         uiCalendarConfig.calendars['myCalendar'].fullCalendar('gotoDate', moment(value));
                         }
                         
                         }*/

                    }
                }, function error(response) {
                    // do nothing
                });
            }
            $scope.setCalendar(localStorage.getItem('club_id'), localStorage.getItem('court_id'), 1);

            $scope.editRightSideBarDetails = function (bookingId) {
                $http({
                    method: 'post',
                    url: apiUrl + 'getEventDetails',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    data: $.param({
                        booking_id: bookingId
                    }),
                }).then(function success(res) {
                    res = res.data;
                    $scope.rightSideBar = false;
                    $scope.rightSideBarDetails = true;
                    $scope.playerFirstName = res.data.UserDetail.Player.first_name;
                    $scope.playerLastName = res.data.UserDetail.Player.last_name;
                    $scope.playerContactNo = res.data.UserDetail.Player.phone;
                    $scope.playerEmail = res.data.UserDetail.User.email;
                    $scope.teamNumber = res.data.ClubBooking.team_number;
                    $scope.remoteUrl = apiUrl + 'getSearchPlayers/' + $scope.teamNumber + '/';
                    $scope.bookingId = bookingId;
                    $("#totalPlayer").val(res.data.Team.length);
                    $scope.teams = res.data.Team;
                    $("#eventCoach").val(res.data.ClubBooking.coach_id);
                    $("#eventSubCoach").val(res.data.ClubBooking.sub_coach_id);
                    var startTime = res.data.ClubBooking.book_start_time;
                    startTime = $scope.getHoursMinutes(startTime);
                    startTime = startTime.split(":");
                    $("#eventStartHours").val(startTime[0]);
                    if (startTime[1] == 0) {
                        startTime[1] = "00";
                    }
                    $("#eventStartMinutes").val(startTime[1]);
                    var endTime = res.data.ClubBooking.book_end_time;
                    endTime = $scope.getHoursMinutes(endTime);
                    endTime = endTime.split(":");
                    $("#eventEndHours").val(endTime[0]);
                    if (endTime[1] == 0) {
                        endTime[1] = "00";
                    }
                    $("#eventEndMinutes").val(endTime[1]);
                    $("#eventCourt").val(res.data.ClubBooking.court_id);
                    $scope.start = res.data.ClubBooking.book_date;
                    //$scope.totalPlayer = res.data.Team.length;
                }, function error(response) {
                    // do nothing
                });

            }

            $scope.removePlayer = function (id) {
                $scope.showConfirm("Are you sure?", id, $scope.confirmRemovePlayer);
            }

            $scope.confirmRemovePlayer = function (id) {
                $http({
                    method: 'post',
                    url: apiUrl + 'removeTeamMember',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    data: $.param({
                        id: id,
                        teamNumber: $scope.teamNumber
                    }),
                }).then(function success(res) {
                    if (res.data.status) {
                        $("#team" + id).remove();
                    } else {
                        $scope.showAlert(1, res.data.message);
                    }
                }, function error(response) {
                    $scope.showAlert(1, response);
                });
            }

            $scope.getNumber = function (num) {
                return new Array(num);
            }

            $scope.getHoursMinutes = function (time) {
                var stime = time.split(":");
                var sh = stime[0];
                var sm = 0;
                if (stime <= 15) {
                    sm = 15;
                } else if (stime <= 30) {
                    sm = 30;
                } else if (stime <= 45) {
                    sm = 45;
                } else {
                    sm = 0;
                }
                return sh + ":" + sm;
            }

            $scope.openPlayerBox = function () {
                $("#collapseTwo").collapse('toggle');
                if ($("#playerInfoSign").hasClass("reguler-plus")) {
                    $("#playerInfoSign").removeClass("reguler-plus").addClass("reguler-minus");
                } else {
                    $("#playerInfoSign").removeClass("reguler-minus").addClass("reguler-plus");
                }
            }

            $scope.openCoachBox = function () {
                $("#collapseOne").collapse('toggle');
                if ($("#coachInfoSign").hasClass("reguler-plus")) {
                    $("#coachInfoSign").removeClass("reguler-plus").addClass("reguler-minus");
                } else {
                    $("#coachInfoSign").removeClass("reguler-minus").addClass("reguler-plus");
                }
            }

            $scope.closeRightSideBarDetails = function () {
                $scope.rightSideBar = true;
                $scope.rightSideBarDetails = false;
            }
        });