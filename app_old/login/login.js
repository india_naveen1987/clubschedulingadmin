'use strict';

angular.module('myApp.login', ['ngRoute'])

        .config(['$routeProvider', function ($routeProvider) {
                $routeProvider.when('/login', {
                    templateUrl: 'login/login.html',
                    controller: 'LoginCtrl'
                });
            }])

        .controller('LoginCtrl', function ($scope, $http, $location, $rootScope) {
            $(".background-div").css("height", $(window).height());
            $scope.username = '';
            $scope.password = '';
            $scope.submitForm = function () {
                if ($scope.username == '') {
                    $("div.alert-danger").removeClass("hide");
                    $scope.message = "Email address is required!";
                } else if ($scope.password == '') {
                    $("div.alert-danger").removeClass("hide");
                    $scope.message = "Password is required!";
                } else {
                    $("div.alert-danger").addClass("hide");
                    $http({
                        method: "POST",
                        url: apiUrl + "login",
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded'
                        },
                        data: $.param({email: $scope.username, password: $scope.password}),
                    }).then(function success(response) {
                        console.log(response)
                        if (response.data.status) {
                            db.transaction(function (tx) {
                                tx.executeSql('CREATE TABLE IF NOT EXISTS USER (id INTEGER, role_id INTEGER, email TEXT, created TEXT)', [], function (tx, results) {
                                    tx.executeSql('SELECT * from USER WHERE id="' + response.data.data.User.id + '"', [], function (tx, results1) {
                                        if (results1.rows.length > 0) {
                                            if (response.data.data.User.role_id == 3) {
                                                $rootScope.userName = response.data.data.Club.name;
                                                localStorage.setItem('club_id',response.data.data.Club.id);
                                                    localStorage.setItem('court_id',response.data.data.Court[0].id);
                                            } else if (response.data.data.User.role_id == 3) {
                                                $rootScope.userName = response.data.data.Coach.name;
                                                localStorage.setItem('coach_id',response.data.data.Coach.id);
                                            }
                                            window.location.href = '#!/dashboard';
                                        } else {
                                            tx.executeSql('INSERT INTO USER (id, role_id, email, created) VALUES("' + response.data.data.User.id + '","' + response.data.data.User.role_id + '","' + response.data.data.User.email + '","' + response.data.data.User.created + '")', [], function (tx, results2) {
                                                if (response.data.data.User.role_id == 3) {
                                                    $rootScope.userName = response.data.data.Club.name;
                                                    localStorage.setItem('club_id',response.data.data.Club.id);
                                                    localStorage.setItem('court_id',response.data.data.Court[0].id);
                                                } else if (response.data.data.User.role_id == 3) {
                                                    $rootScope.userName = response.data.data.Coach.name;
                                                    localStorage.setItem('coach_id',response.data.data.Coach.id);
                                                }
                                                window.location.href = '#!/dashboard';
                                            }, function (err) {
                                                $("div.alert-danger").removeClass("hide");
                                                $scope.message = JSON.stringify(err);
                                            });
                                        }
                                    }, function (err) {
                                        $("div.alert-danger").removeClass("hide");
                                        $scope.message = "Can not get user details!";
                                    });

                                }, function (err) {
                                    $("div.alert-danger").removeClass("hide");
                                    $scope.message = "Can not remember you!";
                                });
                            });
                        } else {
                            $("div.alert-danger").removeClass("hide");
                            $scope.message = response.data.message;
                        }
                    }, function error(response) {
                        $("div.alert-danger").removeClass("hide");
                        $scope.message = response.statusText;
                    });
                }
            }
        });