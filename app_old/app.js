'use strict';
var apiUrl = "http://localhost/tennis/users/";
var uploadUrl = "http://localhost/tennis/uploads/";
var db = '';
var calendar = '';
// Declare app level module which depends on views, and components
angular.module('myApp', [
    'ngRoute',
    'myApp.login',
    'myApp.dashboard',
    'myApp.version',
    'ui.calendar',
    'kendo.directives',
    'datePicker',
    'angucomplete-alt'
]).
        config(function ($locationProvider, $routeProvider) {
            $locationProvider.hashPrefix('!');

            $routeProvider.otherwise({redirectTo: '/login'});
        })

        .directive('compileMe', function ($compile) {
            return {
                template: '',
                scope: {
                    htmlToBind: '=',
                    variables: '='
                },
                link: function (scope, element, attrs) {
                    var content = angular.element(scope.htmlToBind);
                    var compiled = $compile(content);
                    element.append(content);
                    compiled(scope);
                }
            };
        })

        .directive("addExternalHtml", function ($templateRequest, $compile) {
            return {
                scope: {
                    htmlToBind: '=',
                    variables: '='
                },
                link: function (scope, element, attrs) {
                    scope.name = attrs.foo;
                    scope.show = false;
                    $templateRequest("foo.html").then(function (html) {
                        element.append($compile(html)(scope));
                    });
                }
            };
        })


        .controller('myAppCtrl', function ($scope, $location,$rootScope) {
            db = window.openDatabase("Ludo", "1.0", "Ludo", 2 * 1024 * 1024);
    
            $scope.showAlert = function(type,message){
                alert(message);
            }
            
            $scope.showConfirm = function(msg,id,func){
                if(confirm(msg)){
                    func(id);
                }
            }
            
            $scope.logOut = function(){
                $rootScope.userName = '';
                window.location.href = '#!/login';
            }

            $scope.$on('$viewContentLoaded', function () {
                $scope.openMenu = function () {
                    $("#wrapper").addClass("active");
                    $("#sidebar-wrapper").removeClass("active");
                };

                $scope.closeMenu = function () {
                    $("#sidebar-wrapper").addClass("active");
                    $("#wrapper").removeClass("active");
                };

                $scope.closeRightSideBar = function () {
                    $("#rightSideBar").animate({width:'toggle'},350);
                    $(".app-content-body").removeClass("sideBarOn");
                    $(".button-bottom").removeClass("sideBarOn");
                };
            });
        });


