'use strict';

angular.module('myApp.clubsetup', ['ngRoute'])

        .config(['$routeProvider', function ($routeProvider) {
                $routeProvider.when('/clubsetup', {
                    templateUrl: 'clubsetup/clubsetup.html',
                    controller: 'ClubSetupCtrl'
                });
            }])



        .controller('ClubSetupCtrl', function ($scope, $http, $timeout, $compile, $rootScope, $templateCache, $filter) {
            console.log("club setup ctrl");
            $scope.stime = [];
            $scope.surfaces = [];
            var k = 0;
            for (var i = 1; i <= 24; i++) {
                $scope.stime.push(i);
            }

            $scope.$on('$viewContentLoaded', function () {
                $timeout(function () {
                    $("#clubSetupMenu li a").each(function (i, v) {
                        var cid = $(this).text();
                        console.log(cid);
                        cid = cid.replace(" ", "_");
                        $("#" + cid).hide();
                    });
                    $("#addCourt").hide();
                    $("#addPeakTime").hide();
                    $("#General").show();
                }, 500);

                $http({
                    method: 'post',
                    url: apiUrl + 'getGeneralSettings',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                }).then(function success(res) {
                    if (res.data.status) {
                        $("#advance_booking").val(res.data.data.Setting.advance_booking);
                        $("#non_member_fee").val(res.data.data.Setting.non_member_fee);
                        var paymentMethods = res.data.data.Setting.payment_method.split(",");
                        for (var i = 0; i < paymentMethods.length; i++) {
                            if (paymentMethods[i] == 1) {
                                $("#payment_method_credit_card").prop("checked", true);
                            }
                            if (paymentMethods[i] == 2) {
                                $("#payment_method_cash").prop("checked", true);
                            }
                            if (paymentMethods[i] == 3) {
                                $("#payment_method_sponsored").prop("checked", true);
                            }
                        }
                    } else {
                        $scope.showAlert(1, res.data.message);
                    }
                }, function error(response) {
                    $scope.showAlert(1, response);
                });

                $http({
                    method: 'post',
                    url: apiUrl + 'getAllCourtSurfaces',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                }).then(function success(res) {
                    if (res.data.status) {
                        $scope.surfaces = res.data.data;
                    } else {
                        $scope.showAlert(1, res.data.message);
                    }
                }, function error(response) {
                    $scope.showAlert(1, response);
                });
            });

            $scope.saveGeneralSettings = function () {
                var paymentMethods = [];
                if ($("#payment_method_credit_card").is(":checked")) {
                    paymentMethods.push(1);
                }
                if ($("#payment_method_cash").is(":checked")) {
                    paymentMethods.push(2);
                }
                if ($("#payment_method_sponsored").is(":checked")) {
                    paymentMethods.push(3);
                }
                if (paymentMethods.length == 0) {
                    $scope.showAlert(1, "Please select atleast one payment method");
                } else {
                    $http({
                        method: 'post',
                        url: apiUrl + 'saveGeneralSettings',
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded'
                        },
                        data: $.param({
                            advance_booking: $("#advance_booking").val(),
                            non_member_fee: $("#non_member_fee").val(),
                            payment_method: paymentMethods
                        }),
                    }).then(function success(res) {
                        if (res.data.status) {
                            $scope.showAlert(2, "General settings saved");
                        } else {
                            $scope.showAlert(1, res.data.message);
                        }
                    }, function error(response) {
                        $scope.showAlert(1, response);
                    });
                }
            }

            $scope.saveFacilitiesTimings = function () {
                if ($("#apply_all").is(":checked")) {
                    var apply_all = 1;
                } else {
                    var apply_all = 2;
                }
                $http({
                    method: 'post',
                    url: apiUrl + 'addUpdateCourt',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    data: $.param({
                        is_all: apply_all,
                        start_hours: $("#opening_hours").val(),
                        start_minutes: $("#opening_minutes").val(),
                        end_hours: $("#ending_hours").val(),
                        end_minutes: $("#ending_minutes").val(),
                        club_id: localStorage.getItem("club_id")
                    }),
                }).then(function success(res) {
                    if (res.data.status) {
                        $scope.showAlert(2, "Facilities settings saved");
                    } else {
                        $scope.showAlert(1, res.data.message);
                    }
                }, function error(response) {
                    $scope.showAlert(1, response);
                });
            }

            $scope.updateCourtStatus = function (id) {
                var fStatus = 1;
                if ($("#statusFacility" + id).is(":checked")) {
                    fStatus = 1;
                } else {
                    fStatus = 2;
                }
                $http({
                    method: 'post',
                    url: apiUrl + 'addUpdateCourt',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    data: $.param({
                        court_id: id,
                        status: fStatus
                    }),
                }).then(function success(res) {
                    if (res.data.status) {
                        // no need
                    } else {
                        $scope.showAlert(1, res.data.message);
                    }
                }, function error(response) {
                    $scope.showAlert(1, response);
                });
            }

            $scope.deleteCourts = function () {
                var n = $('input:checkbox[id^="check"]:checked').length;
                if (n == 0) {
                    $scope.showAlert(1, "Please select atleast one court");
                } else {
                    $scope.showConfirm("Are you sure?", 0, "removeCourt");
                }
            }

            $scope.selectDeselectAll = function () {
                if ($("#checkAllFacilities").is(":checked")) {
                    $(".checkselect").prop("checked", true);
                } else {
                    $(".checkselect").prop("checked", false);
                }
            }

            $scope.$on('removeCourt', function (ev, args) {
                var ids = [];
                $('input:checkbox[id^="checkFacility"]:checked').each(function () {
                    ids.push($(this).attr("id").replace("checkFacility", ""));

                });
                if (ids.length > 0) {
                    $http({
                        method: 'post',
                        url: apiUrl + 'deleteCourt',
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded'
                        },
                        data: $.param({
                            ids: ids
                        }),
                    }).then(function success(res) {
                        $scope.showAlert(2, "Courts deleted");
                        //window.location.reload();
                    }, function error(response) {
                        $scope.showAlert(1, response);
                    });
                }
            })

            $scope.openMenuItem = function (menu, ev) {
                $("#clubSetupMenu li").removeClass("active-li");
                $(ev.target).parent("li").addClass("active-li");
                $("#clubSetupMenu li a").each(function (i, v) {
                    var cid = $(this).text();
                    cid = cid.replace(" ", "_");
                    $("#" + cid).hide();
                });
                var eid = $(ev.target).text();
                eid = eid.replace(" ", "_");
                $("#" + eid).show();
                if (eid == 'Facilities') {
                    $('#facilitiesTable').DataTable({
                        "autoWidth": false,

                        "ajax": {
                            "destroy": true,
                            "url": apiUrl + 'getAllCourtsByClubId',
                            "type": 'post',
                            "headers": {
                                'Content-Type': 'application/x-www-form-urlencoded'
                            },
                            "data": {
                                "club_id": localStorage.getItem("club_id")
                            },
                            "dataSrc": function (json) {
                                console.log(json);
                                var fData = [];
                                if (json.status) {
                                    $.each(json.data, function (index, val) {
                                        console.log(val);
                                        var checkdelete = "<input type='checkbox' id='checkFacility" + val.Court.id + "' class='checkselect'  />";
                                        var status = '<div class="tg-list-item" style="float:left"><input class="tgl tgl-ios" id="statusFacility' + val.Court.id + '" type="checkbox" ng-change="updateCourtStatus(' + val.Court.id + ')" ng-model="facilityStatus' + val.Court.id + '" /><label class="tgl-btn" for="statusFacility' + val.Court.id + '"></label></div>';
                                        $scope['facilityStatus' + val.Court.id] = val.Court.status == 1 ? true : false;
                                        var name = val.Court.name;
                                        var id = val.Court.id;
                                        var surface = val.CourtSurface.name;
                                        var operating_hours = $filter('formatTime')(val.Court.start_time) + " - " + $filter('formatTime')(val.Court.end_time);
                                        var details = " <a href='javascript:' ng-click='editCourt(" + val.Court.id + ")'><img style='margin-right:30px' class='ancher-img' src='images/edit.png'/></a>";
                                        var res = {
                                            checkdelete: checkdelete,
                                            status: status,
                                            name: name,
                                            id: id,
                                            surface: surface,
                                            operating_hours: operating_hours,
                                            details: details
                                        }
                                        fData.push(res);
                                    });
                                }


                                return fData;

                            }
                        }, "fnDrawCallback": function () {
                            var html = '<a href="javascript:" class="form-control" id="deleteCourts" ng-click="deleteCourts()"><img src="images/delete.png"/></a>';
                            $("#facilitiesTable_info").html(html);
                            var html = '<img src="images/left-arrow.png"/>';
                            $("#facilitiesTable_previous").html(html);
                            var html = '<img src="images/right-arrow.png"/>';
                            $("#facilitiesTable_next").html(html);
                            $("#facilitiesTable_filter").remove();
                            $compile($("#facilitiesTable"))($scope);
                            $compile($("#facilitiesTable_info"))($scope);
                            $compile($("#facilitiesTable_previous"))($scope);
                            $compile($("#facilitiesTable_next"))($scope);

                        },
                        "columns": [
                            {"data": "checkdelete"},
                            {"data": "status"},
                            {"data": "name"},
                            {"data": "id"},
                            {"data": "surface"},
                            {"data": "operating_hours"},
                            {"data": "details"}
                        ],
                        "pageLength": 5


                    });
                } else if (eid == 'Peak_Time') {
                    $http({
                        method: 'post',
                        url: apiUrl + 'getAllPeakTimesByClubId',
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded'
                        },
                        data: $.param({
                            club_id: localStorage.getItem("club_id")
                        }),
                    }).then(function success(res) {
                        if (res.data.status) {
                            $scope.peakTimings = res.data.data;
                        } else {
                            $scope.showAlert(1, res.data.message);
                        }
                    }, function error(response) {
                        $scope.showAlert(1, response);
                    });
                }
            }

            $scope.openAddCourtForm = function () {
                $("#addCourt").show();
                $("#courtFormTitle").text("Add Court");
                $("#courtFormSubmitTitle").text("ADD COURT +");
                $("#courtIdDiv").hide();
                $("#courtId").val("");
                $("#courtStatus").prop("checked", false);
                $("#courtName").val("");
                $("#courtSurface").val("");
                $("#courtOpeningHours").val(1);
                $("#courtOpeningMinutes").val(0);
                $("#courtClosingHours").val(1);
                $("#courtClosingMinutes").val(0);
            }

            $scope.closeAddCourtForm = function () {
                $("#addCourt").hide();
            }

            $scope.addCourt = function () {
                var courtStatus = $("#courtStatus").is(":checked") ? 1 : 2;
                $http({
                    method: 'post',
                    url: apiUrl + 'addUpdateCourt',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    data: $.param({
                        status: courtStatus,
                        name: $("#courtName").val(),
                        court_surface_id: $("#courtSurface").val(),
                        start_hours: $("#courtOpeningHours").val(),
                        start_minutes: $("#courtOpeningMinutes").val(),
                        end_hours: $("#courtClosingHours").val(),
                        end_minutes: $("#courtClosingMinutes").val(),
                        court_id: $("#courtId").val(),
                        club_id: localStorage.getItem("club_id")
                    }),
                }).then(function success(res) {
                    if (res.data.status) {
                        $scope.closeAddCourtForm();
                        $scope.showAlert(2, "Court detail saved");
                    } else {
                        $scope.showAlert(1, res.data.message);
                    }
                }, function error(response) {
                    $scope.showAlert(1, response);
                });
            }

            $scope.editCourt = function (courtId) {
                $http({
                    method: 'post',
                    url: apiUrl + 'getCourtDetails',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    data: $.param({
                        court_id: courtId
                    }),
                }).then(function success(res) {
                    if (res.data.status) {
                        $scope.openAddCourtForm();
                        $("#courtFormTitle").text("Edit Court");
                        $("#courtFormSubmitTitle").text("SAVE CHANGES");
                        $("#courtIdDiv").show();
                        $("#courtId").val(res.data.data.Court.id);
                        if (res.data.data.Court.status == 1) {
                            $("#courtStatus").prop("checked", true);
                        } else {
                            $("#courtStatus").prop("checked", false);
                        }
                        $("#courtName").val(res.data.data.Court.name);
                        $("#courtSurface").val(res.data.data.Court.court_surface_id);
                        var openingHours = res.data.data.Court.start_time.split(":");
                        $("#courtOpeningHours").val(parseInt(openingHours[0]));
                        $("#courtOpeningMinutes").val(parseInt(openingHours[1]));
                        var closingHours = res.data.data.Court.end_time.split(":");
                        $("#courtClosingHours").val(parseInt(closingHours[0]));
                        $("#courtClosingMinutes").val(parseInt(closingHours[1]));
                    } else {
                        $scope.showAlert(1, res.data.message);
                    }
                }, function error(response) {
                    $scope.showAlert(1, response);
                });
            }

            $scope.addPeakTime = function () {
                $("#addPeakTime").show();
                $("#peakTimeFormTitle").text("Add Peak Time");
                $("#peakTimeFormSubmitTitle").text("ADD PEAK TIME +");
                $("#daysCheckBoxes").show();
                $("#addAnotherTime").show();
                $("#extraTimings").html('');
            }

            $scope.deletePeakTimes = function () {
                var ids = [];
                $('input:checkbox[id^="peakTime"]:checked').each(function () {
                    ids.push($(this).attr("id").replace("peakTime", ""));
                });
                if (ids.length > 0) {
                    $http({
                        method: 'post',
                        url: apiUrl + 'deletePeakTime',
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded'
                        },
                        data: $.param({
                            ids: ids
                        }),
                    }).then(function success(res) {
                        $scope.showAlert(2, "Peak timings deleted");
                    }, function error(response) {
                        $scope.showAlert(1, response);
                    });
                } else {
                    $scope.showAlert(1, "Please select atleast 1 peak time");
                }
            }

            $scope.closeAddPeakTimeForm = function () {
                $("#addPeakTime").hide();
            }

            $scope.savePeakTime = function () {
                if ($("input[name='peaktime_id']").val() == '') {
                    var url = apiUrl + 'addPeakTime';
                } else {
                    var url = apiUrl + 'updatePeakTime';
                }
                $http({
                    method: 'post',
                    url: url,
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    data: $("#peakTimeForm").serialize() + "&" + $.param({
                        club_id: localStorage.getItem("club_id")
                    }),
                }).then(function success(res) {
                    if (res.data.status) {
                        $scope.closeAddPeakTimeForm();
                        $scope.showAlert(2, "Peak timings saved");
                    } else {
                        $scope.showAlert(1, res.data.message);
                    }
                }, function error(response) {
                    $scope.showAlert(1, response);
                });
            }

            $scope.addMorePeakTime = function () {
                k++;
                var html = $("#peakTimeHoursDiv div.first:first, #peakTimeHoursDiv div.second:first").clone();
                var $html = $('<div />', {html: html});
                $html.find('select[name="times[' + (k - 1) + '][start_hours]"]').attr("name", "times[" + k + "][start_hours]");
                $html.find('select[name="times[' + (k - 1) + '][start_minutes]"]').attr("name", "times[" + k + "][start_minutes]");
                $html.find('select[name="times[' + (k - 1) + '][end_hours]"]').attr("name", "times[" + k + "][end_hours]");
                $html.find('select[name="times[' + (k - 1) + '][end_minutes]"]').attr("name", "times[" + k + "][end_minutes]");
                html = $html.html();
                $("#extraTimings").append(html);
            }

            $scope.editPeakTimeForm = function (pid) {
                $http({
                    method: 'post',
                    url: apiUrl + 'getPeakTime',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    data: $("#peakTimeForm").serialize() + "&" + $.param({
                        id: pid
                    }),
                }).then(function success(res) {
                    if (res.data.status) {
                        $("#addPeakTime").show();
                        $("#peakTimeFormTitle").text("Edit Peak Time");
                        $("#daysCheckBoxes").hide();
                        $("#addAnotherTime").hide();
                        $("#peakTimeFormSubmitTitle").text("SAVE PEAK TIME");
                        $("input[name='peaktime_id']").val(res.data.data.PeakTime.id);
                        var startTime = res.data.data.PeakTime.start_time.split(":");
                        $("select[name='times[0][start_hours]']").val(startTime[0]);
                        $("select[name='times[0][start_minutes]']").val(parseInt(startTime[1]));
                        var endTime = res.data.data.PeakTime.end_time.split(":");
                        $("select[name='times[0][end_hours]']").val(endTime[0]);
                        $("select[name='times[0][end_minutes]']").val(parseInt(endTime[1]));
                        $("#extraTimings").html('');
                    } else {
                        $scope.showAlert(1, res.data.message);
                    }
                }, function error(response) {
                    $scope.showAlert(1, response);
                });
            }
        })
