'use strict';

angular.module('myApp.bookings', ['ngRoute'])

    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/bookings', {
            templateUrl: 'bookings/bookings.html',
            controller: 'BookingsCtrl'
        });
    }])

    .directive('csSelect', function () {
        return {
            require: '^stTable',
            template: '<input type="checkbox"/>',
            scope: {
                row: '=csSelect'
            },
            link: function (scope, element, attr, ctrl) {

                element.bind('change', function (evt) {
                    scope.$apply(function () {
                        ctrl.select(scope.row, 'multiple');
                    });
                });

                scope.$watch('row.isSelected', function (newValue, oldValue) {
                    if (newValue === true) {
                        element.parent().addClass('st-selected');
                    } else {
                        element.parent().removeClass('st-selected');
                    }
                });
            }
        };
    })

    /*.factory('Resource', ['$q', '$filter', '$timeout', '$http', function ($q, $filter, $timeout, $http) {

        //this would be the service to call your server, a standard bridge between your model an $http

        // the database (normally on your server)
        var randomsItems = [];
        $http({
            method: "POST",
            url: apiUrl + "upcomingClubBookings",
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            data: $.param({ club_id: localStorage.getItem('club_id') }),
        }).then(function success(response) {
            console.log(response)
            if (response.data.status) {
                $.each(response.data.data, function (index, val) {
                    var bn = ("0000" + val.ClubBooking.booking_number).slice(-6);
                    var req = { book_date: val.ClubBooking.book_date, start_time: val.ClubBooking.book_start_time, end_time: val.ClubBooking.book_end_time, court: val.Court.name, prefix_booking_number: val.ClubBooking.prefix_booking_number, booking_number: bn };
                    randomsItems.push(req);
                });
            }
        }, function error(response) {

        });
        /*function createRandomItem(id) {
         var heroes = ['Batman', 'Superman', 'Robin', 'Thor', 'Hulk', 'Niki Larson', 'Stark', 'Bob Leponge'];
         return {
         book_date: id,
         start_time: heroes[Math.floor(Math.random() * 7)],
         court: Math.floor(Math.random() * 1000),
         booking_number: Math.floor(Math.random() * 10000)
         };
         
         }
         
         for (var i = 0; i < 1000; i++) {
         randomsItems.push(createRandomItem(i));
         }*/


    //fake call to the server, normally this service would serialize table state to send it to the server (with query parameters for example) and parse the response
    //in our case, it actually performs the logic which would happened in the server
    /*function getPage(start, number, params) {
        alert("here1")
        var deferred = $q.defer();

        var filtered = params.search.predicateObject ? $filter('filter')(randomsItems, params.search.predicateObject) : randomsItems;

        if (params.sort.predicate) {
            filtered = $filter('orderBy')(filtered, params.sort.predicate, params.sort.reverse);
        }

        var result = filtered.slice(start, start + number);

        $timeout(function () {
            //note, the server passes the information about the data set size
            deferred.resolve({
                data: result,
                numberOfPages: Math.ceil(filtered.length / number)
            });
        }, 1500);


        return deferred.promise;
    }

    return {
        getPage: getPage
    };

}])*/

    .controller('BookingsCtrl', function ($scope, $http, $timeout, $compile, $rootScope) {
        console.log("bookings ctrl");
        console.log($rootScope.searchData);
        if ($rootScope.searchData != undefined) {
            var searchData = $rootScope.searchData;
        } else {
            var searchData = '';
        }
        var table;
        var k = 0;
        $scope.picUrl = uploadUrl;
        $scope.search = {};


        $scope.$on('$viewContentLoaded', function () {
            $http({
                method: 'post',
                url: apiUrl + 'clubProfile',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: $.param({
                    id: localStorage.getItem('club_id'),
                }),
            }).then(function success(res) {
                res = res.data;
                console.log(res);
                $scope.courts = res.data.Court;
                $scope.allCoaches = res.data.Coach;
                $scope.clubMinTime = res.data.ClubOperatingHour[0].start_time;
                $scope.clubMaxTime = res.data.ClubOperatingHour[0].end_time;
                console.log($scope.clubMinTime);
            }, function error(response) {
                // do nothing
            });
            if (searchData != '') {
                $scope.contentLoading(1);
            } else {
                $scope.showUpcomingBookings();
            }
        });


        $scope.contentLoading = function (type) {
            k++;
            if (k > 1) {
                table.destroy();
            }
            table = $('#example').DataTable({
                "autoWidth": false,
                
                "ajax": {
                    "destroy": true,
                    "url": apiUrl + 'upcomingClubBookings',
                    "type": 'post',
                    "headers": {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    "data": {
                        "club_id": localStorage.getItem("club_id"),
                        "type": type,
                        "searchData": searchData
                    },
                    "dataSrc": function (json) {
                        console.log(json);
                        var fData = [];
                        if (json.status) {
                            $.each(json.data, function (index, val) {
                                console.log(val);
                                var checkdelete = "<input type='checkbox' id='check" + val.ClubBooking.id + "' class='checkselect'  />&nbsp;&nbsp;";
                                var book_date = val.ClubBooking.book_date;
                                var book_time = val.ClubBooking.book_start_time + " " + val.ClubBooking.book_end_time;
                                var court = val.Court.name;
                                var courtSort = val.Court.name;
                                var booking_number = val.ClubBooking.prefix_booking_number + " - " + val.ClubBooking.booking_number;
                                var details = " <a href='javascript:' ng-click='openDetails(" + val.ClubBooking.id + ")'><img style='margin-right:30px' class='ancher-img' src='images/edit.png'/></a><a href='javascript:' ng-click='getRightSideBarDetails(" + val.ClubBooking.id + ")'><img src='images/ahed-arrow.png'/></a>";
                                var res = {
                                    checkdelete: checkdelete,
                                    book_date: book_date,
                                    book_time: book_time,
                                    court: court,
                                    courtSort: courtSort,
                                    booking_number: booking_number,
                                    details: details
                                }
                                fData.push(res);
                            });
                        }


                        return fData;
                        /*for (var i = 0, ien = json.length; i < ien; i++) {
                            json[i][0] = '<a href="/message/' + json[i][0] + '>View message</a>';
                        }

                        {
                            "_": "Court "+Court.name,
                            "sort": "Court.name"
                            };
                        return json;*/
                    }
                }, "fnDrawCallback": function () {
                    var html = '<a href="javascript:" class="form-control" id="deleteBookings" ng-click="deleteBookings()"><img src="images/delete.png"/></a>';
                    $("#example_info").html(html);
                    var html = '<img src="images/left-arrow.png"/>';
                    $("#example_previous").html(html);
                    var html = '<img src="images/right-arrow.png"/>';
                    $("#example_next").html(html);
                    $compile($("#example"))($scope);
                    $compile($("#example_info"))($scope);
                    $compile($("#example_previous"))($scope);
                    $compile($("#example_next"))($scope);
                },
                "columns": [
                    { "data": "checkdelete"},
                    { "data": "book_date" },
                    { "data": "book_time" },
                    {
                        "data": {
                            _: "court",
                            sort: "courtSort"
                        }
                    },
                    { "data": "booking_number" },
                    { "data": "details" }
                ],
                "pageLength": 5
                

            });

        }

        $scope.getRightSideBarDetails = function (bookingId) {
            $http({
                method: 'post',
                url: apiUrl + 'getEventDetails',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: $.param({
                    booking_id: bookingId
                }),
            }).then(function success(res) {
                res = res.data;
                $scope.closeAllForms('rightSideBarBookingDetails');
                //$("#rightSideBarBookingDetails").animate({ width: 'toggle' }, 350);
                $(".app-content-body").addClass("sideBarOn");
                $(".button-bottom").addClass("sideBarOn");
                $scope.bookerName = res.data.UserDetail.Player.first_name + " " + res.data.UserDetail.Player.last_name;
                $scope.playerContactNo = res.data.UserDetail.Player.phone;
                $scope.playerEmail = res.data.UserDetail.User.email;
                $scope.teamNumber = res.data.ClubBooking.team_number;
                $scope.remoteUrl = apiUrl + 'getSearchPlayers/' + $scope.teamNumber + '/';
                $scope.bookingId = bookingId;
                $scope.totalPlayer = res.data.Team.length;
                $scope.teams = res.data.Team;
                $scope.coachName = res.data.Coach.name;
                $scope.subCoachName = res.data.SubCoach.name;
                $scope.startTime = res.data.ClubBooking.book_start_time;
                $scope.endTime = res.data.ClubBooking.book_end_time;
                $scope.courtBookingFee = res.data.ClubBooking.court_booking_fee;
                $scope.coachBookingFee = res.data.ClubBooking.coach_booking_fee;
                $scope.subTotal = res.data.ClubBooking.sub_total;
                $scope.taxFee = res.data.ClubBooking.tax_fee;
                $scope.taxAmount = res.data.ClubBooking.tax_amount;
                $scope.grandTotal = res.data.ClubBooking.grand_total;
                $scope.paymentMode = res.data.ClubBooking.payment_mode;
                $scope.clubMembershipId = res.data.Club.membership_id;
                $scope.userAppId = res.data.ClubBooking.user_id;
                $scope.paymentDate = res.data.ClubBooking.payment_date;
                $scope.cardType = res.data.Transaction.card_type;
                $scope.cardNumber = res.data.Transaction.card_number;
                $scope.cardName = res.data.Transaction.card_name;
                var edate = new Date(res.data.ClubBooking.book_date + " " + res.data.ClubBooking.book_start_time);
                var eedate = new Date(res.data.ClubBooking.book_date + " " + res.data.ClubBooking.book_end_time);
                $scope.eventPlayTime = moment(edate).format('h:mma') + " - " + moment(eedate).format('h:mma');
                $scope.courtName = res.data.Court.name;
                $scope.start = res.data.ClubBooking.book_date;
                $scope.eventDate = moment(edate).format('Do MMMM YYYY');
                $scope.eventDay = moment(edate).format('dddd');
                $scope.bookingNumber = res.data.ClubBooking.prefix_booking_number + ("0000" + res.data.ClubBooking.booking_number).slice(-6);
                $scope.matchType = res.data.ClassType.name;
                $scope.courtName = res.data.Court.name;
                $scope.picUrl = uploadUrl;
            }, function error(response) {
                // do nothing
            });

        }

        $scope.searchBookings = function () {
            $scope.search.bookDate = $("#searchDate").val();
            console.log($scope.search)
            searchData = $scope.search;
            $scope.contentLoading(1);
        }

        $scope.closeRightSideBarBookingDetails = function () {
            $("#rightSideBarBookingDetails").hide();
            $(".app-content-body").removeClass("sideBarOn");
            $(".button-bottom").removeClass("sideBarOn");
        }

        $scope.deleteBookings = function () {
            var n = $('input:checkbox[id^="check"]:checked').length;
            if (n == 0) {
                $scope.showAlert(1, "Please select bookings");
            } else {
                $scope.showConfirm("Are you sure?", 0, "removeBookings");
            }

        }

        $scope.$on('removeBookings', function (ev, args) {
            var ids = [];
            $('input:checkbox[id^="check"]:checked').each(function () {
                ids.push($(this).attr("id").replace("check", ""));

            });
            if (ids.length > 0) {
                $http({
                    method: 'post',
                    url: apiUrl + 'deleteClubBooking',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    data: $.param({
                        ids: ids,
                        user_id: localStorage.getItem('user_id')
                    }),
                }).then(function success(res) {
                    $scope.showAlert(2, "Bookings deleted");
                    //window.location.reload();
                }, function error(response) {
                    $scope.showAlert(1, response);
                });
            }
        })


        $scope.showUpcomingBookings = function () {
            if (!$("#upcomingBookings").hasClass("active") && searchData == '') {
                $("#upcomingBookings").addClass("active");
                $("#archievedBookings").removeClass("active");
                $scope.contentLoading(1);
            } else if (searchData != '') {
                searchData = '';
                $scope.contentLoading(1);
            }
        }

        $scope.showArcheivedBookings = function () {
            if (!$("#archievedBookings").hasClass("active") && searchData == '') {
                $("#upcomingBookings").removeClass("active");
                $("#archievedBookings").addClass("active");
                $scope.contentLoading(2);
            } else if (searchData != '') {
                searchData = '';
                $scope.contentLoading(2);
            }
        }

        $scope.clearSearch = function () {
            searchData = '';
            $scope.showUpcomingBookings();
        }

        $scope.selectDeselectAll = function () {
            if ($("#checkAll").is(":checked")) {
                $(".checkselect").prop("checked", true);
            } else {
                $(".checkselect").prop("checked", false);
            }
        }

        $scope.openDetails = function (bid) {
            $http({
                method: 'post',
                url: apiUrl + 'getEventDetails',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: $.param({
                    booking_id: bid
                }),
            }).then(function success(res) {
                res = res.data;
                $scope.closeAllForms('rightSideBarDetails');
                $scope.openCoachBox();
                //$("#rightSideBarDetails").animate({ width: 'toggle' }, 350);
                $(".app-content-body").addClass("sideBarOn");
                $(".button-bottom").addClass("sideBarOn");
                $scope.playerFirstName = res.data.UserDetail.Player.first_name;
                $scope.playerLastName = res.data.UserDetail.Player.last_name;
                $scope.playerContactNo = res.data.UserDetail.Player.phone;
                $scope.playerEmail = res.data.UserDetail.User.email;
                $scope.teamNumber = res.data.ClubBooking.team_number;
                $scope.remoteUrl = apiUrl + 'getSearchPlayers/' + $scope.teamNumber + '/';
                $scope.bookingId = bid;
                $("#totalPlayer").val(res.data.Team.length);
                $scope.teams = res.data.Team;
                $("#eventCoach").val(res.data.ClubBooking.coach_id);
                $("#eventSubCoach").val(res.data.ClubBooking.sub_coach_id);
                var startTime = res.data.ClubBooking.book_start_time;
                startTime = $scope.getHoursMinutes(startTime);
                startTime = startTime.split(":");
                $("#eventStartHours").val(startTime[0]);
                if (startTime[1] == 0) {
                    startTime[1] = "00";
                }
                $("#eventStartMinutes").val(startTime[1]);
                var endTime = res.data.ClubBooking.book_end_time;
                endTime = $scope.getHoursMinutes(endTime);
                endTime = endTime.split(":");
                $("#eventEndHours").val(endTime[0]);
                if (endTime[1] == 0) {
                    endTime[1] = "00";
                }
                $("#eventEndMinutes").val(endTime[1]);
                $("#eventCourt").val(res.data.ClubBooking.court_id);
                $scope.start = res.data.ClubBooking.book_date;
                //$scope.totalPlayer = res.data.Team.length;
            }, function error(response) {
                // do nothing
            });

        }



        $scope.closeRightSideBarDetails = function () {
            $("#rightSideBarDetails").hide();
            $(".app-content-body").removeClass("sideBarOn");
            $(".button-bottom").removeClass("sideBarOn");
        }



    })