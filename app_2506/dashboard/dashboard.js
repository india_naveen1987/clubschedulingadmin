'use strict';

angular.module('myApp.dashboard', ['ngRoute'])

        .config(['$routeProvider', function ($routeProvider) {
                $routeProvider.when('/dashboard', {
                    templateUrl: 'dashboard/dashboard.html',
                    controller: 'DashboardCtrl'
                });
            }])

        .controller('DashboardCtrl', function ($scope, $http, $filter, uiCalendarConfig, $timeout, $rootScope, $compile) {
            $(".background-div").css("height", $(window).height());


            $scope.eventSources = [];
            $scope.allCoaches = [];

            $scope.teamNumber = 0;

            $scope.clubMinTime = '';
            $scope.clubMaxTime = '';
            $scope.picUrl = uploadUrl;
            $scope.club_id = localStorage.getItem('club_id');

            $scope.payment = {};
            var viewName = '';
            var viewDate = '';
            var kendoCurrent = '';
            var kendoView = '';




            $scope.$on('$viewContentLoaded', function () {
                $http({
                    method: 'post',
                    url: apiUrl + 'clubProfile',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    data: $.param({
                        id: localStorage.getItem('club_id'),
                    }),
                }).then(function success(res) {
                    res = res.data;
                    console.log(res);
                    $scope.courts = res.data.Court;
                    $scope.allCoaches = res.data.Coach;
                    $scope.clubMinTime = res.data.ClubOperatingHour[0].start_time;
                    $scope.clubMaxTime = res.data.ClubOperatingHour[0].end_time;
                    console.log($scope.clubMinTime);
                    $timeout(function () {
                        $("#courtId").val(res.data.Court[0].id);
                    }, 1000);
                    //$scope.setCalendar(localStorage.getItem('club_id'), localStorage.getItem('court_id'), 1);
                    $scope.$broadcast("setCalendar", {id: localStorage.getItem('club_id'), court_id: localStorage.getItem('court_id'), type: 1 });
                }, function error(response) {
                    // do nothing
                });

                var $form = $('#payment-form');

            });

            $scope.setCalendarFlow = function(club_id, court_id, type){
                $scope.$broadcast("setCalendar", {id: club_id, court_id: court_id, type: type });
            }


            $scope.showCurrentDayView = function () {
                var value = new Date();

                calendar.navigate(moment(value), "month");
                $timeout(function () {
                    uiCalendarConfig.calendars['myCalendar'].fullCalendar('changeView', 'agendaDay');
                    uiCalendarConfig.calendars['myCalendar'].fullCalendar('gotoDate', moment(value));
                }, 500);
            }

            $scope.$on('setCalendar', function (ev, args) {
                var id = args.id;
                var court_id = args.court_id;
                var type = args.type;
                //alert(id+":"+court_id)
                if (type == 2) {
                    var view = uiCalendarConfig.calendars['myCalendar'].fullCalendar('getView');
                    var date = uiCalendarConfig.calendars['myCalendar'].fullCalendar('getDate');
                    //var date = $("#calendar").fullCalendar('getDate');
                    //var month_int = date.getMonth();
                    console.log(moment(date).format('MM'));
                    console.log(view.name);
                    viewName = view.name;
                    viewDate = moment(date);
                    kendoCurrent = calendar.current();
                    kendoView = calendar.view();
                    console.log(kendoCurrent);
                    console.log(kendoView);
                    calendar.destroy();
                    $("#kendoCalendar").empty();
                }
                $http({
                    method: 'post',
                    url: apiUrl + 'clubProfile',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    data: $.param({
                        id: id,
                        court_id: court_id
                    }),
                }).then(function success(res) {
                    console.log(res)
                    res = res.data;
                    var scheduleList = [];
                    if (res.status) {
                        var resString = JSON.stringify(res.data);
                        resString = resString.replace(/book_start_time/g, 'start');
                        resString = resString.replace(/book_end_time/g, 'end');
                        var schedules = JSON.parse(resString);

                        $.each(schedules, function (index, obj) {
                            var schedule = {};
                            var date = obj.ClubBooking.book_date;
                            schedule.start = date + 'T' + obj.ClubBooking.start;
                            schedule.end = date + 'T' + obj.ClubBooking.end;
                            schedule.type = obj.ClassType.name;
                            schedule.coachId = obj.Coach.id;
                            schedule.coachName = obj.Coach.name;
                            schedule.title = schedule.type + '\n' + schedule.coachName;
                            schedule.id = obj.ClubBooking.id;
                            schedule.data = obj;
                            if (obj.ClassType.id == 1) {
                                schedule.textColor = '#23E6D5';
                            } else if (obj.ClassType.id == 2) {
                                schedule.textColor = '#89E625';
                            } else if (obj.ClassType.id == 5) {
                                schedule.textColor = '#E66630';
                            } else {
                                schedule.textColor = '#89E625';
                            }
                            scheduleList.push(schedule);
                        });
                        console.log(scheduleList)
                        $scope.uiConfig = {
                            calendar: {
                                editable: true,
                                selectable: true,
                                selectHelper: true,
                                allDaySlot: false,
                                slotDuration: '00:15:00',
                                eventBorderColor: 'transparent',
                                eventBackgroundColor: 'transparent',
                                minTime: $scope.clubMinTime,
                                maxTime: $scope.clubMaxTime,
                                eventOverlap: function (stillEvent, movingEvent) {
                                    return false;
                                },
                                header: {
                                    left: 'title,prev,next',
                                    center: '',
                                    right: 'month agendaWeek agendaDay'
                                },
                                events: scheduleList,
                                aspectRatio: 2,
                                dayClick: function (date, jsEvent, view) {
                                    //alert('Clicked on: ' + date.format());
                                    //alert('Coordinates: ' + jsEvent.pageX + ',' + jsEvent.pageY);
                                    //alert('Current view: ' + view.name);
                                    var events = uiCalendarConfig.calendars['myCalendar'].fullCalendar('clientEvents');

                                    //console.log(events)
                                    var currentEvent = false;

                                    for (var i = 0; i < events.length; i++) {
                                        //console.log(date.format());
                                        //console.log(events[i].start.format("YYYY-MM-DD"));
                                        if (date.format() == events[i].start.format("YYYY-MM-DD")) {
                                            currentEvent = true;
                                            break;
                                        }
                                    }
                                    if (currentEvent) {
                                        /*$http({
                                         method: "POST",
                                         url: apiUrl + "getEventDetails",
                                         headers: {
                                         'Content-Type': 'application/x-www-form-urlencoded'
                                         },
                                         data: $.param({ booking_id: calEvent.id }),
                                         }).then(function success(eventResponse) {
                                         var edate = new Date(eventResponse.data.data.ClubBooking.book_date);
                                         $scope.eventDate = moment(edate).format('Do MMMM YYYY');
                                         $scope.eventDay = moment(edate).format('dddd');
                                         var edate = new Date(eventResponse.data.data.ClubBooking.book_date + " " + eventResponse.data.data.ClubBooking.book_start_time);
                                         $scope.startTimeHour = moment(edate).format('h');
                                         $scope.startTimeAM = moment(edate).format('a');
                                         var eedate = new Date(eventResponse.data.data.ClubBooking.book_date + " " + eventResponse.data.data.ClubBooking.book_end_time);
                                         $scope.eventPlayTime = moment(edate).format('h:mma') + " - " + moment(eedate).format('h:mma');
                                         $scope.matchType = eventResponse.data.data.ClassType.name;
                                         $scope.courtName = eventResponse.data.data.Court.name;
                                         $scope.bookingNumber = eventResponse.data.data.ClubBooking.prefix_booking_number + ("0000" + eventResponse.data.data.ClubBooking.booking_number).slice(-6);
                                         $scope.teams = eventResponse.data.data.Team;
                                         $scope.picUrl = uploadUrl;
                                         $scope.bookingId = eventResponse.data.data.ClubBooking.id;
                                         }, function error(eventResponse) {
                                         $scope.showAlert(1, 'Can not get event details!');
                                         });*/
                                        $scope.closeAllForms('rightSideBarAddForm');
                                        //$("#rightSideBarAddForm").animate({ width: 'toggle' }, 350);
                                        $("#bookAddDate").val(date.format());
                                        $(".app-content-body").addClass("sideBarOn");
                                        $(".button-bottom").addClass("sideBarOn");
                                    } else {
                                        $scope.closeAllForms('rightSideBarAddForm');
                                        //$("#rightSideBarAddForm").animate({ width: 'toggle' }, 350);
                                        $("#bookAddDate").val(date.format());
                                        $(".app-content-body").addClass("sideBarOn");
                                        $(".button-bottom").addClass("sideBarOn");
                                    }
                                },
                                viewRender: function (view, element) {
                                    console.log(view);
                                    console.log(element);
                                    console.log(calendar);
                                    console.log($("#kendoCalendar"))
                                    if (view.name == 'month') {
                                        var currentDate = uiCalendarConfig.calendars['myCalendar'].fullCalendar('getDate');
                                        //alert(new Date(currentDate)+" = "+calendar.current())
                                        //alert(moment(calendar.current()).format("MM")+" = "+moment(currentDate).format("MM"))
                                        if (moment(calendar.current()).format("MM") != moment(currentDate).format("MM")) {

                                            calendar.navigate(currentDate, view.name);
                                        }
                                        /*var rows = parseInt(moment(currentDate).format("DD"));
                                        rows = Math.ceil(rows/6);
                                        var column = parseInt(moment(currentDate).format("d"));
                                        column = column+1;
                                        $timeout(function(){
                                            $("div.fc-view table tbody tr td div.fc-scroller div.fc-day-grid div.fc-row:nth-child("+rows+") tbody td:nth-child("+column+")").attr("style","border:1px solid black;margin:0px !important;border-radius:unset;background-color:white;color:#89E625;border-top:none;border-right:none");
                                        },500);*/
                                        
                                        
                                    }
                                    $("#cdayview").remove();
                                    var html = '<img id="cdayview" ng-click="showCurrentDayView()" class="inactive-image" src="images/calander1.png" style="width:20px;height:20px" />';
                                    $(".fc-right").append(html);
                                    $compile($(".fc-right"))($scope);
                                    if(view.name=='agendaWeek'){
                                        var dayName = moment(currentDate).format("ddd").toLowerCase();
                                        $(".fc-"+dayName).css("text-decoration","underline");
                                        $(".fc-"+dayName).css("font-weight","bold");
                                    }
                                },
                                eventClick: function (calEvent, jsEvent, view) {

                                    /*alert('Event: ' + calEvent.title + 'ID: ' + calEvent.id);
                                     alert('Coordinates: ' + jsEvent.pageX + ',' + jsEvent.pageY);
                                     alert('View: ' + view.name);*/
                                    $scope.closeAllForms('rightSideBar');
                                    //$("#rightSideBar").animate({ width: 'toggle' }, 350);
                                    if ($(".app-content-body").hasClass("sideBarOn")) {
                                        $(".app-content-body").removeClass("sideBarOn");
                                        $(".button-bottom").removeClass("sideBarOn");
                                    } else {
                                        $(".app-content-body").addClass("sideBarOn");
                                        $(".button-bottom").addClass("sideBarOn");
                                    }
                                    //window.resizeBy(0,0);
                                    // profile
                                    $scope.getEventDetails(calEvent.id);
                                }
                            }
                        };
                        // kendo ui calendar
                        var eventDates = [];
                        $.each(scheduleList, function (index, obj) {
                            var cdate = new Date(obj.start);
                            cdate.setTime(cdate.getTime() + (cdate.getTimezoneOffset() * 60 * 1000));
                            console.log(cdate)
                            var fdate = +new Date(cdate.getFullYear(), cdate.getMonth(), cdate.getDate());
                            console.log(fdate)
                            eventDates.push(fdate);
                        });
                        console.log(eventDates);
                        $("#kendoCalendar").kendoCalendar({
                            value: new Date(),
                            dates: eventDates,
                            weekNumber: false,
                            month: {
                                // template for dates in month view
                                content: '# if ($.inArray(+data.date, data.dates) != -1) { #' +
                                        '<div class="exhibition"' +
                                        '">#= data.value #</div>' +
                                        '# } else { #' +
                                        '#= data.value #' +
                                        '# } #'
                            },
                            footer: false,
                            change: function () {
                                var value = this.value();
                                console.log(value);
                                //console.log(uiCalendarConfig.calendars['myCalendar'].fullCalendar('gotoDate',moment('2017-04-11')));
                                // uiCalendarConfig.calendars['myCalendar'].fullCalendar('gotoDate',moment(value)); //moment(value).format('YYYY-MM-DD')
                                uiCalendarConfig.calendars['myCalendar'].fullCalendar('changeView', 'agendaDay');
                                uiCalendarConfig.calendars['myCalendar'].fullCalendar('gotoDate', moment(value));
                                var cdate = new Date();
                                cdate = moment(cdate).format("YYYY-MM-DD");
                                value = moment(value).format("YYYY-MM-DD");
                                if (cdate != value) {
                                    $("#cdayview").removeClass("inactive-image");
                                    $("#cdayview").attr("ng-click", "showCurrentDayView()");
                                    $compile($(".fc-right"))($scope);
                                } else {
                                    $("#cdayview").addClass("inactive-image");
                                    $("#cdayview").removeAttr("onClick");
                                }
                            },
                            navigate: function () {
                                var view = this.view();
                                console.log(view.name); //name of the current view

                                var current = this.current();
                                console.log(current); //currently focused date
                                uiCalendarConfig.calendars['myCalendar'].fullCalendar('changeView', view.name);
                                uiCalendarConfig.calendars['myCalendar'].fullCalendar('gotoDate', moment(current));
                                $("#cdayview").removeClass("inactive-image");
                            }
                        });
                        calendar = $("#kendoCalendar").data("kendoCalendar");
                        if (viewName != '') {
                            $timeout(function () {
                                calendar.navigate(kendoCurrent, kendoView.name);
                                $timeout(function () {
                                    uiCalendarConfig.calendars['myCalendar'].fullCalendar('changeView', viewName);
                                    uiCalendarConfig.calendars['myCalendar'].fullCalendar('gotoDate', moment(viewDate).format("YYYY-MM-DD"));
                                }, 500);
                            }, 500);
                        }
                        /*$scope.calendar = {
                         value: new Date(),
                         dates: eventDates,
                         weekNumber: false,
                         month: {
                         // template for dates in month view
                         content: '# if ($.inArray(+data.date, data.dates) != -1) { #' +
                         '<div class="exhibition"' +
                         '">#= data.value #</div>' +
                         '# } else { #' +
                         '#= data.value #' +
                         '# } #'
                         },
                         footer: false,
                         change: function () {
                         var value = this.value();
                         console.log(value);
                         //console.log(uiCalendarConfig.calendars['myCalendar'].fullCalendar('gotoDate',moment('2017-04-11')));
                         // uiCalendarConfig.calendars['myCalendar'].fullCalendar('gotoDate',moment(value)); //moment(value).format('YYYY-MM-DD')
                         uiCalendarConfig.calendars['myCalendar'].fullCalendar('changeView', 'agendaDay');
                         uiCalendarConfig.calendars['myCalendar'].fullCalendar('gotoDate', moment(value));
                         }
                         
                         }*/

                    }
                }, function error(response) {
                    // do nothing
                });
            });


            $scope.editRightSideBarDetails = function (bookingId) {
                $http({
                    method: 'post',
                    url: apiUrl + 'getEventDetails',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    data: $.param({
                        booking_id: bookingId
                    }),
                }).then(function success(res) {
                    res = res.data;
                    $scope.closeAllForms('rightSideBarDetails');
                    $scope.openCoachBox();
                    $scope.playerFirstName = res.data.UserDetail.Player.first_name;
                    $scope.playerLastName = res.data.UserDetail.Player.last_name;
                    $scope.playerContactNo = res.data.UserDetail.Player.phone;
                    $scope.playerEmail = res.data.UserDetail.User.email;
                    $scope.teamNumber = res.data.ClubBooking.team_number;
                    $scope.remoteUrl = apiUrl + 'getSearchPlayers/' + $scope.teamNumber + '/';
                    $scope.bookingId = bookingId;
                    $("#totalPlayer").val(res.data.Team.length);
                    $scope.teams = res.data.Team;
                    $("#eventCoach").val(res.data.ClubBooking.coach_id);
                    $("#eventSubCoach").val(res.data.ClubBooking.sub_coach_id);
                    var startTime = res.data.ClubBooking.book_start_time;
                    startTime = $scope.getHoursMinutes(startTime);
                    startTime = startTime.split(":");
                    $("#eventStartHours").val(startTime[0]);
                    if (startTime[1] == 0) {
                        startTime[1] = "00";
                    }
                    $("#eventStartMinutes").val(startTime[1]);
                    var endTime = res.data.ClubBooking.book_end_time;
                    endTime = $scope.getHoursMinutes(endTime);
                    endTime = endTime.split(":");
                    $("#eventEndHours").val(endTime[0]);
                    if (endTime[1] == 0) {
                        endTime[1] = "00";
                    }
                    $("#eventEndMinutes").val(endTime[1]);
                    $("#eventCourt").val(res.data.ClubBooking.court_id);
                    $scope.start = res.data.ClubBooking.book_date;
                    //$scope.totalPlayer = res.data.Team.length;
                }, function error(response) {
                    // do nothing
                });

            }



        });

