'use strict';

angular.module('myApp.groupclasses', ['ngRoute'])

    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/groupclasses', {
            templateUrl: 'groupclasses/groupclasses.html',
            controller: 'GroupclassesCtrl'
        });
    }])

    .controller('GroupclassesCtrl', function ($scope, $http, $timeout, $compile, $rootScope, $templateCache) {
		console.log('Group Classes Loading...');
		if ($rootScope.searchData != undefined) {
            var searchData = $rootScope.searchData;
        } else {
            var searchData = '';
        }
		$scope.$on('$viewContentLoaded', function () {
            $scope.contentLoading();
        });	
		$scope.contentLoading = function () {
            $http({
                method: 'post',
                url: apiUrl + 'allGroupclasses',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
            }).then(function success(res) {
				console.log(res.data.data);
				console.log($scope);
                if (res.data.status) {	
					var groupClasses = [];
					$.each(res.data.data, function(i, val){
						var edate = new Date(val.ClubBookingSession.book_date + " " + val.ClubBookingSession.book_start_time);
						val.ClubBookingSession.book_start_time = edate;
						var edate = new Date(val.ClubBookingSession.book_date + " " + val.ClubBookingSession.book_end_time);
						val.ClubBookingSession.book_end_time = edate;
						val.ClubBookingSession.class_description = val.ClubBookingSession.class_description.substring(0, 100);
						groupClasses.push(val);
					});
					$rootScope.allCoaches = res.data.coaches;
					$rootScope.courts = res.data.courts;
					$scope.groupClasses = groupClasses;
                } else {
                    $scope.showAlert(1, res.data.message);
                }
            }, function error(response) {
                $scope.showAlert(1, JSON.stringify(response));
            });
        }

        $scope.getRightSideBarDetails = function (groupClassId) {
            $http({
                method: 'post',
                url: apiUrl + 'allGroupclasses',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: $.param({
                    group_class_id: groupClassId
                }),
            }).then(function success(res) {
				if (res.data.status) {
                	$("#rightSideBarGroupClassDetails").show();
                	$(".app-content-body").addClass("sideBarOn");
                	$(".button-bottom").addClass("sideBarOn");
					var val = res.data.data;
					var edate = new Date(val.ClubBookingSession.book_date + " " + val.ClubBookingSession.book_start_time);
					val.ClubBookingSession.book_start_time = edate;
					var edate = new Date(val.ClubBookingSession.book_date + " " + val.ClubBookingSession.book_end_time);
					val.ClubBookingSession.book_end_time = edate;
					console.log(val);
					$rootScope.groupClassDeatilData = val;
					$rootScope.picUrl = uploadUrl;
				} else {
					$scope.showAlert(1, res.data.message);
				}                
            }, function error(response) {
                $scope.showAlert(1, JSON.stringify(response));
            });

        }
		
		$scope.getRightSideBarDetailsEdit = function (groupClassId) {
            $http({
                method: 'post',
                url: apiUrl + 'allGroupclasses',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: $.param({
                    group_class_id: groupClassId
                }),
            }).then(function success(res) {
				if (res.data.status) {
					var editClass = $("#rightSideBarEditGroupClass");
                	editClass.show();
                	$(".app-content-body").addClass("sideBarOn");
                	$(".button-bottom").addClass("sideBarOn");
					var val = res.data.data;
					var book_start_time = val.ClubBookingSession.book_start_time.split(':');
					var book_end_time = val.ClubBookingSession.book_end_time.split(':');
					var edate = new Date(val.ClubBookingSession.book_date + " " + val.ClubBookingSession.book_start_time);
					val.ClubBookingSession.book_start_time = edate;
					var edate = new Date(val.ClubBookingSession.book_date + " " + val.ClubBookingSession.book_end_time);
					val.ClubBookingSession.book_end_time = edate;
					editClass.find('#isClubMember').prop('checked', val.ClubBookingSession.is_club_member);
					editClass.find('#className').val(val.ClubBookingSession.class_name);
					editClass.find('#classDescription').val(val.ClubBookingSession.class_description);
					editClass.find('#classBookDateEdit').val(val.ClubBookingSession.book_date);
					editClass.find('#classStartHourEdit').val(book_start_time[0]);
					editClass.find('#classStartMinuteEdit').val(book_start_time[1]);
					editClass.find('#classEndHourEdit').val(book_end_time[0]);
					editClass.find('#classEndMinuteEdit').val(book_end_time[1]);
					editClass.find('#classPlayerRating').val(val.ClubBookingSession.min_player_rating);
					editClass.find('#classPlayerAgeGroup').val(val.ClubBookingSession.min_player_age+'-'+val.ClubBookingSession.max_player_age);
					editClass.find('#classTotalPlayer').val(val.ClubBookingSession.no_player);
					editClass.find('#groupClassId').val(val.ClubBookingSession.id);
					$rootScope.groupClassDeatilData = val;
					$rootScope.allCoaches = res.data.coaches;
					$rootScope.courts = res.data.courts;
					$rootScope.picUrl = uploadUrl;
					$rootScope.perHeadFee = (val.ClubBookingSession.grand_total / val.ClubBookingSession.no_player).toFixed(4);
					$rootScope.totalClassPlayer = val.ClubBookingSession.no_player;
					$rootScope.grandTotal = val.ClubBookingSession.grand_total;
					$rootScope.taxRate = val.ClubBookingSession.tax_fee;
					$rootScope.taxAmt = val.ClubBookingSession.tax_amount;
					$rootScope.subTotal = val.ClubBookingSession.sub_total;
					$rootScope.coachName = val.Coach.name;
					$rootScope.totalCoachFee = val.ClubBookingSession.coach_booking_fee;
					$rootScope.courtFee = val.ClubBookingSession.court_booking_fee;
					console.log(val);
					setTimeout(function(){
					editClass.find('#classCoach').val(val.ClubBookingSession.coach_id);
					editClass.find('#classSubCoach').val(val.ClubBookingSession.sub_coach_id);
					editClass.find('#classCourtNo').val(val.ClubBookingSession.court_id);
					}, 1000);
				} else {
					$scope.showAlert(1, res.data.message);
				}                
            }, function error(response) {
                $scope.showAlert(1, JSON.stringify(response));
            });

        }
		
		$scope.searchGroupClasses = function () {
            console.log($scope.search)
            searchData = $scope.search;
			searchData.classEndDate = $('#classEndDate').val();
			searchData.classStartDate = $('#classStartDate').val();
			//console.log($('#classEndDate').val());
			$http({
                method: 'post',
                url: apiUrl + 'allGroupclassesSearch',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: $.param({
                    searchData: searchData
                }),
            }).then(function success(res) {
				console.log(res.data.data);
				console.log($scope);
                if (res.data.status) {	
					var groupClasses = [];
					$.each(res.data.data, function(i, val){
						var edate = new Date(val.ClubBookingSession.book_date + " " + val.ClubBookingSession.book_start_time);
						val.ClubBookingSession.book_start_time = edate;
						var edate = new Date(val.ClubBookingSession.book_date + " " + val.ClubBookingSession.book_end_time);
						val.ClubBookingSession.book_end_time = edate;
						val.ClubBookingSession.class_description = val.ClubBookingSession.class_description.substring(0, 100);
						groupClasses.push(val);
					});
					$scope.groupClasses = groupClasses;				
					$scope.closeRightSideBarAddGroupClass();
					$scope.$apply();	
                } else {
                    $scope.showAlert(1, res.data.message);
                }
            }, function error(response) {
                $scope.showAlert(1, JSON.stringify(response));
            });
        }
		
		$scope.editGroupClass = function () {
			var currentForm = $('#editGroupClassFrom');
            var isClubMember = currentForm.find("#isClubMember").is(":checked") ? 1 : 2;
            var ageGroup = currentForm.find("#classPlayerAgeGroup").val().split("-");
            $http({
                method: 'post',
                url: apiUrl + 'addGroupClass',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: $.param({
                    is_club_member: isClubMember,
					id: currentForm.find("#groupClassId").val(),
                    class_name: currentForm.find("#className").val(),
                    class_description: currentForm.find("#classDescription").val(),
                    book_date: currentForm.find("#classBookDateEdit").val(),
                    court_id: currentForm.find("#classCourtNo").val(),
                    start_hours: currentForm.find("#classStartHourEdit").val(),
                    start_minutes: currentForm.find("#classStartMinuteEdit").val(),
                    end_hours: currentForm.find("#classEndHourEdit").val(),
                    end_minutes: currentForm.find("#classEndMinuteEdit").val(),
                    min_player_rating: currentForm.find("#classPlayerRating").val(),
                    min_player_age: ageGroup[0],
                    max_player_age: ageGroup[1],
                    no_player: currentForm.find("#classTotalPlayer").val(),
                    coach_id: currentForm.find("#classCoach").val(),
                    sub_coach_id: currentForm.find("#classSubCoach").val(),
                    court_booking_fee: $rootScope.courtFee,
                    coach_booking_fee: $rootScope.totalCoachFee,
                    sub_total: $rootScope.subTotal,
                    tax_amount: $rootScope.taxAmt,
                    tax_fee: $rootScope.taxRate,
                    grand_total: $rootScope.grandTotal,
                    club_id: localStorage.getItem('club_id'),
                    user_id: localStorage.getItem('user_id')
                }),
            }).then(function success(res) {
                if (res.data.status) {
					$scope.closeRightSideBarAddGroupClass();
                    $scope.contentLoading();
                } else {
                    $scope.showAlert(1, res.data.message);
                }
            }, function error(response) {
                $scope.showAlert(1, response);
            });
        }

        $scope.getCourtEditFee = function ($court) {
            $http({
                method: 'post',
                url: apiUrl + 'getCourtDetails',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: $.param({
                    court_id: $court,
                }),
            }).then(function success(res) {
                if (res.data.status) {
                    console.log(res.data)
                    $rootScope.courtFee = parseFloat(res.data.data.Court.rate);
                    $rootScope.taxRate = parseFloat(res.data.data.Club.tax_rate);
                    if ($rootScope.totalCoachFee != undefined) {
                        $rootScope.subTotal = $rootScope.totalCoachFee + $rootScope.courtFee;
                    } else {
                        $rootScope.subTotal = $rootScope.courtFee;
                    }
                    if ($rootScope.subTotal != undefined) {
                        $rootScope.taxAmt = ($rootScope.subTotal * $rootScope.taxRate) / 100;
                    } else {
                        $rootScope.taxAmt = 0;
                    }
                    $rootScope.grandTotal = parseFloat($rootScope.subTotal) + parseFloat($rootScope.taxAmt);
                    $rootScope.perHeadFee = $rootScope.grandTotal / $rootScope.totalClassPlayer;
                } else {
                    $scope.showAlert(1, res.data.message);
                }
            }, function error(response) {
                $scope.showAlert(1, response);
            });
        }

        $scope.getCoachEditFee = function ($coach) {
            if ($("#classStartHourEdit").val() == "") {
                $("#classCoachEdit").val("");
                $scope.showAlert(1, "Please choose class start hour");
            } else if ($("#classStartMinuteEdit").val() == "") {
                $("#classCoachEdit").val("");
                $scope.showAlert(1, "Please choose class start minute");
            } else if ($("#classEndHourEdit").val() == "") {
                $("#classCoachEdit").val("");
                $scope.showAlert(1, "Please choose class end hour");
            } else if ($("#classEndMinuteEdit").val() == "") {
                $("#classCoachEdit").val("");
                $scope.showAlert(1, "Please choose class end minute");
            } else {
                $http({
                    method: 'post',
                    url: apiUrl + 'getCoachRate',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    data: $.param({
                        coach_id: $coach,
                        book_date: $("#classBookDateEdit").val(),
                        start_hours: $("#classStartHourEdit").val(),
                        start_minutes: $("#classStartMinuteEdit").val(),
                        end_hours: $("#classEndHourEdit").val(),
                        end_minutes: $("#classEndMinuteEdit").val()
                    }),
                }).then(function success(res) {
                    if (res.data.status) {
                        console.log(res.data);
                        $rootScope.totalCoachFee = parseFloat(res.data.data);
                        if ($rootScope.courtFee != undefined) {
                            $rootScope.subTotal = $rootScope.totalCoachFee + $rootScope.courtFee;
                        } else {
                            $rootScope.subTotal = $rootScope.totalCoachFee;
                        }
                        if ($rootScope.taxRate != undefined) {
                            $rootScope.taxAmt = ($rootScope.subTotal * $rootScope.taxRate) / 100;
                        } else {
                            $rootScope.taxAmt = 0;
                        }
                        $rootScope.grandTotal = parseFloat($rootScope.subTotal) + parseFloat($rootScope.taxAmt);
                        $rootScope.perHeadFee = $rootScope.grandTotal / $rootScope.totalClassPlayer;
                        //$scope.courtFee = res.data.data.Court.rate;
                    } else {
                        $scope.showAlert(1, res.data.message);
                    }
                }, function error(response) {
                    $scope.showAlert(1, response);
                });
            }
        }

        $scope.setClassTotalEditPlayer = function (classPlayer) {
            $rootScope.totalClassPlayer = classPlayer;
            $rootScope.perHeadFee = $rootScope.grandTotal / $rootScope.totalClassPlayer;
        }

        $scope.closeRightSideBarAddGroupClass = function () {
            $("#rightSideBarAddGroupClass").hide();
			$("#rightSideBarEditGroupClass").hide();
			$("#rightSideBarGroupClassDetails").hide();
			$("#rightSideBarSearchGroupClass").hide();
            $(".app-content-body").removeClass("sideBarOn");
            $(".button-bottom").removeClass("sideBarOn");
        }
		
		$scope.openLeftSearchBar = function () {
            $("#rightSideBarSearchGroupClass").show();
			//$("#rightSideBarSearchGroupClass").find('input[type="text"]').val('');
			$("#rightSideBarSearchGroupClass").find('select').prop('selectedIndex',0);
            //$(".app-content-body").addClass("sideBarOn");
            //$(".button-bottom").addClass("sideBarOn");
        }
	})
	